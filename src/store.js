import {applyMiddleware, combineReducers, createStore} from 'redux';
import thunk from "redux-thunk";
import promise from 'redux-promise';
import {composeWithDevTools} from "redux-devtools-extension";
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web
import {createBrowserHistory} from "history";
import { routerMiddleware } from 'connected-react-router'
import { connectRouter } from 'connected-react-router'
import treeReducer from './reducers/tree';
import roleReducer from './reducers/role';
import userReducer from './reducers/user';
import authReducer from './reducers/auth';
import galleryReducer from './reducers/gallery';
import ticketReducer from './reducers/ticket';
import settingReducer from './reducers/setting';
import contentReducer from './reducers/content';
import packageTypeReducer from './reducers/packageType';
import attributeReducer from './reducers/attribute';
import brandReducer from './reducers/brand';
import itemProductReducer from './reducers/itemProduct';
import productReducer from './reducers/product'
import orders from './reducers/orders'
import transaction from './reducers/transaction'
import marketer from "./reducers/marketer";
import rewards from "./reducers/rewards";
import period from "./reducers/period";
import requests from "./reducers/requests";
import region from "./reducers/region";
import notification from "./reducers/notification";
import finance from "./reducers/finance";
import dialog from "./reducers/dialog";
export const history = createBrowserHistory();

const createRootReducer = (history) => combineReducers({
    router: connectRouter(history),
    treeReducer,
    roleReducer,
    userReducer,
    authReducer,
    galleryReducer,
    ticketReducer,
    settingReducer,
    contentReducer,
    packageTypeReducer,
    attributeReducer,
    brandReducer,
    itemProductReducer,
    productReducer,
    orders,
    transaction,
    marketer,
    rewards,
    period,
    requests,
    region,
    notification,
    finance,
    dialog

});

const persistConfig = {
    key: 'root',
    storage,
};
const persistedReducer = persistReducer(
    persistConfig,
    createRootReducer(history),
);

export default function configureStore(preloadedState) {
    const store = createStore(
        persistedReducer, // root reducer with router state
        preloadedState,
        composeWithDevTools(
            applyMiddleware(
                routerMiddleware(history),
                thunk,
                promise
            ),
        ),
    );

    const persistor = persistStore(store);

    return {store, persistor};
}



