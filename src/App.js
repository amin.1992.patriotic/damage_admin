import React from 'react';
import {Provider} from "react-redux";
import {ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import { PersistGate } from 'redux-persist/lib/integration/react';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import './assets/css/rest.css'
import { create } from 'jss';
import rtl from 'jss-rtl';
import { StylesProvider, jssPreset } from '@material-ui/core/styles';
import configureStore  from './store'
import Router from './routes'
import './assets/fonts/Yekan-Font-master/stylesheet.css'
import './assets/fonts/iransans/fonts.css'
import 'animate.css/animate.min.css'
import './assets/css/main.css'
import {useHistory} from "react-router";
import { hot } from 'react-hot-loader/root';

const theme = createMuiTheme({
    direction: 'rtl',
    typography: {
        fontFamily: 'iransans',
        fontSize: 13
    },
    overrides: {
        MuiButton: {
          outlinedSecondary: {
              color: '#c2af20'
          }
        },
        MuiChip: {
            root: {
                marginLeft: 3
            }
        }
    },
});
const jss = create({ plugins: [...jssPreset().plugins, rtl()] });

const {store, persistor} = configureStore();


function App() {

    return (
        <Provider store={store}>
            <PersistGate persistor={persistor}>
                <StylesProvider jss={jss}>
                    <MuiThemeProvider theme = { theme }>
                        <Router />
                        <ToastContainer
                            position="bottom-center"
                            autoClose={5000}
                            hideProgressBar={true}
                            newestOnTop={false}
                            closeOnClick
                            rtl
                            pauseOnVisibilityChange
                            draggable={false}
                            pauseOnHover />
                    </MuiThemeProvider>
                </StylesProvider>
            </PersistGate>
        </Provider>
    );
}

export default hot(App);
