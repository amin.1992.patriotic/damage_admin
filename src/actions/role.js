import Api from "../utils/api";
import {FETCH_ROLES} from "../types";


export function fetchRoles() {
    return function (dispatch) {
        new Api().get('/backend/users/roles').then((response) => {
            dispatch({'type' : FETCH_ROLES, 'payload' : response});
        }).catch((error) => {
            console.log(error);
        })
    }
}
