import Api from "../utils/api";
import {FETCH_CONTENT, FETCH_CONTENTS} from "../types";

export function contents(object) {
    return function (dispatch) {
        new Api().get('/blog', object, false).then((response) => {
            if (typeof response != "undefined") {
                dispatch({'type' : FETCH_CONTENTS, 'payload' : response});
            }
        })

    }
}

export function content(id) {

    return function (dispatch) {

        new Api().get('/blog/' + id, false).then((response) => {
            if (typeof response != "undefined") {
                dispatch({'type' : FETCH_CONTENT, 'payload' : response});
            }
        })
    }
}