import Api from "../utils/api";
import {USER_FETCH, USERS_FAILURE, USERS_SUCCESS} from "../types";




export function fetchUsers(object) {
    return function (dispatch) {
        try {
            new Api().get('/backend/users', object).then((response) => {
                if (typeof response != "undefined") {
                    dispatch({'type' : USERS_SUCCESS, 'payload' : response});
                }
            }).catch((error) => {
                dispatch({'type' : USERS_FAILURE, 'err' : error});
            })
        } catch (error) {
            dispatch({'type' : USERS_FAILURE, 'err' : error});
        }

    }
}

export function fetchUser(id) {
    return function (dispatch) {
        try {
            new Api().get('/backend/users/' + id).then((response) => {
                if (typeof response != "undefined") {
                    dispatch({'type' : USER_FETCH, 'payload' : response});
                }
            }).catch((error) => {
                dispatch({'type' : USER_FETCH, 'payload' : null});
            })
        } catch (error) {
            dispatch({'type' : USER_FETCH, 'payload' : null});
        }

    }
}