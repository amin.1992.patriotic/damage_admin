import * as userAction from './user'
import * as treeAction from './tree'
import * as roleAction from './role'
import * as galleryAction from './gallery'
import * as ticketAction from './ticket'
import * as contentAction from './content'
import * as packageTypeAction from './packageType'
import * as attributeAction from './attribute'
import * as brandAction from './brand'
import * as itemProductAction from './itemProduct'
import * as productAction from './product'
import {
    AUTH_REFRESH, FINANCE_FAILURE, FINANCE_REQUESTING, FINANCE_SUCCESS,
    MARKETER_FAILURE,
    MARKETER_REQUESTING,
    MARKETER_SUCCESS, NOTIFICATION_FAILURE, NOTIFICATION_REQUESTING, NOTIFICATION_SUCCESS,
    ORDERS_FAILURE,
    ORDERS_REQUESTING,
    ORDERS_SUCCESS,
    PERIOD_FAILURE,
    PERIOD_REQUESTING,
    PERIOD_SUCCESS, REGION_FAILURE, REGION_REQUESTING, REGION_SUCCESS, REQUESTS_FAILURE,
    REQUESTS_REQUESTING, REQUESTS_SUCCESS,
    REWARD_FAILURE,
    REWARD_REQUESTING,
    REWARD_SUCCESS, TRANSACTIONS_FAILURE, TRANSACTIONS_REQUESTING, TRANSACTIONS_SUCCESS
} from "../types";
import Api from "../utils/api";


export function region() {

    return function (dispatch) {

        dispatch({ type: REGION_REQUESTING });

        try {
            new Api().get('/region', {}).then((response) => {
                if (typeof response !== "undefined") {
                    dispatch({ type: REGION_SUCCESS, payload: response});
                }
            })
        } catch (e) {
            dispatch({ type: REGION_FAILURE, err: e });
        }


    }

}
export function requests(object) {
    return function (dispatch) {
        dispatch({ type: REQUESTS_REQUESTING });
        try {
            new Api().get('/requests', object).then((response) => {
                if (typeof response !== "undefined") {
                    dispatch({ type: REQUESTS_SUCCESS, payload: response});
                }
            })
        } catch (e) {
            dispatch({ type: REQUESTS_FAILURE, err: e });
        }
    }
}

export function period(object) {
    return function (dispatch) {
        dispatch({ type: PERIOD_REQUESTING });
        try {
            new Api().get('/period', object).then((response) => {
                if (typeof response !== "undefined") {
                    dispatch({ type: PERIOD_SUCCESS, payload: response});
                }
            })
        } catch (e) {
            dispatch({ type: PERIOD_FAILURE, err: e });
        }
    }
}


export function reward(object) {
    return function (dispatch) {
        dispatch({ type: REWARD_REQUESTING });
        try {
            new Api().get('/rewards', object).then((response) => {
                if (typeof response !== "undefined") {
                    dispatch({ type: REWARD_SUCCESS, payload: response});
                }
            })
        } catch (e) {
            dispatch({ type: REWARD_FAILURE, err: e });
        }
    }
}


export function marketers(object) {
    return function (dispatch) {
        dispatch({ type: MARKETER_REQUESTING });
        try {
            new Api().get('/marketers', object).then((response) => {
                if (typeof response !== "undefined") {
                    dispatch({ type: MARKETER_SUCCESS, payload: response});
                }
            })
        } catch (e) {
            dispatch({ type: MARKETER_FAILURE, err: e });
        }
    }
}


export function notifications(object) {
    return function (dispatch) {
        dispatch({ type: NOTIFICATION_REQUESTING });
        try {
            new Api().get('/notifications', object).then((response) => {
                if (typeof response !== "undefined") {
                    dispatch({ type: NOTIFICATION_SUCCESS, payload: response});
                }
            })
        } catch (e) {
            dispatch({ type: NOTIFICATION_FAILURE, err: e });
        }
    }
}


export function finance(object) {
    return function (dispatch) {
        dispatch({ type: FINANCE_REQUESTING });
        try {
            new Api().get('/finance', object).then((response) => {
                if (typeof response !== "undefined") {
                    dispatch({ type: FINANCE_SUCCESS, payload: response});
                }
            })
        } catch (e) {
            dispatch({ type: FINANCE_FAILURE, err: e });
        }
    }
}

export function financePerUser(object) {
    return function (dispatch) {
        dispatch({ type: FINANCE_REQUESTING });
        try {
            new Api().get('/finance/group', object).then((response) => {
                if (typeof response !== "undefined") {
                    dispatch({ type: FINANCE_SUCCESS, payload: response});
                }
            })
        } catch (e) {
            dispatch({ type: FINANCE_FAILURE, err: e });
        }
    }
}

export function transactions(object) {
    return function (dispatch) {
        dispatch({ type: TRANSACTIONS_REQUESTING });
        try {
            new Api().get('/transactions', object).then((response) => {
                if (typeof response !== "undefined") {
                    dispatch({ type: TRANSACTIONS_SUCCESS, payload: response});
                }
            })
        } catch (e) {
            dispatch({ type: TRANSACTIONS_FAILURE, err: e });
        }
    }
}

export function orders(object) {
    return function (dispatch) {
        dispatch({ type: ORDERS_REQUESTING });
        try {
            new Api().get('/orders', object).then((response) => {
                if (typeof response !== "undefined") {
                    dispatch({ type: ORDERS_SUCCESS, payload: response});
                }
            })
        } catch (e) {
            dispatch({ type: ORDERS_FAILURE, err: e });
        }
    }
}

export function authRefresh() {
    return function (dispatch) {
        new Api().get('/auth/refresh').then((response) => {
            if (typeof response !== "undefined") {
                dispatch({ type: AUTH_REFRESH, payload: response});
            }
        })
    }
}

export {
    userAction,
    treeAction,
    roleAction,
    galleryAction,
    ticketAction,
    contentAction,
    packageTypeAction,
    attributeAction,
    brandAction,
    itemProductAction,
    productAction
}