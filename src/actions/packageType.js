import Api from "../utils/api";
import {FETCH_PACKAGE_TYPES} from "../types";


export function fetchPackageType() {
    return function (dispatch) {
        new Api().get('/products/packageTypes').then((response) => {
            if (typeof response !== "undefined") {
                dispatch({'type' : FETCH_PACKAGE_TYPES, 'payload' : response});
            }
        }).catch((error) => {
            console.log(error);
        })
    }
}
