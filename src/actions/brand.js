import Api from "../utils/api";
import {FETCH_BRANDS} from "../types";


export function fetchBrands(object) {
    return function (dispatch) {
        new Api().get('/products/brands', object).then((response) => {
            if (typeof response !== "undefined") {
                dispatch({'type' : FETCH_BRANDS, 'payload' : response});
            }
        }).catch((error) => {
            console.log(error);
        })
    }
}
