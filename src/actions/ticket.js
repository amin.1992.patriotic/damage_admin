import Api from "../utils/api";
import {FETCH_TICKET, FETCH_TICKETS, USER_FETCH, USERS_FAILURE, USERS_SUCCESS} from "../types";


export function tickets(object) {
    return function (dispatch) {
        new Api().get('/tickets', object, false).then((response) => {
            if (typeof response != "undefined") {
                dispatch({'type' : FETCH_TICKETS, 'payload' : response});
            }
        })

    }
}

export function ticket(id) {

    return function (dispatch) {

        new Api().get('/tickets/' + id + '/conversations').then((response) => {
            if (typeof response != "undefined") {
                dispatch({'type' : FETCH_TICKET, 'payload' : response});
            }
        })
    }
}