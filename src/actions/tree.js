import Api from "../utils/api";
import {FETCH_NODES, TOGGLE_CHECKED, TOGGLE_EXPANDED} from "../types";
import {toast} from "react-toastify";


export function blogCategories() {
    return function (dispatch) {
        try {
            new Api().get('/blog/categories', {}, false).then((response) => {
                if (typeof response !== "undefined") {
                    dispatch({'type' : FETCH_NODES, 'payload' : response});
                }
            }).catch((error) => {
                console.log(error);
            })
        } catch (e) {
            toast.error(e);
        }

    }
}

export function menu() {
    return function (dispatch) {
        try {
            new Api().get('/blog/menu', {}, false).then((response) => {
                if (typeof response !== "undefined") {
                    dispatch({'type' : FETCH_NODES, 'payload' : response});
                }
            }).catch((error) => {
                console.log(error);
            })
        } catch (e) {
            toast.error(e);
        }

    }
}



export function ticketCategories() {
    return function (dispatch) {
        try {
            new Api().get('/tickets/categories').then((response) => {
                if (typeof response !== "undefined") {
                    dispatch({'type' : FETCH_NODES, 'payload' : response});
                }
            }).catch((error) => {
                console.log(error);
            })
        } catch (e) {
            toast.error(e);
        }

    }
}


export function filters(object) {
    return function (dispatch) {
        try {
            new Api().get('/products/filters', object).then((response) => {
                if (typeof response !== "undefined") {
                    dispatch({'type' : FETCH_NODES, 'payload' : response});
                }
            }).catch((error) => {
                console.log(error);
            })
        } catch (e) {
            toast.error(e);
        }

    }
}

export function productCategories() {
    return function (dispatch) {
        try {
            new Api().get('/products/categories').then((response) => {
                if (typeof response !== "undefined") {
                    dispatch({'type' : FETCH_NODES, 'payload' : response});
                }
            }).catch((error) => {
                console.log(error);
            })
        } catch (e) {
            toast.error(e);
        }

    }
}

export function region() {
    return function (dispatch) {
        try {
            new Api().get('/region').then((response) => {
                if (typeof response !== "undefined") {
                    dispatch({'type' : FETCH_NODES, 'payload' : response});
                }
            }).catch((error) => {
                console.log(error);
            })
        } catch (e) {
            toast.error(e);
        }

    }
}

export function priceParameters() {
    return function (dispatch) {
        try {
            new Api().get('/products/priceParameters').then((response) => {
                if (typeof response !== "undefined") {
                    dispatch({'type' : FETCH_NODES, 'payload' : response});
                }
            }).catch((error) => {
                console.log(error);
            })
        } catch (e) {
            toast.error(e);
        }

    }
}


export function checked(checked) {
    return function (dispatch) {
        dispatch({'type' : TOGGLE_EXPANDED, 'payload' : checked});
    }
}

export function expanded(expanded) {
    return function (dispatch) {
        dispatch({'type' : TOGGLE_CHECKED, 'payload' : expanded});
    }
}
