import Api from "../utils/api";
import {FETCH_GALLERY} from "../types";


export function fetchGalleries() {
    return function (dispatch) {
        new Api().get('/galleries').then((response) => {
            if (typeof response !== "undefined") {
                dispatch({'type' : FETCH_GALLERY, 'payload' : response});
            }
        }).catch((error) => {
            console.log(error);
        })
    }
}
