import Api from "../utils/api";
import {FETCH_ITEM_PRODUCT} from "../types";

export function fetchItemProduct(object) {
    return function (dispatch) {
        new Api().get('/products/itemProduct', object).then((response) => {
            if (typeof response != "undefined") {
                dispatch({'type' : FETCH_ITEM_PRODUCT, 'payload' : response});
            }
        })

    }
}
