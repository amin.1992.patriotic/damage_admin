import Api from "../utils/api";
import {FETCH_ATTRIBUTES} from "../types";


export function fetchAttributes(object) {
    return function (dispatch) {
        new Api().get('/products/attributes', object).then((response) => {
            if (typeof response !== "undefined") {
                dispatch({'type' : FETCH_ATTRIBUTES, 'payload' : response});
            }
        }).catch((error) => {
            console.log(error);
        })
    }
}
