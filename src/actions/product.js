import Api from "../utils/api";
import {FETCH_PRODUCTS} from "../types";

export function fetchProducts(object) {
    return function (dispatch) {
        new Api().get('/products', object).then((response) => {
            if (typeof response != "undefined") {
                dispatch({'type' : FETCH_PRODUCTS, 'payload' : response});
            }
        })
    }
}