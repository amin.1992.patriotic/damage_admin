import React from "react";
import {CircularProgress} from "@material-ui/core";
import './style.css';

const loading = () => {

    return(
        <div className={'loading-baba'}>
            <CircularProgress size={20} className={'loading'} color='secondary' />
        </div>

    );
}


export default loading;