import React, {memo, useEffect, useState} from 'react';
import {Box, Paper} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import {packageTypeAction, treeAction} from "../../actions";
import {connect, useDispatch} from "react-redux";
import Autocomplete from "@material-ui/lab/Autocomplete";
import Button from "@material-ui/core/Button";
import Api from "../../utils/api";
import {toast} from "react-toastify";
import Tree from "../Tree";
import {FETCH_NODES, TOGGLE_CHECKED, TOGGLE_EXPANDED} from "../../types";
import {Loading} from './../../components'
import Container from "@material-ui/core/Container";

const ProductEditFilter = memo((props) => {

    const {treeReducer, productID, fetchProductFilters, onComplete } = props;

    const dispatch = useDispatch();
    const [loading, setLoading] = useState(true);
    const [form, setForm] = useState({})
    const [searchTime, setSearchTime] = useState("");

    const handleChangeElement = (event) => {
        let frm = {...form};

        const word = event.target.value;
        const name = event.target.name;

        if (searchTime) {
            clearTimeout(searchTime)
        }
        let loop = setTimeout(()=>{

            frm[name] = word;
            setForm(frm);

        },500);

        setSearchTime(loop);


    };


    useEffect(() => {
        handleRequest();
    }, [form]);

    const handleRequest = () => {
        fetchProductFilters({
            term: form.title ? form.title : ''
        })
    }



    useEffect( () => {

        dispatch({type: FETCH_NODES, payload: []});

        new Api().get(`/products/${productID}/filters`).then((response) => {
            if (typeof response !== "undefined") {
                // initial edit form
                let checked =  [];
                let expanded = [];
                response.map((category) => {
                    checked.push(category.id);
                    if (Boolean(category.is_main) === false) {
                        expanded.push(category.id)
                    }
                });

                dispatch({type: TOGGLE_CHECKED, payload: checked});
                dispatch({type: TOGGLE_EXPANDED, payload: expanded});
            }
            setLoading(false);
        })
    }, []);

    const handleSubmit = (event) => {

        event.preventDefault();

        setLoading(true);
        new Api().post(`/products/${productID}/filters`, {filters : treeReducer.checked}).then((response) => {
            if (typeof response !== "undefined") {
                if (response.status) {
                    if (typeof onComplete !== undefined) {
                        onComplete();
                    }
                    toast.success(response.msg)
                } else {
                    toast.error(response.msg)
                }
                setLoading(false);
            }
        })

    };

    return(
        <form onSubmit={handleSubmit} className='animate__animated animate__fadeIn'>
            <Box mt={5}>
                <Paper style={{ padding: '25px'}}>
                    <Container>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <TextField
                                    style={{ marginTop: '25px'}}
                                    label="نام"
                                    variant="outlined"
                                    margin='dense'
                                    fullWidth
                                    name='title'
                                    onChange={handleChangeElement}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    placeholder={'جستجو کنید'}
                                    autoComplete={"off"}
                                />
                            </Grid>
                        </Grid>
                    </Container>
                    <Tree/>
                </Paper>
            </Box>
            <Box mt={2} className='display-flex display-flex-direction-row display-flex-justify-content-flex-end'>
                <Button disabled={loading} variant='contained' color='primary' type="submit">تکمیل اطلاعات</Button>
            </Box>
            {loading && <Loading/>}
        </form>
    );
})

const mapStateToProps = state => ({
    treeReducer : state.treeReducer
});

const mapDispatchToProps = (dispatch) => ({
    fetchProductFilters: (object) => {
        dispatch(treeAction.filters(object))
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductEditFilter);