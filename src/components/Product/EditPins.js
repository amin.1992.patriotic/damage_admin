import React, {memo, useEffect, useState} from 'react';
import Api from "../../utils/api";
import {toast} from "react-toastify";
import CurrencyFormat from "react-currency-format";
import Pagination from "react-js-pagination";
import moment from "moment-jalaali";
import {Paper, Tooltip} from '@material-ui/core'
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import Button from "@material-ui/core/Button";
import {CircularProgress} from "@material-ui/core";
import {Loading} from "../index";
import {useHistory} from "react-router";
import { DatePicker } from "jalali-react-datepicker";
import ProductPinsExcel from "./pinsExcel";

const ProductPins = memo((props) => {

    const history = useHistory();
    const {productID, onComplete} = props;
    const [loading, setLoading] = useState(true);
    const [product, setProduct] = useState(null);
    const [page, setPage] = useState(1);
    const [types_id, setTypesId] = useState('');


    const [pinsForm, setPinsForm] = useState({
        product_types_id: '',
        count: 0,
        type: 'inc_count',
        depot_date: '',
        document_number: ''
    });

    useEffect(() => {
        handleRequest();
    }, [page]);

    const handleRequest = () => {
        setLoading(true);
        new Api().get('/products/' + productID + '/pins', {
            'page': page,
            'product_types_id': types_id
        }).then((response) => {
            if (typeof response != "undefined") {
                setProduct(response);
            }
            setLoading(false);
        }).catch((error) => {
            toast.error(error);
        });
    }

    const handleChangePinsForm = (event) => {
        let frm = {...pinsForm};
        frm[event.target.name] = event.target.value;
        setPinsForm(frm);
    };


    useEffect(() => {
        handleRequest();
    }, [types_id])


    const handleSubmitPinsForm = (event) => {
        event.preventDefault();


        if (pinsForm.count === 0) {
            toast.error('تعداد نمیتواند صفر باشد.');
            return;
        }

        if (pinsForm.product_types_id === '') {
            toast.error('تنوع محصول انتخاب نشده است.');
            return;
        }

        if (pinsForm.depot_date === '') {
            toast.error('تاریخ کارتکس را وارد نکرده اید.');
            return;
        }

        if (pinsForm.depot_date === '') {
            toast.error('شماره سند کارتکس را وارد نکرده اید.');
            return;
        }

        setLoading(true);

        new Api().post('/products/' + productID + '/pins', pinsForm).then((response) => {
            if (typeof response !== "undefined") {
                if (response.status) {
                    handleRequest();
                    toast.success(response.msg)
                } else {
                    toast.error(response.msg)
                }
            }
            setLoading(false);
        })
    }

    return(
        <div>
            {product && product.product && <p className={'alert alert-info'} ><b>تعداد کل در همه تنوع ها<span>&nbsp;<span>{product.product.count}</span></span><span>&nbsp;<span>{product.product.package_type && product.product.package_type.title}</span>&nbsp;</span></b></p>}
            {product && <Grid container={true} spacing={2}>
                <Grid item={true} xs={12} sm={12}>
                    <Paper className='pins-form'>
                        <div className={'pins-form-header'}>
                            <h3>مدیریت موجودی محصول</h3>
                        </div>
                        <form onSubmit={handleSubmitPinsForm}>
                            <Grid container={true} spacing={2}>
                                <Grid item sm={4} xs={12} >
                                    <TextField
                                        label="تنوع محصول"
                                        margin='dense'
                                        size={"small"}
                                        variant={"outlined"}
                                        name={'product_types_id'}
                                        select
                                        value={pinsForm.product_types_id}
                                        fullWidth
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                        onChange={handleChangePinsForm}
                                    >
                                        {product && product.product && product.product.types && product.product.types.map((item) => {
                                            return(
                                                <MenuItem value={item.id}>{item.title}</MenuItem>
                                            );
                                        })}
                                    </TextField>
                                </Grid>
                                <Grid item sm={4} xs={12} >
                                    <TextField
                                        label="تعداد"
                                        variant="outlined"
                                        margin='dense'
                                        fullWidth
                                        name={'count'}
                                        value={pinsForm.count}
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                        onChange={handleChangePinsForm}
                                    />
                                </Grid>
                                <Grid item sm={4} xs={12} >
                                    <TextField
                                        label="نوع عملیات"
                                        margin='dense'
                                        size={"small"}
                                        variant={"outlined"}
                                        name={'type'}
                                        select
                                        value={pinsForm.type}
                                        fullWidth
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                        onChange={handleChangePinsForm}
                                    >
                                        <MenuItem value={'inc_count'}>افزایش</MenuItem>
                                        <MenuItem value={'dec_count'}>کاهش</MenuItem>
                                    </TextField>
                                </Grid>
                                <Grid item sm={6} xs={12} >
                                    <TextField
                                        label="شماره سند کارتکس  یا توضیحات"
                                        variant="outlined"
                                        margin='dense'
                                        fullWidth
                                        name={'document_number'}
                                        value={pinsForm.document_number}
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                        onChange={handleChangePinsForm}
                                    />
                                </Grid>
                                <Grid item xs={12} sm={6}>
                                    <div className='datepicker-input-container'>
                                        <label>تاریخ کارتکس</label>
                                        <DatePicker
                                            onClickSubmitButton={(value) => {
                                                setPinsForm({...pinsForm, depot_date: moment(value.value).locale('en').format('YYYY/MM/DD HH:mm:ss')});
                                            }}
                                        />

                                    </div>
                                </Grid>
                                <Grid item xs={12} >
                                    <Button disabled={loading} type={"submit"} variant={"contained"} color={"primary"} fullWidth={true}>ویرایش موجودی</Button>
                                </Grid>
                            </Grid>
                        </form>
                    </Paper>
                </Grid>
            </Grid>}
            <div style={{ display: 'flex', direction: 'row', justifyContent: 'flex-end', margin: '20px 0'}}>
                <TextField
                    label="تنوع محصول"
                    margin='dense'
                    size={"small"}
                    variant={"outlined"}
                    name={'product_types_id'}
                    select
                    fullWidth
                    InputLabelProps={{
                        shrink: true,
                    }}
                    onChange={(event) => setTypesId(event.target.value)}
                >
                    <MenuItem value={''}>انتخاب</MenuItem>
                    {product && product.product && product.product.types && product.product.types.map((item) => {
                        return(
                            <MenuItem value={item.id}>{item.title}</MenuItem>
                        );
                    })}
                </TextField>
                <ProductPinsExcel product_id={productID} object={{}} />
                {product && product.pins && product.pins.data && product.pins.data.length > 0 && <Pagination
                    activePage={page}
                    itemsCountPerPage={parseInt(product.pins.per_page)}
                    totalItemsCount={parseInt(product.pins.total)}
                    pageRangeDisplayed={5}
                    onChange={(prev) => setPage(prev)}
                />}
            </div>

            <div style={{ overflowX: 'auto', marginTop: '30px'}}>
                <table className='table'>
                    <thead>
                    <tr>
                        <th>ردیف</th>
                        <th>تنوع محصول</th>
                        <th>اپراتور</th>
                        <th>شماره سفارش</th>
                        <th>تعداد کسر شده</th>
                        <th>تعداد اضافه شده</th>
                        <th>جمع</th>
                        <th>تاریخ ثبت</th>
                        <th>شماره سند</th>
                        <th>تاریخ کارتکس</th>
                    </tr>
                    </thead>
                    <tbody>
                    {product && product.pins && product.pins.data.map((entity, index) => {
                        return(
                            <tr key={index}>
                                <td className={entity.operator_id ? 'green-background' : ''}>{entity.id}</td>
                                <td>{entity.types && entity.types.title}</td>
                                <td>{entity.operator ? (entity.operator.name ? (entity.operator.name + ' ' + entity.operator.family)  : entity.operator.mobile) : '-'}</td>
                                <td onClick={() => entity.order_id ? history.push('/orders/' + entity.order_id) : ''}>{entity.order_id ? entity.order_id : '-'}</td>
                                <td><CurrencyFormat value={entity.dec_count}  displayType={'text'} thousandSeparator={true}  /></td>
                                <td><CurrencyFormat value={entity.inc_count}  displayType={'text'} thousandSeparator={true}  /></td>
                                <td><CurrencyFormat value={entity.total ? entity.total : ''}  displayType={'text'} thousandSeparator={true}  /></td>
                                <td>{moment(entity.created_at).locale('fa').format('jYYYY/jMM/jDD HH:mm:ss')}</td>
                                <td>{entity.document_number ? entity.document_number : '-'}</td>
                                <td>{entity.depot_date ? moment(entity.depot_date).locale('fa').format('jYYYY/jMM/jDD HH:mm:ss') : '-'}</td>
                                <td>-</td>
                            </tr>
                        );
                    })}
                    </tbody>
                </table>
            </div>
            {product && product.pins && product.pins.data && product.pins.data.length > 0 && <Pagination
                activePage={page}
                itemsCountPerPage={parseInt(product.pins.per_page)}
                totalItemsCount={parseInt(product.pins.total)}
                pageRangeDisplayed={5}
                onChange={(prev) => setPage(prev)}
            />}
            {loading && <Loading /> }
        </div>
    );
})

export default ProductPins;