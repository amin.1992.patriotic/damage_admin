import React, {memo, useEffect, useState} from 'react';
import {Box, Paper} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import {packageTypeAction, treeAction} from "../../actions";
import {connect} from "react-redux";
import Autocomplete from "@material-ui/lab/Autocomplete";
import Button from "@material-ui/core/Button";
import Api from "../../utils/api";
import {toast} from "react-toastify";
import TextEditor from "../Editor";
import {TagAutocomplete} from "../index";
import Chip from "@material-ui/core/Chip";
import {Loading} from './../../components'

const ProductEditSeo = memo((props) => {

    const {productID, onComplete} = props;

    const [form, setForm] = useState({});
    const [loading, setLoading] = useState(true);
    const [tag, setTag] = useState([]);

    useEffect(() => {

        new Api().get('/products/' + productID + '/seo').then((response) => {
            if (typeof response !== "undefined") {

                let init_tags = [];
                if (response.tags) {
                    response.tags.map((t) => {
                        init_tags.push(t.id);
                    });
                }

                setTag(init_tags);

                setForm(response);
            }
            setLoading(false);
        })
    }, []);

    const handleChangeElement = (event) => {

        let frm = form;
        frm[event.target.name] = event.target.value;
        setForm({...frm});
    };


    const handleSubmit = (event) => {
        event.preventDefault();

        setLoading(true);

        let n_form = {...form};
        n_form['tags'] = tag;


        new Api().put('/products/' + productID + '/seo', n_form, false).then((response) => {
            if (typeof response !== "undefined") {
                if (response.status) {
                    if (typeof onComplete !== undefined) {
                        onComplete();
                    }
                    toast.success(response.msg);
                } else {
                    toast.error(response.msg);
                }

                setLoading(false);
            }
        })

    };


    console.log(form);
    return(
        <form onSubmit={handleSubmit} className='animate__animated animate__fadeIn'>
            <Box mt={5}>
                <Box mt={5}>
                    <Paper style={{ padding: '25px'}}>
                        <Grid container>
                            <Grid item xs={12} >
                                <TextField
                                    label="اسلاگ"
                                    variant="outlined"
                                    margin='dense'
                                    value={form.slug}
                                    fullWidth
                                    name='slug'
                                    onChange={handleChangeElement}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    helperText='بدون فاصله وارد کنید.'
                                />
                            </Grid>
                            <Grid item xs={12} >
                                <TextField
                                    label="متا عنوان"
                                    variant="outlined"
                                    margin='dense'
                                    value={form.meta_title}
                                    fullWidth
                                    name='meta_title'
                                    onChange={handleChangeElement}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    helperText='60 تا 255 کاراکتر وارد کنید.'
                                />
                            </Grid>
                            <Grid item xs={12} >
                                <TextField
                                    label="متا توضیحات"
                                    variant="outlined"
                                    margin='dense'
                                    value={form.meta_description}
                                    fullWidth
                                    name='meta_description'
                                    onChange={handleChangeElement}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    helperText='60 تا 255 کاراکتر وارد کنید.'
                                />
                            </Grid>
                        </Grid>
                    </Paper>
                </Box>
                <Box mt={5}>
                    <Paper style={{ padding: '25px'}}>
                        <Grid container>
                            <Grid item xs={12} >
                                <TextField
                                    label="توضیحات کوتاه"
                                    variant="outlined"
                                    margin='dense'
                                    value={form.short_content}
                                    fullWidth
                                    name='short_content'
                                    onChange={handleChangeElement}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    helperText={'توضیحات چکیده از متن'}
                                />
                            </Grid>
                            <Grid item xs={12} style={{ margin: '20px 0'}}>
                                <TextEditor
                                    value={form.content}
                                    onChange={(value) => {
                                        let n_form = form;
                                        n_form['content'] = value;
                                        setForm({...n_form});
                                    }} />
                            </Grid>
                            <Grid item xs={12}>
                                <TagAutocomplete onChange={(value) => {
                                    let tags = tag;
                                    value.forEach((value) => {
                                        tags.push(value.id);
                                    });
                                    setTag(tags);
                                }}/>
                                {form.tags && form.tags.map((item , index) => {
                                    return(
                                        <Chip
                                            label={item.name}
                                            clickable
                                            color="default"
                                            onDelete={() => {
                                                // remove from form
                                                let tags = form['tags'];
                                                tags.splice(index, 1);
                                                form['tags'] = tags;
                                                // remove from tag
                                                tags = tag;
                                                tags.splice(index, 1);
                                                // init tag in form and tag const
                                                setForm({...form});
                                                setTag(tags);
                                            }}
                                        />
                                    )
                                })}
                            </Grid>
                        </Grid>
                    </Paper>
                </Box>
            </Box>
            <Box mt={2} className='display-flex display-flex-direction-row display-flex-justify-content-flex-end'>
                <Button disabled={loading} variant='contained' color='primary' type="submit">تکمیل اطلاعات</Button>
            </Box>
            {loading && <Loading/>}
        </form>
    );
})

export default ProductEditSeo;