import React, {memo, useEffect, useState} from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import {Tooltip} from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import Grid from "@material-ui/core/Grid";
import Api from "../../../utils/api";
import {toast} from "react-toastify";
import GamesIcon from '@material-ui/icons/Games';
import Autocomplete from '@material-ui/lab/Autocomplete';
import {useDispatch} from "react-redux";
import {TOGGLE_CHECKED} from "../../../types";

const Move = memo((props) => {

    const { checked , reload } = props;

    const dispatch = useDispatch();

    const [parent, setParent] = useState(null);
    const [list, setList] = useState([]);
    const [item, setItem] = useState(null);


    const [loading, setLoading] = useState(false);
    const [open, setOpen] = useState(false);


    useEffect(() => {
        if (open === true) {
            new Api().get(`/products/filters/${checked}/findMapState`).then((response) => {
                if (typeof response !== "undefined") {
                    setList(response.list)
                    setItem(response.item)
                }
            })
        }
    }, [open])


    const autoCompleteHandleSelect = (value) => {
        if (value) {
            setParent(value.id)
        } else {
            setParent(null)
        }
    }

    const handleSubmit = (event) => {
        event.preventDefault();

        new Api().put(`/products/filters/${item.id}/dispatchMove`, {'parent' : parent}).then((response) => {
            if (typeof response != "undefined") {
                if (response.status) {
                    dispatch({type: TOGGLE_CHECKED, payload: []});
                    toast.success(response.msg);
                    setOpen(false);
                    reload()
                } else {
                    toast.error(response.msg);
                }
            }
        }).catch((error) => {
            console.log(error);
        })

    }



    return (
        <div>
            <Tooltip title="جا به جایی">
                <IconButton onClick={() => setOpen(true)}>
                    <GamesIcon />
                </IconButton>
            </Tooltip>
            <Dialog fullWidth={true} open={open} aria-labelledby="form-dialog-title">
                <form dir='rtl' onSubmit={handleSubmit}>
                    <DialogTitle id="form-dialog-title">{ 'جابه جایی دسته بندی ' + (item !== null && item.title) }</DialogTitle>
                    <DialogContent>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <Autocomplete
                                    id="combo-box-demo"
                                    options={list}
                                    getOptionLabel={(option) => option.title}
                                    onChange={((event, id) => autoCompleteHandleSelect(id))}
                                    renderInput={
                                        (params) =>
                                            <TextField
                                                variant="outlined"
                                                margin='dense'
                                                fullWidth={true}
                                                {...params}
                                                label="دسته بندی"
                                            />
                                    }
                                />
                            </Grid>
                        </Grid>
                    </DialogContent>
                    <DialogActions>
                        <Button color="primary" onClick={() => setOpen(false)}>
                            انصراف
                        </Button>
                        <Button disabled={loading} color="primary" autoFocus type='submit'>
                            ارسال اطلاعات
                        </Button>
                    </DialogActions>
                </form>
            </Dialog>
        </div>
    );

})


export default Move;