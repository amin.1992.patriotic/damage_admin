import React, {useState, memo, useEffect, useCallback} from "react";
import ReactExport from "react-data-export";
import {Tooltip} from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import ImportExportIcon from '@material-ui/icons/ImportExport';
import moment from "moment-jalaali";
import Api from "../../utils/api";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import {Loading} from "../../components";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import {useSelector} from "react-redux";
const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

const ProductExcelDownload = (props) => {

    const { object } = props;


    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(false);
    const [open, setOpen] = useState(false);

    const fetchData = () => {
        setLoading(true);

        object.excel_export = 1;
        new Api().get('/products', object).then((response) => {
            if (typeof response !== "undefined") {
                let n_p = [];
                response.map((product, index) => {
                    n_p[index] = {
                        'id' : product.id,
                        'title' : product.title,
                        'code' : product.code,
                        'brand' : product.brand ? product.brand.title : '-',
                        'count' : product.types && product.types.length > 0 && product.types[0].count,
                        'package_type' : product.package_type ? product.package_type.title : '-',
                        'price' : product.types && product.types.length > 0 && product.types[0].price,
                        'discount' : product.types && product.types.length > 0 && product.types[0].discount,
                        'weight' : product.types && product.types.length > 0 && product.types[0].weight,
                        'status' : product.status ? 'فعال' : 'غیرفعال',
                    }
                })

                setData([...n_p]);
                setOpen(true);
                setLoading(false);
            }
        })
    };


    return (
        <React.Fragment>
            <IconButton onClick={() => fetchData()}>
                <ImportExportIcon />
            </IconButton>
            {data.length > 0 && <ExcelFile filename={'products_' + moment().unix()} hideElement={false} element={
                <Dialog
                    fullWidth={true}
                    open={open}
                    keepMounted
                    onClose={() => {
                        setOpen(false);
                        setData([]);
                    }}
                    aria-labelledby="alert-dialog-slide-title"
                    aria-describedby="alert-dialog-slide-description"
                >
                    <DialogTitle id="alert-dialog-slide-title">جلوگیری از ارسال درخواست  توسط ربات</DialogTitle>
                    <DialogContent>
                        {loading && <Loading />}
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => {
                            setOpen(false);
                            setData([]);
                        }}  color="primary">
                            بستن
                        </Button>
                        <Button onClick={() => {
                            setOpen(false);
                            setData([]);
                        }} color="primary">
                            دانلود
                        </Button>
                    </DialogActions>
                </Dialog>
            }>
                <ExcelSheet data={data} name="orders">
                    <ExcelColumn  label="شناسه محصول" value="id"/>
                    <ExcelColumn  label="نام محصول" value="title"/>
                    <ExcelColumn  label="برند" value="brand"/>
                    <ExcelColumn  label="کد انبار" value="code"/>
                    <ExcelColumn  label="موجودی" value="count"/>
                    <ExcelColumn  label="وزن" value="weight"/>
                    <ExcelColumn  label="واحد" value="package_type"/>
                    <ExcelColumn  label="قیمت" value="price"/>
                    <ExcelColumn  label="تخفیف" value="discount"/>
                    <ExcelColumn  label="وضعیت" value="status"/>
                </ExcelSheet>
            </ExcelFile>}
            {loading && <Loading />}
        </React.Fragment>
    );

}

export default memo(ProductExcelDownload);

