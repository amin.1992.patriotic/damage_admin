import React, {memo, useEffect, useState} from "react";
import Autocomplete from "@material-ui/lab/Autocomplete/Autocomplete";
import TextField from "@material-ui/core/TextField";
import Api from "../../../../utils/api";

const BrandAutoComplete = memo((props) => {


    const { onChange, values } = props;

    const [options, setOptions] = useState(values ? values : []);
    const [typingTimeout, setTypingTimeout] = useState(0);
    const instance = new Api();

    const autoCompleteHandleSelect = (id) =>
    {
        // return value of props
        onChange(id);
    };

    const autoCompleteHandleChange = (event) =>
    {
        let val = event.target.value;

        if (typingTimeout) {
            clearTimeout(typingTimeout);
        }

        let loop = setTimeout(async () => {
            await new Promise(resolve => {
                resolve(instance.get('/products/brands/autocomplete', {'term': val}).then((response) => {
                    if (typeof response != "undefined") {
                        setOptions(response);
                    }
                }));
            })
        }, 500);

        setTypingTimeout(loop);

    };

    return(
        <Autocomplete
            onChange={((event, value) => autoCompleteHandleSelect(value))}
            options={options}
            getOptionLabel={option => option.title}
            renderInput={params => (
                <TextField
                    {...params}
                    fullWidth
                    margin={"dense"}
                    label="برند"
                    variant="outlined"
                    InputLabelProps={{
                        shrink: true,
                    }}
                    onChange={autoCompleteHandleChange}
                />
            )}
        />
    );
});

export default BrandAutoComplete