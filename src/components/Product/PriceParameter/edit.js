import React, {memo, useEffect, useState} from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import {Tooltip} from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import Grid from "@material-ui/core/Grid";
import Api from "../../../utils/api";
import {toast} from "react-toastify";
import EditIcon from "@material-ui/icons/Edit"
import MenuItem from "@material-ui/core/MenuItem";
import {TagAutocomplete} from './../../../components'
import Chip from "@material-ui/core/Chip";
import {TOGGLE_CHECKED} from "../../../types";
import {useDispatch} from "react-redux";

const Edit = memo((props) => {

    const { checked , reload } = props;
    const dispatch = useDispatch();

    const [form, setForm] = useState({});

    const [loading, setLoading] = useState(false);
    const [open, setOpen] = useState(false);


    const handleChangeElement = (event) => {
        let frm = {...form};
        frm[event.target.name] = event.target.value;
        setForm(frm);
    };

    useEffect(() => {
        if (open === true) {
            handleRequest();
        }
    }, [open])

    const handleRequest = () => {
        new Api().get('/products/priceParameters/' + checked).then((response) => {
            if (typeof response !== "undefined") {
                setForm(response)
            }
        })
    }


    const handleSubmit = (event) => {

        event.preventDefault();

        setLoading(true);

        new Api().put('/products/priceParameters/' + checked,{
            form
        }).then((response) => {
            if (typeof response !== "undefined") {
                if (response.status) {
                    dispatch({type: TOGGLE_CHECKED, payload: []});
                    setTimeout(() => {
                        reload();
                    }, 1000);

                    toast.success(response.msg);
                } else {
                    toast.error(response.msg);
                }
            }
            setLoading(false);
        })

    }


    return (
        <div>
            <Tooltip title="ویرایش">
                <IconButton onClick={() => setOpen(true)}>
                    <EditIcon />
                </IconButton>
            </Tooltip>
            <Dialog fullWidth={true} open={open} aria-labelledby="form-dialog-title">
                <form dir='rtl' onSubmit={handleSubmit}>
                    <DialogTitle id="form-dialog-title">ویرایش دسته بندی</DialogTitle>
                    <DialogContent>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <TextField
                                    label="عنوان"
                                    variant="outlined"
                                    margin='dense'
                                    fullWidth
                                    name='title'
                                    value={form.title}
                                    onChange={handleChangeElement}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    select
                                    label="وضعیت"
                                    value={parseInt(form.status)}
                                    variant="outlined"
                                    margin='dense'
                                    fullWidth
                                    name='status'
                                    onChange={handleChangeElement}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                >
                                    <MenuItem value={1}>فعال</MenuItem>
                                    <MenuItem value={0}>غیرفعال</MenuItem>
                                </TextField>
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    select
                                    label="حذف"
                                    value={parseInt(form.deleted)}
                                    variant="outlined"
                                    margin='dense'
                                    fullWidth
                                    name='deleted'
                                    onChange={handleChangeElement}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                >
                                    <MenuItem value={1}>شود</MenuItem>
                                    <MenuItem value={0}>نشود</MenuItem>
                                </TextField>
                            </Grid>
                        </Grid>
                    </DialogContent>
                    <DialogActions>
                        <Button color="primary" onClick={() => setOpen(false)}>
                            انصراف
                        </Button>
                        <Button disabled={loading} color="primary" autoFocus type='submit'>
                            ارسال اطلاعات
                        </Button>
                    </DialogActions>
                </form>
            </Dialog>
        </div>
    );

})


export default Edit;