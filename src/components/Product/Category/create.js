import React, {memo, useEffect, useState} from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import {Tooltip} from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import Grid from "@material-ui/core/Grid";
import Api from "../../../utils/api";
import {toast} from "react-toastify";

const Create = memo((props) => {

    const { checked, reload } = props;

    const [form, setForm] = useState({
        title: ''
    });

    const [loading, setLoading] = useState(false);
    const [open, setOpen] = useState(false);

    const handleChangeElement = (event) => {
        let frm = {...form};
        frm[event.target.name] = event.target.value;
        setForm(frm);
    };


    const handleSubmit = (event) => {
        event.preventDefault();

        setLoading(true);
        new Api().post('/products/categories',{
            form,
            checked: checked
        }).then((response) => {
            if (typeof response !== "undefined") {
                if (response.status) {
                    setOpen(false);
                    reload()
                } else {
                    toast.error(response.msg);
                }
            }
            setLoading(false);
        })

    }


    return (
        <div>
            <Tooltip title="افزودن دسته بندی جدید">
                <IconButton onClick={() => setOpen(true)}>
                    <AddCircleOutlineIcon />
                </IconButton>
            </Tooltip>
            <Dialog fullWidth={true} open={open} aria-labelledby="form-dialog-title">
                <form dir='rtl' onSubmit={handleSubmit}>
                    <DialogTitle id="form-dialog-title">دسته بندی  جدید</DialogTitle>
                    <DialogContent>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <TextField
                                    label="عنوان"
                                    variant="outlined"
                                    margin='dense'
                                    fullWidth
                                    name='title'
                                    onChange={handleChangeElement}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    helperText='با علامت , جدا کنید'
                                />[2021-04-12T17:56:57.375897+04:30] inputs.INFO: random: 0.5257283629113259 [] []
[2021-04-12T17:56:57.376474+04:30] inputs.INFO: time: 1618234076 [] []
[2021-04-12T17:56:57.376583+04:30] inputs.INFO: origin: localhost [] []
[2021-04-12T17:56:57.376678+04:30] inputs.INFO: origin: localhost [] []
[2021-04-12T17:57:00.187835+04:30] inputs.INFO: random: 0.06334790262773149 [] []
[2021-04-12T17:57:00.188121+04:30] inputs.INFO: time: 1618234079 [] []
[2021-04-12T17:57:00.188269+04:30] inputs.INFO: origin: localhost [] []
[2021-04-12T17:57:00.188403+04:30] inputs.INFO: title: البرز [] []
[2021-04-12T17:57:00.188535+04:30] inputs.INFO: status: 1 [] []
[2021-04-12T17:57:00.188665+04:30] inputs.INFO: deleted:  [] []
[2021-04-12T17:57:00.188791+04:30] inputs.INFO: is_posting: 1 [] []
[2021-04-12T17:57:00.188918+04:30] inputs.INFO: is_bearing:  [] []
[2021-04-12T17:57:00.189043+04:30] inputs.INFO: is_delivery:  [] []
[2021-04-12T17:57:00.189167+04:30] inputs.INFO: in_person:  [] []
[2021-04-12T17:57:00.189283+04:30] inputs.INFO: in_person:  [] []
[2021-04-12T17:58:04.073152+04:30] inputs.INFO: random: 0.5187469945033409 [] []
[2021-04-12T17:58:04.073295+04:30] inputs.INFO: time: 1618234143 [] []
[2021-04-12T17:58:04.073359+04:30] inputs.INFO: origin: localhost [] []
[2021-04-12T17:58:04.073414+04:30] inputs.INFO: origin: localhost [] []
[2021-04-12T17:58:09.511784+04:30] inputs.INFO: random: 0.09888994015472696 [] []
[2021-04-12T17:58:09.512649+04:30] inputs.INFO: time: 1618234148 [] []
[2021-04-12T17:58:09.512754+04:30] inputs.INFO: origin: localhost [] []
[2021-04-12T17:58:09.512845+04:30] inputs.INFO: origin: localhost [] []
[2021-04-12T17:58:11.018964+04:30] inputs.INFO: random: 0.5296122162954788 [] []
[2021-04-12T17:58:11.019142+04:30] inputs.INFO: time: 1618234150 [] []
[2021-04-12T17:58:11.019235+04:30] inputs.INFO: origin: localhost [] []
[2021-04-12T17:58:11.019323+04:30] inputs.INFO: title: البرز [] []
[2021-04-12T17:58:11.019409+04:30] inputs.INFO: status: 1 [] []
[2021-04-12T17:58:11.019494+04:30] inputs.INFO: deleted:  [] []
[2021-04-12T17:58:11.019577+04:30] inputs.INFO: is_posting: 1 [] []
[2021-04-12T17:58:11.019658+04:30] inputs.INFO: is_bearing:  [] []
[2021-04-12T17:58:11.019737+04:30] inputs.INFO: is_delivery:  [] []
[2021-04-12T17:58:11.019815+04:30] inputs.INFO: in_person:  [] []
[2021-04-12T17:58:11.019888+04:30] inputs.INFO: in_person:  [] []

                            </Grid>
                        </Grid>
                    </DialogContent>
                    <DialogActions>
                        <Button color="primary" onClick={() => setOpen(false)}>
                            انصراف
                        </Button>
                        <Button disabled={loading} color="primary" autoFocus type='submit'>
                            ارسال اطلاعات
                        </Button>
                    </DialogActions>
                </form>
            </Dialog>
        </div>
    );

})


export default Create;