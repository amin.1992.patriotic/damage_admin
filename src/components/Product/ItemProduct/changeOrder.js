import React, {memo, useEffect, useState} from "react";
import Dialog from "@material-ui/core/Dialog";
import {Tooltip} from "@material-ui/core";
import {toast} from "react-toastify";
import {connect, useDispatch} from "react-redux";
import ListItemText from '@material-ui/core/ListItemText';
import ListItem from '@material-ui/core/ListItem';
import List from '@material-ui/core/List';
import Slide from '@material-ui/core/Slide';
import Avatar from '@material-ui/core/Avatar';
import AccountTreeIcon from '@material-ui/icons/AccountTree';
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Chip from "@material-ui/core/Chip";
import Api from "../../../utils/api";

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

const ItemProductChangeOrder = memo((props) => {

    const { itemProductReducer, reload, entity, authReducer } = props;

    const [open, setOpen] = useState(false);

    const handleChangeOrder = (index) => {

        new Api().put('/products/itemProduct/' + entity.id + '/status', {field: 'order', 'value' : index}).then((res) => {
            if (typeof res != "undefined") {
                if (res.status) {
                    toast.success(res.msg);
                    reload();
                    setOpen(false);
                } else {
                    toast.error(res.msg);
                }
            }
        })
    }


    return (
        <>
            <Tooltip title='برای تغییر کلیک کنید.'>
                <Chip size={"small"} clickable={true} onClick={() => setOpen(true)} label={entity.order} />
            </Tooltip>
            <Dialog open={open} onClose={() => setOpen(false)} TransitionComponent={Transition}>
                <List>
                    {itemProductReducer.entities.data.length > 0 && itemProductReducer.entities.data.map((item, index) => (
                        <ListItem button  onClick={() => handleChangeOrder(index)}>
                            <ListItemText primary={index} />
                        </ListItem>
                    ))}
                </List>
            </Dialog>
        </>
    );

})


const mapStateToProps = state => ({
    itemProductReducer: state.itemProductReducer,
    authReducer: state.authReducer
});



export default connect(mapStateToProps)(ItemProductChangeOrder)