import React, {memo, useEffect, useState} from 'react';
import {Box, Paper} from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import Button from "@material-ui/core/Button";
import Api from "../../utils/api";
import {toast} from "react-toastify";
import {Loading} from './../../components'
import MenuItem from "@material-ui/core/MenuItem";
import Tooltip from "@material-ui/core/Tooltip";
import IconButton from "@material-ui/core/IconButton";
import Add from "@material-ui/icons/AddCircle";
import RemoveCircleIcon from "@material-ui/icons/RemoveCircle";
import validator from 'validator';
import CurrencyFormat from "react-currency-format";

const ProductPins = memo((props) => {

    const {productID, onComplete} = props;

    const [loading, setLoading] = useState(true);
    const [form, setForm] = useState({});
    const [product, setProduct] = useState(null);

    useEffect(() => {
        handleRequest();
    }, []);

    const handleRequest = () => {
        new Api().get('/products/' + productID + '/types').then((response) => {
            if (typeof response != "undefined") {
                setForm(response.list);
                setProduct(response.product);
            }
            setLoading(false);
        }).catch((error) => {
            toast.error(error);
        });
    }



    const handleSubmit = (event) => {
        event.preventDefault();

        setLoading(true);

        new Api().post('/products/' + productID + '/types', { pins: form }, false).then((response) => {
            if (typeof response != "undefined") {
                if (response.status) {
                    toast.success(response.msg);
                    handleRequest();
                    if (typeof onComplete !== undefined) {
                        onComplete();
                    }
                } else {
                    toast.error(response.msg);
                }

                setLoading(false);
            }
        })

    };



    const handleDuplicateRaw = (event, i, key) => {
        let n_form = form;
        if (['price', 'discount', 'weight', 'per_count', 'tax', 'point'].includes(event.target.name)) {

            if (! validator.isNumeric(event.target.value)) {
                return false;
            }
        }
        n_form[i][event.target.name] = event.target.value;
        setForm([...n_form]);
    };

    const duplicateRaw = (index) => {
        let n_form = form;
        let price_parameters = [];

        form[index]['price_parameters'] && form[index]['price_parameters'].map((res, i) => {
            price_parameters.push({
                id: res.id,
                title: res.title,
                selected: '',
                children: res.children
            });
        });

        n_form.push({
            price: form[index].price,
            discount: form[index].discount,
            tax: form[index].tax,
            weight: form[index].weight,
            per_count: form[index].per_count,
            point: form[index].point,
            order_point: form[index].order_point,
            title: form[index].title,
            count: 0,
            status: 1,
            id: '',
            price_parameters
        });
        setForm([...n_form]);
    }

    const deleteRaw = (index) => {
        let n_form = form;
        n_form.splice(index, 1)
        setForm([...n_form]);
    }


    const autoCompleteHandleSelect = (selected, i, key, index) =>
    {
        let n_form = form;

        if (selected) {

            n_form[i][key][index]['selected'] = selected;

            setForm([...n_form]);
        }

    };



    return(
        <form onSubmit={handleSubmit} className='animate__animated animate__fadeIn'>
            <Box mt={5}>
                <Paper style={{ padding: '25px'}}>
                    {form.length > 0 && form.map((pin, index) => {
                        return (
                            <fieldset style={{ margin: '20px 0'}}>
                                <legend>تنوع محصول<span>&nbsp;#{index + 1}&nbsp;</span></legend>
                                <h4 style={{ textAlign: 'center' }}>موجودی کل<span>&nbsp;<CurrencyFormat value={pin.count} displayType={'text'} thousandSeparator={true} />&nbsp;</span>{product && product.package_type && product.package_type.title}</h4>
                                <table className='table-duplicate-row'>
                                    <tr>
                                        <td><b>عنوان</b></td>
                                        <td><TextField variant={'outlined'} margin={"dense"} fullWidth={true} size={"small"} onChange={(event) => handleDuplicateRaw(event, index)} name='title'  value={pin.title} /></td>
                                    </tr>
                                    {pin.price_parameters && pin.price_parameters.map((parameter, i) => {
                                        return(
                                            <tr>
                                                <td style={{ color: '#ec2d6e' }} key={index}>{parameter.title}</td>
                                                <td key={i}>
                                                    <Autocomplete
                                                        size={"small"}
                                                        onChange={((event, value) => autoCompleteHandleSelect(value, index, 'price_parameters', i))}
                                                        options={parameter.children}
                                                        defaultValue={parameter.selected}
                                                        getOptionLabel={(option) => option.title}
                                                        renderInput={(params) =>
                                                            <TextField
                                                                variant={"outlined"}
                                                                {...params}
                                                                fullWidth
                                                                margin={"dense"}
                                                            />}
                                                    />
                                                </td>
                                            </tr>
                                        );
                                    })}
                                    <tr>
                                        <td><b>قیمت (تومان)</b></td>
                                        <td>
                                            <TextField variant={'outlined'} margin={"dense"} fullWidth={true} size={"small"} onChange={(event) => handleDuplicateRaw(event, index)} name='price'  value={pin.price} />
                                            <CurrencyFormat value={pin.price} displayType={'text'} thousandSeparator={true}  />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><b>تخفیف (درصد)</b></td>
                                        <td>
                                            <TextField variant={'outlined'} margin={"dense"} fullWidth={true} size={"small"} onChange={(event) => handleDuplicateRaw(event, index)} name='discount'  value={pin.discount} />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><b>مالیات (تومان)</b></td>
                                        <td>
                                            <TextField variant={'outlined'} margin={"dense"} fullWidth={true} size={"small"} onChange={(event) => handleDuplicateRaw(event, index)} name='tax' value={pin.tax} />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><b>تعداد<span>&nbsp;{product && product.package_type && product.package_type.title}&nbsp;</span>کسرکننده از موجودی </b></td>
                                        <td>
                                            <TextField variant={'outlined'} margin={"dense"} fullWidth={true} size={"small"} onChange={(event) => handleDuplicateRaw(event, index)} name='per_count' value={pin.per_count} />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><b>پستی (گرم)</b></td>
                                        <td>
                                            <TextField variant={'outlined'} margin={"dense"} fullWidth={true} size={"small"} onChange={(event) => handleDuplicateRaw(event, index)} name='weight'  value={pin.weight} />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><b>امتیاز (شبکه)</b></td>
                                        <td>
                                            <TextField variant={'outlined'} margin={"dense"} fullWidth={true} size={"small"} onChange={(event) => handleDuplicateRaw(event, index)} name='point'  value={pin.point} />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><b>نقطه سفارش</b></td>
                                        <td>
                                            <TextField variant={'outlined'} margin={"dense"} fullWidth={true} size={"small"} onChange={(event) => handleDuplicateRaw(event, index)} name='order_point'  value={pin.order_point} />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><b>وضعیت</b></td>
                                        <td>
                                            <TextField
                                                variant={'outlined'}
                                                margin={"dense"}
                                                fullWidth={true}
                                                size={"small"}
                                                select
                                                value={pin.status}
                                                name='status'
                                                onChange={(event) => handleDuplicateRaw(event, index)}
                                            >
                                                <MenuItem value={1}>فعال</MenuItem>
                                                <MenuItem value={0}>غیرفعال</MenuItem>
                                            </TextField>
                                        </td>
                                    </tr>
                                </table>
                                <div style={{ display: "flex", flexDirection: "row", justifyContent: 'flex-end'}}>
                                    <Tooltip title={'ایجاد'}>
                                        <IconButton color='primary' onClick={() => duplicateRaw(index)}>
                                            <Add />
                                        </IconButton>
                                    </Tooltip>
                                    {pin.id === "" && <Tooltip title={'ایجاد'}>
                                        <IconButton color='secondary' onClick={() => deleteRaw(index)}>
                                            <RemoveCircleIcon />
                                        </IconButton>
                                    </Tooltip>}
                                </div>
                            </fieldset>
                        )
                    })}
                </Paper>
            </Box>
            <Box mt={2} className='display-flex display-flex-direction-row display-flex-justify-content-flex-end'>
                <Button disabled={loading} variant='contained' color='primary' type="submit">تکمیل اطلاعات</Button>
            </Box>
            {loading && <Loading/>}
        </form>
    );
})

export default ProductPins;