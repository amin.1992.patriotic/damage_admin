import React, {useState, memo, useEffect, useCallback} from "react";
import ReactExport from "react-data-export";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import {useSelector} from "react-redux";
import moment from "moment-jalaali";
import {Loading} from "../index";
import Api from "../../utils/api";
import {Tooltip} from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import ImportExportIcon from "@material-ui/icons/ImportExport";
const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

const ProductPinsExcel = (props) => {

    const { product_id, object } = props;


    const [dataSet, setDataSet] = useState([]);
    const [loading, setLoading] = useState(false);
    const [open, setOpen] = useState(false);

    const fetchData = () => {
        setLoading(true);
        object.excel_export = 1;
        new Api().get('/products/' + product_id + '/pins', object).then((response) => {
            if (typeof response !== "undefined") {
                let n_p = [];
                response.pins.map((entity, index) => {
                    n_p[index] = [
                        {value: entity.id, style: { font: {sz: 13, bold: false}, alignment: {horizontal: "center", vertical: "center"}}},
                        {value: (entity.operator ? entity.operator.name + ' ' + entity.operator.family : '-'), style: { font: {sz: 13, bold: false}, alignment: {horizontal: "center", vertical: "center"}}},
                        {value: entity.order_id ? entity.order_id : '-', style: { font: {sz: 13, bold: false}, alignment: {horizontal: "center", vertical: "center"}}},
                        {value: parseInt(entity.dec_count), style: { font: {sz: 13, bold: false}, alignment: {horizontal: "center", vertical: "center"}}},
                        {value: parseInt(entity.inc_count), style: { font: {sz: 13, bold: false}, alignment: {horizontal: "center", vertical: "center"}}},
                        {value: parseInt(entity.total), style: { font: {sz: 13, bold: false}, alignment: {horizontal: "center", vertical: "center"}}},
                        {value: moment(entity.created_at).locale('fa').format('jYYYY/jMM/jDD HH:mm:ss'), style: { font: {sz: 13, bold: false}, alignment: {horizontal: "center", vertical: "center"}}},
                        {value: entity.document_number ? entity.document_number : '-', style: { font: {sz: 13, bold: false}, alignment: {horizontal: "center", vertical: "center"}}},
                        {value: entity.depot_date ? moment(entity.depot_date).locale('fa').format('jYYYY/jMM/jDD HH:mm:ss') : '-', style: { font: {sz: 13, bold: false}, alignment: {horizontal: "center", vertical: "center"}}},
                    ];
                });

                let final = {
                    columns: [
                        {title: "ردیف", width: {wpx: 50}, style: { border: {diagonal : { style: "medium", color: {rgb: "FFFFAA00"}}}, fill: {patternType: "solid", fgColor: {rgb: "FFCCEEFF"}}, font: {sz: 15, bold: true}, alignment: {horizontal: "center", vertical: "center"}}},
                        {title: "اپراتور", width: {wpx: 200}, style: { border: {diagonal : { style: "medium", color: {rgb: "FFFFAA00"}}}, fill: {patternType: "solid", fgColor: {rgb: "FFCCEEFF"}}, font: {sz: 15, bold: true}, alignment: {horizontal: "center", vertical: "center"}}},
                        {title: "شماره سفارش", width: {wpx: 150}, style: { border: {diagonal : { style: "medium", color: {rgb: "FFFFAA00"}}}, fill: {patternType: "solid", fgColor: {rgb: "FFCCEEFF"}}, font: {sz: 15, bold: true}, alignment: {horizontal: "center", vertical: "center"}}},
                        {title: "تعداد کسر شده", width: {wpx: 150}, style: { border: {diagonal : { style: "medium", color: {rgb: "FFFFAA00"}}}, fill: {patternType: "solid", fgColor: {rgb: "FFCCEEFF"}}, font: {sz: 15, bold: true}, alignment: {horizontal: "center", vertical: "center"}}},
                        {title: "تعداد اضافه شده", width: {wpx: 150}, style: { border: {diagonal : { style: "medium", color: {rgb: "FFFFAA00"}}}, fill: {patternType: "solid", fgColor: {rgb: "FFCCEEFF"}}, font: {sz: 15, bold: true}, alignment: {horizontal: "center", vertical: "center"}}},
                        {title: "جمع", width: {wpx: 150}, style: { border: {diagonal : { style: "medium", color: {rgb: "FFFFAA00"}}}, fill: {patternType: "solid", fgColor: {rgb: "FFCCEEFF"}}, font: {sz: 15, bold: true}, alignment: {horizontal: "center", vertical: "center"}}},
                        {title: "تاریخ ثبت", width: {wpx: 150}, style: { border: {diagonal : { style: "medium", color: {rgb: "FFFFAA00"}}}, fill: {patternType: "solid", fgColor: {rgb: "FFCCEEFF"}}, font: {sz: 15, bold: true}, alignment: {horizontal: "center", vertical: "center"}}},
                        {title: "شماره سند", width: {wpx: 150}, style: { border: {diagonal : { style: "medium", color: {rgb: "FFFFAA00"}}}, fill: {patternType: "solid", fgColor: {rgb: "FFCCEEFF"}}, font: {sz: 15, bold: true}, alignment: {horizontal: "center", vertical: "center"}}},
                        {title: "تاریخ کارتکس", width: {wpx: 150}, style: { border: {diagonal : { style: "medium", color: {rgb: "FFFFAA00"}}}, fill: {patternType: "solid", fgColor: {rgb: "FFCCEEFF"}}, font: {sz: 15, bold: true}, alignment: {horizontal: "center", vertical: "center"}}},
                    ],
                    data: n_p
                }

                setDataSet([{...final}]);

                setOpen(true);
                setLoading(false);
            }
        })
    };


    return (
        <React.Fragment>
            <Tooltip title="خروجی اکسل" >
                <IconButton onClick={() => fetchData()}>
                    <ImportExportIcon />
                </IconButton>
            </Tooltip>
            {dataSet[0] && dataSet[0].data && dataSet[0].data.length > 0 && <ExcelFile filename={'product_' + product_id + '_' + moment().unix()} hideElement={false} element={
                <Dialog
                    fullWidth={true}
                    open={open}
                    keepMounted
                    onClose={() => {
                        setOpen(false);
                        setDataSet([]);
                    }}
                    aria-labelledby="alert-dialog-slide-title"
                    aria-describedby="alert-dialog-slide-description"
                >
                    <DialogTitle id="alert-dialog-slide-title">جلوگیری از ارسال درخواست  توسط ربات</DialogTitle>
                    <DialogContent>
                        {loading && <Loading />}
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => {
                            setOpen(false);
                            setDataSet([]);
                        }} color="primary">
                            دانلود
                        </Button>
                    </DialogActions>
                </Dialog>
            }>
                <ExcelSheet dataSet={dataSet} name={'product_' + product_id + '_' + moment().unix()} />
            </ExcelFile>}
            {loading && <Loading/>}
        </React.Fragment>
    );

}

export default memo(ProductPinsExcel);

