import React, {memo, useEffect, useState} from 'react';
import {Box, Paper} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import {packageTypeAction, treeAction} from "../../actions";
import {connect, useDispatch} from "react-redux";
import Autocomplete from "@material-ui/lab/Autocomplete";
import Button from "@material-ui/core/Button";
import Api from "../../utils/api";
import {toast} from "react-toastify";
import {BrandAutoComplete} from "../index";
import {priceParameters} from "../../actions/tree";
import {Loading} from './../../components'
import Chip from "@material-ui/core/Chip";
import {FETCH_NODES} from "../../types";

const ProductBasicInfo = memo((props) => {


    const dispatch = useDispatch();
    const {packageTypeReducer, fetchPackageType, onComplete, productID, fetchPriceParameter, treeReducer} = props;

    const [form, setForm] = useState({});
    const [loading, setLoading] = useState(false);
    const [priceParameter, setPriceParameter] = useState([]);

    useEffect(() => {

        dispatch({type: FETCH_NODES, payload: []});

        fetchPriceParameter();
        fetchPackageType();

        if (productID) {
            setLoading(true);
            new Api().get('/products/' + productID + '/init').then((response) => {
                if (typeof response !== "undefined") {

                    let init_price_parameters = [];
                    if (response.price_parameters) {
                        response.price_parameters.map((t) => {
                            init_price_parameters.push(t.id);
                        });
                    }

                    setPriceParameter(init_price_parameters);

                    setForm(response);
                }
                setLoading(false);
            })
        }
    }, []);

    const handleChangeElement = (event) => {

        let frm = form;
        frm[event.target.name] = event.target.value;
        setForm({...frm});
    };

    const handleChangeElementWithAutoComplete = (name, value) => {
        let frm = form;
        frm[name] = value;
        setForm({...frm})
    }

    const handleSubmit = (event) => {
        event.preventDefault();

        setLoading(true);

        let url = '/products/init';

        if (productID) {
            url = '/products/' + productID + '/init';

            let n_form = {...form};
            n_form['price_parameters'] = priceParameter;

            new Api().put(url, n_form).then((response) => {
                if (typeof response !== "undefined") {
                    if (response.status) {
                        setForm(response.model);
                        if (typeof onComplete !== undefined) {
                            onComplete(response.model.id);
                        }

                    } else {
                        toast.error(response.msg)
                    }

                    setLoading(false);
                }
            })
        } else {

            let n_form = {...form};
            n_form['price_parameters'] = priceParameter;

            new Api().post(url, n_form).then((response) => {
                if (typeof response !== "undefined") {
                    if (response.status) {
                        setForm(response.model);
                        if (typeof onComplete !== undefined) {
                            onComplete(response.model.id);
                        }

                    } else {
                        toast.error(response.msg)
                    }

                    setLoading(false);
                }
            })
        }



    };

    return(
        <form onSubmit={handleSubmit} className='animate__animated animate__fadeIn'>
            <Box mt={5}>
                <Paper style={{ padding: '25px'}}>
                    <Grid container spacing={2}>
                        <Grid item xs={12} >
                            <TextField
                                label="عنوان"
                                variant="outlined"
                                margin='dense'
                                value={form.title}
                                fullWidth
                                name='title'
                                onChange={handleChangeElement}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        </Grid>
                        <Grid item xs={12} sm={10}>
                            <TextField
                                label="هدینگ (h2)"
                                variant="outlined"
                                margin='dense'
                                value={form.heading}
                                fullWidth
                                name='heading'
                                onChange={handleChangeElement}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        </Grid>
                        <Grid item xs={12} sm={2} >
                            <TextField
                                label="کد محصول"
                                variant="outlined"
                                margin='dense'
                                value={form.code}
                                fullWidth
                                name='code'
                                onChange={handleChangeElement}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <BrandAutoComplete values={form.brand} onChange={(value) => handleChangeElementWithAutoComplete('brand_id', value ? value.id : '')}/>
                            {form.brand && <Chip label={form.brand.title} />}
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <Autocomplete
                                onChange={((event, value) => handleChangeElementWithAutoComplete('package_type_id', (value ? value.id : '') ))}
                                options={packageTypeReducer.entities.data}
                                getOptionLabel={(option) => option.title}
                                renderInput={(params) =>
                                    <TextField
                                        {...params}
                                        fullWidth
                                        name='package_type_id'
                                        margin={"dense"}
                                        label="واحد اندازه گیری"
                                        variant="outlined"
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                    />
                                }
                            />
                            {form.package_type && <Chip label={form.package_type.title} />}
                        </Grid>
                        <Grid item xs={12} sm={12}>
                            <Autocomplete
                                multiple={true}
                                freeSolo={true}
                                onChange={((event, value) => {
                                    let priceParameters = priceParameter;
                                    value.forEach((value) => {
                                        priceParameters.push(value.id);
                                    });
                                    setPriceParameter(priceParameters);
                                })}
                                options={treeReducer.nodes}
                                getOptionLabel={(option) => option.title}
                                renderInput={(params) =>
                                    <TextField
                                        {...params}
                                        fullWidth
                                        name='price_parameters'
                                        margin={"dense"}
                                        label="پارامترهای قیمتی"
                                        variant="outlined"
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                    />
                                }
                            />
                            {form.price_parameters && form.price_parameters.map((item , index) => {
                                return(
                                    <Chip
                                        label={item.title}
                                        clickable
                                        color="default"
                                        onDelete={() => {
                                            // remove from form
                                            let price_parameters = form['price_parameters'];
                                            price_parameters.splice(index, 1);
                                            form['price_parameters'] = price_parameters;
                                            // remove from tag
                                            price_parameters = priceParameter;
                                            price_parameters.splice(index, 1);
                                            // init tag in form and tag const
                                            setForm({...form});
                                            setPriceParameter(price_parameters);
                                        }}
                                    />
                                )
                            })}
                        </Grid>
                    </Grid>
                </Paper>
            </Box>
            <Box mt={2} className='display-flex display-flex-direction-row display-flex-justify-content-flex-end'>
                <Button disabled={loading} variant='contained' color='primary' type="submit">تکمیل اطلاعات</Button>
            </Box>
            {loading && <Loading/>}
        </form>
    );
})

const mapStateToProps = state => ({
    packageTypeReducer : state.packageTypeReducer,
    treeReducer: state.treeReducer
})

const mapDispatchToProps = (dispatch) => ({
    fetchPriceParameter: () => {
        dispatch(treeAction.priceParameters())
    },
    fetchPackageType: () => {
        dispatch(packageTypeAction.fetchPackageType())
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductBasicInfo);