import React, {memo, useEffect, useState} from 'react';
import {Box, Paper} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import {packageTypeAction, treeAction} from "../../actions";
import {connect, useDispatch} from "react-redux";
import Autocomplete from "@material-ui/lab/Autocomplete";
import Button from "@material-ui/core/Button";
import Api from "../../utils/api";
import {toast} from "react-toastify";
import Tree from "../Tree";
import {TOGGLE_CHECKED, TOGGLE_EXPANDED} from "../../types";
import Tooltip from "@material-ui/core/Tooltip";
import IconButton from "@material-ui/core/IconButton";
import Add from "@material-ui/icons/AddCircle";
import RemoveCircleIcon from "@material-ui/icons/RemoveCircle";
import MenuItem from "@material-ui/core/MenuItem";
import {Loading} from './../../components'
import VatanEditor from "../VatanEditor/VatanEditor";

const ProductEditAttribute = memo((props) => {

    const { productID ,onComplete } = props;

    const dispatch = useDispatch();
    const [loading, setLoading] = useState(true);
    const [form, setForm] = useState([]);

    useEffect(() => {
        handleRequest();
    }, []);

    const handleRequest = () => {
        new Api().get('/products/' + productID + '/attributes').then((response) => {
            if (typeof response !== "undefined") {
                setForm(response);
            }

            setLoading(false);
        })
    };

    const handleSubmit = (event) => {

        event.preventDefault();

        setLoading(true);

        new Api().post('/products/' + productID + '/attributes', {attributes: form}, false).then((response) => {
            if (typeof response !== "undefined") {
                if (response.status) {
                    handleRequest();
                    toast.success(response.msg);
                    if (typeof onComplete !== undefined) {
                        onComplete();
                    }
                } else {
                    toast.error(response.msg);
                }
            }
            setLoading(false);
        })
    }


    // change state
    const handleDuplicateRaw = (event, i,name) => {
        let n_form = form;
        n_form[i][name] = event;
        setForm([...n_form]);
    };


    const handleChangeElementWithAutoCompleteMultiple = (name, value, index) => {
        let frm = form;
        frm[index][name] = JSON.stringify(value);
        setForm([...frm])
    }




    return(
        <form onSubmit={handleSubmit} className='animate__animated animate__fadeIn'>
            <Box mt={5}>
                <Paper style={{ padding: '25px'}}>
                    <div style={{ overflowX: 'auto'}}>
                        <table className='table-duplicate-row fadeIn'>
                            <thead>
                            <tr>
                                <th style={{ width: '50px'}}>ردیف</th>
                                <th style={{ width: '100px'}}>عنوان</th>
                                <th>مقدار</th>
                            </tr>
                            </thead>
                            <tbody>
                            {form.length > 0 && form.map((item, index) => {
                                return(<tr key={index}>
                                    <td>{index + 1 }</td>
                                    <td>{item.title}</td>
                                    <td>
                                        {!item.attr_values &&
                                            <VatanEditor
                                            value={item.text_value}
                                            onChange={(event) => handleDuplicateRaw(event, index , 'text_value')}
                                            rows={5}
                                            />
                                        }
                                        {item.attr_values &&
                                        <Autocomplete
                                            multiple={true}
                                            freeSolo={true}
                                            onChange={((event, value) => handleChangeElementWithAutoCompleteMultiple('attr_selected', value, index) )}
                                            defaultValue={JSON.parse(item.attr_selected) ? JSON.parse(item.attr_selected) : []}
                                            options={JSON.parse(item.attr_values) ? JSON.parse(item.attr_values) : []}
                                            getOptionLabel={(option) => option.title}
                                            renderInput={(params) =>
                                                <TextField
                                                    {...params}
                                                    fullWidth
                                                    name='price_parameters'
                                                    margin={"dense"}
                                                    variant="outlined"
                                                    InputLabelProps={{
                                                        shrink: true,
                                                    }}
                                                />
                                            }
                                        />}
                                    </td>
                                </tr>);
                            })}
                            </tbody>
                        </table>
                    </div>
                </Paper>
            </Box>
            <Box mt={2} className='display-flex display-flex-direction-row display-flex-justify-content-flex-end'>
                <Button disabled={loading} variant='contained' color='primary' type="submit">تکمیل اطلاعات</Button>
            </Box>
            {loading && <Loading />}
        </form>
    );
})

const mapStateToProps = state => ({
    treeReducer : state.treeReducer
});

const mapDispatchToProps = (dispatch) => ({
    fetchProductCategories: () => {
        dispatch(treeAction.productCategories())
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductEditAttribute);
