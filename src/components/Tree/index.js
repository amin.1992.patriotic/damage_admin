import React, {memo, useEffect, useState} from "react";
import AddCircleOutlineOutlinedIcon from '@material-ui/icons/AddCircleOutlineOutlined';
import RemoveCircleOutlineOutlinedIcon from '@material-ui/icons/RemoveCircleOutlineOutlined';
import './style.css'
import Typography from "@material-ui/core/Typography";
import {treeAction} from "../../actions";
import {connect, useDispatch} from "react-redux";
import {TOGGLE_CHECKED, TOGGLE_EXPANDED} from "../../types";


const Tree = ({tree}) => {

    const dispatch = useDispatch();

    const handleExpand = (id) => {

        let value = tree.expanded;

        let array_index = -1;
        array_index = value.indexOf(id);
        if (array_index > -1) {
            value.splice(array_index, 1);
        } else {
            value.push(id)
        }

        dispatch({type: TOGGLE_EXPANDED, payload: value});

    }

    const handleChecked = (id) => {

        let value = tree.checked;

        let array_index = -1;
        array_index = value.indexOf(id);
        if (array_index > -1) {
            value.splice(array_index, 1);
        } else {
            value.push(id)
        }

        dispatch({type: TOGGLE_CHECKED, payload: value});

    }

    const renderTree = (nodes) => {
        // nodes mapping
        const treeNodes = nodes.map((item, key) => {
            // id  node
            const { id } = item;

            // label title node
            const { title } = item;

            const { status } = item;

            // checked has child
            const hasChild = item.children && item.children.length > 0;

            // checked has child is true fetch children
            const children = hasChild ? renderTree(item.children) : '';

            return (
                <li key={key} className={"tree-box " + (status ? 'active' : 'disable')}>
                    <span className={ hasChild === true ? 'tree-parent has-child' : 'tree-parent has-no-child'}>
                        <span className={'expandBtn'} onClick={() => handleExpand(id)}>
                            {hasChild === true && (tree.expanded.includes(id) ? (
                                <RemoveCircleOutlineOutlinedIcon
                                    color="action"
                                    fontSize="small"
                                />
                            ) : (
                                <AddCircleOutlineOutlinedIcon
                                    color="action"
                                    fontSize="small"
                                />
                            ))}
                        </span>
                        <input checked={tree.checked.includes(id)} type='checkbox' onChange={() => handleChecked(id)}/>
                         <Typography>{title}</Typography>
                    </span>
                    {hasChild === true && (
                        <ul
                            className="tree-children"
                            style={{ display: tree.expanded.includes(id) ? 'block' : 'none'}}
                        >
                            {children}
                        </ul>
                    )}
                </li>
            );
        });

        return treeNodes;
    }

    return (
        <div className='tree'>
            {tree.nodes.length > 0 ? renderTree(tree.nodes) : ''}
        </div>
    );

}

const mapStateToProps = state => ({
    tree: state.treeReducer
});


export default connect(mapStateToProps)(Tree)