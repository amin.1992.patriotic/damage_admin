import React, {memo, useEffect, useState} from "react";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import Button from '@material-ui/core/Button';
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import Grid from "@material-ui/core/Grid";
const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});


const DialogFileInfo = memo((props) => {


    const { files, index, onClose } = props;


    const [open, setOpen] = useState(true);
    const [file, setFile] = useState(files[index]);

    const handleChangeLink = (event) =>
    {
        let n_file = file;
        n_file[event.target.name] = event.target.value;
        setFile({...n_file});
    }



    useEffect(() => {
        if (open === false) {
            onClose(file);
        }
    }, [open]);

    return(
        <Dialog
            fullWidth={open}
            open={open}
            TransitionComponent={Transition}
            keepMounted
            aria-labelledby="alert-dialog-slide-title"
            aria-describedby="alert-dialog-slide-description"
        >
            <DialogTitle id="alert-dialog-slide-title">تغییر لینک ,اولویت نمایش و توضیحات</DialogTitle>
            <DialogContent>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <TextField
                            fullWidth={true}
                            variant={"filled"}
                            label={'توضیحات'}
                            name={'caption'}
                            onChange={handleChangeLink}
                            value={file.caption}
                            helperText='این فیلد به عنوان (alt,caption) در نظر گرفته میشود.'
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            style={{ textAlign: 'left', direction: 'ltr'}}
                            fullWidth={true}
                            variant={"filled"}
                            label={'لینک'}
                            name={'link'}
                            onChange={handleChangeLink}
                            value={file.link}
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            select={true}
                            fullWidth={true}
                            variant={"filled"}
                            label={'اولویت'}
                            name={'order'}
                            onChange={handleChangeLink}
                            value={parseInt(file.order)}
                            InputLabelProps={{
                                shrink: true,
                            }}
                        >
                            {files.map((file, index) => {
                                return(
                                    <MenuItem key={index} value={parseInt(index + 1)}>{parseInt(index + 1)}</MenuItem>
                                );
                            })}
                        </TextField>
                    </Grid>
                </Grid>
            </DialogContent>
            <DialogActions>
                <Button onClick={() => setOpen(false)} color="primary">
                    تایید
                </Button>
            </DialogActions>
        </Dialog>
    );
})



export default DialogFileInfo