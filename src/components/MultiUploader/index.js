import React, {memo, useEffect, useState} from "react";
import {toast} from "react-toastify";
import Fab from '@material-ui/core/Fab';
import CheckIcon from '@material-ui/icons/Check';
import {CircularProgress} from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import LinkedCameraIcon from '@material-ui/icons/LinkedCamera';
import Tooltip from "@material-ui/core/Tooltip";
import './style.css'
import Api from "../../utils/api";
import {ENV} from "../../env";
import axios from 'axios'
import DialogFileInfo from "./dialogFileInfo";
import ImageCropper from "../ImageCropper";

const MultipleUploader = memo((props) => {

    const { onComplete, url, token, water_mark, values, onLoading } = props;

    const [files, setFiles] = useState([]);
    const [current, setCurrent] = useState(null);
    const [openInfoDialog, setOpenInfoDialog] = useState(false);


    useEffect(() => {
        setFiles(values);
    }, [values]);


    useEffect(() => {
        onComplete(files);
    }, [files]);




    const handleSelectFile = async (event) => {

        /* Fetch All File In State */
        let n_files = files;

        let counter = files.length;

        for (let i = 0; i < event.target.files.length; i++) {

            let file_type = '';

            /* File extension that accepted */
            const validImageTypes = ['image/gif', 'image/jpeg', 'image/png', 'image/jpg'];

            /* Video format accepted */
            const validVideoTypes = ['video/mp4', 'video/ogv', 'video/webm', 'video/3gpp'];

            if (validImageTypes.includes(event.target.files[i].type)) {
                file_type = 'image';
                if ((event.target.files[i].size / 1024) > 1000) {
                    toast.error('حداکثر حجم عکس 1 مگابایت است.');
                    return;
                }

            } else if (validVideoTypes.includes(event.target.files[i].type)) {
                file_type = 'video';
                if ((event.target.files[i].size) > 8388608) {
                    toast.error('حداکثر حجم ویدئو 8 مگابایت است.');
                    return;
                }
            } else {
                toast.error('فرمت فایل نامعتبر است.');
                return false;
            }

            /* Init State */
            n_files[i + counter] = {
                'percent': 0,
                'path': '',
                'file': '',
                'collection': true,
                'directory': 'attachment',
                'order': i + counter + 1,
                'link': '',
                'caption': '',
                'mime_type': file_type
            };


            /* Preview Image Or Video File with Absolute Address */
            const reader = new FileReader();
            reader.readAsDataURL(event.target.files[i]);
            reader.onload = () => {
                const form_data = new FormData();
                /* Append New File */
                form_data.append('file', reader.result);
                form_data.append('directory', 'attachment');
                form_data.append('water_mark', water_mark ? 1 : 0);
                form_data.append('base64', false);
                UploadFileToServer(form_data, i + counter);

                n_files[i + counter]['path'] = reader.result;
                setFiles([...n_files]);
            };

        };
    }

    const UploadFileToServer = async(form_data, index) => {

        onLoading(true);
        let n_files = files;
        /* Send Post Request To Server */
        await axios.post(ENV.API[window.location.host] + url, form_data, {
            onUploadProgress: (e) => {
                const done = e.loaded;
                const total = e.total;
                /* Calculate Percent  */
                n_files[index]['percent'] = (Math.floor(done / total * 1000) / 10);
                setFiles([...n_files]);
            },
            headers: {
                'Accept': 'application/json',
                'content-type': 'multipart/form-data',
                'Authorization': 'Bearer ' + token
            },
        }).then((response) => {
            /* Get Real Address From Server */
            if (typeof response != "undefined") {
                if (response.status) {
                    n_files[index]['path'] = response.data.path;
                    n_files[index]['file'] = response.data.file;
                } else {
                    delete n_files[index];
                }
                setFiles([...n_files]);
                onLoading(false);

            }
        }).catch((error) => {

            toast.error(error.message);
            delete  n_files[index];
            setFiles([...n_files]);
        })
    }

    const handleSetCollection = (index) => {
        let n_files = files;
        n_files[index].collection = (Boolean(n_files[index].collection) === true) ? false :  true;

        setFiles([...n_files]);

        if (!n_files[index].collection) {
            toast.success('به مجموعه عکس های اصلی محصول اضافه شد.');
        }
    }

    const handleUnlinkFile = (index) => {

        console.log(files[index])
        let n_files = files;
        new Api().delete('/fileManager/attachment',{'file': files[index].file, 'directory': files[index].directory}, false).then((response) => {
            if (typeof response != "undefined") {
                if (response.status) {
                    delete n_files[index];
                    setFiles([...n_files]);
                    toast.success('فایل با موفقیت حذف گردید');
                } else {
                    toast.error('خطایی رخ داده است.');
                }
            }
        });
    }


    return(
        <div className='gallery'>
            <label className="gallery-box" htmlFor="upload">
                {files && files.length > 0 && files.map((file, index) => {
                    if (file !== undefined) {
                        return (
                            <div key={index} className='img-box' style={{ height: '200px', width: '200px' }}>
                                {file.percent === 100 &&
                                <div>
                                    <Tooltip title='حذف'>
                                        <IconButton onClick={() => handleUnlinkFile(index)} color={"default"} className='remove-btn animated flash'>
                                            <HighlightOffIcon />
                                        </IconButton>
                                    </Tooltip>
                                    <Tooltip title='لینک و اولویت'>
                                        <IconButton onClick={() => {
                                            setCurrent(index);
                                            setOpenInfoDialog(true);
                                        }} style={{ color: (  (file.link || file.caption) ? "#04f080" : "#828282")}} className='link-btn animated flash'>
                                            <LinkedCameraIcon />
                                        </IconButton>
                                    </Tooltip>
                                </div>}
                                <div className="img-box-mask" style={{ height: 200 - (file.percent * 2) + 'px', width: '200px'}}/>
                                <div className="img-box-progress">
                                    <Fab onClick={() => handleSetCollection(index)}  aria-label="save" color={Boolean(file.collection) === true ? 'default': 'primary'}>{ file.percent < 100 ? file.percent : <CheckIcon className='animated flash' />}</Fab>
                                    {file.percent < 100 && <CircularProgress color={"secondary"} className='circle-progress' size={68} />}
                                </div>
                                {file.mime_type === 'image' && <img className={file.percent === 100 ? 'animated flipInX' : 'animated fadeIn'}  width={200} height={200} key={index} src={ file.path !== '' && file.path}/>}
                                {file.mime_type === 'video' && <video className={file.percent === 100 ? 'animated flipInY' : 'animated fadeIn'}  controls  width={200} height={200} src={ file.path !== '' && file.path}/>}
                            </div>
                        );
                    }
                })}
                {files && files.length === 0 && <span>انتخاب فایل</span>}
            </label>
            <input accept={'video/mp4, video/ogv, video/webm, video/3gpp, image/gif, image/jpeg, image/png, image/jpg'}  className='input-file' id='upload' type="file" onChange={handleSelectFile} multiple={true}/>
            <p>حجم عکس نباید بالاتر از 1 مگابایت باشد</p>
            <p>در صورت انتخاب چند عکس به عنوان عکس اصلی نحوه نمایش به صورت رندوم خواهد بود.</p>
            <p>video/mp4, video/ogv, video/webm, video/3gpp, image/gif, image/jpeg, image/png, image/jpg</p>
            <p><span>پس از اتمام آپلود فایل ها به کلیک به روی علامت </span><span style={{ position: 'relative', top: 7.5, marginLeft: 5}}><CheckIcon className='animated flash' /></span><span>عکس شاخص را انتخاب کنید</span></p>

            {openInfoDialog && <DialogFileInfo
                files={files}
                index={current}
                onClose={(file) => {
                    let n_files = files;
                    n_files[current] = file;
                    setFiles([...n_files]);
                    setOpenInfoDialog(false);
                }}
            />}

        </div>
    )
});

export default MultipleUploader