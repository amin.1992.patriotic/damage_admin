import React from 'react';
import SpeedDial from '@material-ui/lab/SpeedDial';
import SpeedDialIcon from '@material-ui/lab/SpeedDialIcon';
import SpeedDialAction from '@material-ui/lab/SpeedDialAction';
import SettingsIcon from '@material-ui/icons/Settings';
import './style.css'
import {useDispatch} from "react-redux";
import {OPEN_SETTING_DIALOG, OPEN_SETTING_LINKS_DIALOG} from "../../types";
import SettingsEthernetIcon from '@material-ui/icons/SettingsEthernet';
import TuneIcon from '@material-ui/icons/Tune';

const actions = [
    { icon: <TuneIcon />, name: 'تنظیمات', 'op' : 'StickySetting' },
    { icon: <SettingsEthernetIcon />, name: 'لینک ها', 'op' : 'DomainLinks' },
];

export default function SpeedDials() {

    const dispatch = useDispatch();

    const [open, setOpen] = React.useState(false);


    const handleClick = (op) => {
        switch (op) {
            case 'StickySetting':
                dispatch({type:OPEN_SETTING_DIALOG, payload: true});
                break;
            case 'DomainLinks':
                dispatch({type:OPEN_SETTING_LINKS_DIALOG, payload: true});
                break;
            default:
                break;

        }

    }

    const handleClose = () => {
        setOpen(false);
    };

    const handleOpen = () => {
        setOpen(true);
    };

    return (
        <SpeedDial
            FabProps={{ color:'secondary'}}
            className='speed-dial'
            ariaLabel="SpeedDial example"
            icon={<SpeedDialIcon color={"secondary"} />}
            onClose={handleClose}
            onOpen={handleOpen}
            open={open}
            direction={"up"}
        >
            {actions.map((action) => (
                <SpeedDialAction
                    key={action.name}
                    icon={action.icon}
                    tooltipTitle={action.name}
                    onClick={() => handleClick(action.op)}
                />
            ))}
        </SpeedDial>
    );
}