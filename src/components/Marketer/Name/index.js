import React, {memo} from "react";
import {Link} from "react-router-dom";
import {ENV} from "../../../env";
import Avatar from "@material-ui/core/Avatar";
import {Tooltip} from "@material-ui/core";

const MarketerLink = memo((props) => {

    const { user } = props;

    return(
        <>
            {user &&
            <Tooltip title={user && (user.name + ' ' + user.family)}>
                <Link style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }} to={'/marketer/' + user.id}>
                    <Avatar  alt={user.name} src={ENV["STORAGE"] + '/' + user.pic} />&nbsp;
                    <div style={{  minWidth: '45%' }}>{user && (user.name + ' ' + user.family).length > 15 ? (user.name + ' ' + user.family).substring(0, 15) + '...' : (user.name + ' ' + user.family)}<br/>{user && user.marketer && user.marketer.marketer_code}</div>
                </Link>
            </Tooltip>
            }
        </>
    );
});

export default MarketerLink;
