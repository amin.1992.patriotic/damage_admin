import React, {useEffect, useState} from 'react';
import "./guilty.css"
import plate from "../../assets/img/plate.png";
import motorPlate from "../../assets/img/motor.png";
import CONST from "../../const";
import DatePicker from "react-datepicker2";
import moment from "moment-jalaali";
import Api from "../../utils/api";
import {useHistory, useParams} from "react-router-dom";
import {validateNationalCode} from "../../utils/validation";
import {toast} from "react-toastify";
const Guilty = (props) => {
    const history = useHistory();
    const  param = useParams();
    const {company} = props
    const  detail = props.detail.supplementary[0]
    const separatorGuilty = detail && detail.plate_guilty && detail.plate_guilty.split("-")
    const separatorInjered = detail && detail.plate_injered && detail.plate_injered.split("-")

    useEffect(() => {
        getPlate()
    } , [])



    const [chassisNumberGuilty, setchassisNumberGuilty] = useState(detail && detail.chassis_number_guilty )
    const [motorNumberGuilty, setMotorNumberGuilty] = useState(detail && detail.motor_number_guilty )
    const [nameGuilty, setNameGuilty] = useState(detail && detail.name_guilty )
    const [certificateGuilty, setCertificateGuilty] = useState(detail && detail.certificate_guilty )
    const [baseNumberGuilty, setBaseNumberGuilty] = useState(detail && detail.base_number_guilty )
    const [issueDateGuilty, setIssueDateGuilty] = useState(detail && detail.issue_date_guilty && moment(detail.issue_date_guilty, 'jYYYY/jM/jD') || moment())
    const [startDateGuilty, setStartDateGuilty] = useState(detail && detail.start_date_guilty && moment(detail.start_date_guilty, 'jYYYY/jM/jD') || moment())
    const [expirationDateGuilty, setExpirationDateGuilty] = useState(detail && detail.expirationDate_guilty && moment(detail.expirationDate_guilty, 'jYYYY/jM/jD') || moment())
    const [restrictionsGuilty, setRestrictionsGuilty] = useState(detail && detail.restrictions_guilty )
    const [nationalCodeGuilty, setNationalCodeGuilty] = useState(detail && detail.nationalCode_guilty )
    const [insuranceNumberGuilty, setInsuranceNumberGuilty] = useState(detail && detail.insurance_number_guilty )
    const [companyGuilty, setCompanyGuilty] = useState(detail && detail.company_guilty )
    const [financialGuilty, setFinancialGuilty] = useState(detail && detail.financial_guilty )
    const [commitmentGuilty, setCommitmentGuilty] = useState(detail && detail.commitment_guilty )
    const [ownerCarGuilty, setOwnerCarGuilty] = useState(detail && detail.owner_car_guilty )
    const [uniqueCodeGuilty, setUniqueCodeGuilty] = useState(detail && detail.unique_code_guilty !== null ? detail.unique_code_guilty : detail && detail.guilty_insurance_unique_code && detail.guilty_insurance_unique_code )
    const [mobileNumberGuilty, setMobileNumberGuilty] = useState(detail && detail.mobile_number_guilty !== null ? detail.mobile_number_guilty : detail && detail.guilty_mobile )
    const [phoneGuilty, setPhoneGuilty] = useState(detail && detail.phone_guilty )
    const [disciplinaryGuilty, setDisciplinaryGuilty] = useState(detail && detail.disciplinary_guilty )
    const [automaticGuilty, setAutomaticGuilty] = useState(detail && detail.automatic_guilty )
    const [addressGuilty, setAddressGuilty] = useState(detail && detail.address_guilty )
    const [carTypeGuilty, setCarTypeGuilty] = useState(detail ? detail.car_type_guilty : 1 )
    const [carSystemGuilty, setCarSystemGuilty] = useState(detail && detail.car_system_guilty )
    const [licenseNumberGuilty, setLicenseNumberGuilty] = useState(detail && detail.license_number_guilty )
    const [colorGuilty, setColorGuilty] = useState(detail && detail.color_guilty  )
    const [modelGuilty, setModelGuilty] = useState(detail && detail.model_guilty )
    const [issuedGuilty, setIssuedGuilty] = useState(detail && detail.issued_guilty )
    const [ownerNationalCodeGuilty, setOwnerNationalCodeGuilty] = useState(detail && detail.owner_national_code_guilty )
    const [fatherNameGuilty, setFatherNameGuilty] = useState(detail && detail.father_name_guilty )
    const [birthdayGuilty, setBirthdayGuilty] = useState(detail && detail.birthday_guilty && moment(detail.birthday_guilty, 'jYYYY/jM/jD') || moment())
    const [plateGuilty, setPlateGuilty] = useState({})

    const alphabet = CONST.ALPHABET
    const carTypeList = CONST.CAR_TYPE


    useEffect(() => {
        handleSetData()
    } ,[birthdayGuilty , startDateGuilty , issueDateGuilty , expirationDateGuilty])

    const getPlate = () => {
        let plate = detail && detail.plate_guilty && detail.plate_guilty.split("-")
        let alphabet = plate && plate.filter((item) => {
            return isNaN(item) && item !== "null"
        })
        let number = plate && plate.filter((item) => {
            return !isNaN(item) && item !== "null"
        })
        console.log(detail)
        setPlateGuilty({
            left: props.detail ? number && number[1] : "",
            right: props.detail ? number && number[2] : "",
            top: props.detail ? number && number[0] : "",
            left2: props.detail ? number && number[0] : "",
            center: props.detail && alphabet && alphabet[0]  ? alphabet && alphabet[0] : "الف",
            down: props.detail ? number && number[1] : "",
        })
    }


    const handleChangeElement = (event) => {
        let frm = {...plateGuilty};
        frm[event.target.name] = event.target.value;
        setPlateGuilty(frm);
    };
    const handleFocusElement = (event) => {
        let frm = {...plateGuilty};
        frm[event.target.name] = "";
        setPlateGuilty(frm);
    };
    const validation = () => {
        if (!validateNationalCode(nationalCodeGuilty && nationalCodeGuilty.toString()) && detail.insurance_type && detail.insurance_type.id === 1 && nationalCodeGuilty !== null) {
            toast.error("کد ملی مقصر صحیح نمی باشد")
            return false
        }
        if (!validateNationalCode(ownerNationalCodeGuilty && ownerNationalCodeGuilty.toString()) && detail.insurance_type && detail.insurance_type.id === 1 && ownerNationalCodeGuilty !== null) {
            toast.error("کد ملی مالک خودروی مقصر صحیح نمی باشد")
            return false
        }
        if (carSystemGuilty === "" ) {
            toast.error("تیپ و سیستم خودرو را وارد کنید .")
            return false
        }
        if (plateGuilty === "" ) {
            toast.error("شماره انتظامی را وارد کنید .")
            return false
        }
        if (colorGuilty === "" ) {
            toast.error("رنگ را وارد کنید .")
            return false
        }
        if (modelGuilty === null ) {
            toast.error("مدل را وارد کنید .")
            return false
        }
        return true
    }
    const handleSetData = () => {

        const obj = {
            damage_id:param.id,
            chassis_number_guilty: chassisNumberGuilty,
            motor_number_guilty: motorNumberGuilty,
            name_guilty: nameGuilty,
            certificate_guilty: certificateGuilty,
            base_number_guilty: baseNumberGuilty,
            issue_date_guilty: moment(issueDateGuilty).format('jYYYY/jM/jD'),
            restrictions_guilty: restrictionsGuilty,
            nationalCode_guilty: nationalCodeGuilty,
            insurance_number_guilty: insuranceNumberGuilty,
            start_date_guilty:moment(startDateGuilty).format('jYYYY/jM/jD') ,
            expirationDate_guilty:moment(expirationDateGuilty).format('jYYYY/jM/jD') ,
            company_guilty: companyGuilty,
            financial_guilty: financialGuilty,
            commitment_guilty: commitmentGuilty,
            owner_car_guilty: ownerCarGuilty,
            unique_code_guilty: uniqueCodeGuilty,
            mobile_number_guilty: mobileNumberGuilty,
            phone_guilty: phoneGuilty,
            disciplinary_guilty: disciplinaryGuilty,
            automatic_guilty: automaticGuilty,
            address_guilty: addressGuilty,
            car_type_guilty: carTypeGuilty,
            car_system_guilty: carSystemGuilty,
            license_number_guilty: licenseNumberGuilty,
            model_guilty:modelGuilty,
            issued_guilty:issuedGuilty,
            color_guilty:colorGuilty,
            owner_national_code_guilty:ownerNationalCodeGuilty,
            father_name_guilty:fatherNameGuilty,
            birthday_guilty:moment(birthdayGuilty).format('jYYYY/jM/jD'),
            plate_guilty: carTypeGuilty !== "2" ? `${(plateGuilty.left2) && (plateGuilty.left2).trim()}-${(plateGuilty.left) && (plateGuilty.left).trim()}-${(plateGuilty.right) && (plateGuilty.right).trim()}-${(plateGuilty.center) && (plateGuilty.center).trim()}` : `${(plateGuilty.top) && (plateGuilty.top).trim()}-${(plateGuilty.down) && (plateGuilty.down).trim()}`,
        }
        props.getData(obj)

    }
    return (
        <div  onKeyDown={() => {props.alarm(true)}}  onBlur={handleSetData}  >
            <div className="insurance-table">
                {
                    !props.editCondition && <div className={"disable-cover"}/>
                }
                <div className="table-head">
                    {`مشخصات مقصر حادثه ${nameGuilty && nameGuilty.length > 0 ? " / " + nameGuilty : ""}`}

                </div>
                <div className="table-body">
                    <div className="table-body-item">
                        نوع خودرو :
                        <select value={carTypeGuilty} onChange={(e) => {
                            props.alarm(true)
                            setCarTypeGuilty(e.target.value)
                        }} className="table-body-item-select">
                            {
                                carTypeList && carTypeList.map((item) => {
                                    return (
                                        <option value={item.id}>{item.title}</option>
                                    )
                                })
                            }
                        </select>
                    </div>
                    <div className="table-body-item">
                        <div>
                            تیپ و سیستم خودرو :
                        </div>
                        <input value={carSystemGuilty} onChange={(e) => {
                            setCarSystemGuilty(e.target.value)
                        }} className="table-body-item-input"/>
                    </div>
                    <div style={{justifyContent: "right"}} className="table-body-item">
                        اتوماتیک :
                        <input name={"automatic"} defaultChecked={automaticGuilty === "1"}
                               value={automaticGuilty} onChange={(e) => {
                            setAutomaticGuilty(e.target.checked ? 1 : 2)
                        }} type={"checkBox"} className="table-body-item-checkBox"/>

                    </div>
                    <div className="table-body-item" style={{justifyContent: "unset"}}>
                        شماره انتظامی :
                        {
                            parseInt(carTypeGuilty) !== 2 ?
                                <>
                                    <img className="table-body-item-img"
                                         src={plate}/>
                                    <input onFocus={(e) => {
                                        handleFocusElement(e)
                                    }} value={plateGuilty.right} onChange={(e) => {
                                        handleChangeElement(e)
                                    }} name={"right"} maxLength={2}
                                           className="table-body-item-right-number"/>
                                    <div className="table-body-item-left-number">
                                        <input onFocus={(e) => {
                                            handleFocusElement(e)
                                        }} value={plateGuilty.left} onChange={(e) => {
                                            handleChangeElement(e)
                                        }} name={"left"} maxLength={3}/>
                                        <select onFocus={(e) => {
                                            handleFocusElement(e)
                                        }} value={plateGuilty.center} onChange={(e) => {
                                            props.alarm(true)
                                            handleChangeElement(e)
                                        }} name={"center"}>
                                            <option value={-1}>{plateGuilty.center? plateGuilty.center: `انتخاب`}</option>
                                            {
                                                alphabet && alphabet.map((item) => {
                                                    return (
                                                        <option value={item.title}>{item.title}</option>
                                                    )
                                                })
                                            }
                                        </select>
                                        <input onFocus={(e) => {
                                            handleFocusElement(e)
                                        }} value={plateGuilty.left2} onChange={(e) => {
                                            handleChangeElement(e)
                                        }} name={"left2"} maxLength={2}/>
                                    </div>
                                </>
                                :
                                <>
                                    <img className="table-body-item-img-motor" src={motorPlate}/>
                                    <div className="table-body-item-motor-number">
                                        <input onFocus={(e) => {
                                            handleFocusElement(e)
                                        }} value={plateGuilty.top} onChange={(e) => {
                                            handleChangeElement(e)
                                        }} name={"top"} maxLength={3}/>
                                        <input onFocus={(e) => {
                                            handleFocusElement(e)
                                        }} value={plateGuilty.down} onChange={(e) => {
                                            handleChangeElement(e)
                                        }} name={"down"} maxLength={5}/>
                                    </div>
                                </>
                        }

                    </div>
                    <div className="table-body-item">
                        <div>
                            رنگ :
                        </div>
                        <select value={colorGuilty} onChange={(e) => {
                            props.alarm(true)
                            setColorGuilty(e.target.value)
                        }} className="table-body-item-select">
                            <option value={"-1"}>{"انتخاب کنید"}</option>
                            {
                                CONST.COLOR && CONST.COLOR.map((item) => {
                                    return (
                                        <option value={item.id}>{item.title}</option>
                                    )
                                })
                            }
                        </select>
                    </div>
                    <div className="table-body-item">
                        مدل :
                        <input value={modelGuilty} type={"number"} onChange={(e) => {setModelGuilty(e.target.value)}}  className="table-body-item-input"/>
                    </div>
                    <div className="table-body-item">
                        <div>
                            شماره شاسی :
                        </div>
                        <input value={chassisNumberGuilty} onChange={(e) => {
                            setchassisNumberGuilty(e.target.value)
                        }} className="table-body-item-input"/>
                    </div>
                    <div className="table-body-item">
                        شماره موتور :
                        <input value={motorNumberGuilty} onChange={(e) => {
                            setMotorNumberGuilty(e.target.value)
                        }} className="table-body-item-input"/>
                    </div>
                    <div className="table-body-item">
                        نام راننده :
                        <input value={nameGuilty} onChange={(e) => {
                            setNameGuilty(e.target.value)
                        }} className="table-body-item-input"/>
                    </div>
                    <div className="table-body-item">
                        کد ملی راننده:
                        <input type={"number"} value={nationalCodeGuilty} onChange={(e) => {
                            setNationalCodeGuilty(e.target.value)
                        }} className="table-body-item-input"/>
                    </div>
                    <div className="table-body-item">
                        نام پدر :
                        <input value={fatherNameGuilty} onChange={(e) => {
                            setFatherNameGuilty(e.target.value)
                        }} className="table-body-item-input"/>
                    </div>
                    <div className="table-body-item">
                        شماره شناسنامه :
                        <input type={"number"} value={certificateGuilty} onChange={(e) => {
                            setCertificateGuilty(e.target.value)
                        }} className="table-body-item-input"/>

                    </div>
                    <div className="table-body-item">
                        شماره گواهینامه :
                        <input type={"number"} value={licenseNumberGuilty} onChange={(e) => {
                            setLicenseNumberGuilty(e.target.value)
                        }} className="table-body-item-input"/>

                    </div>
                    <div className="table-body-item">
                        شماره پایه گواهینامه :
                        <select value={baseNumberGuilty} className={"table-body-item-select"}
                                onChange={(e) => {
                                    props.alarm(true)
                                    setBaseNumberGuilty(e.target.value)
                                }}>
                            <option value={"-1"}>{"انتخاب کنید"}</option>
                            {CONST.BASE_NUMBER && CONST.BASE_NUMBER.map((item) => {
                                return (
                                    <option value={item.id}>{item.title}</option>
                                );
                            })}
                        </select>
                    </div>
                    <div className='datepicker-input-container'>
                        <div className={"table-body-item"}>
                            <div>
                                تاریخ صدور :
                            </div>
                            <DatePicker
                                isGregorian={false}
                                timePicker={false}
                                value={issueDateGuilty}
                                onChange={issueDateGuilty => setIssueDateGuilty(issueDateGuilty)}
                            />
                        </div>
                    </div>
                    <div className="table-body-item">
                        محدودیت رانندگی :
                        <input value={restrictionsGuilty} onChange={(e) => {
                            setRestrictionsGuilty(e.target.value)
                        }} className="table-body-item-input"/>

                    </div>


                    <div className="table-body-item">
                        شماره بیمه نامه:
                        <input value={insuranceNumberGuilty} onChange={(e) => {
                            setInsuranceNumberGuilty(e.target.value)
                        }} className="table-body-item-input"/>

                    </div>
                    <div className="table-body-item">
                        کد یکتا :
                        <input type={"number"} value={uniqueCodeGuilty}
                               onChange={(e) => setUniqueCodeGuilty(e.target.value)}
                               className="table-body-item-input"/>

                    </div>
                    <div className='datepicker-input-container'>
                        {
                            <div className={"table-body-item"}>
                                <div>
                                    تاریخ شروع :
                                </div>
                                <DatePicker
                                    isGregorian={false}
                                    timePicker={false}
                                    value={startDateGuilty}
                                    onChange={startDateGuilty => setStartDateGuilty(startDateGuilty)}
                                />
                            </div>

                        }
                    </div>
                    <div className='datepicker-input-container'>
                        <div className={"table-body-item"}>
                            <div>
                                تاریخ انقضا:
                            </div>
                            <DatePicker
                                isGregorian={false}
                                timePicker={false}
                                value={expirationDateGuilty}
                                onChange={expirationDateGuilty => setExpirationDateGuilty(expirationDateGuilty)}
                            />
                        </div>
                    </div>
                    <div className="table-body-item">
                        صادره از شرکت بیمه:
                        <select value={companyGuilty} className={"table-body-item-select"}
                                onChange={(e) => {
                                    props.alarm(true)
                                    setCompanyGuilty(e.target.value)
                                }}>
                            <option value={"-1"}>{"انتخاب کنید"}</option>
                            {company && company.map((item) => {
                                return (
                                    <option value={item.id}>{item.title}</option>
                                );
                            })}
                        </select>
                    </div>
                    <div className="table-body-item">
                        سقف تعهدات مالی :
                        <input type={"number"} value={financialGuilty} onChange={(e) => {
                            setFinancialGuilty(e.target.value)
                        }} className="table-body-item-input"/>

                    </div>
                    <div className="table-body-item">
                        سقف تعهدات جانی :
                        <input type={"number"} value={commitmentGuilty} onChange={(e) => {
                            setCommitmentGuilty(e.target.value)
                        }} className="table-body-item-input"/>
                    </div>
                    <div className="table-body-item">
                        نام مالک خودرو :
                        <input value={ownerCarGuilty} onChange={(e) => {
                            setOwnerCarGuilty(e.target.value)
                        }} className="table-body-item-input"/>

                    </div>
                    <div   className="table-body-item">
                        کد ملی مالک:
                        <input  type={"number"}  value={ownerNationalCodeGuilty}  onChange={(e) => {
                            setOwnerNationalCodeGuilty(e.target.value)
                        }} className="table-body-item-input"/>

                    </div>
                    <div className='datepicker-input-container'>
                        <div className={"table-body-item"}>
                            <div>
                                تاریخ تولد مالک:
                            </div>

                            <DatePicker
                                isGregorian={false}
                                timePicker={false}
                                value={birthdayGuilty}
                                onChange={birthdayGuilty => setBirthdayGuilty(birthdayGuilty)}
                            />
                        </div>
                    </div>
                    <div className="table-body-item">
                        صادره از :
                        <input  value={issuedGuilty}
                                onChange={(e) => setIssuedGuilty(e.target.value)}
                                className="table-body-item-input"/>

                    </div>
                    <div className="table-body-item">
                        تلفن همراه :
                        <input type={"number"} value={mobileNumberGuilty}
                               onChange={(e) => setMobileNumberGuilty(e.target.value)}
                               className="table-body-item-input"/>

                    </div>
                    <div className="table-body-item">
                        تلفن ثابت :
                        <input type={"number"} value={phoneGuilty} onChange={(e) => {
                            setPhoneGuilty(e.target.value)
                        }} className="table-body-item-input"/>

                    </div>


                    <div style={{width: '100%', justifyContent: "right"}} className="table-body-item">
                        آدرس :
                        <input value={addressGuilty} onChange={(e) => {
                            setAddressGuilty(e.target.value)
                        }} className="table-body-item-input"/>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Guilty;
