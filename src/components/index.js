import Master from "./Layouts/master";
import Header from './Header'
import Sidebar from './Sidebar'
import Head from "./Head";
import Tree from './Tree';
import Loading from './Loading'


// move able component
import Captcha from "./Captcha";
import SpeedDial from "./SpeedDial";
import TextEditor from './Editor'
import ImageCropper from "./ImageCropper";
import MultipleUploader from "./MultiUploader";
import ModelUploader from "./ModelUploader";

/**
 * page components
 */


// user bundle
import UserCreate from "./User/create";
import UserEdit from "./User/edit";
import UserPassword from "./User/password";
import RoleCreate from "./User/Role/create";
import UserAutocomplete from './User/AutoComplete/index';
import Permissions from './User/Role/permission'
// auth
import Logout from "./Auth/logout";


// blog
import BlogCategoryCreate from './Blog/Category/create';
import BlogCategoryEdit from './Blog/Category/edit';
import BlogCategoryMove from './Blog/Category/move';
import MenuCreate from './Blog/Menu/create';
import MenuEdit from './Blog/Menu/edit';
import MenuMove from './Blog/Menu/move';
import BlogBasicInfo from "./Blog/basic";
import BlogEditCategory from "./Blog/EditCategory";
import BlogEditSeo from "./Blog/EditSeo";

// ticket
import TicketCategoryCreate from './Ticket/Category/create';
import TicketCategoryEdit from './Ticket/Category/edit';
import TicketCategoryMove from './Ticket/Category/move';
import TicketConversation from './Ticket/conversation'
import TicketCreate from './Ticket/create'
import TicketChangeCategory from './Ticket/changeCategory'
import TicketChangeStatus from './Ticket/changeStatus'

// setting
import StickySetting from "./Setting/sticky";
import DomainLinks from "./Setting/links";



import TagAutocomplete from './Tag/AutoComplete';

//product
import PackageTypeCreate from "./Product/PackageType/create";
import PackageTypeEdit from "./Product/PackageType/edit";
import AttributeCreate from "./Product/Attribute/create";
import AttributeAutoComplete from "./Product/Attribute/AutoComplete";
import BrandCreate from "./Product/Brand/create";
import BrandAutoComplete from "./Product/Brand/AutoComplete";
import PriceParameterCreate from './Product/PriceParameter/create';
import PriceParameterEdit from './Product/PriceParameter/edit';
import PriceParameterMove from './Product/PriceParameter/move';
import ProductCategoryCreate from './Product/Category/create';
import ProductCategoryMove from './Product/Category/move';
import ProductAutoComplete from "./Product/AutoComplete";
import ItemProductChangeOrder from "./Product/ItemProduct/changeOrder";
import ProductFilterCreate from './Product/Filter/create';
import ProductFilterMove from './Product/Filter/move';
import ProductBasicInfo from "./Product/basic";
import ProductEditCategory from "./Product/EditCategory";
import ProductEditFilter from "./Product/EditFilter";
import ProductEditSeo from "./Product/EditSeo";
import ProductEditAttribute from "./Product/EditAttribute";
import ProductPins from "./Product/EditPins";


//reports
import MapReport from "./Report/MapReport";

export {
    Master,
    Header,
    Sidebar,
    Head,
    Tree,
    Loading,
    // user bundle
    UserCreate,
    UserEdit,
    RoleCreate,
    UserPassword,
    Logout,
    UserAutocomplete,
    Permissions,
    //blog
    BlogCategoryCreate,
    BlogCategoryEdit,
    BlogCategoryMove,
    MenuCreate,
    MenuEdit,
    MenuMove,
    BlogEditSeo,
    BlogBasicInfo,
    BlogEditCategory,
    // ticket
    TicketCategoryCreate,
    TicketCategoryEdit,
    TicketCategoryMove,
    TicketConversation,
    TicketCreate,
    TicketChangeCategory,
    TicketChangeStatus,
    // setting
    StickySetting,
    DomainLinks,
    //product
    ProductEditFilter,
    ProductEditAttribute,
    ProductBasicInfo,
    ProductEditCategory,
    ProductEditSeo,
    PackageTypeCreate,
    PackageTypeEdit,
    AttributeCreate,
    AttributeAutoComplete,
    BrandCreate,
    BrandAutoComplete,
    PriceParameterCreate,
    PriceParameterEdit,
    PriceParameterMove,
    ProductCategoryCreate,
    ProductCategoryMove,
    ProductAutoComplete,
    ItemProductChangeOrder,
    ProductFilterCreate,
    ProductFilterMove,
    ProductPins,
    // report
    MapReport,
    // move able
    ModelUploader,
    Captcha,
    SpeedDial,
    TextEditor,
    TagAutocomplete,
    ImageCropper,
    MultipleUploader








}