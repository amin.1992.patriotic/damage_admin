import React, {useEffect, useRef, useState} from 'react';
import "./select.css"

const Select = (props) => {
    console.log(props.items)
    const [showItems, setShowItems] = useState(false)
    const [selectedItem, setSelectedItem] = useState(props.option && props.option[0])
    const [items, setItems] = useState(props.items || [])
    const [search, setSearch] = useState("")
    const inputRef  = useRef(null)

    const dropDown = () => {
        setShowItems(!showItems)
    }
    useEffect(() => {
        selectItem(selectedItem)
    } ,[])
    const selectItem = (item) => {
        props.onChange({name:props.name , value:item && item.id})
        setSelectedItem(item)
        setShowItems(false)
    }
    const handleSearch = (e) => {
        setSearch(e.target.value)
    }
    useEffect(() => {
        inputRef.current.focus()
    },[showItems])

    return (
        <div onMouseLeave={()=> setShowItems(false)} className={"select"}>
            <label>
                {props.label}
            </label>
            <div  className="select-box--box">
                <div onClick={dropDown} className="select-box--container">
                    <div className="select-box--selected-item">
                        {selectedItem && selectedItem.value && selectedItem.value || props.placeholder}
                    </div>
                    <div className="select-box--arrow" >
                        <span
                            className={`${
                                showItems
                                    ? "select-box--arrow-up"
                                    : "select-box--arrow-down"
                            }`}
                        />
                    </div>
                    <div
                        style={{display: showItems ? "block" : "none"}}
                        className={"select-box--items"}
                    >
                        <div className={"search-box"}>
                            <input ref={inputRef} onChange={(e) => {handleSearch(e)}} value={search}/>
                        </div>
                        {items && items.map(item => (
                            ( search.length === 0 ||  item.value.toLowerCase().includes(search) )&&
                            <div
                                key={item.id}
                                onClick={() => selectItem(item)}
                                className={selectedItem === item ? "selected" : ""}
                            >
                                {item.value}
                            </div>
                        ))}
                    </div>
                </div>
            </div>
            {
                props.des &&
                <strong>{props.des}</strong>
            }
        </div>
    );
};

export default Select;
