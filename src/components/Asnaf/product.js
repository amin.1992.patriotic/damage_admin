import React, {memo, useEffect, useState} from "react";
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import ControlPointIcon from '@material-ui/icons/ControlPoint';
import {Box, Paper, Tooltip} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import {BrandAutoComplete, Loading, ProductAutoComplete} from "../index";
import Chip from "@material-ui/core/Chip";
import Autocomplete from "@material-ui/lab/Autocomplete";
import IconButton from "@material-ui/core/IconButton";
import SyncIcon from "@material-ui/icons/Sync";
import Divider from "@material-ui/core/Divider";
import Api from "../../utils/api";
import {toast} from "react-toastify";

const AsnafProductDialogForm = (props) => {

    const { onComplete } = props;

    const [open, setOpen] = React.useState(false);
    const [loading, setLoading] = useState();
    const [form, setForm] = useState({});
    const init_files = {
        "brandOwnerdoc" : [],
        "businessLicense" : [],
        "healthLicensing" : [],
        "costEstimation" : [],
        "commitment" : [],
        "catalogue" : [],
        "factor" : [],
        "otherDocument" : [],
    };
    const [files, setFiles] = useState(init_files);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleSubmit = (event) => {
        event.preventDefault();

        setLoading(true);
        form.files = files;
        new Api().post('/asnaf/product', form, false).then((response) => {
            if (typeof response !== "undefined") {
                if (response.status) {
                    toast.success(response.msg);
                } else {
                    toast.error(response.msg);
                }
            }

            onComplete();
            setOpen(false);
            setFiles(init_files);
            setForm({});
            setLoading(false);
        })

    }

    const handleChangeInputWithAutoComplete = (name, value) => {
        let n_form = form;
        n_form.product_id = value.id;
        setForm({...n_form});
    }


    const onFileUpload = (event) => {
        event.preventDefault();

        setLoading(true);

        let name = event.target.name;

        let n_files = files;
        for (let i = 0; i < event.target.files.length; i++) {
            n_files[name][i] = {
                'uploaded_file': '',
                'file_name' : event.target.files[i].name
            }
            let file_reader = new FileReader();
            file_reader.readAsDataURL(event.target.files[i]);
            file_reader.onload = () => {
                n_files[name][i]['uploaded_file'] = file_reader.result;
                setFiles({...n_files});
            };

            setLoading(false);
        }

    };


    return(
        <div>
            <Tooltip title="افزودن" onClick={handleClickOpen}>
                <IconButton>
                    <ControlPointIcon/>
                </IconButton>
            </Tooltip>
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
                fullWidth={true}
            >
                <form onSubmit={handleSubmit} className='animate__animated animate__fadeIn' encType="multipart/form-data">
                    <DialogTitle id="alert-dialog-title">ارسال اطلاعات محصول به وزارت خانه</DialogTitle>
                    <DialogContent>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <ProductAutoComplete multiple={false} onChange={(value) => handleChangeInputWithAutoComplete('product_id', value)} />
                            </Grid>
                            <Divider />
                            <Grid item xs={12}>
                                <div style={{ display: "flex", flexDirection: 'column', margin: '5px 0' }}>
                                    <label style={{ margin: '10px 0', fontWeight: 'bold' }}>مدارک مالکیت نام تجاری:</label>
                                    <input onChange={onFileUpload} name={'brandOwnerdoc'} multiple={true} type={'file'}/>
                                </div>
                            </Grid>
                            <Divider />
                            <Grid item xs={12}>
                                <div style={{ display: "flex", flexDirection: 'column', margin: '5px 0' }}>
                                    <label style={{ margin: '10px 0', fontWeight: 'bold' }}>پروانه بهره برداری:</label>
                                    <input onChange={onFileUpload} name={'businessLicense'} multiple={true} type={'file'}/>
                                </div>
                            </Grid>
                            <Divider />
                            <Grid item xs={12}>
                                <div style={{ display: "flex", flexDirection: 'column', margin: '5px 0' }}>
                                    <label style={{ margin: '10px 0', fontWeight: 'bold' }}>مجوزهای بهداشتی و استاندارد:</label>
                                    <input onChange={onFileUpload} name={'healthLicensing'} multiple={true} type={'file'}/>
                                </div>
                            </Grid>
                            <Divider />
                            <Grid item xs={12}>
                                <div style={{ display: "flex", flexDirection: 'column', margin: '5px 0' }}>
                                    <label style={{ margin: '10px 0', fontWeight: 'bold' }}>برآورد هزینه:</label>
                                    <input onChange={onFileUpload} name={'costEstimation'} multiple={true} type={'file'}/>
                                </div>
                            </Grid>
                            <Divider />
                            <Grid item xs={12}>
                                <div style={{ display: "flex", flexDirection: 'column', margin: '5px 0' }}>
                                    <label style={{ margin: '10px 0', fontWeight: 'bold' }}>تعهد نامه ضوابط قیمت گذاری:</label>
                                    <input onChange={onFileUpload} name={'commitment'} multiple={true} type={'file'}/>
                                </div>
                            </Grid>
                            <Divider />
                            <Grid item xs={12}>
                                <div style={{ display: "flex", flexDirection: 'column', margin: '5px 0' }}>
                                    <label style={{ margin: '10px 0', fontWeight: 'bold' }}>کاتالوگ محصولات مصرفی:</label>
                                    <input onChange={onFileUpload} name={'catalogue'} multiple={true} type={'file'}/>
                                </div>
                            </Grid>
                            <Divider />
                            <Grid item xs={12}>
                                <div style={{ display: "flex", flexDirection: 'column', margin: '5px 0' }}>
                                    <label style={{ margin: '10px 0', fontWeight: 'bold' }}>حداقل دو پیش فاکتور واحد های تولیدی مختلف:</label>
                                    <input onChange={onFileUpload} name={'factor'} multiple={true} type={'file'}/>
                                </div>
                            </Grid>
                            <Divider />
                            <Grid item xs={12}>
                                <div style={{ display: "flex", flexDirection: 'column', margin: '5px 0' }}>
                                    <label style={{ margin: '10px 0', fontWeight: 'bold' }}>سایر مدارک:</label>
                                    <input onChange={onFileUpload} name={'otherDocument'} multiple={true} type={'file'}/>
                                </div>
                            </Grid>
                            <Divider />
                        </Grid>
                        {loading && <Loading/>}
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleClose} color="primary">
                            بستن
                        </Button>
                        <Button disabled={loading} type={'submit'} color="primary" autoFocus>
                            ارسال اطلاعات
                        </Button>
                    </DialogActions>
                </form>
            </Dialog>
        </div>
    );

}


export default memo(AsnafProductDialogForm);