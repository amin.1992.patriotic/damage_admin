import React, {memo, useEffect, useState} from "react";
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import ControlPointIcon from '@material-ui/icons/ControlPoint';
import {Box, Paper, Tooltip} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import {BrandAutoComplete, Loading, ProductAutoComplete} from "../index";
import Chip from "@material-ui/core/Chip";
import Autocomplete from "@material-ui/lab/Autocomplete";
import IconButton from "@material-ui/core/IconButton";
import SyncIcon from "@material-ui/icons/Sync";
import Divider from "@material-ui/core/Divider";
import Api from "../../utils/api";
import {toast} from "react-toastify";
import CurrencyFormat from "react-currency-format";
import MenuItem from "@material-ui/core/MenuItem";

const AsnafBranchDialogForm = (props) => {

    const { onComplete } = props;

    const [open, setOpen] = React.useState(false);
    const [loading, setLoading] = useState();
    const [form, setForm] = useState({});
    const [cities, setCities] = useState([]);
    const init_files = {
        "nationalCardCopy" : [],
        "identificationCopy" : [],
        "obligationForm" : [],
        "establishForm" : [],
        "rentalContract" : [],
        "officialNewspaper" : [],
    }
    const [files, setFiles] = useState(init_files);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleSubmit = (event) => {
        event.preventDefault();

        setLoading(true);
        form.files = files;
        new Api().post('/asnaf/branch', form, false).then((response) => {
            if (typeof response !== "undefined") {
                if (response.status) {
                    toast.success(response.msg);
                } else {
                    toast.error(response.msg);
                }
            }

            onComplete();
            setOpen(false);
            setFiles(init_files);
            setForm({});
            setLoading(false);
        })

    }

    useEffect(() => {
        if (open) {
            new Api().get('/asnaf/cities').then((response) => {
                if (typeof response !== "undefined") {
                    setCities(response);
                }
            })
        }
    }, [open]);


    const onFileUpload = (event) => {
        event.preventDefault();

        setLoading(true);

        let name = event.target.name;

        let n_files = files;
        for (let i = 0; i < event.target.files.length; i++) {
            n_files[name][i] = {
                'uploaded_file': '',
                'file_name' : event.target.files[i].name
            }
            let file_reader = new FileReader();
            file_reader.readAsDataURL(event.target.files[i]);
            file_reader.onload = () => {
                n_files[name][i]['uploaded_file'] = file_reader.result;
                setFiles({...n_files});
            };

            setLoading(false);
        }

    };

    const handleChangeElement = (event) => {
        let frm = form;
        frm[event.target.name] = event.target.value;
        setForm({...frm});
    };


    const handleChangeElementWithAutoComplete = (name, value) => {
        let frm = form;
        frm[name] = value;
        setForm({...frm})
    }


    return(
        <div>
            <Tooltip title="افزودن" onClick={handleClickOpen}>
                <IconButton>
                    <ControlPointIcon/>
                </IconButton>
            </Tooltip>
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
                fullWidth={true}
            >
                <form onSubmit={handleSubmit} className='animate__animated animate__fadeIn' encType="multipart/form-data">
                    <DialogTitle id="alert-dialog-title">ارسال اطلاعات شعب به وزارت خانه</DialogTitle>
                    <DialogContent>
                        <Grid container spacing={2}>
                            <Grid item xs={12} sm={12}>
                                <Autocomplete
                                    autoComplete={"off"}
                                    onChange={((event, value) => handleChangeElementWithAutoComplete('city', (value ? value.id : '') ))}
                                    options={cities}
                                    getOptionLabel={(cities) => cities.title}
                                    renderInput={(params) =>
                                        <TextField
                                            {...params}
                                            autoComplete={"off"}
                                            fullWidth
                                            name='city'
                                            margin={"dense"}
                                            label="شهر"
                                            variant="outlined"
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                        />
                                    }
                                />
                            </Grid>
                            <Grid item xs={12} >
                                <TextField
                                    label="کد شعبه"
                                    variant="outlined"
                                    margin='dense'
                                    fullWidth
                                    name='branch_code'
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    onChange={handleChangeElement}
                                />
                            </Grid>
                            <Grid item xs={12} >
                                <TextField
                                    label="نام و نام خانوادگی مسئول"
                                    variant="outlined"
                                    margin='dense'
                                    fullWidth
                                    name='manager_name'
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    onChange={handleChangeElement}
                                />
                            </Grid>
                            <Grid item xs={12} >
                                <TextField
                                    label="تلفن همراه"
                                    variant="outlined"
                                    margin='dense'
                                    fullWidth
                                    name='manager_mobile'
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    onChange={handleChangeElement}
                                />
                            </Grid>
                            <Grid item xs={12} >
                                <TextField
                                    label="کد ملی"
                                    variant="outlined"
                                    margin='dense'
                                    fullWidth
                                    name='manager_nationalcode'
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    onChange={handleChangeElement}
                                />
                            </Grid>
                            <Grid item xs={12} >
                                <TextField
                                    label="کد پستی"
                                    variant="outlined"
                                    margin='dense'
                                    fullWidth
                                    name='postal_code'
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    onChange={handleChangeElement}
                                />
                            </Grid>
                            <Grid item xs={12} >
                                <TextField
                                    label="آدرس"
                                    variant="outlined"
                                    margin='dense'
                                    fullWidth
                                    name='address'
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    onChange={handleChangeElement}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <div style={{ display: "flex", flexDirection: 'column', margin: '5px 0' }}>
                                    <label style={{ margin: '10px 0', fontWeight: 'bold' }}>.کپی کارت ملی :</label>
                                    <input onChange={onFileUpload} name={'nationalCardCopy'} multiple={true} type={'file'}/>
                                </div>
                            </Grid>
                            <Divider />
                            <Grid item xs={12}>
                                <div style={{ display: "flex", flexDirection: 'column', margin: '5px 0' }}>
                                    <label style={{ margin: '10px 0', fontWeight: 'bold' }}>کپی شناسنامه:</label>
                                    <input onChange={onFileUpload} name={'identificationCopy'} multiple={true} type={'file'}/>
                                </div>
                            </Grid>
                            <Divider />
                            <Grid item xs={12}>
                                <div style={{ display: "flex", flexDirection: 'column', margin: '5px 0' }}>
                                    <label style={{ margin: '10px 0', fontWeight: 'bold' }}>فرم تعهد مسئول شعبه :</label>
                                    <input onChange={onFileUpload} name={'obligationForm'} multiple={true} type={'file'}/>
                                </div>
                            </Grid>
                            <Divider />
                            <Grid item xs={12}>
                                <div style={{ display: "flex", flexDirection: 'column', margin: '5px 0' }}>
                                    <label style={{ margin: '10px 0', fontWeight: 'bold' }}>فرم درخواست تاسيس شعبه:</label>
                                    <input onChange={onFileUpload} name={'establishForm'} multiple={true} type={'file'}/>
                                </div>
                            </Grid>
                            <Divider />
                            <Grid item xs={12}>
                                <div style={{ display: "flex", flexDirection: 'column', margin: '5px 0' }}>
                                    <label style={{ margin: '10px 0', fontWeight: 'bold' }}>قرارداد اجاره نامه:</label>
                                    <input onChange={onFileUpload} name={'rentalContract'} multiple={true} type={'file'}/>
                                </div>
                            </Grid>
                            <Divider />
                            <Grid item xs={12}>
                                <div style={{ display: "flex", flexDirection: 'column', margin: '5px 0' }}>
                                    <label style={{ margin: '10px 0', fontWeight: 'bold' }}>روزنامه رسمی ثبت:</label>
                                    <input onChange={onFileUpload} name={'officialNewspaper'} multiple={true} type={'file'}/>
                                </div>
                            </Grid>
                            <Divider />
                        </Grid>
                        {loading && <Loading/>}
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleClose} color="primary">
                            بستن
                        </Button>
                        <Button disabled={loading} type={'submit'} color="primary" autoFocus>
                            ارسال اطلاعات
                        </Button>
                    </DialogActions>
                </form>
            </Dialog>
        </div>
    );

}


export default memo(AsnafBranchDialogForm);