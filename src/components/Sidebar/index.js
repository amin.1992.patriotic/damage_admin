import React, {useState} from "react";
import './style.css';
import Avatar from "@material-ui/core/Avatar";
import ListItem from "@material-ui/core/ListItem";
import {Link} from "react-router-dom";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";
import {connect} from "react-redux";
import {CircularProgress, Typography} from "@material-ui/core";
import plusIcon from './../../assets/img/15.png'
import minusIcon from './../../assets/img/17.png'
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Api from "../../utils/api";
import {toast} from "react-toastify";

const Sidebar = (props) => {
    const {authReducer, onClose} = props;
    const [expanded, setExpanded] = useState([]);

    const handleExpand = (id) => {

        let n_expanded = expanded;

        let array_index = -1;


        array_index = n_expanded.indexOf(id);
        if (array_index > -1) {
            n_expanded.splice(array_index, 1);
        } else {
            n_expanded.push(id);
        }

        setExpanded([...n_expanded]);
    }

    const uploadFile = (event) => {
        const file = event.target.files
        const form_data = new FormData();
        form_data.append('file', file[0]);
        form_data.append('category_id', 1);
        form_data.append('folder ', "blog");
        setTimeout(()=>{
            new Api().post("/fileManager/panel/upload_files", form_data , false)
                .then((response) => {
                    if (typeof response != "undefined") {
                        if (response.status) {
                            toast.success(response.msg)

                        } else {
                            toast.error(response.msg)
                        }
                    }
                }).catch((error) => {
            })
        },2000)

    }
    return (
        <div className={'sidebar'} style={{width: '300px'}}>
            <div className='top'>
                <Avatar title={authReducer.user.name}/>
                <Typography color={"secondary"} variant={"button"}
                            className='user-name'>{authReducer.user.name}</Typography>
                <Typography color={"secondary"} variant={"caption"}
                            className='role'>{authReducer.user.role && authReducer.user.role.title}</Typography>
                <span className='circle'></span>
            </div>
            <div className={'menu-container'}>
                <ul>
                    <li className="tree-box" onClick={() => onClose()}><Link to={'/khesarat'}>پرونده های جاری </Link></li>
                    <Divider/>
                    {
                        authReducer.user.permissions.user && Boolean(authReducer.user.permissions.user.user_index.access) && authReducer.user.permissions.user &&
                        <li className="tree-box">
                            <span className={'tree-parent has-child'} onClick={() => handleExpand(6)}>
                                <img width={12} src={expanded.includes(6) ? minusIcon : plusIcon}/>
                                <span>کاربران</span>
                            </span>
                            <ul className="tree-children" style={{display: expanded.includes(6) ? 'block' : 'none'}}>
                                {authReducer.user.permissions.user && Boolean(authReducer.user.permissions.user.user_index.access) &&
                                <li onClick={() => onClose()}><Link to={'/users'}>کاربران</Link></li>}
                                {authReducer.user.permissions.user && Boolean(authReducer.user.permissions.user.role_index.access) &&
                                <li onClick={() => onClose()}><Link to={'/users/roles'}>نقش ها و سطوح دسترسی</Link>
                                </li>}
                            </ul>
                        </li>
                    }
                    {(authReducer.user.role.id == "super_admin"  || authReducer.user.role.id  == "insurer" ) &&
                    <>
                        <li className="tree-box" onClick={() => onClose()}><Link to={'/archive'}>بایگانی</Link></li>
                        <li className="tree-box" onClick={() => onClose()}><Link to={'/deleted-file'}>پرونده های حذف شده</Link></li>
                    </>
                    }

                    {/*<li className="tree-box" >*/}
                    {/*    <div className={"send-file"}>*/}
                    {/*        <label htmlFor={"upload-file"}>*/}
                    {/*            آپلود فایل*/}
                    {/*        </label>*/}
                    {/*        <input onChange={(e) => uploadFile(e)} id={"upload-file"} type={"file"}/>*/}
                    {/*    </div>*/}
                    {/*</li>*/}
                    {/*<li className="tree-box" onClick={() => onClose()}><Link to={'/deleted-file'}>دانلود فایل </Link>*/}
                    {/*</li>*/}

                </ul>
            </div>
        </div>
    );
}

const mapStateToProps = state => ({
    authReducer: state.authReducer
})

export default connect(mapStateToProps)(Sidebar);
