import React, {memo} from "react";
import {Editor} from "@tinymce/tinymce-react/lib/es2015/main/ts";
import Api from "../../utils/api";
import {toast} from "react-toastify";
import {ENV} from "../../env";

const TextEditor = memo((props) => {

    const {onChange, value, token} = props;


    return(
        <Editor
            value={value}
            apiKey={ENV.TinyMCEApiKey}
            init={{
                setup: (editor) => {
                    editor.ui.registry.addButton('deleteImgBtn', {
                        text: 'حذف عکس',
                        onAction: () => {
                            new Api().delete('/fileManager/attachment', {
                                file: editor.selection.getNode().src,
                                directory: 'upload'
                            }, false).then((response) => {
                                if (typeof response !== "undefined") {
                                    if (response.status) {
                                        toast.success('عکس با موفقیت حذف گردید.');
                                        editor.selection.setContent('');
                                    }
                                }
                            })

                        },
                    });
                },
                menubar: false,
                plugins: 'print preview   searchreplace autolink directionality  visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists  wordcount   imagetools    contextmenu colorpicker textpattern help',
                toolbar: 'source code | table | undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media template link anchor codesample | ltr rtl',
                directionality :"rtl",
                imagetools_toolbar: "rotateleft rotateright | flipv fliph | editimage imageoptions | deleteImgBtn",
                images_upload_handler: function (blobInfo, success, failure) {
                    return new Promise((resolve => {

                        const reader = new FileReader();
                        reader.readAsDataURL(blobInfo.blob());
                        reader.onload = () => {
                            const form_data = new FormData();
                            /* Append New File */
                            form_data.append('file', reader.result);
                            form_data.append('directory', 'upload');
                            form_data.append('water_mark', 0);
                            form_data.append('base64', 1);
                            new Api().post('/fileManager/attachment', form_data, false).then((response) => {
                                if (typeof response != "undefined") {
                                    resolve(success(response.path));
                                }
                            }).catch((error) => {
                                console.log(error);
                            })
                        };

                    }))

                }
            }}
            onChange={(event) =>  onChange(event.target.getContent())}
        />
    );
})

export default TextEditor;