import React, {createRef} from 'react';
import Dropzone from "react-dropzone";
import {resizeImage} from "../../utils/file";
import "./moreImage.css"
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import Button from '@material-ui/core/Button';
import {CircularProgress} from "@material-ui/core";

const MoreImage = (props) => {
    const dropzoneRef = createRef();

    return (
        <div style={{width: '100%',display:"flex",marginTop:"10px"}} className={props.alarmColor ? "red-text" :""}>
            {props.isLabel && <p  style={{
                margin: '3px',
                direction: 'rtl',
                textAlign: 'right',
                fontWeight: 'bold',
            }}>{props.placeholder} : </p>}
            <div>

                {props.selected != false &&
                <img src={props.selected} alt="DropFileItem" className='Selected_img' onClick={()=>(dropzoneRef.current)?dropzoneRef.current.open():console.log('DropIsNotHere')}/>

                }

                <Dropzone
                    ref={dropzoneRef}
                    accept='image/png ,image/jpg, image/jpeg'
                    onDrop={acceptedFiles => {
                        const reader = new FileReader()
                        reader.onloadend = (e) => {
                            let imageSrc = reader.result
                            let binaryStr = ""
                            if(imageSrc.includes("image/png")) {
                                binaryStr =reader.result.replace("png" , 'jpeg')
                            } else {
                                binaryStr = reader.result
                            }
                            let mime = (binaryStr.split(':')[1]).split(';')[0]
                            resizeImage(binaryStr, mime, (newResizedImage)=>{
                                props.onSelect(newResizedImage , acceptedFiles[0].name);
                            });
                        }
                        acceptedFiles.forEach(file => reader.readAsDataURL(file))
                    }}
                    multiple={false}>
                    {({getRootProps, getInputProps}) => (
                        <section>
                            <div {...getRootProps()}>
                                <input disabled={props.disable} {...getInputProps()} />
                                <Button
                                    disabled={props.disable}
                                    variant="contained"
                                    color="default"
                                    startIcon={props.loading ? <CircularProgress/> : <CloudUploadIcon />}
                                >
                                    بارگزاری تصویر بیشتر
                                </Button>

                            </div>
                        </section>
                    )}
                </Dropzone>
            </div>
        </div>
    );
}

MoreImage.defaultProps = {
    isLabel: false,
    placeholder: '',
    containerStyle: {},
    uploadText: 'بارگذاری فایل',
    uploadedText: 'فایل انتخاب شده',
    onSelect: ()=>{},
    selected: false
};

export {MoreImage};
