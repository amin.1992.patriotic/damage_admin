import React, {memo, useEffect, useState} from "react";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import Grid from "@material-ui/core/Grid";

const PeriodAutoComplete = (props) => {

    const { list, onChange } = props;
    const [options, setOptions] = useState([]);

    useEffect(() => {
        let n_options = [];
        n_options[0] = list;
        setOptions([...n_options]);
    }, [list]);

    const handleChanger = (event, value, index) => {

        let n_options = options;
        n_options.splice(index + 1);
        setOptions([...n_options]);
        if (value) {

            if ( value.period !== undefined && value.period.length > 0) {
                n_options[index + 1] = value.period;
                setOptions([...n_options]);
            }

            if (index === 0) {
                onChange('month_id', value.id);
                onChange('period_id', '');
            }

            if (index === 1) {
                onChange('period_id', value.id);
            }

        } else {
            if (index === 0) {
                onChange('month_id', '');
                onChange('period_id', '');
            }

            if (index === 1) {
                onChange('period_id', '');
            }
        }

    };

    return(
        <React.Fragment>
            {options.map((item , index) => {
                return(
                    <Grid item={true} xs={12} sm={3}>
                        <Autocomplete
                            autoComplete={'off'}
                            onChange={((event, value) => handleChanger(event, value, index))}
                            options={item}
                            getOptionLabel={(option) => option.title}
                            renderInput={(params) =>
                                <TextField
                                    autoComplete={"off"}
                                    {...params}
                                    fullWidth
                                    name='period_id'
                                    margin={"dense"}
                                    label="دوره زمانی"
                                    variant="outlined"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                            }
                        />
                    </Grid>
                )
            })}
        </React.Fragment>
    )
}

export default memo(PeriodAutoComplete);