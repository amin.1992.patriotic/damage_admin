import React, {useEffect, useState} from 'react';
import {connect, useDispatch} from "react-redux";
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import { OPEN_SETTING_LINKS_DIALOG} from "../../types";
import Api from "../../utils/api";
import {toast} from "react-toastify";
import {Loading} from './../../components'
import {Box} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Tooltip from "@material-ui/core/Tooltip";
import IconButton from "@material-ui/core/IconButton";
import Add from "@material-ui/icons/AddCircle";
import MenuItem from "@material-ui/core/MenuItem";
import RemoveCircleIcon from '@material-ui/icons/RemoveCircle';


const DomainLinks = (props) => {

    const dispatch = useDispatch();

    const { settingReducer } = props;

    const [form, setForm] = useState({
        app: [{app_id: 0, value : ''}],
        license: [{license_id: 0, value : ''}],
        social_media: [{social_media_id: 0, value : ''}],
        communication_channel: [{communication_channel_id: 0, value : ''}],
    });
    const [loading, setLoading] = useState(false);
    const [links, setLinks] =  useState([]);

    useEffect(() => {

        if (settingReducer.OpenDomainLinksDialog === true) {

            setLoading(true);
            new Api().get('/setting/links').then((response) => {
                if (typeof response != "undefined") {
                    setLinks(response);
                }
            })

            new Api().get('/setting/domainLinks').then((response) => {
                if (typeof response !== "undefined") {
                    setForm({
                        app: response.app.length > 0 ? response.app : [{app_id: 0, value : ''}],
                        license: response.license.length > 0 ? response.license : [{license_id: 0, value : ''}],
                        social_media: response.social_media.length > 0 ? response.social_media : [{social_media_id: 0, value : ''}],
                        communication_channel: response.communication_channels.length > 0 ? response.communication_channels : [{communication_channel_id: 0, value : ''}],
                    });
                }
                setLoading(false);
            })
        }

    }, [settingReducer.OpenDomainLinksDialog]);



    // change state
    const handleDuplicateRaw = (event, i, key) => {
        let n_form = form;
        n_form[key][i][event.target.name] = event.target.value;
        setForm({...n_form});
    };

    const duplicateRaw = (state, key, id) => {
        let n_form = form;
        for (let i=0; i < n_form[state].length; i++) {
            if (n_form[state][i][key] === id) {
                let pusher;
                Object.keys(n_form[state][i]).forEach((key) => {
                    pusher = {
                        value : ''
                    }
                });
                n_form[state].push(pusher)

                setForm({...n_form});

                break;
            }
        }
    }

    const deleteRaw = (state, key, id) => {
        if (form[state].length > 1) {
            let n_form = form;
            for (let i = 0 ; i < n_form[state].length ; i++) {
                if (n_form[state][i][key] === id) {
                    n_form[state].splice(i, 1)
                    setForm({...n_form});
                    break;
                }
            }
        } else {
            toast.error('امکان حذف ایتم وجود ندارد.');
        }

    }

    const handleSubmit = (event) => {
        event.preventDefault();

        setLoading(true);

        new Api().put('/setting/domainLinks', form, false).then((response) => {
            if (typeof response != "undefined") {
                if (response.status) {
                    toast.success(response.msg);
                    setLoading(false);
                } else {
                    toast.error(response.msg);
                }
            }

        })
    }


    return(
        <Dialog
            fullWidth={true}
            open={settingReducer.OpenDomainLinksDialog}
            keepMounted
            aria-labelledby="alert-dialog-slide-title"
            aria-describedby="alert-dialog-slide-description"
        >
            <form onSubmit={handleSubmit}>
                <DialogContent>
                    <Box mb={1}>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <div style={{ overflowX: 'auto'}}>
                                    <table className='table-duplicate-row fadeIn'>
                                        <thead>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>شبکه اجتماعی</th>
                                            <th>لینک</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {form.social_media.length > 0 && form.social_media.map((s, index) => {
                                            return(<tr key={index}>
                                                <td>{index + 1 }</td>
                                                <td>
                                                    <TextField
                                                        name={'social_media_id'}
                                                        select
                                                        value={s.social_media_id}
                                                        fullWidth
                                                        onChange={(event) => handleDuplicateRaw(event, index, 'social_media')}
                                                    >
                                                        <MenuItem key={0} value={0}>انتخاب</MenuItem>
                                                        {links && links.map((loop, index) => {
                                                            if (loop.type === 'social') {
                                                                return(
                                                                    <MenuItem key={index + 1} value={loop.id}>{loop.title}</MenuItem>
                                                                );
                                                            }
                                                        })}
                                                    </TextField>
                                                </td>
                                                <td><TextField  onChange={(event) => handleDuplicateRaw(event, index, 'social_media')} name={'value'} fullWidth value={s.value} /></td>
                                                <td>
                                                    <Tooltip title={'ایجاد'}>
                                                        <IconButton color='primary' onClick={() => duplicateRaw('social_media', 'social_media_id', s.social_media_id)} >
                                                            <Add />
                                                        </IconButton>
                                                    </Tooltip>
                                                    <Tooltip title={'حذف'}>
                                                        <IconButton color='secondary' onClick={() => deleteRaw('social_media', 'social_media_id', s.social_media_id)}>
                                                            <RemoveCircleIcon  />
                                                        </IconButton>
                                                    </Tooltip>
                                                </td>
                                            </tr>);
                                        })}
                                        </tbody>
                                    </table>
                                </div>
                            </Grid>
                        </Grid>
                    </Box>
                    <Box mt={1}>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <div style={{ overflowX: 'auto'}}>
                                    <table className='table-duplicate-row fadeIn'>
                                        <thead>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>اپلیکیشن</th>
                                            <th>لینک</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {form.app.length > 0 && form.app.map((s, index) => {
                                            return(<tr key={index}>
                                                <td>{index + 1 }</td>
                                                <td>
                                                    <TextField
                                                        name={'app_id'}
                                                        select
                                                        value={s.app_id}
                                                        fullWidth
                                                        onChange={(event) => handleDuplicateRaw(event, index, 'app')}
                                                    >
                                                        <MenuItem key={0} value={0}>انتخاب</MenuItem>
                                                        {links && links.map((loop, index) => {
                                                            if (loop.type === 'app') {
                                                                return(
                                                                    <MenuItem key={index + 1} value={loop.id}>{loop.title}</MenuItem>
                                                                );
                                                            }
                                                        })}
                                                    </TextField>
                                                </td>
                                                <td><TextField  onChange={(event) => handleDuplicateRaw(event, index, 'app')} name={'value'} fullWidth value={s.value} /></td>
                                                <td>
                                                    <Tooltip title={'ایجاد'}>
                                                        <IconButton color='primary' onClick={() => duplicateRaw('app', 'app_id', s.app_id)} >
                                                            <Add />
                                                        </IconButton>
                                                    </Tooltip>
                                                    <Tooltip title={'حذف'}>
                                                        <IconButton color='secondary' onClick={() => deleteRaw('app', 'app_id', s.app_id)}>
                                                            <RemoveCircleIcon  />
                                                        </IconButton>
                                                    </Tooltip>
                                                </td>
                                            </tr>);
                                        })}
                                        </tbody>
                                    </table>
                                </div>
                            </Grid>
                        </Grid>
                    </Box>
                    <Box>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <div style={{ overflowX: 'auto'}}>
                                    <table className='table-duplicate-row fadeIn'>
                                        <thead>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>گواهی و لایسنس ها</th>
                                            <th>لینک</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {form.license.length > 0 && form.license.map((s, index) => {
                                            return(<tr key={index}>
                                                <td>{index + 1 }</td>
                                                <td>
                                                    <TextField
                                                        name={'license_id'}
                                                        select
                                                        value={s.license_id}
                                                        fullWidth
                                                        onChange={(event) => handleDuplicateRaw(event, index, 'license')}
                                                    >
                                                        <MenuItem key={0} value={0}>انتخاب</MenuItem>
                                                        {links && links.map((loop, index) => {
                                                            if (loop.type === 'license') {
                                                                return(
                                                                    <MenuItem key={index + 1} value={loop.id}>{loop.title}</MenuItem>
                                                                );
                                                            }
                                                        })}
                                                    </TextField>
                                                </td>
                                                <td><TextField  onChange={(event) => handleDuplicateRaw(event, index, 'license')} name={'value'} fullWidth value={s.value} /></td>
                                                <td>
                                                    <Tooltip title={'ایجاد'}>
                                                        <IconButton color='primary' onClick={() => duplicateRaw('license', 'license_id', s.license_id)} >
                                                            <Add />
                                                        </IconButton>
                                                    </Tooltip>
                                                    <Tooltip title={'حذف'}>
                                                        <IconButton color='secondary' onClick={() => deleteRaw('license', 'license_id', s.license_id)}>
                                                            <RemoveCircleIcon  />
                                                        </IconButton>
                                                    </Tooltip>
                                                </td>
                                            </tr>);
                                        })}
                                        </tbody>
                                    </table>
                                </div>
                            </Grid>
                        </Grid>
                    </Box>
                    <Box>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <div style={{ overflowX: 'auto'}}>
                                    <table className='table-duplicate-row fadeIn'>
                                        <thead>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>پل های ارتباطی</th>
                                            <th>لینک</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {form.communication_channel.length > 0 && form.communication_channel.map((s, index) => {
                                            return(<tr key={index}>
                                                <td>{index + 1 }</td>
                                                <td>
                                                    <TextField
                                                        name={'communication_channel_id'}
                                                        select
                                                        value={s.communication_channel_id}
                                                        fullWidth
                                                        onChange={(event) => handleDuplicateRaw(event, index, 'communication_channel')}
                                                    >
                                                        <MenuItem key={0} value={0}>انتخاب</MenuItem>
                                                        {links && links.map((loop, index) => {
                                                            if (loop.type === 'contact') {
                                                                return(
                                                                    <MenuItem key={index + 1} value={loop.id}>{loop.title}</MenuItem>
                                                                );
                                                            }
                                                        })}
                                                    </TextField>
                                                </td>
                                                <td><TextField  onChange={(event) => handleDuplicateRaw(event, index, 'communication_channel')} name={'value'} fullWidth value={s.value} /></td>
                                                <td>
                                                    <Tooltip title={'ایجاد'}>
                                                        <IconButton color='primary' onClick={() => duplicateRaw('communication_channel', 'communication_channel_id', s.communication_channel_id)} >
                                                            <Add />
                                                        </IconButton>
                                                    </Tooltip>
                                                    <Tooltip title={'حذف'}>
                                                        <IconButton color='secondary' onClick={() => deleteRaw('communication_channel', 'communication_channel_id', s.communication_channel_id)}>
                                                            <RemoveCircleIcon  />
                                                        </IconButton>
                                                    </Tooltip>
                                                </td>
                                            </tr>);
                                        })}
                                        </tbody>
                                    </table>
                                </div>
                            </Grid>
                        </Grid>
                    </Box>
                    {loading && <Loading />}
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => dispatch({type: OPEN_SETTING_LINKS_DIALOG, payload: false})}>
                        بستن
                    </Button>
                    <Button disabled={loading} color="primary" autoFocus type='submit'>
                        ارسال اطلاعات
                    </Button>
                </DialogActions>
            </form>
        </Dialog>
    );
}


const mapStateToProps = state => ({
    settingReducer: state.settingReducer
});

export default connect(mapStateToProps)(DomainLinks);