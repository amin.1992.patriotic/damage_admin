import React, {memo, useEffect, useState} from 'react';
import {Header} from "../index";
import {SpeedDial} from './../../components'
import {connect, useDispatch, useSelector} from "react-redux";
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import {OPEN_SETTING_DIALOG, STICKY_SETTING} from "../../types";
import Api from "../../utils/api";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import Switch from "@material-ui/core/Switch";
import {toast} from "react-toastify";
import {Loading} from './../../components'
import {Divider} from "@material-ui/core";


const StickySetting = memo((props) => {

    const dispatch = useDispatch();


    const AppState = useSelector(state => state);

    const [loading, setLoading] = useState(false);

    useEffect(() => {

        if (AppState.settingReducer.OpenSettingDialog === true) {
            handleRequest();
        }
    }, [AppState.settingReducer.OpenSettingDialog]);


    const handleRequest = () => {
        new Api().get('/setting/sticky').then((response) => {
            if (typeof response !== "undefined") {
                dispatch({type: STICKY_SETTING, payload: response});
            }
        })
    }


    const handleChangeSetting = (event) => {

        let id = event.target.name;
        let status = event.target.checked;
        setLoading(true);

        new Api().put('/setting/sticky', {id: id, status: status }).then((response) => {
            if (typeof response != "undefined") {
                if (response.status) {
                    setLoading(false);
                    toast.success(response.msg);
                } else {
                    toast.error(response.msg)
                }

                handleRequest();
            }
        })

    };


    return(
        <Dialog
            fullWidth={true}
            open={AppState.settingReducer.OpenSettingDialog}
            keepMounted
            aria-labelledby="alert-dialog-slide-title"
            aria-describedby="alert-dialog-slide-description"
        >
            <DialogContent>
                <List>
                    {AppState.settingReducer.StickySetting  && AppState.settingReducer.StickySetting.map((item, index) => {
                        return(
                            <React.Fragment>
                                <ListItem>
                                    <ListItemText id="switch-list-label-wifi" primary={item.title} />
                                    <ListItemSecondaryAction>
                                        <Switch
                                            edge="end"
                                            name={item.id}
                                            checked={Boolean(item.status)}
                                            inputProps={{ 'aria-labelledby': 'switch-list-label-wifi' }}
                                            onChange={handleChangeSetting}
                                        />
                                    </ListItemSecondaryAction>
                                </ListItem>
                                <Divider />
                            </React.Fragment>

                        );
                    })}
                </List>
                {loading && <Loading />}
            </DialogContent>
            <DialogActions>
                <Button onClick={() => dispatch({type: OPEN_SETTING_DIALOG, payload: false})}>
                    بستن
                </Button>
            </DialogActions>
        </Dialog>
    );
})

export default memo(StickySetting)