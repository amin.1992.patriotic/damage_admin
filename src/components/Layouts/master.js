import React, {useEffect} from 'react';
import {Header, Loading} from "../index";
import {SpeedDial, StickySetting, DomainLinks} from './../../components'
import { useDispatch, useSelector} from "react-redux";
import {useHistory} from "react-router";
import moment from 'moment-jalaali';
import {authRefresh} from "../../actions";
import Cookies from "universal-cookie";
import {LOGOUT} from "../../types";
import {toast} from "react-toastify";
const cookies = new Cookies();

const Master = (props) => {

    const { children } = props;
    const history = useHistory();
    const dispatch = useDispatch();
    const AppState = useSelector(state => state);

    useEffect(() => {
        if (cookies.get('auth') === undefined) {
            dispatch({type: LOGOUT});
        }
    }, [cookies.get('auth')])


    if (!AppState.authReducer.login) {
        return null
    }

    return(
        <div className={'master'}>
            <Header />
            <div className={'master-inner'}>
                {children}
            </div>
            {AppState.authReducer.user.permissions.setting && Boolean(AppState.authReducer.user.permissions.setting.setting_read.access) && <SpeedDial />}
            {/* sticky setting dialog */}
            {AppState.authReducer.user.permissions.setting && Boolean(AppState.authReducer.user.permissions.setting.setting_read_sticky.access) && <StickySetting/>}
            {AppState.authReducer.user.permissions.setting && Boolean(AppState.authReducer.user.permissions.setting.setting_read_domain_links.access) && <DomainLinks/>}
        </div>
    );
}
export default Master
