import React, {memo, useEffect, useState} from "react";
import {connect, useDispatch} from "react-redux";
import Container from "@material-ui/core/Container";
import Avatar from "@material-ui/core/Avatar";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import {Link, useHistory} from "react-router-dom";
import Box from "@material-ui/core/Box";
import {Loading} from '../index'
import {LOGIN, LOGOUT} from "../../types";
import Api from "../../utils/api";
import {toast} from "react-toastify";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';

const Logout = memo((props) => {


    const { open, onClose } = props;

    const dispatch = useDispatch();
    const history = useHistory();



    const [loading, setLoading] = useState(false);

    const Transition = React.forwardRef(function Transition(props, ref) {
        return <Slide direction="up" ref={ref} {...props} />;
    });



    const handleLogout = () =>
    {

        setLoading(true);

        new Api().get('/auth/logout').then((response) => {
            if (typeof response != "undefined") {
                if (response.status) {
                    dispatch({type: LOGOUT});
                }
            }

            setTimeout(() => {
                history.push('/');
            }, 1000)
        });
    }


    return (
        <Dialog
            fullWidth={true}
            open={open}
            keepMounted
            onClose={() => onClose()}
            aria-labelledby="alert-dialog-slide-title"
            aria-describedby="alert-dialog-slide-description"
        >
            <DialogTitle id="alert-dialog-slide-title">آیا برای خروج مطمئن هستید؟</DialogTitle>
            <DialogContent>
                {loading && <Loading />}
            </DialogContent>
            <DialogActions>
                <Button onClick={() => onClose()} color="primary">
                    خیر
                </Button>
                <Button onClick={handleLogout} color="primary">
                    بله خارج میشوم
                </Button>
            </DialogActions>
        </Dialog>
    );

})


const mapStateToProps = state => ({
    authReducer: state.authReducer
});




export default connect(mapStateToProps)(Logout)