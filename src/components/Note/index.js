import React, {memo, useEffect, useRef, useState} from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import Button from "@material-ui/core/Button";
import DialogActions from "@material-ui/core/DialogActions";
import Timeline from '@material-ui/lab/Timeline';
import TimelineItem from '@material-ui/lab/TimelineItem';
import TimelineSeparator from '@material-ui/lab/TimelineSeparator';
import TimelineConnector from '@material-ui/lab/TimelineConnector';
import TimelineContent from '@material-ui/lab/TimelineContent';
import TimelineOppositeContent from '@material-ui/lab/TimelineOppositeContent';
import TimelineDot from '@material-ui/lab/TimelineDot';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import {Loading} from "../index";
import NoteAddIcon from '@material-ui/icons/NoteAdd';
import {IconButton, Tooltip} from "@material-ui/core";
import Api from "../../utils/api";
import moment from "moment-jalaali";
import {toast} from "react-toastify";
import SpeakerNotesIcon from '@material-ui/icons/SpeakerNotes';
import EventNoteIcon from '@material-ui/icons/EventNote';
import AttachFileIcon from '@material-ui/icons/AttachFile';
import Grid from "@material-ui/core/Grid";
import AttachmentIcon from '@material-ui/icons/Attachment';
import {ENV} from "../../env";

const EntityNote = memo((props) => {

    const { id, onComplete } = props;

    const [open, setOpen] = useState(false);
    const [list, setList] = useState([]);
    const [loading, setLoading] = useState(true);
    const [note, setNote] = useState('');
    const [file, setFile] = useState(null);


    useEffect(() => {
        if (open) {
            handleRequest()
        }

    }, [open]);

    const handleRequest = () => {
        new Api().get('/orders/' + id + '/notes').then((response) => {
            if (typeof response !== "undefined") {
                setList(response);
                setFile(null);
                setNote('');
            }
            setLoading(false);
        }).catch((err) => {
            console.log(err)
        })
    }

    const handleSubmit = (event) => {
        event.preventDefault();

        if (note === '') {
            toast.error('متن یاداشت را وارد کنید.');
            return;
        }
        setLoading(true);
        new Api().post('/orders/' + id + '/notes', {
            note: note,
            file: file
        }).then((response) => {
            if (typeof response !== "undefined") {
                handleRequest();
            }
            setLoading(false);
        }).catch((err) => {
            console.log(err)
        })
    }


    const handleFileUpload = (event) => {


        setLoading(true);

        const form_data = new FormData();
        form_data.append('file', event.target.files[0]);
        form_data.append('base64', 0);


        new Api().upload('/fileManager/attachment', form_data).then((res) => {
            if (typeof res != "undefined") {
                if (res.result.status) {
                    setFile(res.result.file)
                } else {
                    toast.error(res.result.msg);
                }
            }
            setLoading(false)
        }).catch((error) => {
            toast.error(error);
        })
    }

    return(
        <React.Fragment>
            <Tooltip title={'افزودن یادداشت'}>
                <IconButton onClick={() => setOpen(true)}>
                    <EventNoteIcon />
                </IconButton>
            </Tooltip>
            <Dialog
                fullWidth={true}
                open={open}
                keepMounted
                aria-labelledby="alert-dialog-slide-title"
                aria-describedby="alert-dialog-slide-description"
            >
                <form onSubmit={handleSubmit}>
                    <DialogContent>
                        {list.length > 0 && <Timeline align="alternate">
                            {list.map((item, key) => {
                                return(
                                    <TimelineItem>
                                        <TimelineOppositeContent>
                                            <Typography variant="body2" color="textSecondary">{moment(item.created_at).locale('fa').format('jYYYY/jMM/jDD HH:mm:ss')}</Typography>
                                        </TimelineOppositeContent>
                                        <TimelineSeparator>
                                            <TimelineDot />
                                            <TimelineConnector />
                                        </TimelineSeparator>
                                        <TimelineContent>
                                            <Paper elevation={3} style={{ padding: '15px', textAlign: 'right'}}>
                                                <Typography variant="button">{item.user && item.user.name + ' ' + item.user.family}</Typography>
                                                <br/>
                                                <Typography variant={"caption"}>{item.note}</Typography>
                                                <br />
                                            </Paper>
                                            {item.files.length > 0 && item.files.map((f, index) => {
                                                return(<a target='_blank' href={ENV["STORAGE"] + '/notes/' + item.id + '/' + f.file}><AttachmentIcon /></a>)
                                            })}
                                        </TimelineContent>
                                    </TimelineItem>
                                );
                            })}
                        </Timeline>}
                        <textarea value={note} style={{ width: 'calc(100% - 36px)'}} onChange={(event) => setNote(event.target.value)} placeholder={'تایپ کنید'} name={'note'}></textarea>
                        <Grid item={true} xs={12}>
                            <div style={{ position: 'relative'}}>
                                <label htmlFor={'attach_' + id} style={{width: '100%',height:50, zIndex: -1, display: 'flex', alignItems: 'center'}}>
                                    <AttachFileIcon color={file ? 'secondary' : 'default'}/>
                                    <span>پیوست فایل</span>
                                    <input accept=".zip,.rar,.7zip,image/x-png,image/gif,image/jpeg, video/mp4" onChange={(event) => handleFileUpload(event)} style={{position:'absolute', right:0, display:'none'}} id={'attach_' + id} type='file'/>
                                </label>
                            </div>
                        </Grid>
                        {loading && <Loading />}
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => {
                            setOpen(false)
                            onComplete()
                        }}>
                            بستن
                        </Button>
                        <Button disabled={loading} type={"submit"}>
                            ثبت یاداشت
                        </Button>
                    </DialogActions>
                </form>
            </Dialog>
        </React.Fragment>
    );
});

export default EntityNote;