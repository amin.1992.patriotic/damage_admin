import React, {memo, useEffect, useState} from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import {Tooltip} from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import Grid from "@material-ui/core/Grid";
import {toast} from "react-toastify";
import MenuItem from "@material-ui/core/MenuItem";
import Api from "../../utils/api";
import {roleAction, ticketAction, userAction} from "../../actions";
import {connect} from "react-redux";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { UserAutocomplete } from './../../components'
const Create = memo((props) => {

    const { treeReducer, fetchTickets } = props;

    const initStat = {}


    const [form, setForm] = useState(initStat);

    const [loading, setLoading] = useState(false);
    const [open, setOpen] = useState(false);


    const handleChangeElement = (event) => {
        let frm = {...form};
        frm[event.target.name] = event.target.value;
        setForm(frm);
    };


    const handleChangeElementWithAutoComplete = (name, value) => {
        let frm = {...form};
        frm[name] = value;
        setForm(frm);
    };



    const handleSubmit = (event) => {
        event.preventDefault();

        setLoading(true);

        new Api().post('/tickets', form).then((response) => {
            if (typeof response !== "undefined") {
                if (response.status) {
                    setOpen(false);
                    setForm(initStat);
                    fetchTickets();
                } else {
                    toast.error(response.msg);
                }
            }
            setLoading(false);
        })

    }


    return (
        <div>
            <Tooltip title="افزودن">
                <IconButton onClick={() => setOpen(true)}>
                    <AddCircleOutlineIcon />
                </IconButton>
            </Tooltip>
            <Dialog fullWidth={true} open={open} aria-labelledby="form-dialog-title">
                <form dir='rtl' onSubmit={handleSubmit}>
                    <DialogTitle id="form-dialog-title">تیکت جدید</DialogTitle>
                    <DialogContent>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <UserAutocomplete onChange={(value) => handleChangeElementWithAutoComplete('created_by', value)}/>
                            </Grid>
                            <Grid item xs={12}>
                                <Autocomplete
                                    onChange={((event, value) => handleChangeElementWithAutoComplete('category_id', (value ? value.id : '') ))}
                                    options={treeReducer.nodes}
                                    getOptionLabel={(option) => option.title}
                                    renderInput={(params) =>
                                        <TextField
                                            {...params}
                                            fullWidth
                                            name='category_id'
                                            margin={"dense"}
                                            label="بخش"
                                            variant="outlined"
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                        />
                                    }
                                />
                            </Grid>
                            <Grid item xs={12} >
                                <TextField
                                    label="عنوان"
                                    variant="outlined"
                                    margin='dense'
                                    fullWidth
                                    name='title'
                                    onChange={handleChangeElement}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                            </Grid>
                        </Grid>
                    </DialogContent>
                    <DialogActions>
                        <Button color="primary" onClick={() => setOpen(false)}>
                            انصراف
                        </Button>
                        <Button disabled={loading} color="primary" autoFocus type='submit'>
                            ارسال اطلاعات
                        </Button>
                    </DialogActions>
                </form>
            </Dialog>
        </div>
    );

})


const mapStateToProps = state => ({
    treeReducer: state.treeReducer
});

const mapDispatchToProps = (dispatch) => ({
    fetchTickets: () => {
        dispatch(ticketAction.tickets())
    }
});



export default connect(mapStateToProps, mapDispatchToProps)(Create)