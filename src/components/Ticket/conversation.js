import React, {createRef, memo, useEffect, useState} from "react";
import Dialog from "@material-ui/core/Dialog";
import {Tooltip} from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import {toast} from "react-toastify";
import Api from "../../utils/api";
import { ticketAction} from "../../actions";
import {connect, useDispatch} from "react-redux";
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import QuestionAnswerIcon from "@material-ui/icons/QuestionAnswer";
import FaceIcon from '@material-ui/icons/Face';
import Avatar from '@material-ui/core/Avatar';
import InputBase from '@material-ui/core/InputBase';
import AttachFileIcon from '@material-ui/icons/AttachFile';
import Paper from '@material-ui/core/Paper';
import SendIcon from '@material-ui/icons/Send';
import './conversation.css'
import PersonIcon from '@material-ui/icons/Person';
import moment from 'moment-jalaali'
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import {Loading} from './../../components'
import {FETCH_TICKET} from "../../types";
import Chip from "@material-ui/core/Chip";
import auth from "../../reducers/auth";
import {ENV} from "../../env";
import MarketerLink from "../Marketer/Name";

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

const Conversation = memo((props) => {

    const { ticket_id, fetchTicket, ticketReducer, reload, authReducer } = props;

    const dispatch = useDispatch();

    const initStat = {
        content: '',
        file: ''
    }


    const [form, setForm] = useState(initStat);
    const [cnv_deleteMode, setCnvDeleteMode] = useState('')
    const [loading, setLoading] = useState(false);
    const [open, setOpen] = useState(false);
    const messagesEnd = createRef();
    const [messageReady, setMessageReady] = useState(false);
    const [file, setFile] = useState(null);

    useEffect(() => {
        if (messageReady === true) {
            sendMsg();
        }
    }, [messageReady])

    const handleChangeElement = (event) => {
        let frm = {...form};
        frm[event.target.name] = event.target.value;
        setForm(frm);
    };

    const scrollToBottom = () => {
        if (messagesEnd.current) {
            messagesEnd.current.scrollIntoView({ behavior: "smooth" });
        }
    };

    useEffect(() => {
        scrollToBottom()
    }, [ticketReducer.entity])

    useEffect(() => {
        if (open === true) {
            handleRequest();
        } else {
            dispatch({type: FETCH_TICKET, payload: null});
        }
    }, [open]);

    const handleRequest = () => {
        fetchTicket(ticket_id);
    };


    const handleSubmit = (event) => {
        event.preventDefault();
        sendMsg();
    };

    const handleDeleteConversation = (id) =>
    {
        setCnvDeleteMode(id);
        new Api().delete('/tickets/' + ticket_id + '/conversations/' + id, {}, false).then((response) => {
            if (typeof response != "undefined") {
                if (response.status) {
                    handleRequest();
                } else {
                    toast.error(response.msg);
                }
            }
        }).catch((error) => {
            toast.error(error);
        })
    }

    const sendMsg = () => {

        setLoading(true);
        form.file = file;
        new Api().post('/tickets/' + ticket_id + '/conversations', form, false).then((response) => {
            if (typeof response !== "undefined") {
                if (response.status) {
                    handleRequest();
                    reload();
                    setForm(initStat);
                    setMessageReady(false);
                } else {
                    toast.error(response.msg);
                }
            }
            setLoading(false);
        })
    }

    const handleAutoMessage = (name) => {
        let frm = {...form};
        frm["content"] = name;
        setForm(frm);
        setMessageReady(true);
    }


    const handleFileUpload = (event) => {

        const form_data = new FormData();
        form_data.append('file', event.target.files[0]);
        form_data.append('base64', 0);

        setLoading(true);
        new Api().upload('/fileManager/attachment', form_data).then((res) => {
            if (typeof res != "undefined") {
                if (res.result.status) {
                    setFile(res.result.file)
                } else {
                    toast.error(res.result.msg);
                }
            }
            setLoading(false)
        }).catch((error) => {
            toast.error(error);
        })
    }

    return (
        <>
            <Tooltip title="مشاهده">
                <IconButton onClick={() => setOpen(true)}>
                    <QuestionAnswerIcon />
                </IconButton>
            </Tooltip>
            <Dialog fullScreen open={open} onClose={() => setOpen(false)} TransitionComponent={Transition}>
                <AppBar>
                    <Toolbar className='display-flex display-flex-direction-row display-flex-justify-content-space-between'>
                        <div className={'user-title'}>
                            {ticketReducer.entity && ticketReducer.entity.created_by && <MarketerLink user={ticketReducer.entity.created_by} />}
                        </div>
                        <IconButton edge="start" color="inherit" onClick={() => setOpen(false)} aria-label="close">
                            <CloseIcon />
                        </IconButton>
                    </Toolbar>
                </AppBar>
                <Paper className='chat-box-inner'>
                    {ticketReducer.entity && ticketReducer.entity.conversations && ticketReducer.entity.conversations.map((conversation, index) => {
                        return(
                            <div key={index} className={Boolean(conversation.is_customer) === true  ? 'customer' : 'operator'}>
                                {conversation.is_customer && conversation.files && conversation.files.length > 0 && conversation.files.map((file, index) => {
                                    return(
                                        <a href={ENV["STORAGE"] +  '/' + file} target='_blank'>
                                            <AttachFileIcon />
                                        </a>
                                    );
                                })}
                                {Boolean(conversation.is_customer) === false && <Avatar  alt={conversation.created_by && conversation.created_by.name} src={ENV["STORAGE"] + '/' + conversation.created_by.pic} /> }
                                <div className={(Boolean(conversation.is_customer) === true  ? 'customer' : 'operator') + '-reply'}>
                                    <p>{conversation.content}</p>
                                    <small>{moment(conversation.created_at, 'YYYY/MM/DD HH:mm:ss').locale('fa').format('jYYYY/jMM/jDD HH:mm:ss')}</small>
                                </div>
                                {Boolean(conversation.is_customer) === true && <Avatar  alt={conversation.created_by && conversation.created_by.name} src={ENV["STORAGE"] + '/' + conversation.created_by.pic} /> }
                                {Boolean(authReducer.user.permissions.ticket.ticket_delete_conversation) && Boolean(conversation.is_customer) === false &&
                                <IconButton onClick={() => handleDeleteConversation(conversation.id)} color={"secondary"} ref={(messagesEnd) => {
                                    if (cnv_deleteMode === conversation.id) {
                                        return messagesEnd;
                                    }
                                }}>
                                    <HighlightOffIcon />
                                </IconButton>}
                                {!conversation.is_customer && conversation.files && conversation.files.length > 0 && conversation.files.map((file, index) => {
                                    return(
                                        <a href={ENV["STORAGE"] +  '/' + file} target='_blank'>
                                            <AttachFileIcon />
                                        </a>
                                    );
                                })}
                            </div>
                        );
                    })}
                    {cnv_deleteMode === '' && <div ref={messagesEnd} />}
                    <div className='display-flex display-flex-direction-row message-ready'>
                        {ticketReducer.entity && ticketReducer.entity.category && ticketReducer.entity.category.tags.map((item) => {
                            return(
                                <Chip size={"small"} clickable={true} variant={"outlined"} color={"primary"} label={item.name} onClick={(event) => handleAutoMessage(item.name)} />
                            );
                        })}
                    </div>
                </Paper>
                <Paper className='display-flex display-flex-direction-row display-flex-justify-content-space-between chat-form' component="form" onSubmit={handleSubmit}>
                    {/* Send Icon */}
                    <IconButton type="submit" color={ form.content !== '' ? "primary" :  "default" }  aria-label="directions">
                        <SendIcon />
                    </IconButton>
                    {/* Input For Write Text Message */}
                    <InputBase  className='chat-input' value={messageReady === false ? form.content : ''} name='content' onChange={handleChangeElement} placeholder="تایپ کنید ..."/>
                    <div className={'chat-loading'}>
                        {loading && <Loading />}
                    </div>
                    {/* Attch File Icon */}
                    <>
                        <AttachFileIcon color={form.file !== '' ? 'primary' : ''} style={{ position: "absolute", bottom: '25px', left: '10px', zIndex: 0}}/>
                        <label htmlFor="attach" style={{width:50,height:50, zIndex: 1}}/>
                        <input accept=".zip,.rar,.7zip,image/x-png,image/gif,image/jpeg, video/mp4" onChange={(event) => handleFileUpload(event)} style={{position:'absolute', right:100, zIndex:-100000, display:'none'}} id='attach' type='file'/>
                    </>
                </Paper>
            </Dialog>
        </>
    );

})


const mapStateToProps = state => ({
    ticketReducer: state.ticketReducer,
    authReducer: state.authReducer
});

const mapDispatchToProps = (dispatch) => ({
    fetchTicket: (id) => {
        dispatch(ticketAction.ticket(id))
    },
});



export default connect(mapStateToProps, mapDispatchToProps)(Conversation)