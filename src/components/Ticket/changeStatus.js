import React, {createRef, memo, useEffect, useState} from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import {Tooltip} from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import Grid from "@material-ui/core/Grid";
import {toast} from "react-toastify";
import MenuItem from "@material-ui/core/MenuItem";
import Api from "../../utils/api";
import {roleAction, ticketAction, userAction} from "../../actions";
import {connect, useDispatch} from "react-redux";
import ListItemText from '@material-ui/core/ListItemText';
import ListItem from '@material-ui/core/ListItem';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import QuestionAnswerIcon from "@material-ui/icons/QuestionAnswer";
import FaceIcon from '@material-ui/icons/Face';
import Avatar from '@material-ui/core/Avatar';
import InputBase from '@material-ui/core/InputBase';
import AttachFileIcon from '@material-ui/icons/AttachFile';
import Paper from '@material-ui/core/Paper';
import SendIcon from '@material-ui/icons/Send';
import './conversation.css'
import PersonIcon from '@material-ui/icons/Person';
import moment from 'moment-jalaali'
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import {Loading} from './../../components'
import {FETCH_TICKET} from "../../types";
import AccountTreeIcon from '@material-ui/icons/AccountTree';
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Chip from "@material-ui/core/Chip";
import CategoryIcon from '@material-ui/icons/Category';

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

const changeStatus = memo((props) => {

    const { ticketReducer, reload, entity, authReducer } = props;

    const dispatch = useDispatch();

    const initStat = {
        category_id: '',
    }

    const [loading, setLoading] = useState(false);
    const [open, setOpen] = useState(false);



    useEffect(() => {
        if (open === false) {
            dispatch({type: FETCH_TICKET, payload: null});
        }
    }, [open]);


    const handlechangeStatus = (category_id) => {

        new Api().put('/tickets/' + entity.id, {status: category_id}, false).then((res) => {
            if (typeof res != "undefined") {
                if (res.status) {
                    toast.success(res.msg);
                    reload();
                    setOpen(false);
                } else {
                    toast.error(res.msg);
                }
            }
        })
    }


    return (
        <>
            <Tooltip title='برای تغییر کلیک کنید.'>
                <Chip size={"small"} clickable={true} variant={"outlined"} onClick={() => setOpen(true)} color={ (entity.status === 'open' || entity.status === 'pending') ? 'secondary' : 'primary'} label={ticketReducer.status[entity.status]} />
            </Tooltip>
            {Boolean(authReducer.user.permissions.ticket.ticket_update.access) &&
            <Dialog open={open} onClose={() => setOpen(false)} TransitionComponent={Transition}>
                <List>
                    {Object.keys(ticketReducer.status).map((status, index) => (
                        <ListItem button  onClick={() => handlechangeStatus(status)}>
                            <ListItemAvatar>
                                <Avatar>
                                    <CategoryIcon />
                                </Avatar>
                            </ListItemAvatar>
                            <ListItemText primary={ticketReducer.status[status]} />
                        </ListItem>
                    ))}
                </List>
            </Dialog>}
        </>
    );

})


const mapStateToProps = state => ({
    ticketReducer: state.ticketReducer,
    authReducer: state.authReducer
});

const mapDispatchToProps = (dispatch) => ({
});



export default connect(mapStateToProps, mapDispatchToProps)(changeStatus)