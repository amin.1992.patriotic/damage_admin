import React, {memo, useEffect, useState} from 'react';
import {Box, Paper} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import {packageTypeAction, treeAction} from "../../actions";
import {connect, useSelector} from "react-redux";
import Autocomplete from "@material-ui/lab/Autocomplete";
import Button from "@material-ui/core/Button";
import Api from "../../utils/api";
import {toast} from "react-toastify";
import TextEditor from "../Editor";
import {ProductAutoComplete, TagAutocomplete} from "../index";
import Chip from "@material-ui/core/Chip";
import {Loading} from './../../components'

const BrandEditSeo = memo((props) => {

    const {brandID, onComplete} = props;

    const { authReducer} =  useSelector(state => state);

    const [form, setForm] = useState({});
    const [loading, setLoading] = useState(true);
    const [tag, setTag] = useState([]);
    const [product, setProduct] = useState([]);

    useEffect(() => {
        setLoading(true);
        new Api().get('/products/brands/' + brandID, {}).then((response) => {
            if (typeof response !== "undefined") {
                setForm(response);
            }
            setLoading(false);
        })
    }, []);

    const handleChangeElement = (event) => {

        let frm = form;
        frm[event.target.name] = event.target.value;
        setForm({...frm});
    };


    const handleSubmit = (event) => {
        event.preventDefault();

        setLoading(true);
        let url = '/products/brands/' + brandID;
        new Api().put(url, form).then((response) => {
            if (typeof response !== "undefined") {
                if (response.status) {
                    if (typeof onComplete !== undefined) {
                        onComplete();
                    }

                } else {
                    toast.error(response.msg)
                }

                setLoading(false);
            }
        })
    };


    return(
        <form onSubmit={handleSubmit} className='animate__animated animate__fadeIn'>
            <Box mt={5}>
                <Box mt={5}>
                    <Paper style={{ padding: '25px'}}>
                        <Grid container>
                            <Grid item xs={12} >
                                <TextField
                                    label="اسلاگ"
                                    variant="outlined"
                                    margin='dense'
                                    value={form.slug}
                                    fullWidth
                                    name='slug'
                                    onChange={handleChangeElement}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    helperText='بدون فاصله وارد کنید.'
                                />
                            </Grid>
                            <Grid item xs={12} >
                                <TextField
                                    label="متا عنوان"
                                    variant="outlined"
                                    margin='dense'
                                    value={form.meta_title}
                                    fullWidth
                                    name='meta_title'
                                    onChange={handleChangeElement}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    helperText='60 تا 255 کاراکتر وارد کنید.'
                                />
                            </Grid>
                            <Grid item xs={12} >
                                <TextField
                                    label="متا توضیحات"
                                    variant="outlined"
                                    margin='dense'
                                    value={form.meta_description}
                                    fullWidth
                                    name='meta_description'
                                    onChange={handleChangeElement}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    helperText='60 تا 255 کاراکتر وارد کنید.'
                                />
                            </Grid>
                        </Grid>
                    </Paper>
                </Box>
                <Box mt={5}>
                    <Paper style={{ padding: '25px'}}>
                        <Grid container>
                            <Grid item xs={12} style={{ margin: '20px 0'}}>
                                <TextEditor
                                    token={authReducer.token}
                                    value={form.content}
                                    onChange={(value) => {
                                        let n_form = form;
                                        n_form['content'] = value;
                                        setForm({...n_form});
                                    }} />
                            </Grid>
                        </Grid>
                    </Paper>
                </Box>
            </Box>
            <Box mt={2} className='display-flex display-flex-direction-row display-flex-justify-content-flex-end'>
                <Button disabled={loading} variant='contained' color='primary' type="submit">تکمیل اطلاعات</Button>
            </Box>
            {loading && <Loading/>}
        </form>
    );
})




export default BrandEditSeo;