import React, {memo, useEffect, useState} from 'react';
import {Box, Paper} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Api from "../../utils/api";
import {toast} from "react-toastify";
import {Loading} from './../../components'
import MenuItem from "@material-ui/core/MenuItem";

const BrandBasicInfo = memo((props) => {

    const { onComplete, brandID} = props;


    const [form, setForm] = useState({});
    const [loading, setLoading] = useState(false);

    console.log(brandID)

    useEffect(() => {
        setLoading(true);
        new Api().get('/products/brands/' + brandID, {}).then((response) => {
            if (typeof response !== "undefined") {
                setForm(response);
            }
            setLoading(false);
        })
    }, []);

    const handleChangeElement = (event) => {

        let frm = form;
        frm[event.target.name] = event.target.value;
        setForm({...frm});
    };


    const handleSubmit = (event) => {
        event.preventDefault();

        setLoading(true);
        let url = '/products/brands/' + brandID;
        new Api().put(url, form).then((response) => {
            if (typeof response !== "undefined") {
                if (response.status) {
                    if (typeof onComplete !== undefined) {
                        onComplete();
                    }

                } else {
                    toast.error(response.msg)
                }

                setLoading(false);
            }
        })
    };

    return(
        <form onSubmit={handleSubmit} className='animate__animated animate__fadeIn'>
            <Box mt={5}>
                <Paper style={{ padding: '25px'}}>
                    <Grid container spacing={2}>
                        <Grid item xs={12} >
                            <TextField
                                label="عنوان"
                                variant="outlined"
                                margin='dense'
                                value={form.title}
                                fullWidth
                                name='title'
                                onChange={handleChangeElement}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                label="هدینگ (h2)"
                                variant="outlined"
                                margin='dense'
                                value={form.heading}
                                fullWidth
                                name='heading'
                                onChange={handleChangeElement}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        </Grid>
                        <Grid item xs={12} >
                            <TextField
                                select
                                label="وضعیت"
                                variant="outlined"
                                value={parseInt(form.status)}
                                margin='dense'
                                fullWidth
                                name='status'
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                onChange={handleChangeElement}
                            >
                                <MenuItem key={0} value={1}>فعال</MenuItem>
                                <MenuItem key={0} value={0}>غیرفعال</MenuItem>
                            </TextField>
                        </Grid>
                    </Grid>
                </Paper>
            </Box>
            <Box mt={2} className='display-flex display-flex-direction-row display-flex-justify-content-flex-end'>
                <Button disabled={loading} variant='contained' color='primary' type="submit">تکمیل اطلاعات</Button>
            </Box>
            {loading && <Loading/>}
        </form>
    );
})


export default BrandBasicInfo;