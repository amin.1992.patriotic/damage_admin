import React, {useEffect, useRef, useState} from 'react';
import './editor.css'

const VatanEditor = (props) => {

    const valueRef = useRef()
    function format(command, value) {
        document.execCommand(command, false, value);
    }
    const [ listOfColor , setListOfColor ] = useState([])
    const [ newColor , setNewColor ] = useState("")
    const [ loop , setLoop ] = useState("")
    const [ text , setText ] = useState("")
    // function setUrl() {
    //     var url = document.getElementById('txtFormatUrl').value;
    //     var sText = document.getSelection();
    //     document.execCommand('insertHTML', false, '<a href="' + url + '" target="_blank">' + sText + '</a>');
    //     document.getElementById('txtFormatUrl').value = '';
    // }
    // const uploadImage = ()=> {
    //     var files   = document.querySelector('input[type=file]').files;
    //     function readAndPreview(file) {
    //         if ( /\.(jpe?g|png|gif)$/i.test(file.name) ) {
    //             var reader = new FileReader();
    //
    //             reader.addEventListener("load", function () {
    //                 format('InsertImage', this.result)
    //             }, false);
    //             reader.readAsDataURL(file);
    //         }
    //     }
    //     if (files) {
    //         [].forEach.call(files, readAndPreview);
    //     }
    // }
    useEffect(()=>{
        setText(props.value)

        setListOfColor(JSON.parse(localStorage.getItem("listOfColor")))
    } ,[])


    const concatList = () => {
        listOfColor.push(newColor)
        const newArr = [...listOfColor]
        setListOfColor(newArr)
        localStorage.setItem("listOfColor" , JSON.stringify(listOfColor))
    }
    const deleteHtmlTags = () =>{
        var tmp = document.createElement("DIV");
        tmp.innerHTML = valueRef.current.innerText;
        setText(tmp.textContent||tmp.innerText)
    }

    return (
        <div className="editor-section">
            <div className="sample-toolbar">
                <a href="javascript:void(0)" onClick={() => {
                    format('bold')
                }}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor"
                         className="bi bi-type-bold" viewBox="0 0 16 16">
                        <path
                            d="M8.21 13c2.106 0 3.412-1.087 3.412-2.823 0-1.306-.984-2.283-2.324-2.386v-.055a2.176 2.176 0 0 0 1.852-2.14c0-1.51-1.162-2.46-3.014-2.46H3.843V13H8.21zM5.908 4.674h1.696c.963 0 1.517.451 1.517 1.244 0 .834-.629 1.32-1.73 1.32H5.908V4.673zm0 6.788V8.598h1.73c1.217 0 1.88.492 1.88 1.415 0 .943-.643 1.449-1.832 1.449H5.907z"/>
                    </svg>
                </a>
                <a href="javascript:void(0)" onClick={() => {
                    format('underline')
                }}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor"
                         className="bi bi-type-underline" viewBox="0 0 16 16">
                        <path
                            d="M5.313 3.136h-1.23V9.54c0 2.105 1.47 3.623 3.917 3.623s3.917-1.518 3.917-3.623V3.136h-1.23v6.323c0 1.49-.978 2.57-2.687 2.57-1.709 0-2.687-1.08-2.687-2.57V3.136zM12.5 15h-9v-1h9v1z"/>
                    </svg>
                </a>
                <a href="javascript:void(0)" onClick={() => {
                    format('justifyRight')
                }}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor"
                         className="bi bi-justify-right" viewBox="0 0 16 16">
                        <path fill-rule="evenodd"
                              d="M6 12.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5zm-4-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5z"/>
                    </svg>
                </a>

                <a href="javascript:void(0)" onClick={() => {
                    format('justifyCenter')
                }}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor"
                         className="bi bi-justify" viewBox="0 0 16 16">
                        <path fill-rule="evenodd"
                              d="M2 12.5a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5z"/>
                    </svg>
                </a>
                <a href="javascript:void(0)" onClick={() => {
                    format('justifyLeft')
                }}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor"
                         className="bi bi-justify-left" viewBox="0 0 16 16">
                        <path fill-rule="evenodd"
                              d="M2 12.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5z"/>
                    </svg>
                </a>
                <a href="javascript:void(0)" onClick={() => {
                    format('insertUnorderedList')
                }}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor"
                         className="bi bi-list-ul" viewBox="0 0 16 16">
                        <path fill-rule="evenodd"
                              d="M5 11.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm-3 1a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm0 4a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm0 4a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
                    </svg>
                </a>
                <a href="javascript:void(0)" onClick={() => {
                    format('insertOrderedList')
                }}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor"
                         className="bi bi-list-ol" viewBox="0 0 16 16">
                        <path fill-rule="evenodd"
                              d="M5 11.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5z"/>
                        <path
                            d="M1.713 11.865v-.474H2c.217 0 .363-.137.363-.317 0-.185-.158-.31-.361-.31-.223 0-.367.152-.373.31h-.59c.016-.467.373-.787.986-.787.588-.002.954.291.957.703a.595.595 0 0 1-.492.594v.033a.615.615 0 0 1 .569.631c.003.533-.502.8-1.051.8-.656 0-1-.37-1.008-.794h.582c.008.178.186.306.422.309.254 0 .424-.145.422-.35-.002-.195-.155-.348-.414-.348h-.3zm-.004-4.699h-.604v-.035c0-.408.295-.844.958-.844.583 0 .96.326.96.756 0 .389-.257.617-.476.848l-.537.572v.03h1.054V9H1.143v-.395l.957-.99c.138-.142.293-.304.293-.508 0-.18-.147-.32-.342-.32a.33.33 0 0 0-.342.338v.041zM2.564 5h-.635V2.924h-.031l-.598.42v-.567l.629-.443h.635V5z"/>
                    </svg>
                </a>
                <a href="javascript:void(0)" onClick={() => {
                    deleteHtmlTags()
                }}>
                    <img src="https://img.icons8.com/ios-glyphs/30/000000/clear-formatting.png"/>

                </a>
                {/*<a>*/}
                {/*    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor"*/}
                {/*         className="bi bi-fonts" viewBox="0 0 16 16">*/}
                {/*        <path*/}
                {/*            d="M12.258 3h-8.51l-.083 2.46h.479c.26-1.544.758-1.783 2.693-1.845l.424-.013v7.827c0 .663-.144.82-1.3.923v.52h4.082v-.52c-1.162-.103-1.306-.26-1.306-.923V3.602l.431.013c1.934.062 2.434.301 2.693 1.846h.479L12.258 3z"/>*/}
                {/*    </svg>*/}
                {/*    <select onChange={ (e)=> {  format('fontSize' , e.target.value)} }>*/}
                {/*        <option>سایز</option>*/}
                {/*        <option>1</option>*/}
                {/*        <option>2</option>*/}
                {/*        <option>3</option>*/}
                {/*        <option>4</option>*/}
                {/*        <option>5</option>*/}
                {/*        <option>6</option>*/}
                {/*        <option>7</option>*/}
                {/*    </select>*/}
                {/*</a>*/}
                {/*<a>*/}
                {/*<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor"*/}
                {/*     className="bi bi-palette" viewBox="0 0 16 16">*/}
                {/*    <path*/}
                {/*        d="M8 5a1.5 1.5 0 1 0 0-3 1.5 1.5 0 0 0 0 3zm4 3a1.5 1.5 0 1 0 0-3 1.5 1.5 0 0 0 0 3zM5.5 7a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm.5 6a1.5 1.5 0 1 0 0-3 1.5 1.5 0 0 0 0 3z"/>*/}
                {/*    <path*/}
                {/*        d="M16 8c0 3.15-1.866 2.585-3.567 2.07C11.42 9.763 10.465 9.473 10 10c-.603.683-.475 1.819-.351 2.92C9.826 14.495 9.996 16 8 16a8 8 0 1 1 8-8zm-8 7c.611 0 .654-.171.655-.176.078-.146.124-.464.07-1.119-.014-.168-.037-.37-.061-.591-.052-.464-.112-1.005-.118-1.462-.01-.707.083-1.61.704-2.314.369-.417.845-.578 1.272-.618.404-.038.812.026 1.16.104.343.077.702.186 1.025.284l.028.008c.346.105.658.199.953.266.653.148.904.083.991.024C14.717 9.38 15 9.161 15 8a7 7 0 1 0-7 7z"/>*/}
                {/*</svg>*/}
                {/*<select onChange={ (e)=> { format('foreColor' , e.target.value )} }>*/}
                {/*         <option>{"red"}</option>*/}
                {/*         <option>{"green"}</option>*/}
                {/*         <option>{"blue"}</option>*/}
                {/*         <option>{"gray"}</option>*/}
                {/*         {*/}
                {/*             JSON.parse(localStorage.getItem("listOfColor")) && JSON.parse(localStorage.getItem("listOfColor")).map((item) => {*/}
                {/*              return(*/}
                {/*                  <option>{item}</option>*/}
                {/*              )*/}
                {/*          })*/}
                {/*         }*/}
                {/*</select>*/}
                {/*<div className="insert-color">*/}
                {/*    <input  placeholder={"رنگ مورد نظر خود را وارد کنید"} onChange={(e)=>{setNewColor(e.target.value)}}/>*/}
                {/*    <span onClick={()=>{concatList()}}>ثبت</span>*/}
                {/*</div>*/}
                {/*</a>*/}
                <a>
                    <select onChange={ (e)=> { format('formatBlock' , e.target.value )} }>
                        <option>{"size"}</option>
                        <option>{"h1"}</option>
                        <option>{"h2"}</option>
                        <option>{"h3"}</option>
                        <option>{"h4"}</option>
                        <option>{"h5"}</option>
                        <option>{"h6"}</option>
                        <option>{"h6"}</option>
                    </select>
                </a>

                {/*<a href="javascript:void(0)" onClick={() => {*/}
                {/*    var urlPrompt = prompt("Enter Youtube Url:", "http://");*/}
                {/*    var urlReplace = urlPrompt.replace("watch?v=", "embed/");*/}
                {/*    var embed = '<video title="YouTube video player" src="'+urlReplace+'" allowfullscreen="true" width="480" frameborder="0" height="390">';*/}
                {/*    format("Inserthtml", embed)*/}
                {/*}}>*/}
                {/*    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor"*/}
                {/*         className="bi bi-list-ol" viewBox="0 0 16 16">*/}
                {/*        <path fill-rule="evenodd"*/}
                {/*              d="M5 11.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5z"/>*/}
                {/*        <path*/}
                {/*            d="M1.713 11.865v-.474H2c.217 0 .363-.137.363-.317 0-.185-.158-.31-.361-.31-.223 0-.367.152-.373.31h-.59c.016-.467.373-.787.986-.787.588-.002.954.291.957.703a.595.595 0 0 1-.492.594v.033a.615.615 0 0 1 .569.631c.003.533-.502.8-1.051.8-.656 0-1-.37-1.008-.794h.582c.008.178.186.306.422.309.254 0 .424-.145.422-.35-.002-.195-.155-.348-.414-.348h-.3zm-.004-4.699h-.604v-.035c0-.408.295-.844.958-.844.583 0 .96.326.96.756 0 .389-.257.617-.476.848l-.537.572v.03h1.054V9H1.143v-.395l.957-.99c.138-.142.293-.304.293-.508 0-.18-.147-.32-.342-.32a.33.33 0 0 0-.342.338v.041zM2.564 5h-.635V2.924h-.031l-.598.42v-.567l.629-.443h.635V5z"/>*/}
                {/*    </svg>*/}
                {/*</a>*/}


                {/*<a href="javascript:void(0)" className="editor-insert-image-icon">*/}
                {/*    <input id={"myFile"} multiple onChange={(e)=>{uploadImage(e)}} id={"img"} type={"file"}/>*/}
                {/*    <label  htmlFor={"img"}>*/}
                {/*        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor"*/}
                {/*             className="bi bi-image" viewBox="0 0 16 16">*/}
                {/*            <path d="M6.002 5.5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z"/>*/}
                {/*            <path*/}
                {/*                d="M2.002 1a2 2 0 0 0-2 2v10a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V3a2 2 0 0 0-2-2h-12zm12 1a1 1 0 0 1 1 1v6.5l-3.777-1.947a.5.5 0 0 0-.577.093l-3.71 3.71-2.66-1.772a.5.5 0 0 0-.63.062L1.002 12V3a1 1 0 0 1 1-1h12z"/>*/}
                {/*        </svg>*/}
                {/*    </label>*/}
                {/*</a>*/}

                {/*<a href="javascript:void(0)" onClick={() => {*/}
                {/*    setUrl()*/}
                {/*}}><span className="fa fa-link fa-fw"/></a>*/}
                {/*<span><input id="txtFormatUrl" placeholder="لینک" className="form-control"/></span>*/}
            </div>
            <div onBlur={ () =>  props.onChange && props.onChange(valueRef.current.innerHTML)} ref={valueRef} contenteditable="true" className="editor" id="sampleeditor" dangerouslySetInnerHTML={{__html:text}}/>

        </div>
    );
};

export default VatanEditor;
