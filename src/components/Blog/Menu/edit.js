import React, {memo, useEffect, useState} from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import {Tooltip} from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import Grid from "@material-ui/core/Grid";
import Api from "../../../utils/api";
import {toast} from "react-toastify";
import EditIcon from "@material-ui/icons/Edit"
import MenuItem from "@material-ui/core/MenuItem";
import CurrencyFormat from 'react-currency-format';
import validator from "validator";
import {TOGGLE_CHECKED, TOGGLE_EXPANDED} from "../../../types";
import {useDispatch} from "react-redux";

const MenuEdit = memo((props) => {

    const { checked , reload } = props;

    const dispatch = useDispatch();

    const [form, setForm] = useState({});

    const [loading, setLoading] = useState(false);
    const [open, setOpen] = useState(false);


    const handleChangeElement = (event) => {
        let frm = {...form};
        frm[event.target.name] = event.target.value;
        setForm(frm);
    };

    useEffect(() => {
        if (open === true) {
            new Api().get('/blog/menu/'+ checked, {}, false).then((response) => {
                if (typeof response !== "undefined") {
                    setForm(response)
                }
            })
        }
    }, [open])


    const handleSubmit = (event) => {

        event.preventDefault();

        if (form.external_link) {
            if (! validator.isURL(form.external_link)) {
                toast.error('لینک نامعتبر است.');
                return;
            }
        }

        setLoading(true);
        new Api().put('/blog/menu/' + checked,{
            form
        }, false).then((response) => {
            if (typeof response !== "undefined") {
                if (response.status) {
                    setOpen(false);
                    dispatch({type: TOGGLE_CHECKED, payload: []});
                    reload()
                } else {
                    toast.error(response.msg);
                }
            }
            setLoading(false);
        })

    }



    return (
        <div>
            <Tooltip title="ویرایش">
                <IconButton onClick={() => setOpen(true)}>
                    <EditIcon />
                </IconButton>
            </Tooltip>
            <Dialog fullWidth={true} open={open} aria-labelledby="form-dialog-title">
                <form dir='rtl' onSubmit={handleSubmit}>
                    <DialogTitle id="form-dialog-title">ویرایش دسته بندی</DialogTitle>
                    <DialogContent>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <TextField
                                    label="عنوان"
                                    variant="outlined"
                                    margin='dense'
                                    fullWidth
                                    name='title'
                                    value={form.title}
                                    onChange={handleChangeElement}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    label="لینک به بیرون"
                                    variant="outlined"
                                    margin='dense'
                                    fullWidth
                                    name='external_link'
                                    value={form.external_link}
                                    onChange={handleChangeElement}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    helperText={'در صورتی که میخاید به بیرون از سایت لینک دهید وارد کنید'}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    select
                                    label="وضعیت"
                                    value={parseInt(form.status)}
                                    variant="outlined"
                                    margin='dense'
                                    fullWidth
                                    name='status'
                                    onChange={handleChangeElement}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                >
                                    <MenuItem value={1}>فعال</MenuItem>
                                    <MenuItem value={0}>غیرفعال</MenuItem>
                                </TextField>
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    select
                                    label="محل قرارگیری"
                                    value={"" + form.type + ""}
                                    variant="outlined"
                                    margin='dense'
                                    fullWidth
                                    name='type'
                                    onChange={handleChangeElement}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                >
                                    <MenuItem value={"header"}>هدر</MenuItem>
                                    <MenuItem value={"footer"}>فوتر</MenuItem>
                                </TextField>
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    select
                                    label="حذف"
                                    value={parseInt(form.deleted)}
                                    variant="outlined"
                                    margin='dense'
                                    fullWidth
                                    name='deleted'
                                    onChange={handleChangeElement}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                >
                                    <MenuItem value={1}>شود</MenuItem>
                                    <MenuItem value={0}>نشود</MenuItem>
                                </TextField>
                            </Grid>
                        </Grid>
                    </DialogContent>
                    <DialogActions>
                        <Button color="primary" onClick={() => setOpen(false)}>
                            انصراف
                        </Button>
                        <Button disabled={loading} color="primary" autoFocus type='submit'>
                            ارسال اطلاعات
                        </Button>
                    </DialogActions>
                </form>
            </Dialog>
        </div>
    );

})


export default MenuEdit;