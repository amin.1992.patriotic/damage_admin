import React, {memo, useEffect, useState} from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import {Tooltip} from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import Grid from "@material-ui/core/Grid";
import Api from "../../../utils/api";
import {toast} from "react-toastify";
import MenuItem from "@material-ui/core/MenuItem";
import {TOGGLE_CHECKED} from "../../../types";
import {useDispatch} from "react-redux";

const MenuCreate = memo((props) => {

    const { checked, reload } = props;

    const [form, setForm] = useState({
        title: ''
    });


    const dispatch = useDispatch();

    const [loading, setLoading] = useState(false);
    const [open, setOpen] = useState(false);

    const handleChangeElement = (event) => {
        let frm = {...form};
        frm[event.target.name] = event.target.value;
        setForm(frm);
    };


    const handleSubmit = (event) => {
        event.preventDefault();

        setLoading(true);
        new Api().post('/blog/menu',{
            form,
            checked: checked
        }, false).then((response) => {
            if (typeof response !== "undefined") {
                if (response.status) {
                    dispatch({type: TOGGLE_CHECKED, payload: []});
                    setOpen(false);
                    reload()
                } else {
                    toast.error(response.msg);
                }
            }
            setLoading(false);
        })

    }


    return (
        <div>
            <Tooltip title="افزودن دسته بندی جدید">
                <IconButton onClick={() => setOpen(true)}>
                    <AddCircleOutlineIcon />
                </IconButton>
            </Tooltip>
            <Dialog fullWidth={true} open={open} aria-labelledby="form-dialog-title">
                <form dir='rtl' onSubmit={handleSubmit}>
                    <DialogTitle id="form-dialog-title">دسته بندی  جدید</DialogTitle>
                    <DialogContent>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <TextField
                                    label="عنوان"
                                    variant="outlined"
                                    margin='dense'
                                    fullWidth
                                    name='title'
                                    onChange={handleChangeElement}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    helperText='با علامت , جدا کنید'
                                />
                            </Grid>
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                select
                                label="محل قرارگیری"
                                value={form.type}
                                variant="outlined"
                                margin='dense'
                                fullWidth
                                name='type'
                                onChange={handleChangeElement}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            >
                                <MenuItem value={'header'}>هدر</MenuItem>
                                <MenuItem value={'footer'}>فوتر</MenuItem>
                            </TextField>
                        </Grid>
                    </DialogContent>
                    <DialogActions>
                        <Button color="primary" onClick={() => setOpen(false)}>
                            انصراف
                        </Button>
                        <Button disabled={loading} color="primary" autoFocus type='submit'>
                            ارسال اطلاعات
                        </Button>
                    </DialogActions>
                </form>
            </Dialog>
        </div>
    );

})


export default MenuCreate;