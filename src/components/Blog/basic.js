import React, {memo, useEffect, useState} from 'react';
import {Box, Paper} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import {packageTypeAction, treeAction} from "../../actions";
import {connect} from "react-redux";
import Autocomplete from "@material-ui/lab/Autocomplete";
import Button from "@material-ui/core/Button";
import Api from "../../utils/api";
import {toast} from "react-toastify";
import {BrandAutoComplete} from "../index";
import {priceParameters} from "../../actions/tree";
import {Loading} from './../../components'
import Chip from "@material-ui/core/Chip";
import MenuItem from "@material-ui/core/MenuItem";

const BasicInfo = memo((props) => {

    const { onComplete, contentID} = props;

    const [form, setForm] = useState({});
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        if (contentID) {
            setLoading(true);
            new Api().get('/blog/' + contentID + '/init', {}, false).then((response) => {
                if (typeof response !== "undefined") {
                    setForm(response);
                }
                setLoading(false);
            })
        }
    }, []);

    const handleChangeElement = (event) => {

        let frm = form;
        frm[event.target.name] = event.target.value;
        setForm({...frm});
    };

    const handleChangeElementWithAutoComplete = (name, value) => {
        let frm = form;
        frm[name] = value;
        setForm({...frm})
    }

    const handleSubmit = (event) => {
        event.preventDefault();

        setLoading(true);

        let url = '/blog/init';

        if (contentID) {
            url = '/blog/' + contentID + '/init';
            new Api().put(url, form, false).then((response) => {
                if (typeof response !== "undefined") {
                    if (response.status) {
                        setForm(response.model);
                        if (typeof onComplete !== undefined) {
                            onComplete(response.model.id);
                        }

                    } else {
                        toast.error(response.msg)
                    }

                    setLoading(false);
                }
            })
        } else {
            new Api().post(url, form, false).then((response) => {
                if (typeof response !== "undefined") {
                    if (response.status) {
                        setForm(response.model);
                        if (typeof onComplete !== undefined) {
                            onComplete(response.model.id);
                        }

                    } else {
                        toast.error(response.msg)
                    }

                    setLoading(false);
                }
            })
        }



    };

    return(
        <form onSubmit={handleSubmit} className='animate__animated animate__fadeIn'>
            <Box mt={5}>
                <Paper style={{ padding: '25px'}}>
                    <Grid container spacing={2}>
                        <Grid item xs={12} >
                            <TextField
                                label="عنوان"
                                variant="outlined"
                                margin='dense'
                                value={form.title}
                                fullWidth
                                name='title'
                                onChange={handleChangeElement}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                label="هدینگ (h2)"
                                variant="outlined"
                                margin='dense'
                                value={form.heading}
                                fullWidth
                                name='heading'
                                onChange={handleChangeElement}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                select
                                label="نمایش در وبلاگ"
                                variant="outlined"
                                margin='dense'
                                fullWidth
                                name='is_blog'
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                value={parseInt(form.is_blog)}
                                onChange={handleChangeElement}
                            >
                                <MenuItem key={0} value={-1}>انتخاب</MenuItem>
                                <MenuItem key={1} value={1}>بله</MenuItem>
                                <MenuItem key={2} value={0}>خیر</MenuItem>
                            </TextField>
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                select
                                label="نمایش در پنل کاربر"
                                variant="outlined"
                                margin='dense'
                                fullWidth
                                name='is_private'
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                onChange={handleChangeElement}
                                value={parseInt(form.is_private)}
                            >
                                <MenuItem key={0} value={-1}>انتخاب</MenuItem>
                                <MenuItem key={1} value={1}>بله</MenuItem>
                                <MenuItem key={2} value={0}>خیر</MenuItem>
                            </TextField>
                        </Grid>
                        {/*<Grid item xs={12} sm={4}>*/}
                        {/*    <TextField*/}
                        {/*        select*/}
                        {/*        label="نمایش پاپ اپ"*/}
                        {/*        variant="outlined"*/}
                        {/*        margin='dense'*/}
                        {/*        fullWidth*/}
                        {/*        name='is_popup'*/}
                        {/*        InputLabelProps={{*/}
                        {/*            shrink: true,*/}
                        {/*        }}*/}
                        {/*        onChange={handleChangeElement}*/}
                        {/*        value={parseInt(form.is_popup)}*/}
                        {/*    >*/}
                        {/*        <MenuItem key={0} value={-1}>انتخاب</MenuItem>*/}
                        {/*        <MenuItem key={1} value={1}>بله</MenuItem>*/}
                        {/*        <MenuItem key={2} value={0}>خیر</MenuItem>*/}
                        {/*    </TextField>*/}
                        {/*</Grid>*/}
                    </Grid>
                </Paper>
            </Box>
            <Box mt={2} className='display-flex display-flex-direction-row display-flex-justify-content-flex-end'>
                <Button disabled={loading} variant='contained' color='primary' type="submit">تکمیل اطلاعات</Button>
            </Box>
            {loading && <Loading/>}
        </form>
    );
})

const mapStateToProps = state => ({
})


export default BasicInfo;