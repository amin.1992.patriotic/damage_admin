import React, {memo, useEffect, useState} from 'react';
import {Box, Paper} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import {packageTypeAction, treeAction} from "../../actions";
import {connect, useDispatch} from "react-redux";
import Autocomplete from "@material-ui/lab/Autocomplete";
import Button from "@material-ui/core/Button";
import Api from "../../utils/api";
import {toast} from "react-toastify";
import Tree from "../Tree";
import {FETCH_NODES, TOGGLE_CHECKED, TOGGLE_EXPANDED} from "../../types";
import {Loading} from './../../components'

const EditCategory = memo((props) => {

    const {treeReducer, contentID, fetchBlogCategories, onComplete } = props;

    const dispatch = useDispatch();
    const [loading, setLoading] = useState(true);

    useEffect(() => {

        dispatch({type: FETCH_NODES, payload: []});

        fetchBlogCategories();

        new Api().get(`/blog/${contentID}/categories`, false).then((response) => {

            if (typeof response !== "undefined") {
                // initial edit form
                let checked =  [];
                let expanded = [];
                response.map((category) => {
                    checked.push(category.id);
                    if (Boolean(category.is_main) === false) {
                        expanded.push(category.id)
                    }
                });

                dispatch({type: TOGGLE_CHECKED, payload: checked});
                dispatch({type: TOGGLE_EXPANDED, payload: expanded});
            }

            setLoading(false);
        });

    }, []);

    const handleSubmit = (event) => {

        event.preventDefault();

        setLoading(true);

        new Api().post(`/blog/${contentID}/categories`, {categories : treeReducer.checked}).then((response) => {
            if (typeof response !== "undefined") {
                if (response.status) {
                    if (typeof onComplete !== undefined) {
                        onComplete();
                    }
                    toast.success(response.msg)
                } else {
                    toast.error(response.msg)
                }
                setLoading(false);
            }
        })

    };

    return(
        <form onSubmit={handleSubmit} className='animate__animated animate__fadeIn'>
            <Box mt={5}>
                <Paper style={{ padding: '25px'}}>
                    <Tree/>
                </Paper>
            </Box>
            <Box mt={2} className='display-flex display-flex-direction-row display-flex-justify-content-flex-end'>
                <Button disabled={loading} variant='contained' color='primary' type="submit">تکمیل اطلاعات</Button>
            </Box>
            {loading && <Loading />}
        </form>
    );
})

const mapStateToProps = state => ({
    treeReducer : state.treeReducer
});

const mapDispatchToProps = (dispatch) => ({
    fetchBlogCategories: () => {
        dispatch(treeAction.blogCategories())
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(EditCategory);