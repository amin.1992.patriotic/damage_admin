import React from "react";
import logoIcon from "../../assets/img/logo_vv.png";
import moment from "moment-jalaali";
import CurrencyFormat from "react-currency-format";

const Invoice = (props) => {

    const { invoice } = props;

    let product_count = 0;

    return(
        <div style={{direction: 'rtl', fontFamily: 'Tahoma', fontSize: '13px', padding: '20px 0' }}>
            <div>
                <div>
                    <div style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', 'marginBottom': '50px' }}>
                        <img width="150" src={logoIcon}/>
                        <div style={{ textAlign: 'center'}}>
                            <h1 style={{ fontSize: '14px'}}>آنیک ام ال ام</h1>
                            <span>حواله تحویل کالا</span>
                        </div>
                        <div className="factor_header_right">
                            <div className="factor_number"><span> شماره سفارش : </span><span>{invoice.order_id}</span></div>
                            <div className="factor_time"><span>  تاریخ سفارش : </span><span>{moment(invoice.created_at, 'YYYY/MM/DD HH:mm').locale('fa').format('jYYYY/jMM/jDD HH:mm')}</span>
                            </div>
                        </div>
                    </div>
                    <div className="member_information">
                        <table className="table" cellSpacing="0" border="1" style={{ width: '100%', marginBottom: '50px'}}>
                            <tbody>
                            <tr>
                                <td><b>گیرنده</b>:&nbsp;<span>{invoice && invoice.reciver_name}</span></td>
                                <td><b>کد پستی</b>:&nbsp;<span>{invoice && invoice.postal_code}</span></td>
                                <td><b>شماره تماس</b>:&nbsp;<span>{invoice && invoice.reciver_mobile}</span></td>
                                <td><b>کد ملی</b>:&nbsp;<span>{invoice && invoice.national_code}</span></td>
                            </tr>
                            <tr>
                                <td colSpan="5"><b>گیرنده</b>:&nbsp;<span>{invoice && invoice.address_main}</span></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <table border={1} cellSpacing="0" style={{ width: '100%', textAlign: 'center'}}>
                        <thead>
                        <th>ردیف</th>
                        <th>کد محصول</th>
                        <th>محصول</th>
                        <th>تعداد</th>
                        <th>قیمت واحد</th>
                        <th>تخفیف واحد</th>
                        <th>قیمت با تخفیف</th>
                        <th>مالیات کل</th>
                        <th>قیمت کل </th>
                        </thead>
                        <tbody>
                        {invoice && JSON.parse(invoice.basket).map((item,index) => {
                            product_count += item.count;
                            return (
                                <tr>
                                    <td>{index + 1}</td>
                                    <td>{item.code}</td>
                                    <td>{item.title}</td>
                                    <td>{item.count}</td>
                                    <td><CurrencyFormat value={item.price} displayType="text" thousandSeparator /></td>
                                    <td><CurrencyFormat value={ item.discount} displayType="text" thousandSeparator />&nbsp;%</td>
                                    <td><CurrencyFormat value={item.count * item.off_price} displayType="text" thousandSeparator /></td>
                                    <td><CurrencyFormat value={item.count * item.tax} displayType="text" thousandSeparator /></td>
                                    <td><CurrencyFormat value={(item.off_price * item.count + item.count * item.tax)} displayType="text" thousandSeparator /></td>
                                </tr>
                            )
                        })}
                        <tr>
                            <td colSpan={3}><b>جمع کل:</b></td>
                            <td><b>{product_count}</b></td>
                            <td><b><CurrencyFormat value={invoice.price} displayType="text" thousandSeparator /></b></td>
                            <td>-</td>
                            <td><b><CurrencyFormat value={invoice.off_price} displayType="text" thousandSeparator /></b></td>
                            <td><b><CurrencyFormat value={invoice.tax} displayType="text" thousandSeparator /></b></td>
                            <td><b><CurrencyFormat value={invoice.off_price + invoice.tax} displayType="text" thousandSeparator /></b></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div style={{ marginTop: '50px'}}>
                    {invoice.post_cost > 0 && <div style={{ margin: '20px 0', fontWeight: 'bold'}}>
                      <b>هزینه ارسال پستی:</b>&nbsp;<CurrencyFormat value={invoice.post_cost} displayType="text" thousandSeparator />&nbsp;تومان  میباشد
                    </div>}
                </div>
            </div>
        </div>
    )
}

export default Invoice