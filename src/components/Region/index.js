import React, {memo, useEffect, useState} from "react";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import Grid from "@material-ui/core/Grid";

const RegionAutoComplete = (props) => {

    const { list, onChange } = props;
    const [options, setOptions] = useState([]);

    useEffect(() => {
        let n_options = [];
        n_options[0] = list;
        setOptions([...n_options]);
    }, [list]);

    const handleChanger = (event, value, index) => {

        let n_options = options;
        n_options.splice(index + 1);
        setOptions([...n_options]);
        if (value) {

            if ( value.children !== undefined && value.children.length > 0) {
                n_options[index + 1] = value.children;
                setOptions([...n_options]);
            }

            onChange('region_id', value.id);

        } else {

            onChange('region_id', '');
        }

    };

    return(
        <React.Fragment>
            {options.map((item , index) => {
                return(
                    <Grid item={true} xs={12} sm={3}>
                        <Autocomplete
                            autoComplete={'off'}
                            onChange={((event, value) => handleChanger(event, value, index))}
                            options={item}
                            getOptionLabel={(option) => option.title}
                            renderInput={(params) =>
                                <TextField
                                    autoComplete={"off"}
                                    {...params}
                                    fullWidth
                                    name='period_id'
                                    margin={"dense"}
                                    label="موقعیت"
                                    variant="outlined"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                            }
                        />
                    </Grid>
                )
            })}
        </React.Fragment>
    )
}

export default memo(RegionAutoComplete);