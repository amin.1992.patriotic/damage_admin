import React from "react";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import CurrencyFormat from 'react-currency-format';

const MapReport = (props) => {

    const {list} = props;

    return(
            <>
                {list.length > 0 && <Box>
                    <Grid spacing={2}  container={true}>
                        {list.map((report, index) => {
                            return (
                                <Grid key={index} item xs={12} sm={4} md={3}>
                                    <Paper className='animate__animated animate__flipInX' style={{ padding: '10px 0px', textAlign: "center", margin: '5px 0 0 0'}}>
                                        <Typography variant={"subtitle1"}>
                                            {report.title}
                                        </Typography>
                                        <Typography component="h4" variant={"h4"}  style={{ marginTop: '10px'}}>
                                            {report.counter ? <CurrencyFormat value={report.counter} displayType={'text'} thousandSeparator={true}  /> : '-'}
                                        </Typography>
                                    </Paper>
                                </Grid>
                            );
                        })}
                    </Grid>
                </Box>}
            </>
    );
}

export default MapReport;