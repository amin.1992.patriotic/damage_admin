import React, {useEffect, useState} from 'react';
import plateIcon from "../../assets/img/plate.png";
import motorPlate from "../../assets/img/motor.png";
import CONST from "../../const";
import DatePicker from "react-datepicker2";
import moment from "moment-jalaali";
import {validateNationalCode} from "../../utils/validation";
import {toast} from "react-toastify";
import Api from "../../utils/api";
import {useHistory} from "react-router-dom";
import Button from "@material-ui/core/Button";

const Injured = (props) => {

    const {company} = props
    const separatorGuilty = props.detail && props.detail.plate_guilty && props.detail.plate_guilty.split("-")
    const separatorInjered = props.detail && props.detail.plate_injered && props.detail.plate_injered.split("-")



    const [chassisNumberInjered, setchassisNumberInjered] = useState(props.detail && props.detail.chassis_number_injered)
    const [motorNumberInjered, setMotorNumberInjered] = useState(props.detail && props.detail.motor_number_injered)
    const [nameInjered, setNameInjered] = useState(props.detail && props.detail.name_injered)
    const [certificateInjered, setCertificateInjered] = useState(props.detail && props.detail.certificate_injered)
    const [baseNumberInjered, setBaseNumberInjered] = useState(props.detail && props.detail.base_number_injered)
    const [issueDateInjered, setIssueDateInjered] = useState(props.detail && props.detail.issue_date_injered && moment(props.detail.issue_date_injered, 'jYYYY/jM/jD') || moment())
    const [startDateInjered, setStartDateInjered] = useState(props.detail && props.detail.startDate_injered && moment(props.detail.startDate_injered, 'jYYYY/jM/jD') || moment())
    const [expirationDateInjered, setExpirationDateInjered] = useState(props.detail && props.detail.expiration_date_injered && moment(props.detail.expiration_date_injered, 'jYYYY/jM/jD') || moment())
    const [restrictionsInjered, setRestrictionsInjered] = useState(props.detail && props.detail.restrictions_injered)
    const [nationalCodeInjered, setNationalCodeInjered] = useState(props.detail && props.detail.national_code_injered)
    const [ownerNationalCodeInjered, setOwnerNationalCodeInjered] = useState(props.detail && props.detail.owner_national_code_injered)
    const [insuranceNumberInjered, setInsuranceNumberInjered] = useState(props.detail && props.detail.insurance_number_injered)
    const [companyInjered, setCompanyInjered] = useState(props.detail && props.detail.company_injered)
    const [ownerCarInjered, setOwnerCarInjered] = useState(props.detail && props.detail.owner_car_injered)
    const [uniqueCodeInjered, setUniqueCodeInjered] = useState(props.detail && props.detail.unique_code_injered  ? props.detail && props.detail.unique_code_injered :props.detail && props.detail.injured_insurance_unique_code && props.detail.injured_insurance_unique_code)
    const [mobileNumberInjered, setMobileNumberInjered] = useState(props.detail && props.detail.mobile_number_injered !== null && props.detail.mobile_number_injered )
    const [phoneInjered, setPhoneInjered] = useState(props.detail && props.detail.phone_injered)
    const [disciplinaryInjered, setDisciplinaryInjered] = useState(props.detail && props.detail.disciplinary_injered)
    const [automaticInjered, setAutomaticInjered] = useState(props.detail && props.detail.automatic_injered)
    const [damagedPointsInjered, setDamagedPointsInjered] = useState(props.detail && props.detail.damaged_points_injered)
    const [addressInjered, setAddressInjered] = useState(props.detail && props.detail.address_injered)
    const [carTypeInjered, setCarTypeInjered] = useState(props.detail ? props.detail.car_type_injered : 1)
    const [carSystemInjered, setCarSystemInjered] = useState(props.detail && props.detail.car_system_injered)
    const [colorInjered, setColorInjered] = useState(props.detail && props.detail.color_injered)
    const [licenseNumberInjered, setLicenseNumberInjered] = useState(props.detail && props.detail.license_number_injered)
    const [modelInjered, setModelInjered] = useState(props.detail && props.detail.model_injered)
    const [shaba, setShaba] = useState(props.detail && props.detail.shaba)
    const [damageBranch, setDamageBranch] = useState(props.detail && props.detail.damage_branch)
    const [bankName, setBankName] = useState(props.detail && props.detail.bank_name)
    const [fatherNameInjered, setFatherNameInjered] = useState(props.detail && props.detail.father_name_injered)
    const [birthdayInjered, setBirthdayInjered] = useState(props.detail && props.detail.birthday_injered && moment(props.detail.birthday_injered, 'jYYYY/jM/jD') || moment())
    const [startDateInjeredBody, setStartDateInjeredBody] = useState(props.detail && props.detail.start_date_injered_body && moment(props.detail.start_date_injered_body, 'jYYYY/jM/jD') || moment())
    const [expirationDateInjeredBody, setExpirationDateInjeredBody] = useState(props.detail && props.detail.expiration_date_injered_body && moment(props.detail.expiration_date_injered_body, 'jYYYY/jM/jD') || moment())
    const [issuedInjered, setIssuedInjered] = useState(props.detail && props.detail.issued_injered)
    const [insuranceNumberInjeredBody, setInsuranceNumberInjeredBody] = useState(props.detail && props.detail.insurance_number_injered_body)
    const [companyInjeredBody, setCompanyInjeredBody] = useState(props.detail && props.detail.company_injered_body)
    const [carTypeGuilty, setCarTypeGuilty] = useState(props.detail ? props.detail.car_type_guilty : 1)
    const [plate, setPlate] = useState({})



    useEffect(() => {
        handleSetData()
    } ,[birthdayInjered , startDateInjered , issueDateInjered , expirationDateInjered , startDateInjeredBody , expirationDateInjeredBody])



    useEffect(() => {
        getPlate()
    } , [])

    const getPlate = () => {
        let plate = props.detail && props.detail.plate_injered && props.detail.plate_injered.split("-")
        let alphabet = plate && plate.filter((item) => {
            return isNaN(item) && item !== "null"
        })
        let number = plate && plate.filter((item) => {
            return !isNaN(item) && item !== "null"
        })
        setPlate({
            leftInjered: props.detail ? number && number[1] : "",
            rightInjered: props.detail ? number && number[2] : "",
            topInjered: props.detail ? number && number[0] : "",
            left2Injered: props.detail ? number && number[0] : "",
            centerInjered: props.detail && alphabet && alphabet[0]  ? alphabet && alphabet[0] : "الف",
            downInjered: props.detail ? number && number[1] : "",
        })
    }

    const handleChangeElement = (event) => {
        let frm = {...plate};
        frm[event.target.name] = event.target.value;
        setPlate(frm);
    };
    const handleFocusElement = (event) => {
        let frm = {...plate};
        frm[event.target.name] = "";
        setPlate(frm);
    };

    const validation = () => {
        if (!validateNationalCode(nationalCodeInjered && nationalCodeInjered.toString()) && props.detail.insurance_type && props.detail.insurance_type.id === 1 && nationalCodeInjered !== null) {
            toast.error("کد ملی مقصر صحیح نمی باشد")
            return false
        }
        if (!validateNationalCode(ownerNationalCodeInjered && ownerNationalCodeInjered.toString()) && props.detail.insurance_type && props.detail.insurance_type.id === 1 && ownerNationalCodeInjered !== null) {
            toast.error("کد ملی مالک خودروی مقصر صحیح نمی باشد")
            return false
        }
        if (carSystemInjered === "" ) {
            toast.error("تیپ و سیستم خودرو را وارد کنید .")
            return false
        }
        if (plate === "" ) {
            toast.error("شماره انتظامی را وارد کنید .")
            return false
        }
        if (colorInjered === "" ) {
            toast.error("رنگ را وارد کنید .")
            return false
        }
        if (modelInjered === null ) {
            toast.error("مدل را وارد کنید .")
            return false
        }
        return true
    }

    const handleSetData = (e) => {


        const obj = {
            chassis_number_injered: chassisNumberInjered,
            motor_number_injered: motorNumberInjered,
            name_injered: nameInjered,
            certificate_injered: certificateInjered,
            base_number_injered: baseNumberInjered,
            issue_date_injered:moment(issueDateInjered).format('jYYYY/jM/jD') ,
            restrictions_injered: restrictionsInjered,
            national_code_injered: nationalCodeInjered,
            insurance_number_injered: insuranceNumberInjered,
            startDate_injered:moment(startDateInjered).format('jYYYY/jM/jD') ,
            expiration_date_injered:moment(expirationDateInjered).format('jYYYY/jM/jD') ,
            company_injered: companyInjered,
            financial_injered: null,
            commitment_injered: null,
            owner_car_injered: ownerCarInjered,
            unique_code_injered: uniqueCodeInjered,
            mobile_number_injered: mobileNumberInjered,
            phone_injered: phoneInjered,
            disciplinary_injered: disciplinaryInjered,
            automatic_injered: automaticInjered,
            address_injered: addressInjered,
            car_type_injered: carTypeInjered,
            car_system_injered: carSystemInjered,
            license_number_injered: licenseNumberInjered,
            id:props.detail && props.detail.id ? props.detail && props.detail.id : null,
            shaba:shaba,
            company_injered_body:companyInjeredBody,
            insurance_number_injered_body:insuranceNumberInjeredBody,
            damage_branch:damageBranch,
            bank_name:bankName,
            color_injered:colorInjered,
            model_injered:modelInjered,
            issued_injered:issuedInjered,
            owner_national_code_injered:ownerNationalCodeInjered,
            father_name_injered:fatherNameInjered,
            birthday_injered:moment(birthdayInjered).format('jYYYY/jM/jD'),
            expiration_date_injered_body:moment(expirationDateInjeredBody).format('jYYYY/jM/jD'),
            start_date_injered_body:moment(startDateInjeredBody).format('jYYYY/jM/jD'),
            damaged_points_injered: damagedPointsInjered,
            plate_injered: carTypeInjered !== "2" ? `${(plate.left2Injered) && (plate.left2Injered).trim()}-${(plate.leftInjered) && (plate.leftInjered).trim()}-${(plate.centerInjered) && (plate.centerInjered).trim()}-${(plate.rightInjered) && (plate.rightInjered).trim()}` : `${(plate.topInjered) && (plate.topInjered).trim()}-${(plate.downInjered) && (plate.downInjered).trim()}`,

        }
        props.getData(obj)
    }

    const alphabet = CONST.ALPHABET
    const carTypeList = CONST.CAR_TYPE


    return (
        <div onKeyDown={() => {props.alarm(true)}} onBlur={(e) => handleSetData(e)}  className="insurance-table">
            {
                !props.editCondition && <div className={"disable-cover"}/>
            }

            <div className="table-head">
                {`مشخصات زیان دیده ${nameInjered && nameInjered.length > 0 ? " / " + nameInjered : ""}`}
            </div>
            <div className="table-body">
                <div className="table-body-item">
                    نوع خودرو :
                    <select value={carTypeInjered} onChange={(e) => {
                        props.alarm(true)
                        setCarTypeInjered(e.target.value)
                    }} className="table-body-item-select">
                        {
                            carTypeList && carTypeList.map((item) => {
                                return (
                                    <option value={item.id}>{item.title}</option>
                                )
                            })
                        }
                    </select>
                </div>

                <div className="table-body-item">
                    تیپ و سیستم خودرو :
                    <input value={carSystemInjered} onChange={(e) => {
                        setCarSystemInjered(e.target.value)
                    }} className="table-body-item-input"/>
                </div>
                {
                    carTypeInjered !== "5" &&
                        <>
                            <div style={{justifyContent: "right"}} className="table-body-item">
                                اتوماتیک :
                                <input value={automaticInjered} defaultChecked={automaticInjered === "1"}
                                       onChange={(e) => {
                                           setAutomaticInjered(e.target.checked ? 1 : 2);
                                       }} type={"checkBox"} className="table-body-item-checkBox"/>
                            </div>
                            <div className="table-body-item" style={{justifyContent: "unset"}}>
                                شماره انتظامی :
                                {
                                    parseInt(carTypeInjered) !== 2 ?
                                        <>
                                            <img className="table-body-item-img" src={plateIcon}/>
                                            <input onFocus={(e) => {
                                                handleFocusElement(e)
                                            }} value={plate.rightInjered} onChange={(e) => {
                                                handleChangeElement(e)
                                            }} name={"rightInjered"} maxLength={2}
                                                   className="table-body-item-right-number"/>
                                            <div className="table-body-item-left-number">
                                                <input onFocus={(e) => {
                                                    handleFocusElement(e)
                                                }} value={plate.leftInjered} onChange={(e) => {
                                                    handleChangeElement(e)
                                                }} name={"leftInjered"} maxLength={3}/>
                                                <select onFocus={(e) => {
                                                    handleFocusElement(e)
                                                }} value={plate.centerInjered} onChange={(e) => {
                                                    props.alarm(true)
                                                    handleChangeElement(e)
                                                }} name={"centerInjered"}>
                                                    <option value={-1}>{plate.centerInjered? plate.centerInjered : `انتخاب`}</option>
                                                    {
                                                        alphabet && alphabet.map((item) => {
                                                            return (
                                                                <option value={item.title}>{item.title}</option>
                                                            )
                                                        })
                                                    }
                                                </select>
                                                <input onFocus={(e) => {
                                                    handleFocusElement(e)
                                                }} value={plate.left2Injered} onChange={(e) => {
                                                    handleChangeElement(e)
                                                }} name={"left2Injered"} maxLength={2}/>
                                            </div>
                                        </>
                                        :
                                        <>
                                            <img className="table-body-item-img-motor" src={motorPlate}/>
                                            <div className="table-body-item-motor-number">
                                                <input onFocus={(e) => {
                                                    handleFocusElement(e)
                                                }} value={plate.topInjered} onChange={(e) => {
                                                    handleChangeElement(e)
                                                }} name={"topInjered"} maxLength={3}/>
                                                <input onFocus={(e) => {
                                                    handleFocusElement(e)
                                                }} value={plate.downInjered} onChange={(e) => {
                                                    handleChangeElement(e)
                                                }} name={"downInjered"} maxLength={5}/>
                                            </div>
                                        </>
                                }

                            </div>
                            <div className="table-body-item">
                                <div>
                                    رنگ :
                                </div>
                                <select value={colorInjered} onChange={(e) => {
                                    props.alarm(true)
                                    setColorInjered(e.target.value)
                                }} className="table-body-item-select">
                                    <option value={"-1"}>{"انتخاب کنید"}</option>
                                    {
                                        CONST.COLOR && CONST.COLOR.map((item) => {
                                            return (
                                                <option value={item.id}>{item.title}</option>
                                            )
                                        })
                                    }
                                </select>
                            </div>
                            <div className="table-body-item">
                                مدل :
                                <input type={"number"} value={modelInjered} onChange={(e) => {setModelInjered(e.target.value)}}  className="table-body-item-input"/>
                            </div>
                            <div className="table-body-item">
                                شماره شاسی :
                                <input value={chassisNumberInjered} onChange={(e) => {
                                    setchassisNumberInjered(e.target.value)
                                }} className="table-body-item-input"/>

                            </div>

                            <div className="table-body-item">
                                شماره موتور :
                                <input value={motorNumberInjered} onChange={(e) => {
                                    setMotorNumberInjered(e.target.value)
                                }} className="table-body-item-input"/>

                            </div>
                            <div className="table-body-item">
                                نام راننده :
                                <input value={nameInjered} onChange={(e) => setNameInjered(e.target.value)}
                                       className="table-body-item-input"/>

                            </div>
                            <div className="table-body-item">
                                کد ملی راننده:
                                <input type={"number"} value={nationalCodeInjered} onChange={(e) => {
                                    setNationalCodeInjered(e.target.value)
                                }} className="table-body-item-input"/>
                            </div>

                            <div onChange={(e) => {
                                setFatherNameInjered(e.target.value)
                            }} className="table-body-item">
                                نام پدر:
                                <input value={fatherNameInjered}  className="table-body-item-input"/>

                            </div>

                            <div className="table-body-item">
                                شماره شناسنامه :
                                <input type={"number"} value={certificateInjered} onChange={(e) => {
                                    setCertificateInjered(e.target.value)
                                }} className="table-body-item-input"/>

                            </div>
                            <div className="table-body-item">
                                شماره گواهینامه :
                                <input type={"number"} value={licenseNumberInjered} onChange={(e) => {
                                    setLicenseNumberInjered(e.target.value)
                                }} className="table-body-item-input"/>

                            </div>
                            <div className="table-body-item">
                                شماره پایه گواهینامه :
                                <select value={baseNumberInjered} className={"table-body-item-select"}
                                        onChange={(e) => {
                                            props.alarm(true)
                                            setBaseNumberInjered(e.target.value)
                                        }}>
                                    <option value={"-1"}>{"انتخاب کنید"}</option>

                                    {CONST.BASE_NUMBER && CONST.BASE_NUMBER.map((item) => {
                                        return (
                                            <option value={item.id}>{item.title}</option>
                                        );
                                    })}
                                </select>
                            </div>

                            <div className='datepicker-input-container'>
                                <div className={"table-body-item"}>
                                    <div>
                                        تاریخ صدور :
                                    </div>
                                    <DatePicker
                                        isGregorian={false}
                                        timePicker={false}
                                        value={issueDateInjered}
                                        onChange={issueDateInjered => setIssueDateInjered(issueDateInjered)}
                                    />
                                </div>
                            </div>
                            <div className="table-body-item">
                                محدودیت رانندگی :
                                <input value={restrictionsInjered} onChange={(e) => {
                                    setRestrictionsInjered(e.target.value)
                                }} className="table-body-item-input"/>

                            </div>

                            <div className="table-body-item">
                                شماره بیمه نامه ثالث:
                                <input value={insuranceNumberInjered} onChange={(e) => {
                                    setInsuranceNumberInjered(e.target.value)
                                }} className="table-body-item-input"/>

                            </div>
                            <div className="table-body-item">
                                کد یکتا ثالث:
                                <input type={"number"} value={uniqueCodeInjered}
                                       onChange={(e) => setUniqueCodeInjered(e.target.value)}
                                       className="table-body-item-input"/>

                            </div>
                            <div className='datepicker-input-container'>
                                <div className={"table-body-item"}>
                                    <div>
                                        تاریخ شروع ثالث:
                                    </div>

                                    <DatePicker
                                        isGregorian={false}
                                        timePicker={false}
                                        value={startDateInjered}
                                        onChange={startDateInjered => setStartDateInjered(startDateInjered)}
                                    />
                                </div>
                            </div>
                            <div className='datepicker-input-container'>
                                <div className={"table-body-item"}>
                                    <div>
                                        تاریخ انقضا ثالث:
                                    </div>

                                    <DatePicker
                                        isGregorian={false}
                                        timePicker={false}
                                        value={expirationDateInjered}
                                        onChange={expirationDateInjered => setExpirationDateInjered(expirationDateInjered)}
                                    />
                                </div>
                            </div>

                            <div className="table-body-item">
                                شرکت بیمه ثالث:
                                <select value={companyInjered} className={"table-body-item-select"} onChange={(e) => {
                                    props.alarm(true)
                                    setCompanyInjered(e.target.value)
                                }}>
                                    <option value={"-1"}>{"انتخاب کنید"}</option>
                                    {company && company.map((item) => {
                                        return (
                                            <option value={item && item.id && item.id}>{item && item.title}</option>
                                        );
                                    })}
                                </select>
                            </div>
                            {
                                props.type == "2" &&
                                <>
                                    <div className="table-body-item">
                                        شماره بیمه نامه بدنه:
                                        <input value={insuranceNumberInjeredBody} onChange={(e) => {
                                            setInsuranceNumberInjeredBody(e.target.value)
                                        }} className="table-body-item-input"/>

                                    </div>

                                    <div className='datepicker-input-container'>
                                        <div className={"table-body-item"}>
                                            <div>
                                                تاریخ شروع بدنه:
                                            </div>

                                            <DatePicker
                                                isGregorian={false}
                                                timePicker={false}
                                                value={startDateInjeredBody}
                                                onChange={startDateInjeredBody => setStartDateInjeredBody(startDateInjeredBody)}
                                            />
                                        </div>
                                    </div>
                                    <div className='datepicker-input-container'>
                                        <div className={"table-body-item"}>
                                            <div>
                                                تاریخ انقضا بدنه:
                                            </div>

                                            <DatePicker
                                                isGregorian={false}
                                                timePicker={false}
                                                value={expirationDateInjeredBody}
                                                onChange={expirationDateInjeredBody => setExpirationDateInjeredBody(expirationDateInjeredBody)}
                                            />
                                        </div>
                                    </div>

                                    <div className="table-body-item">
                                        شرکت بیمه بدنه:
                                        <select value={companyInjeredBody} className={"table-body-item-select"} onChange={(e) => {
                                            props.alarm(true)
                                            setCompanyInjeredBody(e.target.value)
                                        }}>
                                            <option value={"-1"}>{"انتخاب کنید"}</option>

                                            {company && company.map((item) => {
                                                return (
                                                    <option value={item.id}>{item.title}</option>
                                                );
                                            })}
                                        </select>
                                    </div>
                                </>
                            }
                        </>
                }

                <div className="table-body-item">
                    نام مالک خودرو :
                    <input value={ownerCarInjered} onChange={(e) => {
                        setOwnerCarInjered(e.target.value)
                    }} className="table-body-item-input"/>

                </div>

                <div className="table-body-item">
                    کد ملی مالک :
                    <input type={"number"} value={ownerNationalCodeInjered} onChange={(e) => {
                        setOwnerNationalCodeInjered(e.target.value)
                    }} className="table-body-item-input"/>

                </div>
                <div className='datepicker-input-container'>
                    <div className={"table-body-item"}>
                        <div>
                            تاریخ تولد مالک:
                        </div>

                        <DatePicker
                            isGregorian={false}
                            timePicker={false}
                            value={birthdayInjered}
                            onChange={birthdayInjered => setBirthdayInjered(birthdayInjered)}
                        />
                    </div>
                </div>
                <div className="table-body-item">
                    صادره از :
                    <input value={issuedInjered}
                           onChange={(e) => setIssuedInjered(e.target.value)}
                           className="table-body-item-input"/>

                </div>
                <div className="table-body-item">
                    تلفن همراه :
                    <input type={"number"} value={mobileNumberInjered}
                           onChange={(e) => setMobileNumberInjered(e.target.value)}
                           className="table-body-item-input"/>

                </div>
                <div className="table-body-item">
                    تلفن ثابت :
                    <input type={"number"} value={phoneInjered} onChange={(e) => {
                        setPhoneInjered(e.target.value)
                    }} className="table-body-item-input"/>

                </div>

                <div style={{width: '100%', justifyContent: "right"}} className="table-body-item">
                    آدرس :
                    <input value={addressInjered} onChange={(e) => {
                        setAddressInjered(e.target.value)
                    }} className="table-body-item-input"/>
                </div>
                <div style={{width: '100%', justifyContent: "right"}} className="table-body-item">
                    نقاط آسیب دیده :
                    <input value={damagedPointsInjered} onChange={(e) => {
                        setDamagedPointsInjered(e.target.value)
                    }} className="table-body-item-input"/>
                </div>

                <div style={{width: "45%"}} className="table-body-item">
                    شماره حساب/شبا :
                    <input  value={shaba}  onChange={(e) => {setShaba(e.target.value)}} style={{width: "70%"}} type={"number"} className="table-body-item-input"/>

                </div>

                <div className="table-body-item">
                    نام بانک :
                    <input  value={bankName}   onChange={(e) => {setBankName(e.target.value)}}  className="table-body-item-input"/>

                </div>

                <div className="table-body-item">
                    شعبه دریافت خسارت :
                    <input value={damageBranch} onChange={(e) => {setDamageBranch(e.target.value)}}  className="table-body-item-input"/>

                </div>
            </div>

        </div>
    );
};

export default Injured;