import React, {useEffect, useRef, useState} from 'react';
import "./vatanButton.css"
const VatanButton = () => {
    const ref = useRef()
    const [ position , setPosition ] = useState(null)

    const mouse = (e) => {
       const y = e.clientY - e.target.offsetTop
       const x = e.clientX - e.target.offsetLeft
       e.target.style.setProperty("--x" , x + "px")
       e.target.style.setProperty("--y" , y + "px")
    }

    return (
        <div ref ={ref} onMouseMove={(e) => {mouse(e)}} className={"button"}>

        </div>
    );
};

export default VatanButton;