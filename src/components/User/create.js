import React, {memo, useEffect, useState} from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import {Tooltip} from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import Grid from "@material-ui/core/Grid";
import {toast} from "react-toastify";
import MenuItem from "@material-ui/core/MenuItem";
import Api from "../../utils/api";
import {roleAction, userAction} from "../../actions";
import {connect} from "react-redux";
import CONST from "../../const"
const UserCreate = memo((props) => {

    const { reload, fetchRoles, roles } = props;

    const initStat = {
        name: '',
        family: '',
        mobile: '',
        email: '',
        username: '',
        role_id: '',
        status: 1,
        password: '',
        region_id: '',
        company_id: '',
    }


    const [form, setForm] = useState(initStat);

    const [loading, setLoading] = useState(false);
    const [open, setOpen] = useState(false);
    const [cityList, setCityList] = useState(false);
    const [companyList, setCompanyList] = useState(false);



    useEffect(() => {
        if (open === true) {
            fetchRoles()
        }
        getCity()
        getCompany()
    }, [open])

    const getCity = () => {
        new Api().get("/region")
            .then((res) => {
                setCityList(res)
            })
    }
    const getCompany = () => {
        new Api().get("/insurance/company")
            .then((res) => {
                setCompanyList(res)
            })
    }
    const handleChangeElement = (event) => {
        let frm = {...form};
        frm[event.target.name] = event.target.value;
        setForm(frm);
    };


    const handleSubmit = (event) => {
        event.preventDefault();

        setLoading(true);
        new Api().post('/backend/users', form).then((response) => {
            if (typeof response !== "undefined") {
                if (response.status) {
                    setOpen(false);
                    setForm(initStat);
                    reload()
                } else {
                    toast.error(response.msg);
                }
            }
            setLoading(false);
        })

    }


    return (
        <div>
            <Tooltip title="افزودن">
                <IconButton onClick={() => setOpen(true)}>
                    <AddCircleOutlineIcon />
                </IconButton>
            </Tooltip>
            <Dialog fullWidth={true} open={open} aria-labelledby="form-dialog-title">
                <form dir='rtl' onSubmit={handleSubmit}>
                    <DialogTitle id="form-dialog-title">کاربر جدید</DialogTitle>
                    <DialogContent>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <TextField
                                    label="نام"
                                    variant="outlined"
                                    margin='dense'
                                    fullWidth
                                    name='name'
                                    onChange={handleChangeElement}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    label="نام خانوادگی"
                                    variant="outlined"
                                    margin='dense'
                                    fullWidth
                                    name='family'
                                    onChange={handleChangeElement}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    label="موبایل"
                                    variant="outlined"
                                    margin='dense'
                                    fullWidth
                                    name='mobile'
                                    onChange={handleChangeElement}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    label="نام کاربری"
                                    variant="outlined"
                                    margin='dense'
                                    fullWidth
                                    name='username'
                                    onChange={handleChangeElement}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    label="ایمیل"
                                    variant="outlined"
                                    margin='dense'
                                    fullWidth
                                    name='email'
                                    onChange={handleChangeElement}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    select
                                    label="وضعیت"
                                    variant="outlined"
                                    margin='dense'
                                    fullWidth
                                    name='status'
                                    onChange={handleChangeElement}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    value={form.status}
                                >
                                    <MenuItem value={1}>فعال</MenuItem>
                                    <MenuItem value={0}>غیرفعال</MenuItem>
                                </TextField>
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    select
                                    label="نقش"
                                    variant="outlined"
                                    margin='dense'
                                    fullWidth
                                    name='role_id'
                                    onChange={handleChangeElement}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                >
                                    {roles && roles.data && roles.data.map((item) => {
                                        return(
                                            <MenuItem value={item.id}>{item.title}</MenuItem>
                                        );
                                    })}

                                </TextField>
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    select
                                    label="شرکت بیمه"
                                    variant="outlined"
                                    margin='dense'
                                    fullWidth
                                    name='company_id'
                                    onChange={handleChangeElement}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                >
                                    {companyList && companyList.map((item) => {
                                        return(
                                            <MenuItem value={item.id}>{item.title}</MenuItem>
                                        );
                                    })}

                                </TextField>
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    select
                                    label="شهر"
                                    variant="outlined"
                                    margin='dense'
                                    fullWidth
                                    name='region_id'
                                    onChange={handleChangeElement}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                >
                                    {cityList && cityList.map((item) => {
                                        return(
                                            item.children && item.children.map((j) => {
                                                return(
                                                    <MenuItem value={j.id}>{j.title}</MenuItem>
                                                )
                                            })
                                        )

                                    })}

                                </TextField>
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    label="کلمه عبور"
                                    variant="outlined"
                                    margin='dense'
                                    fullWidth
                                    name='password'
                                    onChange={handleChangeElement}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                            </Grid>
                        </Grid>
                    </DialogContent>
                    <DialogActions>
                        <Button color="primary" onClick={() => setOpen(false)}>
                            انصراف
                        </Button>
                        <Button disabled={loading} color="primary" autoFocus type='submit'>
                            ارسال اطلاعات
                        </Button>
                    </DialogActions>
                </form>
            </Dialog>
        </div>
    );

})


const mapStateToProps = state => ({
    roles: state.roleReducer
});

const mapDispatchToProps = (dispatch) => ({
    fetchRoles: () => {
        dispatch(roleAction.fetchRoles())
    }
});



export default connect(mapStateToProps, mapDispatchToProps)(UserCreate)