import React, {memo, useEffect, useState} from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import {Tooltip} from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import Grid from "@material-ui/core/Grid";
import EditIcon from "@material-ui/icons/Edit"
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import {Checkbox} from "@material-ui/core";
import Chip from "@material-ui/core/Chip";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import {Loading} from './../../../components'
import MenuItem from "@material-ui/core/MenuItem";
import AccessibilityIcon from '@material-ui/icons/Accessibility';
import Api from "../../../utils/api";
import {toast} from "react-toastify";

const RoleHasPermissions = memo((props) => {

    const { role_id } = props;

    const [loading, setLoading] = useState(true);
    const [open, setOpen] = useState(false);
    const [role, setRole] = useState(null);
    const [permissions, setPermissions] = useState([]);

    useEffect(() => {
        if (open === true) {
            new Api().get('/backend/users/roles/' + role_id).then((response) => {
                if (typeof response !== "undefined") {
                    setRole(response);
                }
            })

            new Api().get('/backend/users/roles/' + role_id + '/permissions?map=true').then((response) => {
                if (typeof response !== "undefined") {
                    setPermissions(response);
                    setLoading(false);
                }
            })
        } else {
            setLoading(true);
            setPermissions([]);
            setRole(null);
        }
    }, [open])


    const handleCheckAll = (parent) => {

        let n_permissions = permissions;

        n_permissions[parent]['actions'].forEach((value, key) => {
            n_permissions[parent]['actions'][key]['access'] = true
        });

        setPermissions([...n_permissions]);
    }

    const handleChangeInput = (event, parent, child) =>
    {
        let n_permissions = permissions;
        n_permissions[parent]['actions'][child]['access'] = event.target.checked ? 1 : 0;
        setPermissions([...n_permissions]);
    }

    const handleSubmit = (event) => {
        event.preventDefault();

        setLoading(true);

        new Api().put('/backend/users/roles/' + role_id + '/permissions', {permissions}, false).then((response) => {
            if (typeof response !== "undefined") {
                if (response.status) {
                    toast.success(response.msg);
                    setTimeout(() => {
                        setOpen(false);
                    }, 1000);
                } else {
                    toast.error(response.msg);
                }

                setLoading(false);
            }

            setLoading(false);
        })


    }


    return (
        <div>
            <Tooltip title="سطح دسترسی">
                <IconButton onClick={() => setOpen(true)}>
                    <AccessibilityIcon />
                </IconButton>
            </Tooltip>
            <Dialog fullScreen={true}  open={open} aria-labelledby="form-dialog-title">
                <AppBar>
                    <Toolbar className='display-flex display-flex-justify-content-space-between'>
                        <Typography variant="h6" >
                            {role && role.title}
                        </Typography>
                        <IconButton edge="start" color="inherit" onClick={() => setOpen(false)} aria-label="close">
                            <CloseIcon />
                        </IconButton>
                    </Toolbar>
                </AppBar>
                <DialogTitle id="form-dialog-title">سطح دسترسی</DialogTitle>
                <DialogContent>
                    <form dir='rtl' onSubmit={handleSubmit}>
                        <div style={{ minHeight: 1000}}>
                            {permissions && permissions.map((permission, parent) => {
                                return(
                                    <Grid style={{margin: '30px 0'}} key={parent}  container>
                                        <Grid  item xs={12}>
                                            <h3><Chip clickable={true} onClick={() => handleCheckAll(parent)} color="default"  label={ permission.controller.key} /></h3>
                                        </Grid>
                                        {permission.actions.map((action, child) => {
                                            return(
                                                <Grid key={child}  item sm={4}>
                                                    <FormControlLabel
                                                        control={
                                                            <Checkbox
                                                                key={child}
                                                                value={action.id}
                                                                name={action.id}
                                                                checked={parseInt(action.access) === 1}
                                                                onChange={(event) => handleChangeInput(event, parent, child)}
                                                            />
                                                        }
                                                        label={action.title}
                                                    />
                                                </Grid>
                                            );
                                        })}
                                    </Grid>
                                )
                            })}
                        </div>
                        {!loading && <Button disabled={loading} color="primary" variant={"contained"} autoFocus type='submit'>
                            ارسال اطلاعات
                        </Button>}
                    </form>
                    {loading && <Loading />}
                </DialogContent>
            </Dialog>
        </div>
    );

})


export default RoleHasPermissions;