import React, {memo, useEffect, useState} from "react";
import Grid from "@material-ui/core/Grid";
import Api from "../../../utils/api";
import Autocomplete from "@material-ui/lab/Autocomplete/Autocomplete";
import TextField from "@material-ui/core/TextField";

const UserAutoComplete = memo((props) => {


    const { onChange, label } = props;

    const [options, setOptions] = useState([]);
    const [typingTimeout, setTypingTimeout] = useState(0);
    const instance = new Api();

    const autoCompleteHandleSelect = (id) =>
    {
       // return value of props
        onChange(id);
    };

    const autoCompleteHandleChange = (event) =>
    {
        let val = event.target.value;

        if (typingTimeout) {
            clearTimeout(typingTimeout);
        }

        let loop = setTimeout(async () => {
            await new Promise(resolve => {
                resolve(instance.get('/backend/users/autocomplete', {'term': val}).then((response) => {
                    if (typeof response != "undefined") {
                        setOptions(response);
                    }
                }));
            })
        }, 500);

        setTypingTimeout(loop);

    };

    return(
        <Autocomplete
            onChange={((event, value) => autoCompleteHandleSelect(value ? value.id : ''))}
            options={options}
            getOptionLabel={option => option.name + ' - ' + option.mobile}
            renderInput={params => (
                <TextField
                    {...params}
                    fullWidth
                    margin={"dense"}
                    label={label ? label : props.name}
                    variant="outlined"
                    InputLabelProps={{
                        shrink: true,
                    }}
                    onChange={autoCompleteHandleChange}
                />
            )}
        />
    );
});

export default UserAutoComplete
