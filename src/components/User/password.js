import React, {memo, useEffect, useState} from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import {Tooltip} from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import LockIcon from '@material-ui/icons/Lock';
import Grid from "@material-ui/core/Grid";
import {toast} from "react-toastify";
import MenuItem from "@material-ui/core/MenuItem";
import Api from "../../utils/api";
import {roleAction, userAction} from "../../actions";
import {connect, useDispatch} from "react-redux";
import {USER_FETCH} from "../../types";
import Typography from "@material-ui/core/Typography";


const UserChangePassword = memo((props) => {

    const { user, fetchUser, userReducer } = props;

    const dispatch = useDispatch();


    const initStat = {
        password: '',
    }


    const [form, setForm] = useState(initStat);
    const [loading, setLoading] = useState(false);
    const [open, setOpen] = useState(false);



    useEffect(() => {
        // remove old user from redux
        dispatch({type: USER_FETCH, payload: null})
        // set form with init state
        setForm(initStat);
        // fetch new user if dialog is open
        if (open === true) {
            fetchUser(user);
        }
    }, [open]);



    const handleChangeElement = (event) => {
        let frm = {...form};
        frm[event.target.name] = event.target.value;
        setForm(frm);
    };


    const handleSubmit = (event) => {
        event.preventDefault();

        setLoading(true);
        new Api().put(`/backend/users/${user}/password`, form).then((response) => {
            if (typeof response !== "undefined") {
                if (response.status) {
                    setOpen(false);
                } else {
                    toast.error(response.msg);
                }
            }
            setLoading(false);
        })

    }


    return (
        <div>
            <Tooltip title="تغییر رمز عبور">
                <IconButton onClick={() => setOpen(true)}>
                    <LockIcon />
                </IconButton>
            </Tooltip>
            <Dialog fullWidth={true} open={open} aria-labelledby="form-dialog-title">
                <form dir='rtl' onSubmit={handleSubmit}>
                    <DialogTitle id="form-dialog-title"><span>{`تغییر رمز عبور کاربر`}</span>&nbsp;<Typography variant={"subtitle1"} color={"primary"}>{userReducer.user && userReducer.user.name}</Typography></DialogTitle>
                    <DialogContent>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <TextField
                                    label="کلمه عبور"
                                    variant="outlined"
                                    margin='dense'
                                    fullWidth
                                    name='password'
                                    onChange={handleChangeElement}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                            </Grid>
                        </Grid>
                    </DialogContent>
                    <DialogActions>
                        <Button color="primary" onClick={() => setOpen(false)}>
                            انصراف
                        </Button>
                        <Button disabled={loading} color="primary" autoFocus type='submit'>
                            ارسال اطلاعات
                        </Button>
                    </DialogActions>
                </form>
            </Dialog>
        </div>
    );

})


const mapStateToProps = state => ({
    userReducer: state.userReducer
});

const mapDispatchToProps = (dispatch) => ({
    fetchUser: (id) => {
        dispatch(userAction.fetchUser(id))
    }
});



export default connect(mapStateToProps, mapDispatchToProps)(UserChangePassword)