import React, {memo, useEffect, useState} from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import {Tooltip} from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import LockIcon from '@material-ui/icons/Lock';
import Grid from "@material-ui/core/Grid";
import {toast} from "react-toastify";
import MenuItem from "@material-ui/core/MenuItem";
import Api from "../../utils/api";
import {roleAction, userAction} from "../../actions";
import {connect, useDispatch} from "react-redux";
import {USER_FETCH} from "../../types";
import Typography from "@material-ui/core/Typography";
import EditIcon from '@material-ui/icons/Edit';

const UserEdit = memo((props) => {

    /**
     * user props from component props call, int id
     * fetchUser action load user data from redux
     * userReducer and roleReducer access to data
     */
    const { user, fetchUser, userReducer, roleReducer, reload, fetchRoles, roles } = props;

    const dispatch = useDispatch();

    // init state form
    const initStat = {
        name: '',
        family: '',
        mobile: '',
        email: '',
        username: '',
        role_id: '',
        status: '',
        region_id: '',
        company_id: '',
    }


    const [form, setForm] = useState(initStat);
    const [loading, setLoading] = useState(false);
    const [open, setOpen] = useState(false);
    const [cityList, setCityList] = useState(false);
    const [companyList, setCompanyList] = useState(false);
    const [citySearch, setCitySearch] = useState("");

    useEffect(() => {

        // remove old user from redux
        dispatch({type: USER_FETCH, payload: null});
        // set form with init state
        setForm(initStat);
        // fetch new user if dialog is open
        if (open === true) {
            getCity()
            getCompany()
            fetchUser(user);
            fetchRoles();
        }
    }, [open]);


    useEffect(() => {
        if (userReducer.user) {
            setForm({
                name: userReducer.user.name,
                family: userReducer.user.family,
                mobile: userReducer.user.mobile,
                email: userReducer.user.email,
                username: userReducer.user.username,
                role_id: userReducer.user && userReducer.user.role && userReducer.user.role.id,
                status: userReducer.user.status,
                region_id: userReducer.user.region_id,
                company_id:userReducer.user.company_id,
            })
        }
    }, [userReducer.user]);



    const handleChangeElement = (event) => {
        let frm = {...form};
        frm[event.target.name] = event.target.value;
        setForm(frm);
    };

    const getCity = () => {
        new Api().get("/region")
            .then((res) => {
                setCityList(res)
            })
    }
    const getCompany = () => {
        new Api().get("/insurance/company")
            .then((res) => {
                setCompanyList(res)
            })
    }
    const handleSubmit = (event) => {
        event.preventDefault();

        setLoading(true);

        new Api().put('/backend/users/' + user, form).then((response) => {
            if (typeof response !== "undefined") {
                if (response.status) {
                    setOpen(false);
                    reload()
                } else {
                    toast.error(response.msg);
                }
            }

            setLoading(false);
        })

    }


    return (
        <div>
            <Tooltip title="ویرایش">
                <IconButton onClick={() => setOpen(true)}>
                    <EditIcon />
                </IconButton>
            </Tooltip>
            <Dialog fullWidth={true} open={open} aria-labelledby="form-dialog-title">
                <form dir='rtl' onSubmit={handleSubmit}>
                    <DialogTitle id="form-dialog-title">ویرایش اطلاعات</DialogTitle>
                    <DialogContent>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <TextField
                                    label="نام"
                                    variant="outlined"
                                    margin='dense'
                                    fullWidth
                                    value={form.name}
                                    name='name'
                                    onChange={handleChangeElement}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    label="نام خانوادگی"
                                    variant="outlined"
                                    margin='dense'
                                    fullWidth
                                    value={form.family}
                                    name='family'
                                    onChange={handleChangeElement}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    label="موبایل"
                                    variant="outlined"
                                    margin='dense'
                                    fullWidth
                                    name='mobile'
                                    value={form.mobile}
                                    onChange={handleChangeElement}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    label="username"
                                    variant="outlined"
                                    margin='dense'
                                    fullWidth
                                    name='username'
                                    value={form.username}
                                    onChange={handleChangeElement}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    label="ایمیل"
                                    variant="outlined"
                                    margin='dense'
                                    fullWidth
                                    name='email'
                                    value={form.email}
                                    onChange={handleChangeElement}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    select
                                    label="وضعیت"
                                    variant="outlined"
                                    margin='dense'
                                    fullWidth
                                    name='status'
                                    value={form.status}
                                    onChange={handleChangeElement}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    value={form.status}
                                >
                                    <MenuItem value={1}>فعال</MenuItem>
                                    <MenuItem value={0}>غیرفعال</MenuItem>
                                </TextField>
                            </Grid>
                            <Grid item xs={12} >
                                <TextField
                                    select
                                    label="نقش"
                                    variant="outlined"
                                    value={form.role_id}
                                    margin='dense'
                                    fullWidth
                                    name='role_id'
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    onChange={handleChangeElement}
                                >
                                    <MenuItem key={0} value={0}>انتخاب</MenuItem>
                                    {roles && roles.data && roles.data.map((role, index) => {
                                        return <MenuItem key={index} value={role.id}>{role.title ? role.title : role.id}</MenuItem>
                                    })}
                                </TextField>
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    select
                                    label="شرکت بیمه"
                                    variant="outlined"
                                    margin='dense'
                                    fullWidth
                                    name='company_id'
                                    onChange={handleChangeElement}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                >
                                    {companyList && companyList.map((item) => {
                                        return(
                                            <MenuItem value={item.id}>{item.title}</MenuItem>
                                        );
                                    })}

                                </TextField>
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    select
                                    label="شهر"
                                    variant="outlined"
                                    margin='dense'
                                    fullWidth
                                    name='region_id'
                                    onChange={handleChangeElement}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                >
                                    {/*<TextField*/}
                                    {/*    label="جستجو"*/}
                                    {/*    variant="outlined"*/}
                                    {/*    margin='dense'*/}
                                    {/*    fullWidth*/}
                                    {/*    name='email'*/}
                                    {/*    value={citySearch}*/}
                                    {/*    onChange={(e) => {setCitySearch(e.target.value)}}*/}
                                    {/*    InputLabelProps={{*/}
                                    {/*        shrink: true,*/}
                                    {/*    }}*/}
                                    {/*/>*/}

                                    {cityList && cityList.map((item) => {
                                        return(
                                            item.children && item.children.map((j) => {
                                                return(
                                                    j.title.includes(citySearch) &&
                                                    <MenuItem value={j.id}>{j.title}</MenuItem>
                                                )
                                            })
                                        )

                                    })}

                                </TextField>
                            </Grid>
                        </Grid>
                    </DialogContent>
                    <DialogActions>
                        <Button color="primary" onClick={() => setOpen(false)}>
                            انصراف
                        </Button>
                        <Button disabled={loading} color="primary" autoFocus type='submit'>
                            ارسال اطلاعات
                        </Button>
                    </DialogActions>
                </form>
            </Dialog>
        </div>
    );

})


const mapStateToProps = state => ({
    userReducer: state.userReducer,
    roles: state.roleReducer,

});

const mapDispatchToProps = (dispatch) => ({
    fetchRoles: () => {
        dispatch(roleAction.fetchRoles())
    },
    fetchUser: (id) => {
        dispatch(userAction.fetchUser(id))
    }
});



export default connect(mapStateToProps, mapDispatchToProps)(UserEdit)