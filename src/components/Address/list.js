import React, {memo} from "react";
import Grid from "@material-ui/core/Grid";
import Chip from "@material-ui/core/Chip";
import Button from "@material-ui/core/Button";
import AddIcon from "@material-ui/icons/Add";
import './style.css'

import {DIALOG_PAYLOAD} from "../../types";
import {useDispatch} from "react-redux";
import AddAddress from "./add";

const AddressMemo = memo((props) => {

    const { data, id } = props;
    const dispatch  = useDispatch();

    return(
        <Grid container={true} spacing={2}>
            {data && data.map((adr, index) => {
                return (
                    <Grid key={index} xs={12} sm={6} item={true}>
                        <div className={'address-box'}>
                            <div className='address-box-inner'>
                                <div className='address-box-inner-item'>
                                    <span className={'key'}>موقعیت:</span>&nbsp;<span className={'value'}>
                                                {adr.regions && JSON.parse(adr.regions).map((item) => {
                                                    return(
                                                        <Chip label={item.title} />
                                                    );
                                                })}
                                            </span>
                                </div>
                                <div className='address-box-inner-item'>
                                    <span className={'key'}>آدرس:</span>&nbsp;<span className={'value'}>{adr.main}</span>
                                </div>
                                <div className='address-box-inner-item'>
                                    <span className={'key'}>کد پستی:</span>&nbsp;<span className={'value'}>{adr.postal_code}</span>
                                </div>
                                <div className='address-box-inner-item'>
                                    <span className={'key'}>گیرنده:</span>&nbsp;<span className={'value'}>{adr.reciver_name}</span>
                                </div>
                                <div className='address-box-inner-item'>
                                    <span className={'key'}>شماره تماس:</span>&nbsp;<span className={'value'}>{adr.reciver_mobile}</span>
                                </div>
                                <div className='address-box-inner-item'>
                                    <span className={'key'}>کد ملی:</span>&nbsp;<span className={'value'}>{adr.reciver_national_code}</span>
                                </div>
                            </div>
                        </div>
                    </Grid>
                );
            })
            }
            <Grid xs={12} sm={6} item={true}>
                <div className='address-box address-box-dashed'>
                    <AddIcon />
                    <Button onClick={() => dispatch({type: DIALOG_PAYLOAD, payload: true, dType: 'ADD_ADDRESS'})} variant="text" color="primary">
                        ایجاد آدرس جدید
                    </Button>
                </div>
            </Grid>
        </Grid>
    )
});

export default AddressMemo;