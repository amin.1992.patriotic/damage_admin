import React, {useState, memo, useEffect, useCallback} from "react";
import ReactExport from "react-data-export";
import {Tooltip} from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import ImportExportIcon from '@material-ui/icons/ImportExport';
import moment from "moment-jalaali";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import {useSelector} from "react-redux";
import Api from "../../../utils/api";
import {Loading} from "../../index";
const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

const ReagentExport = (props) => {

    const { object } = props;


    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(false);
    const [open, setOpen] = useState(false);

    const fetchData = () => {
        setLoading(true);

        object.excel_export = 1;
        new Api().get('/rewards/reagent', object).then((response) => {
            if (typeof response !== "undefined") {
                let n_p = [];
                response.map((entity, index) => {
                    n_p[index] = {
                        'marketer_id': entity.marketer_id,
                        'name': entity.user && (entity.user.name && entity.user.name + ' ' + entity.user.family),
                        'period': entity.period && entity.period.month && entity.period.month.title + ' (' + entity.period.title + ')',
                        'node' : entity.node,
                        'lft_sales' : entity.lft_sales,
                        'lft_wallet' : entity.lft_wallet,
                        'rgt_sales' : entity.rgt_sales,
                        'rgt_wallet' : entity.rgt_wallet,
                        'value' : entity.value,
                    }
                })

                setData([...n_p]);
                setOpen(true);
                setLoading(false);
            }
        })
    };


    return (
        <React.Fragment>
            <IconButton onClick={() => fetchData()}>
                <ImportExportIcon />
            </IconButton>
            {data.length > 0 && <ExcelFile filename={'reagent_' + moment().unix()} hideElement={false} element={
                <Dialog
                    fullWidth={true}
                    open={open}
                    keepMounted
                    onClose={() => {
                        setOpen(false);
                        setData([]);
                    }}
                    aria-labelledby="alert-dialog-slide-title"
                    aria-describedby="alert-dialog-slide-description"
                >
                    <DialogTitle id="alert-dialog-slide-title">جلوگیری از ارسال درخواست  توسط ربات</DialogTitle>
                    <DialogContent>
                        {loading && <Loading />}
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => {
                            setOpen(false);
                            setData([]);
                        }}  color="primary">
                            بستن
                        </Button>
                        <Button onClick={() => {
                            setOpen(false);
                            setData([]);
                        }} color="primary">
                            دانلود
                        </Button>
                    </DialogActions>
                </Dialog>
            }>
                <ExcelSheet data={data} name="orders">
                    <ExcelColumn  label="شناسه کاربر" value="marketer_id"/>
                    <ExcelColumn  label="نام و نام خانوادگی" value="name"/>
                    <ExcelColumn  label="دوره زمانی" value="period"/>
                    <ExcelColumn  label="نود" value="node"/>
                    <ExcelColumn  label="کانال چپ" value="lft_sales"/>
                    <ExcelColumn  label="پتانسیل رشد چپ" value="lft_wallet"/>
                    <ExcelColumn  label="کانال راست" value="rgt_sales"/>
                    <ExcelColumn  label="پتانسیل رشد راست" value="rgt_wallet"/>
                    <ExcelColumn  label="مبلغ کل" value="value"/>
                </ExcelSheet>
            </ExcelFile>}
            {loading && <Loading/> }
        </React.Fragment>
    );

}

export default memo(ReagentExport);

