import React, {memo, useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import Button from "@material-ui/core/Button";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import {Typography} from "@material-ui/core";
import Api from "../../utils/api";
import {Loading} from "../index";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";
import CurrencyFormat from "react-currency-format";
import Container from "@material-ui/core/Container";

const RewardDetailDialog = memo((props) => {

    const { marketer, period } = props;
    const AppState = useSelector(state => state);
    const [open, setOpen ] = useState(false);
    const [loading, setLoading] = useState(true);
    const [list, setList] = useState([]);

    useEffect(() => {
        if (open) {
            new Api().get('/rewards/' + marketer + '/detail/' + period ).then((response) => {
                if (typeof response != "undefined") {
                    setList(response)
                }
                setLoading(false);
            });
        }

    }, [open]);


    let total = 0;

    return (
        <React.Fragment>
            <Button onClick={() => setOpen(true)} color={"primary"} variant={"text"} ><Typography>مشاهده</Typography></Button>
            <Dialog
                fullWidth={true}
                open={open}
                keepMounted
                onClose={() => setOpen(false)}
                aria-labelledby="alert-dialog-slide-title"
                aria-describedby="alert-dialog-slide-description"
            >
                <DialogTitle id="alert-dialog-slide-title">جزئیات محاسبه پورسانت</DialogTitle>
                <DialogContent>
                    {list.length > 0 && <div style={{ overflowX: 'auto'}}>
                        <table className='table'>
                            <thead>
                            <tr>
                                <th>نوع پاداش</th>
                                <th>مبلغ (تومان)</th>
                            </tr>
                            </thead>
                            <tbody>
                            {list.map((entity, index) => {
                                total = parseInt(entity.total) + parseInt(total);
                                return(
                                    <tr key={index}>
                                        <td>{AppState.rewards.type && AppState.rewards.type[entity.type]}</td>
                                        <td><CurrencyFormat value={entity.total}  displayType={'text'} thousandSeparator={true}  /></td>
                                    </tr>
                                );
                            })}
                            <tr>
                                <td><b>جمع</b></td>
                                <td><CurrencyFormat value={total}  displayType={'text'} thousandSeparator={true}  /></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>}
                    {loading && <Loading/>}
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => setOpen(false)} color="primary">
                        بستن
                    </Button>
                </DialogActions>
            </Dialog>
        </React.Fragment>

    );

})



export default memo(RewardDetailDialog)