import React, {useCallback, useEffect, useRef, useState} from "react";
import ReactCrop from "react-image-crop";
import "react-image-crop/dist/ReactCrop.css";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import Api from "../../utils/api";

// Setting a high pixel ratio avoids blurriness in the canvas crop preview.
const pixelRatio = 4;


const ImageCropper = (props) => {

    const { file, isSquare, onClose, open } = props;

    const [img, setImg] = useState();
    const imgRef = useRef(null);
    const previewCanvasRef = useRef(null);
    const [crop, setCrop] = useState({ unit: "%", width: 30, aspect: isSquare});
    const [completedCrop, setCompletedCrop] = useState(null);

    useEffect(() => {
        if (file) {
            setImg(file);
        }
    }, [file]);


    const generateFinalImage = () => {

        if (!crop || !previewCanvasRef.current) {
            return;
        }
        const tmpCanvas = document.getElementById('my-canvas');
        tmpCanvas.width = crop.width;
        tmpCanvas.height = crop.height;

        const ctx = tmpCanvas.getContext("2d");
        ctx.drawImage(
            previewCanvasRef.current,
            0,
            0,
            previewCanvasRef.current.width,
            previewCanvasRef.current.height,
            0,
            0,
            crop.width,
            crop.height
        );


        tmpCanvas.toBlob(function(blob) {
            let newImg = document.createElement('img'),
                url = URL.createObjectURL(blob);

            newImg.onload = function() {
                // no longer need to read the blob so it's revoked
                URL.revokeObjectURL(url);
            };

            onClose(url);
        });

    }


    const onLoad = useCallback(img => {
        imgRef.current = img;
    }, []);


    useEffect(() => {
        if (!completedCrop || !previewCanvasRef.current || !imgRef.current) {
            return;
        }

        const image = imgRef.current;
        const canvas = previewCanvasRef.current;
        const crop = completedCrop;

        const scaleX = image.naturalWidth / image.width;
        const scaleY = image.naturalHeight / image.height;
        const ctx = canvas.getContext("2d");

        canvas.width = crop.width * pixelRatio;
        canvas.height = crop.height * pixelRatio;

        ctx.setTransform(pixelRatio, 0, 0, pixelRatio, 0, 0);
        ctx.imageSmoothingEnabled = false;

        ctx.drawImage(
            image,
            crop.x * scaleX,
            crop.y * scaleY,
            crop.width * scaleX,
            crop.height * scaleY,
            0,
            0,
            crop.width,
            crop.height
        );
    }, [completedCrop]);


    return (

        <div className="App">
            <Dialog
                className='display-flex display-flex-justify-content-space-between'
                fullWidth={open}
                open={open}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">تغیر سایز عکس</DialogTitle>
                <DialogContent>
                    <ReactCrop
                        src={img}
                        onImageLoaded={onLoad}
                        crop={crop}
                        onChange={c => setCrop(c)}
                        onComplete={c => setCompletedCrop(c)}
                    />
                    <canvas ref={previewCanvasRef} id='my-canvas' />
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => generateFinalImage()} color="primary" autoFocus>
                        تایید
                    </Button>
                </DialogActions>
            </Dialog>

        </div>
    );
}


export default ImageCropper;