import React, {memo, useEffect, useState} from "react";
import Grid from "@material-ui/core/Grid";
import Api from "../../../utils/api";
import Autocomplete from "@material-ui/lab/Autocomplete/Autocomplete";
import TextField from "@material-ui/core/TextField";
import validator from "validator";


const UserAutoComplete = memo((props) => {


    const { onChange } = props;

    const [options, setOptions] = useState([]);
    const [typingTimeout, setTypingTimeout] = useState(0);
    const instance = new Api();

    const autoCompleteHandleSelect = (value) => // value is a array that contain all selected element
    {
        onChange(value);
    };

    const autoCompleteHandleChange = (event) =>
    {
        let val = event.target.value;

        if (typingTimeout) {
            clearTimeout(typingTimeout);
        }

        let loop = setTimeout(async () => {
            await new Promise(resolve => {
                resolve(instance.get('/tags/autocomplete', {'term': val}).then((response) => {
                    if (typeof response != "undefined") {
                        if (response.length > 0) {
                            setOptions(response);
                        } else {

                            let options = [];
                            options.push({
                                id: val,
                                name: val
                            });
                            setOptions(options);

                        }
                    }
                }));
            })
        }, 750);

        setTypingTimeout(loop);

    };

    return(
        <Autocomplete
            multiple
            freeSolo
            onChange={((event, value) => autoCompleteHandleSelect(value))}
            options={options}
            getOptionLabel={option => option.name}
            renderInput={params => (
                <TextField
                    {...params}
                    fullWidth
                    margin={"dense"}
                    label="تگ"
                    variant="outlined"
                    InputLabelProps={{
                        shrink: true,
                    }}
                    onChange={autoCompleteHandleChange}
                />
            )}
        />
    );
});

export default UserAutoComplete