import React from "react";
import {connect, useSelector} from "react-redux";
import {useHistory} from "react-router";
import './style.css'

const NoMatch = (props) => {

    const history = useHistory();

    const AppState = useSelector(state => state);

    if (!AppState.authReducer.login) {
        history.push('/');
        return null;
    }

    return(
        <div className='not-found'>
            <h1>صفحه‌ای که دنبال آن بودید پیدا نشد!</h1>
            <h2>Oops, Page was not found!</h2>
        </div>
    );
}

export default NoMatch