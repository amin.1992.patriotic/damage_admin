import React, {useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import {Sidebar} from "../index";
import Drawer from "@material-ui/core/Drawer";
import GrainIcon from '@material-ui/icons/Grain';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import {Link} from "react-router-dom";
import AccountCircleRoundedIcon from '@material-ui/icons/AccountCircleRounded';
import {Logout} from './../../components'
import Modal from "@material-ui/core/Modal";
import TextareaAutosize from "@material-ui/core/TextareaAutosize";
import {CircularProgress} from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import Api from "../../utils/api";
import {toast} from "react-toastify";
import {useSelector} from "react-redux";


const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
    modal: {
        display: 'flex',
        padding: theme.spacing(1),
        alignItems: 'center',
        justifyContent: 'center',
    },
    pop: {
        display: "flex",
        flexDirection: "column",
        background: "white",
        padding: "50px"
    }
}));

const Header = () => {

    const classes = useStyles();
    const token = useSelector(state => state.authReducer.token)
    const [toggleModal, setToggleModal] = useState(false);
    const [toggleModalPassword, setToggleModalPassword] = useState(false);
    const [drawer, setDrawer] = useState(false);
    const [anchorEl, setAnchorEl] = useState(null);
    const [logout, setLogout] = useState(false);
    const [loading, setLoading] = useState(false);
    const [password, setPassword] = useState({
        old:'',
        new:'',
        repeatNew:''
    });

    const handleChangePassword = (name , value) => {
        setPassword(prevState => ({...prevState, [name] : value}) );
    }
    const sendData = () => {
        if(password.old === "" ) {
            toast.error("کلمه عبور قدیمی را وارد کنید")
            return false
        }
        if(password.new !== password.repeatNew ) {
            toast.error("کلمه عبور جدید مطابقت ندارد")
            return false
        }
        if(password.new.length < 6 ) {
            toast.error("کلمه عبور باید حداقل 6 کاراکتر باشد")
            return false
        }
        setLoading(true)

        new Api().put(`/auth/panel/reset_password` , {password:password.new , old_password:password.old ,captcha :token})
            .then((res) => {
                if (typeof res !== "undefined") {
                    res.status ? toast.success(res.msg) : toast.error(res.msg)
                    setLoading(false)
                }
            })
    }
    return(
        <div className={classes.root}>
            <Modal
                disablePortal
                disableEnforceFocus
                disableAutoFocus
                open={toggleModalPassword}
                onClose={() => {
                    setToggleModalPassword(false)
                }}
                aria-labelledby="server-modal-title"
                aria-describedby="server-modal-description"
                className={classes.modal}
            >
                <div className={classes.pop}>
                    <TextField
                               size={"small"}
                               name={"old"}
                               onChange={(e) => {
                                    handleChangePassword(e.target.name , e.target.value)
                               }}  className="damage-input"
                               label="کلمه عبور قدیمی"
                               variant="outlined"
                               type={"password"}
                    />
                    <TextField
                               size={"small"}
                               name={"new"}
                               onChange={(e) => {
                                   handleChangePassword(e.target.name , e.target.value)

                               }}  className="damage-input"
                               label="کلمه عبور جدید"
                               variant="outlined"
                               type={"password"}

                    />
                    <TextField
                               size={"small"}
                               name={"repeatNew"}
                               onChange={(e) => {
                                   handleChangePassword(e.target.name , e.target.value)

                               }}  className="damage-input"
                               label="تکرار کلمه عبور جدید"
                               variant="outlined"
                               type={"password"}

                    />
                    <Button onClick={() => {
                        sendData()
                    }} variant="contained" color="primary">
                        {!loading ? "ثبت" : <CircularProgress color={"secondary"} size={20}/>}
                    </Button>
                </div>
            </Modal>

            <AppBar position="static">
                <Toolbar className='display-flex display-flex-direction-row display-flex-justify-content-space-between'>
                    <IconButton  onClick={() => setDrawer(true)} edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                        <MenuIcon style={{ color: '#fff'}} />
                    </IconButton>
                    <Link to={'/'}>
                        <Typography style={{ color: '#fff'}} className="display-flex display-flex-justify-content-center">
                            <GrainIcon />&nbsp;
                            <b>پنل مدیریت خسارت</b>
                        </Typography>
                    </Link>
                    <IconButton id='fade-menu' aria-haspopup="true" onClick={(event) => setAnchorEl(event.currentTarget)}>
                        <AccountCircleRoundedIcon style={{ color: '#fff'}} />
                    </IconButton>
                    <Menu
                        id="fade-menu"
                        anchorEl={anchorEl}
                        open={Boolean(anchorEl)}
                        onClose={() => setAnchorEl(null)}
                    >
                        <MenuItem onClick={() => {
                            setToggleModalPassword(true);
                            setAnchorEl(null);
                        }}>
                            <Typography>تغییر کلمه عبور</Typography>
                        </MenuItem>
                        <MenuItem onClick={() => {
                            setLogout(true);
                            setAnchorEl(null);
                        }}>
                            <Typography>خروج از سایت</Typography>
                        </MenuItem>
                    </Menu>
                </Toolbar>
                <Drawer anchor="left" open={drawer} onClose={() => setDrawer(false)}>
                    <Sidebar onClose={() => setDrawer(false)}/>
                </Drawer>
                <Logout open={logout} onClose={() => setLogout(false)} />
            </AppBar>
        </div>
    );
}


export default Header;
