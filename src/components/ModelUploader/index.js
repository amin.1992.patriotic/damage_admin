import React, {memo, useEffect, useState} from 'react';
import {Box, Container, Paper} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import {packageTypeAction, treeAction} from "../../actions";
import {connect, useDispatch} from "react-redux";
import Autocomplete from "@material-ui/lab/Autocomplete";
import Button from "@material-ui/core/Button";
import Api from "../../utils/api";
import {toast} from "react-toastify";
import Tree from "../Tree";
import {FETCH_NODES, TOGGLE_CHECKED, TOGGLE_EXPANDED} from "../../types";
import {Loading, MultipleUploader} from '../index'
import Switch from "@material-ui/core/Switch";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import FormControlLabel from '@material-ui/core/FormControlLabel';

const ModelUploader = memo((props) => {

    const {entityID, model, authReducer, onComplete } = props;

    const [loading, setLoading] = useState(true);
    const [water_mark, setWaterMark] = useState(false);
    const [serverFiles, setServerFiles] = useState([]);
    const [upFiles, setUpFiles] = useState([]);


    useEffect(() => {
        handleRequest();
    }, []);

    const handleRequest = () => {
        new Api().get('/fileManager/files/' + model + '/' + entityID, {}, false).then((response) => {
            if (typeof response !== "undefined") {
                setServerFiles(response);
                setLoading(false);
            }
        })
    }



    const handleSubmit = (event) => {

        event.preventDefault();

        setLoading(true);
        new Api().post('/fileManager/files/' + model + '/' + entityID, {files : upFiles}, false).then((response) => {
            if (typeof response !== "undefined") {
                if (response.status) {
                    handleRequest()
                    toast.success(response.msg);
                    if (typeof onComplete !== "undefined") {
                        onComplete();
                    }
                } else {
                    toast.error(response.msg);
                }
                setLoading(false);
            }
        });

    }



    return(
        <form onSubmit={handleSubmit} className='animate__animated animate__fadeIn'>
            <Box mt={5}>
                <Paper style={{ padding: '25px'}}>
                    <Box mb={3}>
                        <FormControlLabel
                            control={
                                <Switch
                                    checked={water_mark}
                                    onChange={(event) =>  setWaterMark(event.target.checked)}
                                    name="water_mark"
                                    color="primary"
                                />
                            }
                            label="قرار دادن واترمارک"
                        />
                    </Box>
                    <MultipleUploader
                        values={serverFiles}
                        onComplete={(files) => setUpFiles(files) }
                        token={authReducer.token}
                        url={'/fileManager/attachment'}
                        water_mark={water_mark}
                        onLoading={(loaded) => setLoading(loaded)}
                    />
                </Paper>
            </Box>
            <Box mt={2} className='display-flex display-flex-direction-row display-flex-justify-content-flex-end'>
                <Button disabled={loading} variant='contained' color='primary' type="submit">تکمیل اطلاعات</Button>
            </Box>
            {loading && <Loading />}
        </form>
    );
})

const mapStateToProps = state => ({
    authReducer : state.authReducer
});



export default connect(mapStateToProps)(ModelUploader);