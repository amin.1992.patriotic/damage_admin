import React, {memo} from "react";
import Grid from "@material-ui/core/Grid";
import {Link} from "react-router-dom";
import Button from "@material-ui/core/Button";
import NavigationIcon from "@material-ui/icons/Navigation";
import {Box} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";

const Head = memo((props) => {
    const data = props.data;
    return(
        <Box mb={5}>
            <Grid container alignItems="center">
                <Grid item xs={12} sm={6}>
                    <Typography variant={"h6"}>{data.title}</Typography>
                    <Typography style={{ color: '#8e8e8e'}}>{data.description}</Typography>
                </Grid>

            </Grid>
        </Box>
    );
})


export default Head;