import React, {memo, useEffect, useState} from "react";
import {TextField} from "@material-ui/core";
import Loading from "../Loading";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import {toast} from "react-toastify";

import {Paper} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {useHistory} from "react-router";
import Api from "../../utils/api";
import {UserAutocomplete} from "../index";
import MenuItem from "@material-ui/core/MenuItem";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import moment from "moment-jalaali";

const MarketerRevoke = (props) => {


    const {id} = props;

    const AppState = useSelector(state => state);
    const dispatch = useDispatch();
    const history = useHistory();
    const [loading, setLoading] = useState(false);
    const [form, setForm] = useState({
        content: '',
        sending_data: 0,
        status: 0
    });
    const [marketer, setMarketer] = useState(null);



    const handleChangeElement = (event) => {
        let frm = form;
        frm[event.target.name] = event.target.value;
        setForm({...frm});
    };

    useEffect(() => {
        handleRequest();
    }, []);

    const handleRequest = () => {

        setLoading(true);
        new Api().get('/marketers/'+id+'/info').then((response) => {
            if (typeof response !== "undefined") {
                if (response.status) {
                    setMarketer(response.info);
                    setForm({...form, status: response.info.status})
                }
            }
            setLoading(false);
        })
    }


    const handleSubmit = (event) => {

        event.preventDefault();

        setLoading(true);

        new Api().post('/marketers/'+id+'/revoke', form).then((response) => {
            if (typeof response !== "undefined") {
                if (response.status === true) {
                    toast.success('تغییرات با موفقیت اعمال شد.');
                } else {
                    toast.error(response.msg);
                }

                handleRequest();
                setLoading(false);
            }

        })
    };



    return(
        <React.Fragment>
            <Paper className={'profile-form'}>
                {marketer && <div className={'alert alert-info'}><p><span>وضعیت کاربر</span>&nbsp;<b>{AppState.marketer && AppState.marketer.status && AppState.marketer.status[marketer.status]}</b>&nbsp;<span>است</span></p></div>}
                <form onSubmit={handleSubmit}>
                    <Grid container spacing={2}>
                        <Grid item xs={12} >
                            <Grid item={true} xs={12}>
                                <textarea rows={10} className={'ticket-textarea'} placeholder={'دلیل تعلیق جایگاه و یا بازگشت به مجموعه'} name='content' value={form.content} onChange={handleChangeElement}></textarea>
                            </Grid>
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                select
                                label="ورود به دفترکار"
                                value={parseInt(form.status)}
                                variant="outlined"
                                margin='dense'
                                fullWidth
                                name='status'
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                onChange={handleChangeElement}
                            >
                                <MenuItem value={1}>باز</MenuItem>
                                <MenuItem value={0}>بسته</MenuItem>
                                <MenuItem value={2}>تعلیق</MenuItem>
                            </TextField>
                        </Grid>
                        {form.status === 0 && <Grid item xs={12}>
                            <FormControlLabel
                                control={
                                    <Checkbox
                                        name="sending_data"
                                        color="primary"
                                        value={1}
                                        onChange={handleChangeElement}
                                    />
                                }
                                label="ارسال اطلاعات به وزارتخانه"
                            />
                        </Grid>}
                        <Grid item xs={12} >
                            <Button type={"submit"} disabled={loading} color={"primary"} variant={"contained"}>ویرایش اطلاعات</Button>
                        </Grid>
                    </Grid>
                </form>
            </Paper>
            {loading && <Loading />}
        </React.Fragment>
    );

}


export default memo(MarketerRevoke);