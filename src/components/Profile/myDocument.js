import React, {memo, useCallback, useEffect, useState} from "react";
import {Box, Container, TextField} from "@material-ui/core";
import Loading from "../Loading";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import {toast} from "react-toastify";
import MenuItem from "@material-ui/core/MenuItem";
import {Paper} from "@material-ui/core";
import moment from "moment-jalaali";
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import IconButton from "@material-ui/core/IconButton";
import Api from "../../utils/api";
import validator from "validator";
import ArrowDownwardIcon from "@material-ui/icons/ArrowDownward";
import ArrowUpwardIcon from "@material-ui/icons/ArrowUpward";
import SortIcon from "@material-ui/icons/Sort";
import {useSelector} from "react-redux";
import {ENV} from "../../env";
import Popover from '@material-ui/core/Popover';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

const DocumentProfile = (props) => {


    const {id} = props;

    const AppState = useSelector(state => state);
    const [loading, setLoading] = useState(true);
    const [data, setData] = useState([]);
    const [ShowImg, setShowImg] = useState(false);
    const [img, setImg] = useState('')

    useEffect(() => {
        handleRequest();
    }, []);

    const handleRequest = () => {
        new Api().get('/marketers/profile/'+id+'/document').then((response) => {
            if (typeof response !== "undefined") {
                setData(response.data);
            }
            setLoading(false);
        })
    }

    const changeStatus = (type, status) => {
        setLoading(true);
        new Api().put('/marketers/profile/'+id+'/document/' + type, {status: status}).then((response) => {
            if (typeof response !== "undefined") {
                if (response.status) {
                    handleRequest();
                } else {
                    toast.error('خطایی رخ داده است');
                }
            }
        })
    }

    return(
        <React.Fragment>
            <Paper className={'profile-form'}>
                <div style={{ overflowX: 'auto'}}>
                    <table className='table'>
                        <thead>
                        <tr>
                            <th>ردیف</th>
                            <th>عنوان</th>
                            <th>عکس فعلی</th>
                            <th>عکس جایگزین</th>
                            <th>تاریخ ایجاد</th>
                            <th>به روز رسانی</th>
                            <th>وضعیت</th>
                            <th>عملیات</th>
                        </tr>
                        </thead>
                        {data.map((item, index) => {
                            return(
                                <tr>
                                    <td>{index + 1}</td>
                                    <td>{AppState.marketer.document && AppState.marketer.document[item.type]}</td>
                                    <td>{item.old_file ? <img style={{ cursor: 'pointer'}} onClick={() => {
                                        setImg(ENV["STORAGE"] + '/' + item.old_file);
                                        setShowImg(true);
                                    }} width={50} height={50} src={ENV["STORAGE"] + '/' + item.old_file} /> : '-'}</td>
                                    <td>{item.new_file ? <img style={{ cursor: 'pointer'}} onClick={() => {
                                        setImg(ENV["STORAGE"] + '/' + item.new_file);
                                        setShowImg(true);
                                    }} width={50} height={50} src={ENV["STORAGE"] + '/' + item.new_file} /> : '-'}</td>
                                    <td style={{ direction: 'ltr'}}>{moment(item.created_at).locale('fa').format('jYYYY/jMM/jDD HH:mm:ss')}</td>
                                    <td style={{ direction: 'ltr'}}>{moment(item.updated_at).locale('fa').format('jYYYY/jMM/jDD HH:mm:ss')}</td>
                                    <td style={{ direction: 'ltr'}}><b className={'status-' + item.status}>{AppState.marketer.document_status && AppState.marketer.document_status[item.status]}</b></td>
                                    <td>
                                        {item.status === 0 && <Button onClick={() => changeStatus(item.type, 1)} color={"primary"} size={"small"} variant={"outlined"}>تایید</Button>}&nbsp;
                                        {item.status !== 2 && <Button onClick={() => changeStatus(item.type, 2)}  color={"secondary"} size={"small"} variant={"outlined"}>عدم تایید</Button>}&nbsp;
                                        {item.status === 2 && <span>-</span>}&nbsp;
                                    </td>
                                </tr>
                            );
                        })}
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </Paper>
            <Dialog
                open={ShowImg}
                onClose={() => setShowImg(false)}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <img style={{ borderRadius: 0, maxHeight: 300}} src={img}/>
            </Dialog>
            {loading && <Loading />}
        </React.Fragment>
    );

}


export default memo(DocumentProfile);