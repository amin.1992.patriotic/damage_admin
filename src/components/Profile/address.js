import React, {memo, useEffect, useState} from "react";
import Loading from "../Loading";
import Api from "../../utils/api";
import AddressMemo from "../Address/list";
import Container from "@material-ui/core/Container";
import AddAddress from "../Address/add";
import Grid from "@material-ui/core/Grid";
const MarketerAddress = (props) => {

    const { id } = props;

    const [loading, setLoading] = useState(true);
    const [data, setData] = useState([]);

    useEffect(() => {
        handleRequest();
    }, []);

    const handleRequest = () => {

        new Api().get('/marketers/' + id + '/address').then((response) => {
            if (typeof response !== "undefined") {
                setData(response)
            }
            setLoading(false);
        })
    };



    return(
        <Container>
            {data.length > 0 && <AddressMemo data={data}  />}
            {loading && <Loading />}
            <AddAddress id={id} onComplete={() => handleRequest()}/>
        </Container>
    );

}


export default memo(MarketerAddress);