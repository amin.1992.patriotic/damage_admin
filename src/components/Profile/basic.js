import React, {memo, useCallback, useEffect, useState} from "react";
import {Box, Container, TextField} from "@material-ui/core";
import Loading from "../Loading";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import {toast} from "react-toastify";
import MenuItem from "@material-ui/core/MenuItem";
import {Paper} from "@material-ui/core";
import moment from "moment-jalaali";
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import IconButton from "@material-ui/core/IconButton";
import Api from "../../utils/api";
import validator from "validator";
const BasicProfile = (props) => {


    const {id} = props;
    const [loading, setLoading] = useState(true);
    const [image, setImage] = useState(null);
    const [form, setForm] = useState({
        name: '',
        family: '',
    })

    useEffect(() => {
        handleRequest();
    }, []);

    const handleRequest = () => {
        new Api().get('/marketers/profile/'+id+'/basic').then((response) => {
            if (typeof response !== "undefined") {
                let n_form = response.data;
                if (n_form.birth_date) {
                    let element = moment(response.data.birth_date).locale('fa').format('jYYYY-jMM-jDD');
                    n_form.day = element.split('-')[2];
                    n_form.month = element.split('-')[1];
                    n_form.year = element.split('-')[0];
                }

                setImage(response.data.pic);

                setForm({...n_form});
            }
            setLoading(false);
        })
    }


    const handleChangeElement = (event) => {
        let frm = form;
        frm[event.target.name] = event.target.value;
        setForm({...frm});
    };


    const handleSubmit = (event) => {

        event.preventDefault();

        setLoading(true);

        let n_form = form;

        n_form.birth_date = moment(form.year + '-' + form.month + '-' + form.day, 'jYYYY/jMM/jDD').format('YYYY/MM/DD');

        if (!validator.isDate(n_form.birth_date)) {
            n_form.birth_date = null
        }

        new Api().put('/marketers/profile/'+ id +'/basic', n_form).then((response) => {
            if (typeof response !== "undefined") {
                if (response.status === true) {
                    toast.success('تغییرات با موفقیت اعمال شد.')
                    handleRequest();
                } else {
                    setLoading(false);
                    toast.error(response.msg);
                }
            }

        })
    };


    return(
        <React.Fragment>
            <Paper className={'profile-form'}>
                <form onSubmit={handleSubmit}>
                    <Grid container spacing={2}>
                        <Grid item xs={12} >
                            <TextField
                                label="نام *"
                                variant="outlined"
                                margin='dense'
                                fullWidth
                                name='name'
                                onChange={handleChangeElement}
                                value={form.name}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        </Grid>
                        <Grid item xs={12} >
                            <TextField
                                label="نام خانوادگی *"
                                variant="outlined"
                                margin='dense'
                                fullWidth
                                name='family'
                                value={form.family}
                                onChange={handleChangeElement}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        </Grid>
                        <Grid item xs={12} >
                            <TextField
                                label="نام پدر *"
                                variant="outlined"
                                margin='dense'
                                fullWidth
                                name='father_name'
                                value={form.father_name !== 'NULL' ? form.father_name : ''}
                                onChange={handleChangeElement}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        </Grid>
                        <Grid item xs={12} sm={3} >
                            <TextField
                                label="نام وارث"
                                variant="outlined"
                                margin='dense'
                                fullWidth
                                name='heir_name'
                                value={form.heir_name !== 'NULL' ? form.heir_name : ''}
                                onChange={handleChangeElement}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        </Grid>
                        <Grid item xs={12} sm={3} >
                            <TextField
                                label="نام خانوادگی وارث"
                                variant="outlined"
                                margin='dense'
                                fullWidth
                                name='heir_last_name'
                                value={form.heir_last_name !== 'NULL' ? form.heir_last_name : ''}
                                onChange={handleChangeElement}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        </Grid>
                        <Grid item xs={12} sm={3} >
                            <TextField
                                label="کد ملی وارث"
                                variant="outlined"
                                margin='dense'
                                fullWidth
                                name='heir_national_code'
                                value={form.heir_national_code !== 'NULL' ? form.heir_national_code : ''}
                                onChange={handleChangeElement}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        </Grid>
                        <Grid item xs={12} sm={3}>
                            <TextField
                                label="نسبت وارث"
                                margin='dense'
                                size={"small"}
                                variant={"outlined"}
                                name={'heir_relation'}
                                select
                                fullWidth
                                value={form.heir_relation !== 'NULL' ? parseInt(form.heir_relation) : ''}
                                onChange={handleChangeElement}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            >
                                <MenuItem>انتخاب</MenuItem>
                                <MenuItem value={1}>مادر</MenuItem>
                                <MenuItem value={2}>پدر</MenuItem>
                                <MenuItem value={3}>همسر</MenuItem>
                                <MenuItem value={4}>فرزند</MenuItem>
                                <MenuItem value={5}>برادر</MenuItem>
                                <MenuItem value={6}>خواهر</MenuItem>
                            </TextField>
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                label="جنسیت *"
                                margin='dense'
                                size={"small"}
                                variant={"outlined"}
                                name={'gender'}
                                select
                                fullWidth
                                value={parseInt(form.gender)}
                                onChange={handleChangeElement}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            >
                                <MenuItem>انتخاب</MenuItem>
                                <MenuItem value={"1"}>مرد</MenuItem>
                                <MenuItem value={"0"}>زن</MenuItem>
                            </TextField>
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                label="سطح تحصیلات *"
                                margin='dense'
                                size={"small"}
                                variant={"outlined"}
                                name={'education'}
                                select
                                fullWidth
                                value={parseInt(form.education)}
                                onChange={handleChangeElement}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            >
                                <MenuItem value={4}>دکتری و بالاتر</MenuItem>
                                <MenuItem value={3}>کارشناسی ارشد</MenuItem>
                                <MenuItem value={2}>کارشناسی</MenuItem>
                                <MenuItem value={1}>دیپلم</MenuItem>
                                <MenuItem value={0}>دیپلم و پایین تر</MenuItem>
                            </TextField>
                        </Grid>
                        <Grid item xs={4}>
                            <TextField
                                label="روز تولد *"
                                margin='dense'
                                size={"small"}
                                variant={"outlined"}
                                name={'day'}
                                select
                                fullWidth
                                value={parseInt(form.day)}
                                onChange={handleChangeElement}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            >
                                <MenuItem>انتخاب</MenuItem>
                                {Array(31).fill().map((_, idx) => 0 + idx).map((item, index) => {
                                    return(
                                        <MenuItem value={item + 1}>{item + 1}</MenuItem>
                                    );
                                })}
                            </TextField>
                        </Grid>
                        <Grid item xs={4}>
                            <TextField
                                label="ماه تولد *"
                                margin='dense'
                                size={"small"}
                                variant={"outlined"}
                                name={'month'}
                                select
                                fullWidth
                                onChange={handleChangeElement}
                                value={parseInt(form.month)}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            >
                                <MenuItem>انتخاب</MenuItem>
                                {Array(12).fill().map((_, idx) => 0 + idx).map((item, index) => {
                                    return(
                                        <MenuItem value={item + 1}>{item + 1}</MenuItem>
                                    );
                                })}
                            </TextField>
                        </Grid>
                        <Grid item xs={4}>
                            <TextField
                                label="سال تولد *"
                                margin='dense'
                                size={"small"}
                                variant={"outlined"}
                                name={'year'}
                                select
                                fullWidth
                                value={parseInt(form.year)}
                                onChange={handleChangeElement}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            >
                                <MenuItem>انتخاب</MenuItem>
                                {Array(52).fill().map((_, idx) => 1381 - idx).map((item, index) => {
                                    return(
                                        <MenuItem value={item}>{item}</MenuItem>
                                    );
                                })}
                            </TextField>
                        </Grid>
                        <Grid item xs={12} >
                            <Button type={"submit"} disabled={loading} color={"primary"} variant={"contained"}>ویرایش اطلاعات</Button>
                        </Grid>
                    </Grid>
                </form>
            </Paper>
            {loading && <Loading />}
        </React.Fragment>
    );

}


export default memo(BasicProfile);