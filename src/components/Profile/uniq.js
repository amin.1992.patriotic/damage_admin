import React, {memo, useEffect, useState} from "react";
import {Container, TextField} from "@material-ui/core";
import Loading from "../Loading";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import {toast} from "react-toastify";
import MenuItem from "@material-ui/core/MenuItem";
import {Paper} from "@material-ui/core";
import moment from "moment-jalaali";
import Api from "../../utils/api";

const UniqueProfile = (props) => {


    const {id} = props;

    const [loading, setLoading] = useState(true);
    const [form, setForm] = useState({})

    useEffect(() => {
        handleRequest();
    }, []);

    const handleRequest = () => {

        new Api().get('/marketers/profile/'+id+'/unique').then((response) => {
            if (typeof response !== "undefined") {
                setForm(response.data)
            }
            setLoading(false);
        })
    }


    const handleChangeElement = (event) => {
        let frm = form;
        frm[event.target.name] = event.target.value;
        setForm({...frm});
    };


    const handleSubmit = (event) => {

        event.preventDefault();

        if (isNaN(form.mobile)) {
            toast.error('موبایل نامعتبر است');
            return;
        }

        if (isNaN(form.national_code)) {
            toast.error('کد ملی نامعتبر است');
            return;
        }
        setLoading(true);

        new Api().put('/marketers/profile/'+id+'/unique', form).then((response) => {
            if (typeof response !== "undefined") {
                if (response.status === true) {
                    toast.success('تغییرات با موفقیت اعمال شد.')
                    handleRequest();
                } else {
                    toast.error(response.msg);
                    setLoading(false);
                }

            }

        })
    };



    return(
        <React.Fragment>
            <Paper className={'profile-form'}>
                <form onSubmit={handleSubmit}>
                    <Grid container spacing={2}>
                        <Grid item xs={12} >
                            <TextField
                                label="نام کاربری *"
                                variant="outlined"
                                margin='dense'
                                fullWidth
                                name='username'
                                onChange={handleChangeElement}
                                value={form.username}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        </Grid>
                        <Grid item xs={12} >
                            <TextField
                                label="موبایل *"
                                variant="outlined"
                                margin='dense'
                                fullWidth
                                name='mobile'
                                value={form.mobile}
                                onChange={handleChangeElement}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        </Grid>
                        <Grid item xs={12} >
                            <TextField
                                label="ایمیل"
                                variant="outlined"
                                margin='dense'
                                fullWidth
                                name='email'
                                value={form.email !== 'NULL' ? form.email : ''}
                                onChange={handleChangeElement}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        </Grid>
                        <Grid item xs={12} >
                            <TextField
                                label="کد ملی *"
                                variant="outlined"
                                margin='dense'
                                fullWidth
                                name='national_code'
                                value={form.national_code !== 'NULL' ? form.national_code : ''}
                                onChange={handleChangeElement}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        </Grid>
                        <Grid item xs={12} >
                            <Button type={"submit"} disabled={loading} color={"primary"} variant={"contained"}>ویرایش اطلاعات</Button>
                        </Grid>
                    </Grid>
                </form>
            </Paper>
            {loading && <Loading />}
        </React.Fragment>
    );

}


export default memo(UniqueProfile);