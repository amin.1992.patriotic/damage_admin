import React, {memo, useEffect, useState} from "react";
import {TextField} from "@material-ui/core";
import Loading from "../Loading";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import {toast} from "react-toastify";

import {Paper} from "@material-ui/core";
import {useDispatch} from "react-redux";
import {useHistory} from "react-router";
import Api from "../../utils/api";

const PasswordProfile = (props) => {


    const {id} = props;

    const dispatch = useDispatch();
    const history = useHistory();
    const [loading, setLoading] = useState(false);
    const [form, setForm] = useState({})



    const handleChangeElement = (event) => {
        let frm = form;
        frm[event.target.name] = event.target.value;
        setForm({...frm});
    };


    const handleSubmit = (event) => {

        event.preventDefault();

        setLoading(true);

        new Api().put('/marketers/profile/'+id+'/password', form).then((response) => {
            if (typeof response !== "undefined") {
                if (response.status === true) {
                    toast.success('تغییرات با موفقیت اعمال شد.');
                } else {
                    toast.error(response.msg);
                }

                setLoading(false);
            }

        })
    };



    return(
        <React.Fragment>
            <Paper className={'profile-form'}>
                <form onSubmit={handleSubmit}>
                    <Grid container spacing={2}>
                        <Grid item xs={12} >
                            <TextField
                                label="رمز عبور جدید *"
                                variant="outlined"
                                margin='dense'
                                fullWidth
                                name='password'
                                onChange={handleChangeElement}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        </Grid>
                        <Grid item xs={12} >
                            <TextField
                                label="تکرار رمز *"
                                variant="outlined"
                                margin='dense'
                                fullWidth
                                name='password_confirmation'
                                onChange={handleChangeElement}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        </Grid>
                        <Grid item xs={12} >
                            <Button type={"submit"} disabled={loading} color={"primary"} variant={"contained"}>ویرایش اطلاعات</Button>
                        </Grid>
                    </Grid>
                </form>
            </Paper>
            {loading && <Loading />}
        </React.Fragment>
    );

}


export default memo(PasswordProfile);