import React, {memo, useEffect, useState} from "react";
import {Container, TextField} from "@material-ui/core";
import Loading from "../Loading";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import {toast} from "react-toastify";
import MenuItem from "@material-ui/core/MenuItem";
import {Paper} from "@material-ui/core";
import moment from "moment-jalaali";
import Api from "../../utils/api";

const BankProfile = (props) => {

    const {id} = props;

    const [loading, setLoading] = useState(true);
    const [form, setForm] = useState({})

    useEffect(() => {
        handleRequest();
    }, []);

    const handleRequest = () => {

        new Api().get('/marketers/profile/'+id+'/bank').then((response) => {
            if (typeof response !== "undefined") {
                setForm(response.data)
            }
            setLoading(false);
        })
    }


    const handleChangeElement = (event) => {
        let frm = form;
        frm[event.target.name] = event.target.value;
        setForm({...frm});
    };


    const handleSubmit = (event) => {

        event.preventDefault();

        if (isNaN(form.shaba)) {
            toast.error('شبا نامعتبر است');
            return;
        }

        if (isNaN(form.account_number)) {
            toast.error('شماره حساب نامعتبر است');
            return;
        }


        setLoading(true);

        new Api().put('/marketers/profile/'+id+'/bank', form).then((response) => {
            if (typeof response !== "undefined") {
                if (response.status === true) {
                    toast.success('تغییرات با موفقیت اعمال شد.')
                    handleRequest();
                } else {
                    toast.error(response.msg);
                    setLoading(false);
                }

            }

        })
    };



    return(
        <React.Fragment>
            <Paper className={'profile-form'}>
                <form onSubmit={handleSubmit}>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <TextField
                                select
                                label="بانک"
                                value={form.bank}
                                variant="outlined"
                                margin='dense'
                                fullWidth
                                name='bank'
                                value={form.bank ? form.bank : 'mellat'}
                                onChange={handleChangeElement}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            >
                                <MenuItem value={'mellat'}>ملت</MenuItem>
                                <MenuItem value={'parsian'}>پارسیان</MenuItem>
                                <MenuItem value={'saderat'}>صادرات</MenuItem>
                            </TextField>
                        </Grid>
                        <Grid item xs={12} >
                            <TextField
                                label="شماره شبا *"
                                variant="outlined"
                                margin='dense'
                                fullWidth
                                name='shaba'
                                onChange={handleChangeElement}
                                value={form.shaba !== 'NULL' && form.shaba !== 'null' ? form.shaba : ''}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                helperText={'بدون IR وارد کنید'}
                            />
                        </Grid>
                        <Grid item xs={12} >
                            <TextField
                                label="شماره حساب *"
                                variant="outlined"
                                margin='dense'
                                fullWidth
                                name='account_number'
                                value={form.account_number !== 'NULL' && form.account_number !== 'null' ? form.account_number : ''}
                                onChange={handleChangeElement}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        </Grid>
                        <Grid item xs={12} >
                            <TextField
                                label="شماره کارت"
                                variant="outlined"
                                margin='dense'
                                fullWidth
                                name='card_number'
                                value={form.card_number !== 'NULL' && form.card_number !== 'null' ? form.card_number : ''}
                                onChange={handleChangeElement}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                helperText={'این فیلد اختیاری است و جهت مطابقت با شماره شبا مورد استفاده قرار میگیرد.'}
                            />
                        </Grid>
                        <Grid item xs={12} >
                            <Button type={"submit"} disabled={loading} color={"primary"} variant={"contained"}>ویرایش اطلاعات</Button>
                        </Grid>
                    </Grid>
                </form>
            </Paper>
            {loading && <Loading />}
        </React.Fragment>
    );

}


export default memo(BankProfile);