import React, {useEffect, useState} from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import {Box} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import {Loading} from "../../index";
import Button from "@material-ui/core/Button";
import Api from "../../../utils/api";
import CurrencyFormat from "react-currency-format";
import DialogActions from "@material-ui/core/DialogActions";
import PrintSpecificFactor from "../Print/specificFactor";
import PrintSpecificLabel from "../Print/specificLabel";

const DepotOrderList = (props) => {

    const { id, onComplete } = props;

    const [open, setOpen] = useState(false);
    const [list, setList] = useState([]);
    const [loading, setLoading] = useState(false);

    const handleRequest = () => {
        setLoading(true);
        new Api().get('/depot/' + id + '/orders').then((response) => {
            if (typeof response !== "undefined") {
                setList(response);
            }

            setLoading(false);
        })
    }

    useEffect(() => {
        if (open) {
            handleRequest()
        }
    }, [open]);

    const handleDeleted = (order_id, deleted) => {
        setLoading(true);
        new Api().put('/depot/' + id + '/orders', {order_id: order_id, deleted :deleted}).then((response) => {
            if (typeof response !== "undefined") {
                setList(response);
                onComplete();
            }

            setLoading(false);
        })
    }

    return(
        <React.Fragment>
            <Button style={{ marginBottom: 5}} onClick={() => setOpen(true)} size={"small"} variant={"outlined"}>لیست سفارشات</Button>
            <Dialog
                fullScreen={true}
                open={open}
                keepMounted
                aria-labelledby="alert-dialog-slide-title"
                aria-describedby="alert-dialog-slide-description"
            >
                <DialogContent>
                    <Box mb={1}>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <div style={{ overflowX: 'auto'}}>
                                    <table style={{ margin: 0}} className='table-duplicate-row fadeIn'>
                                        <thead>
                                        <tr>
                                            <th>شماره سفارش</th>
                                            <th>کاربر</th>
                                            <th>شماره تماس</th>
                                            <th>مبلغ (تومان)</th>
                                            <th>فاکتور المثنی</th>
                                            <th>برچسب المثنی</th>
                                            <th>عملیات</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {list && list.map((item, index) => {
                                            return(
                                                <tr>
                                                    <td><CurrencyFormat value={item.order_id} displayType={'text'} thousandSeparator={true}  /></td>
                                                    <td>{item.name + ' ' + item.family}</td>
                                                    <td>{item.mobile}</td>
                                                    <td><CurrencyFormat value={item.total_pay} displayType={'text'} thousandSeparator={true}  /></td>
                                                    <td>
                                                        {item.factor === 0 ? <PrintSpecificFactor onComplete={() => handleRequest()} id={id} order_id={item.order_id} /> : 'فاکتور المثنی یکبار گرفته شده است.'}
                                                    </td>
                                                    <td>
                                                        {item.label === 0 ? <PrintSpecificLabel onComplete={() => handleRequest()} id={id} order_id={item.order_id} /> : 'برچسب المثنی یکبار گرفته شده است.'}
                                                    </td>
                                                    <td>
                                                        {item.deleted === 0 && <Button onClick={() => handleDeleted(item.order_id, item.deleted)} size={"small"} color={"secondary"} variant={"outlined"}>حذف</Button>}
                                                        {item.deleted === 1 && <Button onClick={() => handleDeleted(item.order_id, item.deleted)} size={"small"} color={"primary"} variant={"outlined"}>بازگردانی</Button>}
                                                    </td>
                                                </tr>
                                            );
                                        })}
                                        </tbody>
                                    </table>
                                </div>
                            </Grid>
                        </Grid>
                    </Box>
                    {loading && <Loading />}
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => setOpen(false)}>
                        بستن
                    </Button>
                </DialogActions>
            </Dialog>
        </React.Fragment>
    );
}

export default DepotOrderList;