import React, {useEffect} from "react";
import {Button} from "@material-ui/core";
import './print.css'
const AccumulativePrint = (props) => {

    const { data } = props;


    function PrintElem(elem)
    {
        var mywindow = window.open('', 'PRINT', 'height=100%,width=100%');

        mywindow.document.write('<html><head><title>' + document.title  + '</title>');
        mywindow.document.write('</head><body >');
        // mywindow.document.write('<h1>' + document.title  + '</h1>');
        mywindow.document.write(document.getElementById(elem).innerHTML);
        mywindow.document.write('</body></html>');

        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10*/

        mywindow.print();
        mywindow.close();

        return true;
    }

    return(
        <React.Fragment>
            <Button onClick={() => PrintElem('print')}>پرینت</Button>
            <div id={'print'} className={'div-to-print'}>
                <table style={{ margin: 0 }} className='table-duplicate-row'>
                    <thead>
                    <tr>
                        <th>ردیف</th>
                        <th>محصول</th>
                        <th>کد انبار</th>
                        <th>برند</th>
                        <th>تعداد</th>
                    </tr>
                    </thead>
                    <tbody>
                    {data.map((item, index) => {
                        return(
                            <tr>
                                <td>{index + 1}</td>
                                <td><b>{item.product_title}</b></td>
                                <td>{item.code}</td>
                                <td>{item.brand_title}</td>
                                <td>{item.counter}</td>
                            </tr>
                        );
                    })}
                    </tbody>
                </table>
            </div>

        </React.Fragment>

    );
}

export default AccumulativePrint