import React, {useEffect, useState, useRef} from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import {Box} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import {Loading} from "../../index";
import Button from "@material-ui/core/Button";
import Api from "../../../utils/api";
import DialogActions from "@material-ui/core/DialogActions";
import AccumulativeExcelDownload from "./excel";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import DialogTitle from "@material-ui/core/DialogTitle";
import AccumulativePrint from "./print";
import ReactToPrint from 'react-to-print';

const Accumulative = (props) => {

    const { id, depot, onComplete } = props;

    const [open, setOpen] = useState(false);
    const [list, setList] = useState([]);
    const [loading, setLoading] = useState(false);


    useEffect(() => {
        if (open) {
            setLoading(true);
            new Api().put('/depot/' + id + '/accumulative').then((response) => {
                if (typeof response !== "undefined") {
                    setList(response);
                    onComplete();
                }

                setLoading(false);
            })
        }
    }, [open]);


    return(

        <React.Fragment>
            <Button style={{ marginBottom: 5}} onClick={() => setOpen(true)} size={"small"} variant={"outlined"}>گزارش تجمیعی</Button>
            <Dialog
                fullScreen={true}
                open={open}
                keepMounted
                aria-labelledby="alert-dialog-slide-title"
                aria-describedby="alert-dialog-slide-description"
            >
                <AppBar>
                    <Toolbar className='display-flex display-flex-justify-content-space-between'>
                        <Typography variant={"button"} >
                            {'گزارش از فاکتورهای' + ' ' + depot.from_order + ' تا ' + depot.to_order + ' به تعداد ' + depot.count}
                        </Typography>
                        <IconButton edge="start" color="inherit" onClick={() => setOpen(false)} aria-label="close">
                            <CloseIcon />
                        </IconButton>
                    </Toolbar>
                </AppBar>
                <DialogTitle id="form-dialog-title">گزارش تجمیعی</DialogTitle>
                <DialogContent>
                    <div style={{ overflowX: 'auto'}}>
                        <table style={{ margin: 0 }} className='table-duplicate-row'>
                            <thead>
                            <tr>
                                <th>ردیف</th>
                                <th>محصول</th>
                                <th>کد انبار</th>
                                <th>برند</th>
                                <th>تعداد</th>
                            </tr>
                            </thead>
                            <tbody>
                            {list && list.map((item, index) => {
                                return(
                                    <tr>
                                        <td>{index + 1}</td>
                                        <td><b>{item.product_title}</b></td>
                                        <td>{item.code}</td>
                                        <td>{item.brand_title}</td>
                                        <td>{item.counter}</td>
                                    </tr>
                                );
                            })}
                            </tbody>
                        </table>
                    </div>
                    {loading && <Loading />}
                </DialogContent>
                <DialogActions>
                    <AccumulativeExcelDownload name={ depot.from_order + '_' + depot.to_order + '_' + depot.count} data={list} />
                    {/*<AccumulativePrint name={ depot.from_order + '_' + depot.to_order + '_' + depot.count} data={list} />*/}
                    <Button onClick={() => setOpen(false)}>
                        بستن
                    </Button>
                </DialogActions>
            </Dialog>
        </React.Fragment>
    );
}

export default Accumulative;