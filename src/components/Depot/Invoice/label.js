import React from "react";
import moment from "moment-jalaali";


const DepotLabel = (props) => {

    const { invoice } = props;

    return(
        <div style={{direction: 'rtl', fontFamily: 'Tahoma', fontSize: '13px', pageBreakAfter: "always", pageBreakInside: 'avoid', border: '2px solid #000' }}>
            <div>
                <div style={{display: 'flex', flexDirection: 'row',alignItems: 'center',borderBottom: '2px solid #000', justifyContent: 'center', padding: '20px 5px' }}>
                    <h1 style={{ fontSize: '14px'}}>شرکت کیهان کالا پارس</h1>
                </div>
                <div style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', borderBottom: '2px solid #000', padding: '10px 5px' }}>
                    <div>
                        <b>کد پستی فرستنده</b>:&nbsp;<span>14515447</span>
                    </div>
                    <div>
                        <b>تلفن:</b>&nbsp;<span style={{ direction:'ltr', textAlign: 'left'}}>02144900500</span>
                    </div>
                </div>
                <div style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', borderBottom: '2px solid #000', padding: '10px 5px' }}>
                    <div>
                        <b>گیرنده</b>:&nbsp;<span>{invoice && invoice.reciver_name}</span>
                    </div>
                    <div>
                        <b>کد ملی</b>:&nbsp;<span>{invoice && invoice.national_code}</span>
                    </div>
                    <div>
                        <b>تلفن</b>:&nbsp;<span>{invoice && invoice.reciver_mobile}</span>
                    </div>
                    <div>
                        <b>کد پستی</b>:&nbsp;<span>{invoice && invoice.postal_code}</span>
                    </div>
                </div>
                <div style={{display: 'flex', flexDirection: 'row', borderBottom: '2px solid #000', padding: '10px 5px'}}>
                    <div>
                        <b>موقعیت مکانی</b>:&nbsp;<span>{invoice && invoice.address_regions && JSON.parse(invoice.address_regions).map((item, index) => {
                        return (
                            item.title + (JSON.parse(invoice.address_regions).length - 1 !== index ? ' - ' : '')
                        )
                    })}</span>
                    </div>
                </div>
                <div style={{display: 'flex', flexDirection: 'row', borderBottom: '2px solid #000', padding: '10px 5px'}}>
                    <div>
                        <b>آدرس</b>:&nbsp;<span>{invoice && invoice.address_main}</span>
                    </div>
                </div>
                <div style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', borderBottom: '2px solid #000', padding: '10px 5px' }}>
                    <div>
                        <b>شماره سفارش</b>:&nbsp;<span>{invoice.order_id}</span>
                    </div>
                    <div>
                        <b>تاریخ سفارش:</b>:&nbsp;<span>{moment(invoice.created_at).locale('fa').format('jYYYY/jMM/jDD HH:mm')}</span>
                    </div>
                </div>
                <div style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', padding: '10px 5px' }}>
                    <div>
                        <b>روش ارسال</b>:&nbsp;
                        <span> پست  ( هزینه ارسال: طبق تعرفه پست)</span>
                    </div>
                    <div>-</div>
                </div>
            </div>
        </div>
    )
}

export default DepotLabel;