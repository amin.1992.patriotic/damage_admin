import React from "react";
import moment from "moment-jalaali";
import CurrencyFormat from "react-currency-format";

const DepotFactor = (props) => {

    const { invoice } = props;

    let product_count = 0;
    let product_weight = 0;

    return(
        <div style={{direction: 'rtl', fontFamily: 'Tahoma', fontSize: '13px', padding: '20px 0', pageBreakAfter: "always", pageBreakInside: 'avoid' }}>
            <div style={{  position: 'relative'}}>
                <div style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', 'marginBottom': '50px' }}>
                    <div>
                        <div style={{ margin : '10px 0'}} className="factor_time"><span>  تاریخ پرینت: </span><span>{moment().locale('fa').format('jYYYY/jMM/jDD HH:mm')}</span></div>
                        <div style={{ margin : '10px 0'}} className="factor_time"><span>  روش ارسال: </span><span>پستی</span></div>
                    </div>
                    <div style={{ textAlign: 'center'}}>
                        <h1 style={{ fontSize: '14px'}}>آنیک ام ال ام</h1>
                        <span>حواله تحویل کالا</span>
                    </div>
                    <div className="factor_header_right">
                        <div style={{ margin : '10px 0'}} className="factor_number"><span> شماره سفارش: </span><span>{invoice.order_id}</span></div>
                        <div style={{ margin : '10px 0'}} className="factor_time"><span>  تاریخ سفارش: </span><span>{moment(invoice.created_at, 'YYYY/MM/DD HH:mm').locale('fa').format('jYYYY/jMM/jDD HH:mm')}</span></div>
                    </div>
                </div>
                <div className="member_information">
                    <table className="table" cellSpacing="0" border="1" style={{ width: '100%', marginBottom: '50px'}}>
                        <tbody>
                        <tr>
                            <td><b>گیرنده</b>:&nbsp;<span>{invoice && invoice.reciver_name}</span></td>
                            <td><b>کد پستی</b>:&nbsp;<span>{invoice && invoice.postal_code}</span></td>
                            <td><b>شماره تماس</b>:&nbsp;<span>{invoice && invoice.reciver_mobile}</span></td>
                            <td><b>کد ملی</b>:&nbsp;<span>{invoice && invoice.national_code}</span></td>
                        </tr>
                        <tr>
                            <td colSpan="5"><b>موقعیت مکانی:</b>:&nbsp;<span>{invoice && invoice.address_regions && JSON.parse(invoice.address_regions).map((item, index) => {
                                    return (
                                        item.title + (JSON.parse(invoice.address_regions).length - 1 !== index ? ' - ' : '')
                                    )
                                })
                            }</span>
                            </td>
                        </tr>
                        <tr>
                            <td colSpan="5"><b>آدرس گیرنده</b>:&nbsp;<span>{invoice && invoice.address_main}</span></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <table border={1} cellSpacing="0" style={{ width: '100%', textAlign: 'center'}}>
                    <thead>
                    <th>ردیف</th>
                    <th>کد محصول</th>
                    <th>محصول</th>
                    <th>تعداد</th>
                    <th>وزن واحد</th>
                    <th>قیمت واحد</th>
                    <th>تخفیف واحد</th>
                    <th>وزن کل</th>
                    <th>قیمت با تخفیف</th>
                    <th>مالیات کل</th>
                    <th>قیمت کل </th>
                    </thead>
                    <tbody>
                    {invoice && JSON.parse(invoice.basket).map((item,index) => {
                        product_count += item.count;
                        product_weight += item.weight * item.count;
                        return (
                            <tr>
                                <td>{index + 1}</td>
                                <td>{item.code}</td>
                                <td>{item.title}</td>
                                <td>{item.count}</td>
                                <td>{item.weight}</td>
                                <td><CurrencyFormat value={item.price} displayType="text" thousandSeparator /></td>
                                <td><CurrencyFormat value={ item.discount} displayType="text" thousandSeparator />&nbsp;%</td>
                                <td><CurrencyFormat value={ item.count * item.weight} displayType="text" thousandSeparator />&nbsp;</td>
                                <td><CurrencyFormat value={item.count * item.off_price} displayType="text" thousandSeparator /></td>
                                <td><CurrencyFormat value={item.count * item.tax} displayType="text" thousandSeparator /></td>
                                <td><CurrencyFormat value={(item.off_price * item.count + item.count * item.tax)} displayType="text" thousandSeparator /></td>
                            </tr>
                        )
                    })}
                    <tr>
                        <td colSpan={3}><b>جمع:</b></td>
                        <td><b>{product_count}</b></td>
                        <td><b>-</b></td>
                        <td><b><CurrencyFormat value={invoice.price} displayType="text" thousandSeparator /></b></td>
                        <td>-</td>
                        <td>{product_weight}</td>
                        <td><b><CurrencyFormat value={invoice.off_price} displayType="text" thousandSeparator /></b></td>
                        <td><b><CurrencyFormat value={invoice.tax} displayType="text" thousandSeparator /></b></td>
                        <td><b><CurrencyFormat value={invoice.off_price + invoice.tax} displayType="text" thousandSeparator /></b></td>
                    </tr>
                    <tr>
                        <td style={{ borderLeft: '0'}} colSpan={5}><b>تخفیف خرده فروشی:</b>&nbsp;<span className={'span-info'} >(پایان دوره)</span></td>
                        <td style={{ borderRight: '0'}} colSpan={5}></td>
                        <td><b><CurrencyFormat value={invoice.retail_reward} displayType="text" thousandSeparator /></b></td>
                    </tr>
                    <tr>
                        <td style={{ borderLeft: '0'}} colSpan={5}><b>هزینه ارسال:</b></td>
                        <td style={{ borderRight: '0'}} colSpan={5}></td>
                        <td><b><CurrencyFormat value={invoice.post_cost} displayType="text" thousandSeparator /></b></td>
                    </tr>
                    <tr>
                        <td style={{ borderLeft: '0'}} colSpan={5}><b>جمع کل:</b></td>
                        <td style={{ borderRight: '0'}} colSpan={5}></td>
                        <td><b><CurrencyFormat value={invoice.total_pay} displayType="text" thousandSeparator /></b></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default DepotFactor