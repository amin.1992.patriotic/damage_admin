import React, {useEffect, useState, memo} from "react";
import {Button} from "@material-ui/core";
import Api from "../../../utils/api";
import DepotFactor from "../Invoice/factor";

const PrintFactor = (props) => {

    const { id, onComplete } = props;
    const [loading, setLoading] = useState(true);
    const [data, setData] = useState([]);

    useEffect(() => {

        if (!loading) {

            var mywindow = window.open(id, 'PRINT', 'height='+ window.outerHeight+',width='+ window.outerWidth);

            mywindow.document.write('<html><head><title>' + document.title  + '</title>');
            mywindow.document.write('</head><body >');
            // mywindow.document.write('<h1>' + document.title  + '</h1>');
            mywindow.document.write(document.getElementById('print-' + id).innerHTML);
            mywindow.document.write('</body></html>');

            mywindow.document.close(); // necessary for IE >= 10
            mywindow.focus(); // necessary for IE >= 10*/

            mywindow.print();
            mywindow.close();
        }

    }, [loading]);

    const handleRequested = () => {

        setLoading(true);

        new Api().get('/depot/' + id + '/accumulative/orders/factor').then((response) => {
            if (typeof response !== "undefined") {
                setData(response);
                setLoading(false);
                onComplete();
            }
        })
    }

    return(
        <React.Fragment>
            <Button style={{ marginBottom: 5}} size={"small"} variant={"outlined"} onClick={() => handleRequested()}>پرینت فاکتور</Button>
            <span id={'print-' + id} style={{ display: 'none'}}>
                {data.map((item) => {
                    return <DepotFactor invoice={item} />
                })}
            </span>
        </React.Fragment>
    );
}


export default PrintFactor;