import React, {useEffect, useState, memo} from "react";
import {Button} from "@material-ui/core";
import Api from "../../../utils/api";
import DepotFactor from "../Invoice/factor";
import PrintIcon from '@material-ui/icons/Print';
import {toast} from "react-toastify";
import {Loading} from "../../index";
import moment from "moment-jalaali";
import DepotLabel from "../Invoice/label";
import LabelIcon from '@material-ui/icons/Label';
const ArrayFactor = (props) => {

    const { ids, object, is_label } = props;
    const [loading, setLoading] = useState(false);
    const [entities, setEntities] = useState([]);

    let time = moment().unix();

    useEffect(() => {

        if (!loading && entities.length > 0) {

            var mywindow = window.open('', 'PRINT', 'height='+ window.outerHeight+',width='+ window.outerWidth);

            mywindow.document.write('<html><head><title>' + document.title  + '</title>');
            mywindow.document.write('</head><body >');
            // mywindow.document.write('<h1>' + document.title  + '</h1>');
            mywindow.document.write(document.getElementById('id_' + time).innerHTML);
            mywindow.document.write('</body></html>');

            mywindow.document.close(); // necessary for IE >= 10
            mywindow.focus(); // necessary for IE >= 10*/

            mywindow.print();
            mywindow.close();
        }

    }, [loading, entities]);

    useEffect(() => {
        return () => {
            setEntities([]);
        }
    }, []);

    const handleRequested = () => {

        if (ids.length === 0) {

            setLoading(true);

            object.print = 1;
            new Api().get('/orders', object).then((response) => {
                if (typeof response !== "undefined") {
                    setEntities(response);
                    setLoading(false);
                }
            })

        } else {
            setLoading(true);

            new Api().get('/orders/' + ids.join(',') + '/array').then((response) => {
                if (typeof response !== "undefined") {
                    setEntities(response);
                    setLoading(false);
                }
            })
        }


    }

    return(
        <React.Fragment>
            {typeof is_label !== undefined && is_label === true ? <LabelIcon onClick={() => handleRequested()}  /> : <PrintIcon onClick={() => handleRequested()} />}
            <span id={'id_' + time} style={{ display: 'none'}}>
            {entities.map((item) => {
                return typeof is_label !== undefined && is_label === true ? <DepotLabel invoice={item}/> : <DepotFactor invoice={item} />
            })}
            </span>
            {loading && <Loading/>}
        </React.Fragment>
    );
}


export default ArrayFactor;