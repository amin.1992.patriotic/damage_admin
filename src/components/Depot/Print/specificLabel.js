import React, {useEffect, useState, memo} from "react";
import {Button} from "@material-ui/core";
import Api from "../../../utils/api";
import DepotLabel from "../Invoice/label";
import DepotFactor from "../Invoice/factor";
import moment from "moment-jalaali";
const PrintSpecificLabel = (props) => {

    const { id, order_id, onComplete } = props;
    const [loading, setLoading] = useState(true);
    const [data, setData] = useState([]);

    const time = moment().add(60, 'seconds').unix()

    useEffect(() => {

        if (!loading) {

            var mywindow = window.open('', 'PRINT', 'height='+ window.outerHeight+',width='+ window.outerWidth);

            mywindow.document.write('<html><head><title>' + document.title  + '</title>');
            mywindow.document.write('</head><body>');
            mywindow.document.write(document.getElementById('id_' + time).innerHTML);
            mywindow.document.write('</body></html>');

            mywindow.document.close(); // necessary for IE >= 10
            mywindow.focus(); // necessary for IE >= 10*/

            mywindow.print();
            mywindow.close();

        }

    }, [loading]);

    const handleRequested = () => {

        setLoading(true);

        new Api().get('/depot/' + id + '/orders/' + order_id + '/label').then((response) => {
            if (typeof response !== "undefined") {
                setData(response);
                setLoading(false);

                onComplete()
            }
        })
    }


    return(
        <React.Fragment>
            <Button size={"small"} variant={"outlined"}
                    onClick={() => handleRequested()}
            >پرینت فاکتور</Button>
            <span id={'id_' + time} style={{ display: 'none'}}>
                         {data.map((item) => {
                             return (
                                 <div className={'print'}>
                                     <DepotLabel invoice={item} />
                                 </div>
                             )
                         })}

            </span>
        </React.Fragment>
    );
}


export default PrintSpecificLabel;
