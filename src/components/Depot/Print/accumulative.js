import React, {useEffect, useState, memo} from "react";
import {Button, Tooltip} from "@material-ui/core";
import Api from "../../../utils/api";
import DepotFactor from "../Invoice/factor";
import PrintIcon from '@material-ui/icons/Print';
import {toast} from "react-toastify";
import {Loading} from "../../index";
import moment from "moment-jalaali";
import IconButton from "@material-ui/core/IconButton";
import SyncIcon from "@material-ui/icons/Sync";
import AssessmentIcon from '@material-ui/icons/Assessment';

const PrintAccumlative = (props) => {

    const { ids, object } = props;
    const [loading, setLoading] = useState(false);
    const [entities, setEntities] = useState([]);

    let time = moment().unix();

    useEffect(() => {

        if (!loading && entities.length > 0) {

            var mywindow = window.open('', 'PRINT', 'height='+ window.outerHeight+',width='+ window.outerWidth);

            mywindow.document.write('<html><head><title>' + document.title  + '</title>');
            mywindow.document.write('</head><body >');
            // mywindow.document.write('<h1>' + document.title  + '</h1>');
            mywindow.document.write(document.getElementById('id_' + time).innerHTML);
            mywindow.document.write('</body></html>');

            mywindow.document.close(); // necessary for IE >= 10
            mywindow.focus(); // necessary for IE >= 10*/

            mywindow.print();
            mywindow.close();
        }

    }, [loading, entities]);

    useEffect(() => {
        return () => {
            setEntities([]);
        }
    }, []);

    const handleRequested = () => {

        if (ids.length === 0) {

            setLoading(true);

            object.cumulative = 1;
            new Api().get('/orders', object).then((response) => {
                if (typeof response !== "undefined") {
                    setEntities(response);
                    setLoading(false);
                }
            })

        } else {
            setLoading(true);

            object.ids = ids.join(',');
            object.cumulative = 1;
            new Api().get('/orders', object).then((response) => {
                if (typeof response !== "undefined") {
                    setEntities(response);
                    setLoading(false);
                }
            })
        }


    }

    return(
        <React.Fragment>
            <Tooltip title="گزارش تجمیعی" onClick={() => handleRequested()}>
                <IconButton>
                    <AssessmentIcon />
                </IconButton>
            </Tooltip>
            <span id={'id_' + time} style={{ display: 'none'}}>
                <div style={{direction: 'rtl', fontFamily: 'Tahoma', fontSize: '13px', padding: '20px 0', pageBreakAfter: "always", pageBreakInside: 'avoid' }}>
                  <table border={1} cellSpacing="0" style={{ width: '100%', textAlign: 'center'}}>
                    <thead>
                    <tr>
                        <th>ردیف</th>
                        <th>محصول</th>
                        <th>کد انبار</th>
                        <th>برند</th>
                        <th>تعداد</th>
                    </tr>
                    </thead>
                    <tbody>
                        {entities.map((item, index) => {
                            return(
                                <tr>
                                    <td>{index + 1}</td>
                                    <td><b>{item.product_title}</b></td>
                                    <td>{item.code}</td>
                                    <td>{item.brand_title}</td>
                                    <td>{item.counter}</td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
                </div>
            </span>
            {loading && <Loading/>}
        </React.Fragment>
    );
}


export default PrintAccumlative;