import React, {useEffect, memo} from "react";
import {ENV} from "../../env";

const ReCaptcha = (props) => {

    const {onComplete, regenerate} = props;

    useEffect(() => {

        if (regenerate) {
            const loadScriptByURL = (id, url, callback) => {
                const isScriptExist = document.getElementById(id);

                if (!isScriptExist) {
                    var script = document.createElement("script");
                    script.type = "text/javascript";
                    script.src = url;
                    script.id = id;
                    script.onload = function () {
                        if (callback) callback();
                    };
                    document.body.appendChild(script);
                }

                if (isScriptExist && callback) callback();
            }

            // load the script by passing the URL
            loadScriptByURL("recaptcha-key", `https://www.google.com/recaptcha/api.js?render=${ENV["captcha"]}`, function () {
                window.grecaptcha.ready(() => {
                    window.grecaptcha.execute(ENV["captcha"], { action: 'submit' })
                        .then((token) => {
                            onComplete(token)
                        });
                });
            });
        }

    }, [regenerate]);

    return null;
}


export default memo(ReCaptcha);