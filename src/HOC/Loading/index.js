import React from "react";
import {CircularProgress} from "@material-ui/core";
import './style.css';

const WithLoading = (Component) => {

    return function({ loading, ...props }) {
        if ( loading ) return <CircularProgress className={'loading'} color='secondary' />
        return <Component {...props} />
    }

}


export default WithLoading;