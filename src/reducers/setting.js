import {FETCH_SETTING, OPEN_SETTING_DIALOG, OPEN_SETTING_LINKS_DIALOG, STICKY_SETTING} from "../types";


export const initialState = {
    OpenSettingDialog: false,
    OpenDomainLinksDialog: false,
    StickySetting: [],
    setting: {},
    DomainLinks: [],

};

export default (state = initialState, action) => {
    switch (action.type) {
        case OPEN_SETTING_DIALOG:
            return {
                ...state,
                OpenSettingDialog: action.payload
            }
        case OPEN_SETTING_LINKS_DIALOG:
            return {
                ...state,
                OpenDomainLinksDialog: action.payload
            }
        case STICKY_SETTING:
            return {
                ...state,
                StickySetting: action.payload
            }
        case FETCH_SETTING:
            return {
                ...state,
                setting: action.payload
            }
        default:
            return state;
    }
};
