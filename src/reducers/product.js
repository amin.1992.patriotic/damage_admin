import {FETCH_PRODUCTS} from "../types";


export const initialState = {
    entities: [],
};

export default (state = initialState, action) => {
    switch (action.type) {
        case FETCH_PRODUCTS:
            return {
                ...state,
                entities: action.payload
            };
        default:
            return state;
    }
};
