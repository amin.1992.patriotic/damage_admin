// Export for unit testing

import {FETCH_NODES, TOGGLE_CHECKED, TOGGLE_EXPANDED} from "../types";

export const initialState = {
    loading: true,
    expanded:[],
    checked:[],
    nodes:[],
    menu_types : {
        header: 'هدر سایت',
        footer: 'فوتر سایت'
    }
};

export default (state = initialState, action) => {
    switch (action.type) {
        case TOGGLE_EXPANDED:
            return {
                ...state,
                expanded: action.payload
            };
            break;
        case TOGGLE_CHECKED:
            return {
                ...state,
                checked: action.payload
            };
            break;
        case FETCH_NODES:
            return {
                ...state,
                nodes: action.payload
            };
        default:
            return state;
    }
};
