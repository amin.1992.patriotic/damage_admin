import {PERIOD_FAILURE, PERIOD_REQUESTING, PERIOD_SUCCESS} from "../types";

export const initialState = {
    ready: 'invalid',
    data: [],
};

export default (state = initialState, action) => {
    switch (action.type) {
        case PERIOD_REQUESTING:
            return {
                ...state,
                ready: 'request'
            };
        case PERIOD_SUCCESS:
            return {
                ...state,
                ready: 'success',
                data: action.payload,
            };
        case PERIOD_FAILURE:
            return {
                ...state,
                ready: 'failure',
                err: action.err
            };
        default:
            return state;
    }
};
