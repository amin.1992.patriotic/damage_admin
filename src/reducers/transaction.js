import moment from "moment-jalaali";
import {TRANSACTIONS_FAILURE, TRANSACTIONS_REQUESTING, TRANSACTIONS_SUCCESS} from "../types";
export const initialState = {
    ready: 'invalid',
    err: null,
    entities: [],
    type: {
        "order" : 'سفارش',
        "maintain" : 'شارژ کیف پول مینتین',
        "wallet": 'کیف پول'
    },
    gateway:{
        null: 'بدون درگاه',
        "parsian" : 'پارسیان',
        "zarinpal" : 'زرین پال',
        "saderat" : 'صادرات',
    },
    status: {
        'pending' : 'در انتظار',
        'success': 'موفقیت آمیز',
        'failed': 'خطا'
    }
};

export default (state = initialState, action) => {
    switch (action.type) {
        case TRANSACTIONS_REQUESTING:
            return {
                ...state,
                ready: 'request'
            };
        case TRANSACTIONS_SUCCESS:
            return {
                ...state,
                ready: 'success',
                entities: action.payload,
            };
        case TRANSACTIONS_FAILURE:
            return {
                ...state,
                ready: 'failure',
                err: action.err
            };
        default:
            return state;
    }
};
