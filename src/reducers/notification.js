import {NOTIFICATION_FAILURE, NOTIFICATION_REQUESTING, NOTIFICATION_SUCCESS} from "../types";

export const initialState = {
    ready: 'invalid',
    err: null,
    entities: [],
    status: {
        "0" : 'در انتظار',
        "1" : 'موفقیت آمیز',
        "2" : 'خطا'
    },
    template: {
        'OrderCreated': "ثبت سفارش در انتظار",
        'OrderConfirmation': "خرید موفق",
        'OrderTracking': 'وضعیت سفارشات',
        'Welcome': "خوش آمد گویی",
        'verify': "تایید موبایل",
        'TicketAnswer': "پاسخ تیکت",
        'AdminReportSales': "گزارش ادمین",
        'AdminReportProduct': "گزارش ادمین - محصولات",
        'AdminReportOther': "گزارش ادمین - سایر",
        'ProgrammerReport': "گزارش برنامه نویس",
        'AdminTicketCreated': "ایجاد تیکت در پنل مدیریت",
        'RequestSubmit': "ارسال درخواست لغو و ...",
        'DocumentNotVerify': "عدم تایید مدارک",
        'Contract': "قرارداد",
        'MarketerCode': "دریافت کد وزارت",
        'WalletCredit' : 'شارژ کیف پول',
        'Commission': 'واریز پورسانت',
    }
};

export default (state = initialState, action) => {
    switch (action.type) {
        case NOTIFICATION_REQUESTING:
            return {
                ...state,
                ready: 'request'
            };
        case NOTIFICATION_SUCCESS:
            return {
                ...state,
                ready: 'success',
                entities: action.payload,
            };
        case NOTIFICATION_FAILURE:
            return {
                ...state,
                ready: 'failure',
                err: action.err
            };
        default:
            return state;
    }
};
