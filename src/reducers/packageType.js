// Export for unit testing
import {CURRENT_PACKAGE_TYPE, FETCH_PACKAGE_TYPES} from "../types";

export const initialState = {
    entity: null,
    entities: []
};

export default (state = initialState, action) => {
    switch (action.type) {
        case FETCH_PACKAGE_TYPES:
            return {
                ...state,
                entities: action.payload
            };
        case CURRENT_PACKAGE_TYPE:
            return {
                ...state,
                entity: action.payload
            };
        default:
            return state;
    }
};
