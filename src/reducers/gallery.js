// Export for unit testing

import {FETCH_GALLERY} from "../types";

export const initialState = {
    loading: true,
    data: [],
    show_in: {
        web: 'سایت',
        app: 'اپ',
        both: 'هر دو',
    }
};

export default (state = initialState, action) => {
    switch (action.type) {
        case FETCH_GALLERY:
            return {
                ...state,
                data: action.payload
            };
        default:
            return state;
    }
};
