import {CURRENT_ITEM_PRODUCT, FETCH_ITEM_PRODUCT} from "../types";

export const initialState = {
    entities: [],
    entity: null,
};

export default (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ITEM_PRODUCT:
            return {
                ...state,
                entities: action.payload
            };
        case CURRENT_ITEM_PRODUCT:
            return {
                ...state,
                entity: action.payload
            };
        default:
            return state;
    }
};
