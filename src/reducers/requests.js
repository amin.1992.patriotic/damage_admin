import {REQUESTS_FAILURE, REQUESTS_REQUESTING, REQUESTS_SUCCESS} from "../types";

export const initialState = {
    ready: 'invalid',
    err: null,
    entities: [],
    status: {
        "0" : 'در انتظار',
        "1" : 'تایید شده',
        "2" : 'رد شده',
    },
};

export default (state = initialState, action) => {
    switch (action.type) {
        case REQUESTS_REQUESTING:
            return {
                ...state,
                ready: 'request'
            };
        case REQUESTS_SUCCESS:
            return {
                ...state,
                ready: 'success',
                entities: action.payload,
            };
        case REQUESTS_FAILURE:
            return {
                ...state,
                ready: 'failure',
                err: action.err
            };
        default:
            return state;
    }
};
