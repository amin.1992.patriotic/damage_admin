import {FINANCE_FAILURE, FINANCE_REQUESTING, FINANCE_SUCCESS} from "../types";

export const initialState = {
    ready: 'invalid',
    err: null,
    entities: [],
    type: {
        "voucher" : 'بن غیرنقدی کالا',
        "wallet" : 'کیف پول'
    },
    value: {
        "debtor" : 'بدهکار',
        "creditor" : 'بستانکار'
    },
};

export default (state = initialState, action) => {
    switch (action.type) {
        case FINANCE_REQUESTING:
            return {
                ...state,
                ready: 'request'
            };
        case FINANCE_SUCCESS:
            return {
                ...state,
                ready: 'success',
                entities: action.payload,
            };
        case FINANCE_FAILURE:
            return {
                ...state,
                ready: 'failure',
                err: action.err
            };
        default:
            return state;
    }
};
