import {ORDERS_FAILURE, ORDERS_REQUESTING, ORDERS_SUCCESS} from "../types";

export const initialState = {
    ready: 'invalid',
    err: null,
    entities: [],
    status: {
        '0' : 'در انتظار',
        '1': 'تایید شده',
        '2': 'منقضی',
    },
    paid: {
        '0' : 'بدون پرداخت',
        '1': 'پرداخت شده',
    },
    transport: {
        '0' : 'پیش فرض',
        '1': 'خروج از انبار',
        '2': 'در حال آماده سازی',
    },
    delivery: {
        '0' : 'پیش فرض',
        '1': 'تحویل پست',
        '2': 'تحویل مشتری',
    },
    returned: {
        '0' : 'پیش فرض',
        '1': 'استرداد',
        '2': 'بای بک',
    }
};

export default (state = initialState, action) => {
    switch (action.type) {
        case ORDERS_REQUESTING:
            return {
                ...state,
                ready: 'request'
            };
        case ORDERS_SUCCESS:
            return {
                ...state,
                ready: 'success',
                entities: action.payload,
            };
        case ORDERS_FAILURE:
            return {
                ...state,
                ready: 'failure',
                err: action.err
            };
        default:
            return state;
    }
};
