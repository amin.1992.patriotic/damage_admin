import {MARKETER_FAILURE, MARKETER_REQUESTING, MARKETER_SUCCESS} from "../types";

export const initialState = {
    ready: 'invalid',
    err: null,
    response: [],
    status: {
        "0": 'غیرفعال',
        "1": 'فعال',
        "2": 'تعلیق'
    },
    document_status: {
        '0' : 'در انتظار',
        '1' : 'تایید شده',
        '2' : 'رد شده'
    },
    document: {
        'pic' : 'عکس پروفایل',
        'national_card' : 'کارت ملی',
        'birth_certificate' : 'شناسنامه'
    }
};

export default (state = initialState, action) => {
    switch (action.type) {
        case MARKETER_REQUESTING:
            return {
                ...state,
                ready: 'request'
            };
        case MARKETER_SUCCESS:
            return {
                ...state,
                ready: 'success',
                response: action.payload
            };
        case MARKETER_FAILURE:
            return {
                ...state,
                ready: 'failure',
                err: action.err
            };
        default:
            return state;
    }
};
