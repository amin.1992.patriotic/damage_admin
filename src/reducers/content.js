import {FETCH_CONTENT, FETCH_CONTENTS} from "../types";


export const initialState = {
    entities: [],
    entity: null,
    flag: {
        "1" : 'بله',
        "0" : 'خیر'
    }
};

export default (state = initialState, action) => {
    switch (action.type) {
        case FETCH_CONTENTS:
            return {
                ...state,
                entities: action.payload
            };
        case FETCH_CONTENT:
            return {
                ...state,
                entity: action.payload
            };
        default:
            return state;
    }
};
