import {FETCH_TICKET, FETCH_TICKETS} from "../types";


export const initialState = {
    loading: true,
    entities: [],
    entity: null,
    status: {
        'open': 'در انتظار پاسخ',
        'answered': 'پاسخ داده شده',
        'pending': 'در حال بررسی',
        'close': 'بسته شده',
    }
};

export default (state = initialState, action) => {
    switch (action.type) {
        case FETCH_TICKETS:
            return {
                ...state,
                entities: action.payload
            };
        case FETCH_TICKET:
            return {
                ...state,
                entity: action.payload
            };
        default:
            return state;
    }
};
