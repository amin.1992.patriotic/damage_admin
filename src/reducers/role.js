// Export for unit testing

import {FETCH_ROLES, SET_ROLE} from "../types";

export const initialState = {
    loading: true,
    role: null,
    data: []
};

export default (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ROLES:
            return {
                ...state,
                data: action.payload
            };
        case SET_ROLE:
            return {
                ...state,
                role: action.payload
            };
        default:
            return state;
    }
};
