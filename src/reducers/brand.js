// Export for unit testing


import {CURRENT_BRAND, FETCH_BRANDS} from "../types";

export const initialState = {
    entity: null,
    entities: []
};

export default (state = initialState, action) => {
    switch (action.type) {
        case FETCH_BRANDS:
            return {
                ...state,
                entities: action.payload
            };
        case CURRENT_BRAND:
            return {
                ...state,
                entity: action.payload
            };
        default:
            return state;
    }
};
