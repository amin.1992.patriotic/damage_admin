// Export for unit testing
// @ts-ignore
import moment from 'moment-jalaali';
// @ts-ignore
import Cookies from "universal-cookie";

import {AUTH_REFRESH, LOGIN, LOGOUT} from "../types";

export const initialState = {
    login: false,
    token: null,
    user: null,
    expiration: moment().unix()
};

const cookies = new Cookies();

export default (state = initialState, action) => {
    switch (action.type) {
        case LOGIN:
            cookies.set('auth', action.payload.token, { path: '/', expires: moment().add(1, "year").toDate() });
            return {
                ...state,
                login: true,
                user: action.payload.user,
                token: action.payload.token,
                expiration: moment().add(1, 'hours').unix(),
            };
        case LOGOUT:
            cookies.remove('auth');
            return {
                ...state,
                user: null,
                token: null,
                login: false
            };
        case AUTH_REFRESH:
            return {
                ...state,
                user: action.payload,
                expiration: moment().add(1, 'hours').unix(),
            };
        default:
            return state;
    }
};
