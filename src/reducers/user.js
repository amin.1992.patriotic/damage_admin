// Export for unit testing
import {USER_FETCH, USERS_FAILURE, USERS_REQUESTING, USERS_SUCCESS} from "../types";

export const initialState = {
    status: 'invalid',
    err: null,
    users: [],
    user: null
};

export default (state = initialState, action) => {
    switch (action.type) {
        case USERS_REQUESTING:
            return {
                ...state,
                status: 'request'
            };
        case USERS_SUCCESS:
            return {
                ...state,
                status: 'success',
                users: action.payload
            };
        case USERS_FAILURE:
            return {
                ...state,
                status: 'failure',
                err: action.err
            };
        case USER_FETCH:
            return {
                ...state,
                user: action.payload
            };
        default:
            return state;
    }
};
