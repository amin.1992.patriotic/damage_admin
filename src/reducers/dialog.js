import {DIALOG_PAYLOAD} from "../types";

export const initialState = {
    open: false,
    dType: ''
};

export default (state = initialState, action) => {
    switch (action.type) {
        case DIALOG_PAYLOAD:
            return {
                ...state,
                open: action.payload,
                dType: action.dType
            };
        default:
            return state;
    }
};
