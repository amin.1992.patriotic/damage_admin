// Export for unit testing


import {CURRENT_ATTRIBUTE, FETCH_ATTRIBUTES} from "../types";

export const initialState = {
    entity: null,
    entities: []
};

export default (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ATTRIBUTES:
            return {
                ...state,
                entities: action.payload
            };
        case CURRENT_ATTRIBUTE:
            return {
                ...state,
                entity: action.payload
            };
        default:
            return state;
    }
};
