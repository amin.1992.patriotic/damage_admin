import {REWARD_FAILURE, REWARD_REQUESTING, REWARD_SUCCESS} from "../types";

export const initialState = {
    ready: 'invalid',
    err: null,
    entities: [],
    status: {
        "0" : 'در انتظار',
        "1" : 'پرداخت شده',
    },
    type: {
        "retail" : 'خرده فروشی',
         "reagent" : 'معرفان',
        "reward_as_reward": 'پورسانت از پورسانت'
    },
    reason: {
        "1" : "عدم خرید ماهانه",
        "2" : "نداشتن یا عدم درج شماره شبا",
        "3" : "نداشتن کد بازاریابی",
        "4" : "عدم ثبت قرارداد پشتیبانی",
    }
};

export default (state = initialState, action) => {
    switch (action.type) {
        case REWARD_REQUESTING:
            return {
                ...state,
                ready: 'request'
            };
        case REWARD_SUCCESS:
            return {
                ...state,
                ready: 'success',
                entities: action.payload,
            };
        case REWARD_FAILURE:
            return {
                ...state,
                ready: 'failure',
                err: action.err
            };
        default:
            return state;
    }
};
