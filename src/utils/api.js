import axios from 'axios';
import { toast } from 'react-toastify';
import Cookies from 'universal-cookie/lib';
import moment from "moment-jalaali";
import {ENV} from "../env";
let CryptoJS = require("crypto-js");

const cookies = new Cookies();

class Api {


    _headers() {

        let token = '';

        if (cookies.get('auth')) {
            token = cookies.get('auth');
        }

        return {
            Accept: 'application/json',
            Authorization: `Bearer ${token}`
        };
    }

    // check response after receive
    _dispatchResponse(response) {
        if (typeof response !== 'undefined') {
            if (response.status === 401) {
                cookies.remove('auth');
                if (window.location.pathname !== '/') {
                    setTimeout(() => {
                        window.location.href = '/'
                    }, 1000);
                }
            } else if (response.status === 404) {
                toast.error('خطای سرور: آدرس وجود ندارد و یا سرور دان است.');
            } else if(response.status === 400) {
                toast.error('ساعت سیستم خود را به روی Asia/Tehran تنظیم نمایید');
                toast.error('عدم دسترسی')
            } else {
                toast.error(response.statusText);
            }
        } else {
            toast.error('خطای سرور');
        }
    }


    _recercive(object)
    {

        let n_object = '';
        let keys = Object.keys(object);

        keys.forEach((item, index) => {
            if (typeof object[item] === 'object' && object[item] !== null) {
                if (object[item].length === undefined) { // {}
                    n_object += item + ':' + JSON.stringify(object[item]) + ';'
                } else { // [1,2,3] or [{}, {}, {}]
                    n_object += item + ':[' + object[item] + '];'
                }

            } else {
                n_object += item + ':' + (object[item] ? object[item] : null) + ';'
            }

        });


        return n_object;
    }

    _makeRequest(object) {

        let n_object = 'random:' + Math.random() + ';' +'time:' + moment().add(60, 'seconds').unix()  + ';' + 'origin:' + window.location.host + ';'

        n_object += this._recercive(object)

        let key = CryptoJS.enc.Hex.parse(ENV["k"]);
        let iv = CryptoJS.enc.Hex.parse(ENV["i"]);


        console.log(n_object);

        let encrypted = CryptoJS.AES.encrypt(n_object, key, {
            iv,
            padding: CryptoJS.pad.ZeroPadding,
        });

        return encrypted.toString();
    }

    /**
     * ---------------------------------------------------------------
     *  GET POST PUT DELETE API
     * ---------------------------------------------------------------
     */
    get(url, object= {}, hash = true) {
        console.log(object)

        return axios.get( ENV.API[window.location.host]+ `${url}`, {
            headers: this._headers(),
            params:  hash ? {'request' : this._makeRequest(object)} : object
        }).then( (response) => {
            return response.data;
        }).catch((error) => {
            return this._dispatchResponse(error.response)
        })
    }

    post(url, object= {}, hash= true) {
        console.log(object)
        return axios.post( ENV.API[window.location.host]+ `${url}`, hash ? {'request' : this._makeRequest(object)} : object, {
            headers: this._headers(),
        }).then( (response) => {
            return response.data;
        }).catch((error) => {
            return this._dispatchResponse(error.response)
        })
    }

    put(url, object={}, hash = true) {
        return axios.put( ENV.API[window.location.host]+ `${url}`,  hash ? {'request' : this._makeRequest(object)} : object,{
            headers: this._headers(),
        }).then( (response) => {
            return response.data;
        }).catch((error) => {
            return this._dispatchResponse(error.response)
        })
    }

    delete(url, object = {}, hash= true) {
        return axios.delete( ENV.API[window.location.host]+ `${url}`, {
            params : hash ? {'request' : this._makeRequest(object)} : object,
            headers: this._headers(),
        }).then((response) => {
            return response.data;
        }).catch((error) => {
            return this._dispatchResponse(error.response)
        })
    }


    upload(url, object= {}) {
        let progress
        return axios.post( ENV.API[window.location.host]+ `${url}`, object, {
            headers: {
                'content-type': 'multipart/form-data',
                Authorization: `Bearer ` + cookies.get('auth')
            },
            onUploadProgress: progressEvent => {
                progress = (progressEvent.loaded / progressEvent.total) * 100;
                return progressEvent
            },
        }).then( (response) => {
            return {result : response.data , progress:progress};
        }).catch((error) => {
            return this._dispatchResponse(error.response)
        })
    }



}

export default Api;
