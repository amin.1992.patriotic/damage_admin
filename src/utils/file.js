function getBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
}

function resizeImage(base64Str, mime, callback) {

  var img = new Image();
  img.src = base64Str;
  img.onload = () => {
    var canvas = document.createElement('canvas');
    var MAX_WIDTH = 900;
    var MAX_HEIGHT = 900;
    var width = img.width;
    var height = img.height;
    if (width > height) {
      if (width > MAX_WIDTH) {
        height *= MAX_WIDTH / width;
        width = MAX_WIDTH;
      }
    } else {
      if (height > MAX_HEIGHT) {
        width *= MAX_HEIGHT / height;
        height = MAX_HEIGHT;
      }
    }
	if(width < MAX_WIDTH && height < MAX_HEIGHT)
		if(callback)
			callback(base64Str);
    canvas.width = width;
    canvas.height = height;
    var ctx = canvas.getContext('2d');
    ctx.drawImage(img, 0, 0, width, height);

    if(callback)
      callback (canvas.toDataURL(mime, 0.9));
  }
}

export { getBase64, resizeImage };
