import React, {useEffect} from 'react'
import {
    Role,
    User,
    Login,
    PasswordReset,

} from "./../pages";
import {BrowserRouter, Redirect, Route, useHistory} from "react-router-dom";
import Master from "../components/Layouts/master";
import {connect, useDispatch, useSelector} from "react-redux";
import Insurance from "../pages/insurance";
import References from "../pages/references/references";
import Filing from "../pages/filing/filing";
import Print from "../pages/print/print";
import InsuranceDetail from "../pages/insuranceDetail/index";
import UserLog from "../pages/user/log";
import Archive from "../pages/archive/archive";
import DeletedFile from "../pages/deletedFile/deletedFile";
import Construction from "../pages/construction/construction";

const routes = (props) => {
    const construction = false
    const constructionToken = sessionStorage.getItem("construction-token")
    const {authReducer} = props;

    if (!authReducer.login) {
        return (
            <BrowserRouter>
                {
                    !construction ?
                        <>
                            <Route exact path="/" component={Login}/>
                            <Route exact path="/password/reset" component={PasswordReset}/>
                        </>
                        :
                        <>
                            <Route exact path="/" component={Construction}/>
                            <Route exact path="/admin_login" component={Login}/>
                            <Route exact path="/password/reset" component={Construction}/>
                        </>
                }

                {/*<NoMatch/>*/}
            </BrowserRouter>
        );

    }


    return (
        <BrowserRouter>
            <Master>
                {/* user bundle */}
                {
                   ( !construction ) ?
                       <>
                           <Route path="/users" component={User} exact={true}/>
                           <Route path="/users/roles" component={Role} exact={true}/>
                           <Route path="/users/log" component={UserLog} exact={true}/>

                           {/*insurance*/}

                           <Route exact={true} path="/archive" component={Archive}/>
                           <Route exact={true} path="/deleted-file" component={DeletedFile}/>
                           <Route exact={true} path="/print/:id" component={Print}/>

                           <Route exact={true} path="/khesarat" component={Insurance}/>
                           <Route exact={true} path="/" component={Insurance}/>


                           <Route path="/khesarat/:id" component={InsuranceDetail}/>
                           <Route path="/references/:id" component={References}/>
                           <Route path="/filing/:id" component={Filing}/>
                       </> :
                       <>

                           <Route exact={true} path="/archive" component={Construction}/>
                           <Route exact={true} path="/deleted-file" component={Construction}/>
                           <Route exact={true} path="/print/:id" component={Construction}/>

                           <Route exact={true} path="/khesarat" component={Construction}/>
                           <Route exact={true} path="/" component={Construction}/>


                           <Route path="/khesarat/:id" component={Construction}/>
                           <Route path="/references/:id" component={Construction}/>
                           <Route path="/filing/:id" component={Construction}/>
                       </>
                }

            </Master>
        </BrowserRouter>
    );
}

const mapStateToProps = state => ({
    authReducer: state.authReducer
});


export default connect(mapStateToProps)(routes)
