import React, {useEffect, useState} from 'react';
import "./style.css"
import {Link, useHistory, useLocation, useParams} from "react-router-dom";
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import {makeStyles} from '@material-ui/core/styles';
import {ENV} from "../../env";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import {toast} from "react-toastify";
import Api from "../../utils/api";
import DialogContent from "@material-ui/core/DialogContent";
import TextareaAutosize from "@material-ui/core/TextareaAutosize";
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Modal from '@material-ui/core/Modal';
import {MoreImage} from "../../components/moreImage/moreImage"
import {useSelector} from "react-redux";
import TextField from '@material-ui/core/TextField';
import {separate} from "../../utils/separate";
import moment from "moment-jalaali";
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import DatePicker from 'react-datepicker2';
import CONST from "../../const";
import {CircularProgress} from "@material-ui/core";
import Guilty from "../../components/guilty/guilty";
import Injured from "../../components/injured/injured";
import {Loading} from "../../components";
import deleteIcon from "../../assets/img/delete.svg";
import lock from "../../assets/img/lock.png";
import unlock from "../../assets/img/unlock.png";

const InsuranceDetail = () => {
    const [evaluatorVisitorName, setEvaluatorVisitorName] = useState({});
    const [loadingImage, setLoadingImage] = useState(false);

    const useStyles = makeStyles((theme) => ({
        root: {
            textAlign: "left",
            fontSize: "1rem",
            borderRadius: "5px",
        },
        formField: {
            '& > *': {
                margin: theme.spacing(1),
                display: "flex",
                justifyContent: "flex-start",
                width: '250px',
            },
        },
        img: {
            width: "150px",
            height: "150px",
            margin: "20px 0",
            padding: "3px",
            border: "1px dotted gray",
            borderRadius: "5px"
        },
        paper: {
            flexDirection: 'column',
            padding: theme.spacing(2),
            textAlign: 'center',
            color: theme.palette.text.secondary,
            backgroundColor: "white",
            // justifyContent: 'right',
            display: "flex",
            alignItems: "center",
            marginBottom: "0"
        },
        formControl: {
            minWidth: 260,
            marginBottom: 20
        },
        modal: {
            display: 'flex',
            padding: theme.spacing(1),
            alignItems: 'center',
            justifyContent: 'center',
        },
        btns: {
            display: "flex",
            marginTop: "50px",
            width: "100%"
        },
        pop: {
            display: "flex",
            flexDirection: "column",
            background: "white",
            padding: "50px"
        }
    }));
    const classes = useStyles();
    const history = useHistory();
    const rootRef = React.useRef(null);
    const [mainLoading, setMainLoading] = useState(true);
    const param = useParams();
    const [detail, setDetail] = useState({});
    const [editable, setEditable] = useState(false);
    const AppState = useSelector(state => state);
    const [saveLoading, setSaveLoading] = useState(false)
    const editCondition = (AppState.authReducer.user.role.id === "operator" || AppState.authReducer.user.role.id === "super_admin" || AppState.authReducer.user.role.id === "final_expert" || AppState.authReducer.user.role.id === "expert_final" || AppState.authReducer.user.role.id === "evaluator")
    const [deleteFileModal, setDeleteFileModal] = useState(false);
    const [loadingId, setLoadingId] = useState(null);

    const id = param.id

    const loading = {
        "filing": false,
        "reject": false,
        "incomplete": false,
        "evaluator": false,
        "expert": false,
        "save": false,
        "final_expert": false,
        "insurer": false,
        "print": false,
        "zip": false,
    }
    const [newInjured, setNewInjured] = useState([])
    const [injuredList, setInjuredList] = useState([])
    const [guiltyList, setGuiltyList] = useState([])
    const [companyList, setCompanyList] = useState(null);
    const [data, setData] = useState();
    const [tab, setTab] = useState(1);
    const [form, setForm] = useState({});
    const [evaluatorVisitorTime, setEvaluatorVisitorTime] = useState(false);
    const [salary, setSalary] = useState(null);
    const [damagePriceList, setDamagePriceList] = useState(null);
    const [descriptionAccessories, setDescriptionAccessories] = useState();
    const [salaryValue, setSalaryValue] = useState([]);
    const [damagePriceListValue, setDamagePriceListValue] = useState([]);
    const [descriptionAccessoriesValue, setDescriptionAccessoriesValue] = useState([]);
    const [total, setTotal] = useState(0);
    const [salaryTotal, setSalaryTotal] = useState(0);
    const [moreTr, setMoreTr] = useState([]);
    const [conditionStatus, setConditionStatus] = useState(0);
    const [openModal, setOpenModal] = useState("");
    const [description, setDescription] = useState("");
    const [toggleModal, setToggleModal] = useState(false);
    const [city, setCity] = useState("");
    const [insurance, setInsurance] = useState(-1);
    const [status, setStatus] = useState(-1);
    const [referId, setReferId] = useState(-1);
    const [fromDate, setFromDate] = useState(false);
    const [toDate, setToDate] = useState(false);
    const [receiver, setReceiver] = useState(-1);
    const [evaluatorName, setEvaluatorName] = useState(null);
    const [evaluator, setEvaluator] = useState("");
    const [expert, setExpert] = useState(null);
    const [expertName, setExpertName] = useState(null);
    const [insurer, setInsurer] = useState(null);
    const [expert_final, setexpert_final] = useState(null);
    const [expert_finalName, setExpert_finalName] = useState(null);
    const [insurerName, setInsurerName] = useState(null);
    const [imageGallery, setImageGallery] = useState(false);
    const [imageNumber, setImageNumber] = useState({});
    const [moreImageGuilty, setMoreImageGuilty] = useState([])
    const [moreImageInjured, setMoreImageInjured] = useState([])
    const [addNewImageGuilty, setAddNewImageGuilty] = useState([])
    const [addNewImageInjured, setAddNewImageInjured] = useState([])
    const [priceAppliances, setPriceAppliances] = useState(0)
    const [wageAmount, setWageAmount] = useState(0)
    const [parts, setParts] = useState(0)
    const [transport, setTransport] = useState(0)
    const [deductions, setDeductions] = useState(0)
    const [accumulationDamages, setAccumulationDamages] = useState(0)
    const [valueCar, setValueCar] = useState(0)
    const [logData, setLogData] = useState([])
    const [logDataUn, setLogDataUn] = useState([])
    const [evaluatorList, setEvaluatorList] = useState([])
    const [dataLog, setDataLog] = useState([])
    const [selectedValue, setSelectedValue] = useState('1');
    const [krokiNumber, setKrokiNumber] = useState(null);
    const [haveKroki, setHaveKroki] = useState(1);
    const [incomplete, setIncomplete] = useState(null);
    const [files, setFiles] = useState([]);
    const [injuredTab, setInjuredTab] = useState(1);
    const [dateVisit, setDateVisit] = useState(moment());
    const [timeVisit, setTimeVisit] = useState(1);
    const [supplementary, setSupplementary] = useState([]);
    const [alarm, setAlarm] = useState(false);
    const [alarmModal, setAlarmModal] = useState(false);
    const [nextTab, setNextTab] = useState(null);
    const [injuredNextTab, setInjuredNextTab] = useState(null);
    const [modalId, setModalId] = useState(null);
    const [deleteImageModal, setDeleteImageModal] = useState(false);
    const [imageModalId, setImageModalId] = useState(null);


    useEffect(() => {
        getCompany()
        setMainLoading(true)
        window.scrollTo({top: 0, behavior: "smooth"})
        newData()
        handleAccumulate()
    }, [])


    useEffect(() => {
        setImageModalId(null)
        setEditable(false)
        setInjuredList([])
    }, [tab, injuredTab])
    useEffect(() => {
        handleAccumulate()
    }, [transport, parts, deductions])

    useEffect(() => {
        getDamagePrice()
        new Api().get(`/backend/insurance/damage/${id}/operators`, {})
            .then((res) => {
                console.log(res)
                if (typeof res !== "undefined") {
                    setLogDataUn(res)
                }
            })
            .catch((err) => {
                throw err
            })
        new Api().get(`/backend/roles/evaluator/users?page=${1}`)
            .then((res) => {
                if (typeof res !== "undefined") {
                    setEvaluator(res)
                }
            })
        new Api().get(`/backend/roles/expert/users?page=${1}`)
            .then((res) => {
                if (typeof res !== "undefined") {
                    setExpert(res)
                }
            })
        new Api().get(`/backend/roles/expert_final/users?page=${1}`)
            .then((res) => {
                if (typeof res !== "undefined") {
                    setexpert_final(res)
                }
            })
        new Api().get(`/backend/roles/insurer/users?page=${1}`)
            .then((res) => {
                if (typeof res !== "undefined") {
                    setInsurer(res)
                }
            })
        new Api().get(`/backend/insurance/damage/${id}/log`, {})
            .then((res) => {
                if (typeof res !== "undefined") {
                    if (typeof res.result !== "undefined") {
                        setDataLog(res.result)

                    }
                }
            })
            .catch((err) => {
                throw err
            })
    }, [])
    const getCompany = () => {
        new Api().get("/insurance/company")
            .then((res) => {
                setCompanyList(res)
            })
    }
    const handleNewInjured = () => {
        setInjuredTab(supplementary.length)
        setSupplementary(
            [...supplementary,
                <Injured
                    alarm={(e) => {
                        setAlarm(e)
                    }}
                    editCondition={editCondition && editable}
                    type={detail && detail.insurance_type && detail.insurance_type.id}
                    getData={(e) => {
                        setInjuredList([...injuredList, e])
                    }}
                    company={companyList}
                />
            ]
        )
    }
    const removeInjured = (s_id, index) => {
        setLoadingId(true)
        let p = supplementary.indexOf(index)
        supplementary.splice(p, 1)
        setTimeout(() => {
            !s_id && setInjuredTab(1)
        }, 200)
        !s_id && setDeleteFileModal(false)
        !s_id && setLoadingId(false)
        if (s_id) {
            new Api().delete(`/backend/insurance/damage/${id}/supplementary/delete`, {supplementary_id: s_id})
                .then((res) => {
                    if (typeof res !== "undefined") {
                        setLoadingId(false)
                        setDeleteFileModal(false)
                        toast.success(res.msg)
                        setInjuredTab(1)
                    }
                })
                .catch((err) => {
                    setLoadingId(false)
                    setDeleteFileModal(false)
                    throw err
                })
        }

    }
    const newData = () => {
        new Api().get(`/backend/insurance/damage/${id}`, {})
            .then((res) => {
                if (typeof res !== "undefined") {
                    res && res.refer && res.refer.map((item) => {
                        if (item.receiver.role_id === "evaluator") {
                            setEvaluatorVisitorName({name: item.receiver.name, family: item.receiver.family})
                        }
                    })
                    let formData = {
                        "date_occurrence": moment(res.date_occurrence),
                        "time_occurrence": res.time_occurrence,
                        "address_occurrence": res.address_occurrence,
                        "description": res.description,
                        "guilty_mobile": res.guilty_mobile,
                        "guilty_insurance_unique_code": res.guilty_insurance_unique_code,
                        "injured_insurance_unique_code": res.injured_insurance_unique_code,
                    }
                    setSupplementary(res.supplementary)
                    setForm({...formData})
                    setDetail(res)
                    setFiles(res.files)
                    setKrokiNumber(res.convertible_number)
                    setHaveKroki(res.has_convertible)
                    setMainLoading(false)
                    res.supplementary && res.supplementary.length === 0 && handleNewInjured()
                    setInjuredTab(res && res.supplementary && res.supplementary.length - 1 < injuredTab ? res && res.supplementary && res.supplementary.length - 1 : injuredTab)
                }
            })
    }
    const handleChangeElement = (event) => {
        let frm = {...form};

        if (event.target.name === "time_occurrence") {
            if (event.target.value.length > 1 && event.target.value.length < 3) {
                frm[event.target.name] = event.target.value + ":";
            } else if (event.target.value.length < 6 && (event.target.name === "time_occurrence")) {
                frm[event.target.name] = event.target.value;
            } else if (event.target.name !== "time_occurrence") {
                frm[event.target.name] = event.target.value;
            }
        } else if (event.target.name === "time_visit") {
            if (event.target.value.length > 1 && event.target.value.length < 3) {
                setTimeVisit(event.target.value + ":")
            } else if (event.target.value.length < 6) {
                setTimeVisit(event.target.value)
            }
        } else {
            frm[event.target.name] = event.target.value;
        }
        setForm(frm);
    };
    const handleDelete = async (index) => {
        injuredList[injuredTab - 1].files.splice(index, 1);
        setMoreImageInjured([])
        injuredList.filter((item) => {
            return item !== null
        })
    }
    const handleSetData = () => {
        setSaveLoading(true)
        let injured = []
        let guilty = {...guiltyList, files: moreImageGuilty}
        injured = [{...injuredList[injuredList && injuredList.length - 1]}]
        const obj = {injured: injured, guilty: guilty, encrypt: true}
        new Api().post(`/backend/insurance/damage/${param.id}/supplementary`, obj, false)
            .then((res) => {
                newData()
                setInjuredList([])
                setMoreImageInjured([])
                setAddNewImageGuilty([])
                setMoreImageGuilty([])
                setAlarm(false)
                setSaveLoading(false)
                setEditable(false)
                if (typeof res !== "undefined") {
                    toast.success(res.msg)
                }
            })
    }
    const edit = () => {
        setSaveLoading(true)

        const obj = {
            "directory": "damage",
            "date_occurrence": moment(form.date_occurrence).format('YYYY/M/D'),
            "time_occurrence": form.time_occurrence,
            "address_occurrence": form.address_occurrence,
            "des": form.description,
            "guilty_mobile": form.guilty_mobile,
            "guilty_insurance_unique_code": form.guilty_insurance_unique_code,
            "injured_insurance_unique_code": form.injured_insurance_unique_code,
            "city_id": detail.city_id,
            "insurance_type": detail.insurance_type.id,
            "type": detail.type,
            "lat": detail.lat,
            "lng": detail.lng,
            "has_convertible": haveKroki,
            "convertible_number": krokiNumber,
            "injured_insurance_number": detail.injured_insurance_number,
            "files": detail.files && detail.files.concat(moreImageGuilty).concat(moreImageInjured)
        }
        new Api().put(`/insurance/damage/${detail.id}`, obj, false)
            .then((res) => {
                setSaveLoading(false)
                if (typeof res !== "undefined")
                    setAlarm(false)
                setEditable(false)
                toast.success(res.msg)
            })
    }

    const handleAccumulate = () => {
        setAccumulationDamages(((+damagePriceList + +priceAppliances) + (+wageAmount + +salary) + +transport) - (+parts + +deductions))

    }

    const handleSwitchCondition = async (item) => {
        if (item.status === 12) {
            loading.zip = true
            new Api().get(`/backend/insurance/damage/${detail.id}/download`)
                .then((res) => {
                    loading.zip = false
                    if (res.status) {
                        window.location.assign(res.result)
                    } else {
                        toast.error(res.msg)
                    }
                })
                .catch((err) => {
                    loading.zip = false
                })
            // window.location.assign(`https://la1.bimic.ir/index.php/api/backend/insurance/damage/${detail.id}/download?encrypt=true`)
        }
        if (item.status === 11) {
            history.push(`/print/${param.id}`, {detail})
            return
        }
        if (item.status === 9) {
            history.push(`/filing/${param.id}`, {detail})
            return
        }
        setDescription('');
        setConditionStatus(item.status);
        setToggleModal(true)
    }
    const handleList = (item) => {
        item.map((item, index) => {
            logData.push({id: item.id, name: item.r_full_name})
        })
        let chars = logData;
        let uniqueChars = [...new Set(chars)];
        setLogDataUn(uniqueChars)
    }


    const condition = [
        {status: 2, title: "رد درخواست", t_id: "reject"},
        {status: 5, title: "اعلام نقص مدارک", t_id: "incomplete"},
        {status: 6, title: "ارجاع به ارزیاب", t_id: "evaluator"},
        {status: 7, title: "ارجاع به کارشناس اولیه", t_id: "expert"},
        {status: 8, title: "ارجاع به کارشناس نهایی", t_id: "expert_final"},
        {status: 10, title: "ارجاع به بیمه گر", t_id: "insurer"},
        {status: 11, title: "پرینت", t_id: "print"},
        {status: 12, title: "دانلود تصاویر پرونده", t_id: "zip"},
    ]
    const roleCondition = {
        "expert": ["incomplete", "expert_final"],
        "expert_final": ["filing", "reject", "incomplete", "insurer", "save"],
        "insurer": ["expert_final", "print", "zip"],
        "guest": [1],
        "super_admin": ["filing", "reject", "incomplete", "evaluator", "expert", "save", "expert_final", "insurer", "print", "zip"],
        "operator": ["filing", "reject", "incomplete", "evaluator", "save", "expert_final"],
        "evaluator": ["filing", "incomplete", "expert", "save"],
    }

    const sendDamagePrice = () => {
        setSaveLoading(true)
        const obj = {
            encrypt: false,
            transport: transport,
            commission: wageAmount,
            deductions: deductions,
            scrap_pieces: parts,
            accessories: priceAppliances,
            total: accumulationDamages,
            car_price: valueCar,
            details: evaluatorList,
            time_visit: timeVisit,
            date_visit: moment(dateVisit).format('jYYYY/jM/jD'),
        }
        new Api().post(`/backend/insurance/damage/${param.id}/determining`, obj, false)
            .then((res) => {
                setSaveLoading(false)
                setAlarm(false)
                toast.success(res.msg)
                setEditable(false)
            })
            .catch((err) => {
                setSaveLoading(false)
                toast.success("خطا")
            })
    }
    const getDamagePrice = () => {
        new Api().get(`/backend/insurance/damage/${param.id}/determining`)
            .then((res) => {
                if (typeof res !== "undefined" && res.result && res.result[0]) {
                    setPriceAppliances(res.result[res.result.length - 1].accessories)
                    setWageAmount(res.result[res.result.length - 1].commission)
                    setParts(res.result[res.result.length - 1].scrap_pieces)
                    setTransport(res.result[res.result.length - 1].transport)
                    setDeductions(res.result[res.result.length - 1].deductions)
                    setAccumulationDamages(res.result[res.result.length - 1].total)
                    setValueCar(res.result[res.result.length - 1].car_price)
                    setEvaluatorList(JSON.parse(res.result[res.result.length - 1].details) || [])
                    setEvaluatorVisitorTime(true)
                    setDateVisit(res.result[res.result.length - 1].date_visit && res.result[res.result.length - 1].date_visit.includes("1400") ? moment(res.result[res.result.length - 1].date_visit, 'jYYYY/jM/jD') : (res.result[res.result.length - 1].date_visit && res.result[res.result.length - 1].date_visit ? moment(res.result[res.result.length - 1].date_visit, 'YYYY/M/D') : moment()),)
                    setTimeVisit(res.result[res.result.length - 1].time_visit)
                }
            })
    }
    const addImage = async (e, key) => {

        if (key === "guilty") {
            setMoreImageGuilty(
                [...moreImageGuilty,
                    {
                        name: `evaluator_${key}_${moreImageGuilty.length + 1}`,
                        file: e.file,
                        mime_type: "image",
                        caption: key === "guilty" ? "مقصر" : "زیان دیده",
                        collection: 0,
                        directory: "damage"
                    },
                ]
            )
            setAddNewImageGuilty([...addNewImageGuilty, e.path])
        } else {
            setMoreImageInjured(
                [...moreImageInjured,
                    {
                        name: `evaluator_${key}_${moreImageInjured.length + 1}`,
                        file: e.file,
                        mime_type: "image",
                        caption: key === "guilty" ? "مقصر" : "زیان دیده",
                        collection: 0,
                        directory: "damage"
                    },
                ]
            )
            setAddNewImageInjured([...addNewImageInjured, e.path])
            if (supplementary.props !== undefined) {
                setInjuredList([{
                    files: [...moreImageInjured, {
                        name: `evaluator_${key}_${moreImageInjured.length + 1}`,
                        file: e.file,
                        mime_type: "image",
                        caption: key === "guilty" ? "مقصر" : "زیان دیده",
                        collection: 0,
                        directory: "damage"
                    }]
                }])
            } else {
                setInjuredList([{
                    ...injuredList[injuredList && injuredList.length - 1],
                    id: supplementary[injuredTab] && supplementary[injuredTab].id,
                    files: [...moreImageInjured, {
                        name: `evaluator_${key}_${moreImageInjured.length + 1}`,
                        file: e.file,
                        mime_type: "image",
                        caption: key === "guilty" ? "مقصر" : "زیان دیده",
                        collection: 0,
                        directory: "damage"
                    }]
                }])
            }
        }

    }

    const selectFile = (file, key, base64 = true, more = false) => {
        setLoadingImage(true)
        const form_data = new FormData();
        form_data.append('file', file);
        form_data.append('base64', base64);
        form_data.append('damage_id', param.id);
        form_data.append('directory', "damage");
        new Api().upload("/backend/fileManager/attachment", form_data)
            .then((response) => {
                setLoadingImage(false)
                if (typeof response != "undefined") {
                    console.log(response)
                    if (response.result.status) {
                        toast.success(response.result.msg)
                        if (more) {
                            addImage(response.result, key)
                        } else {
                            key(response.result)
                        }
                    } else {
                        toast.error(response.result.msg)
                    }
                }
            }).catch((error) => {
            setLoadingImage(false)
        })
    }
    const removeFile = (files, index) => {
        setLoadingId(true)

        new Api().delete('/backend/fileManager/attachment', {
            'file': files.file,
            'directory': files.directory
        }, false).then((response) => {
            if (typeof response != "undefined") {
                if (response.status) {
                    setLoadingId(false)
                    newData()
                    setDeleteImageModal(true)
                    toast.success('فایل با موفقیت حذف گردید');
                } else {
                    setLoadingId(false)
                    toast.error('خطایی رخ داده است.');
                }
            }
        });
    }
    const handleRole = (role) => {
        switch (role) {
            case "evaluator" :
                return "/ ارزیاب "
                break;
            case "final_expert" :
                return "/ کارشناس نهایی "
                break;
            case "super_admin" :
                return "/ سوپر ادمین "
                break;
            case "expert" :
                return "/ کارشناس "
                break;
            case "insurer" :
                return "/ بیمه گر "
                break;
            case "guest" :
                return "/ مهمان "
                break;
            case "admin" :
                return "/ ادمین "
                break;
            case "operator" :
                return "/ اپراتور "
                break;
            default :
                return ""
                break
        }
    }
    const handleCondition = (id) => {
        switch (conditionStatus) {
            case 2 :
                return (
                    <>
                        <Modal
                            disablePortal
                            disableEnforceFocus
                            disableAutoFocus
                            open={toggleModal}
                            onClose={() => {
                                setToggleModal(false)
                            }}
                            aria-labelledby="server-modal-title"
                            aria-describedby="server-modal-description"
                            className={classes.modal}
                            container={() => rootRef.current}
                        >
                            <div className={classes.pop}>
                                <h2 id="transition-modal-title">لطفا علت رد خسارت را وارد کنید.</h2>
                                <form noValidate autoComplete="off">
                                    <TextareaAutosize onChange={(e) => {
                                        setDescription(e.target.value)
                                    }} aria-label="minimum height" rowsMin={10} value={description}
                                                      placeholder="لطفا علت رد خسارت  را وارد کنید."/>
                                </form>
                                <Button onClick={() => {
                                    handleSendData(evaluatorName)
                                }} variant="contained" color="primary">
                                    {!saveLoading ? "ثبت" : <CircularProgress/>}
                                </Button>
                            </div>
                        </Modal>
                    </>

                )
            case 5:
                return (
                    <>
                        <Modal
                            disablePortal
                            disableEnforceFocus
                            disableAutoFocus
                            open={toggleModal}
                            onClose={() => {
                                setToggleModal(false)
                            }}
                            aria-labelledby="server-modal-title"
                            aria-describedby="server-modal-description"
                            className={classes.modal}
                            container={() => rootRef.current}

                        >
                            <div className={classes.pop}>
                                <FormControl className={classes.formControl}>
                                    <InputLabel htmlFor="grouped-select">لیست همکاران مرتبط با این پرونده</InputLabel>
                                    <Select onChange={(event) => {
                                        setIncomplete(event && event.target.value);
                                    }} defaultValue="" id="grouped-select">
                                        {
                                            logDataUn && logDataUn.map((item, index) => {
                                                return (
                                                    <MenuItem
                                                        value={`${item.id}`}>{`${item.name} ${item.family}  ${handleRole(item.role_id)}`}</MenuItem>
                                                )
                                            })
                                        }
                                    </Select>
                                </FormControl>
                                <form noValidate autoComplete="off">
                                    <TextareaAutosize onChange={(e) => {
                                        setDescription(e.target.value)
                                    }} aria-label="minimum height" rowsMin={10}
                                                      placeholder="لطفا لیست مدارک را وارد کنید."/>
                                </form>
                                <Button onClick={() => {
                                    handleSendData(incomplete)
                                }} variant="contained" color="primary">
                                    {!saveLoading ? "ثبت" : <CircularProgress/>}
                                </Button>
                            </div>
                        </Modal>
                    </>
                )
            case 6 :
                return (
                    <>
                        <Modal
                            disablePortal
                            disableEnforceFocus
                            disableAutoFocus
                            open={toggleModal}
                            onClose={() => {
                                setToggleModal(false)
                            }}
                            aria-labelledby="server-modal-title"
                            aria-describedby="server-modal-description"
                            className={classes.modal}
                            container={() => rootRef.current}

                        >
                            <div className={classes.pop}>
                                <FormControl className={classes.formControl}>
                                    <InputLabel htmlFor="grouped-select">لیست ارزیاب ها</InputLabel>
                                    <Select onChange={(event) => {
                                        setEvaluatorName(event && event.target.value);
                                    }} defaultValue="" id="grouped-select">
                                        {
                                            evaluator && evaluator.map((item) => {
                                                return (
                                                    <MenuItem
                                                        value={`${item.id}`}>{`${item.name + " " + item.family + " -- " + item.mobile}`}</MenuItem>
                                                )
                                            })
                                        }
                                    </Select>
                                </FormControl>
                                <form noValidate autoComplete="off">
                                    <TextareaAutosize onChange={(e) => {
                                        setDescription(e.target.value)
                                    }} aria-label="minimum height" rowsMin={10}
                                                      placeholder="توضیحات"/>
                                </form>
                                <Button onClick={() => {
                                    handleSendData(evaluatorName)
                                }} variant="contained" color="primary">
                                    {!saveLoading ? "ثبت" : <CircularProgress/>}
                                </Button>
                            </div>
                        </Modal>
                    </>
                )
            case 7 :
                return (
                    <>
                        <Modal
                            disablePortal
                            disableEnforceFocus
                            disableAutoFocus
                            open={toggleModal}
                            onClose={() => {
                                setToggleModal(false)
                            }}
                            aria-labelledby="server-modal-title"
                            aria-describedby="server-modal-description"
                            className={classes.modal}
                            container={() => rootRef.current}

                        >
                            <div className={classes.pop}>
                                <FormControl className={classes.formControl}>
                                    <InputLabel htmlFor="grouped-select">لیست کارشناس ها</InputLabel>
                                    <Select onChange={(event) => {
                                        setExpertName(event && event.target.value);
                                    }} defaultValue="" id="grouped-select">
                                        {
                                            expert && expert.map((item) => {
                                                return (
                                                    <MenuItem
                                                        value={`${item.id}`}>{`${item.name + " " + item.family + " -- " + item.mobile}`}</MenuItem>
                                                )
                                            })
                                        }
                                    </Select>
                                </FormControl>
                                <form noValidate autoComplete="off">
                                    <TextareaAutosize onChange={(e) => {
                                        setDescription(e.target.value)
                                    }} aria-label="minimum height" rowsMin={10}
                                                      placeholder="توضیحات"/>
                                </form>
                                <Button onClick={() => {
                                    handleSendData(expertName)
                                }} variant="contained" color="primary">
                                    {!saveLoading ? "ثبت" : <CircularProgress/>}
                                </Button>
                            </div>
                        </Modal>
                    </>
                )
            case 8 :
                return (
                    <>
                        <Modal
                            disablePortal
                            disableEnforceFocus
                            disableAutoFocus
                            open={toggleModal}
                            onClose={() => {
                                setToggleModal(false)
                            }}
                            aria-labelledby="server-modal-title"
                            aria-describedby="server-modal-description"
                            className={classes.modal}
                            container={() => rootRef.current}

                        >
                            <div className={classes.pop}>
                                <FormControl className={classes.formControl}>
                                    <InputLabel htmlFor="grouped-select">لیست کارشناس نهایی</InputLabel>
                                    <Select onChange={(event) => {
                                        setExpert_finalName(event && event.target.value);
                                    }} defaultValue="" id="grouped-select">
                                        {
                                            expert_final && expert_final.map((item) => {
                                                return (
                                                    <MenuItem
                                                        value={`${item.id}`}>{`${item.name + " " + item.family + " -- " + item.mobile}`}</MenuItem>
                                                )
                                            })
                                        }
                                    </Select>
                                </FormControl>
                                <form noValidate autoComplete="off">
                                    <TextareaAutosize onChange={(e) => {
                                        setDescription(e.target.value)
                                    }} aria-label="minimum height" rowsMin={10}
                                                      placeholder="توضیحات"/>
                                </form>
                                <Button onClick={() => {
                                    handleSendData(expert_final)
                                }} variant="contained" color="primary">
                                    {!saveLoading ? "ثبت" : <CircularProgress/>}
                                </Button>
                            </div>
                        </Modal>
                    </>
                )
            case 10 :
                return (
                    <>
                        <Modal
                            disablePortal
                            disableEnforceFocus
                            disableAutoFocus
                            open={toggleModal}
                            onClose={() => {
                                setToggleModal(false)
                            }}
                            aria-labelledby="server-modal-title"
                            aria-describedby="server-modal-description"
                            className={classes.modal}
                            container={() => rootRef.current}

                        >
                            <div className={classes.pop}>
                                <FormControl className={classes.formControl}>
                                    <InputLabel htmlFor="grouped-select">لیست بیمه گرها</InputLabel>
                                    <Select onChange={(event) => {
                                        setInsurerName(event && event.target.value);
                                    }} defaultValue="" id="grouped-select">
                                        {
                                            insurer && insurer.map((item) => {
                                                return (
                                                    <MenuItem
                                                        value={`${item.id}`}>{`${item.name + " " + item.family + " -- " + item.mobile}`}</MenuItem>
                                                )
                                            })
                                        }
                                    </Select>
                                </FormControl>
                                <form noValidate autoComplete="off">
                                    <TextareaAutosize onChange={(e) => {
                                        setDescription(e.target.value)
                                    }} aria-label="minimum height" rowsMin={10}
                                                      placeholder="توضیحات"/>
                                </form>
                                <Button onClick={() => {
                                    handleSendData(insurer)
                                }} variant="contained" color="primary">
                                    {!saveLoading ? "ثبت" : <CircularProgress/>}
                                </Button>
                            </div>
                        </Modal>
                    </>
                )
        }
    }
    const handleSendData = (evaluatorName) => {
        let obj
        switch (conditionStatus) {
            case 2 : {
                if (description === "") {
                    toast.error("لطفا دلیل رد درخواست را بنویسید");
                    return
                }
                obj = {
                    reject: 1,
                    desc: description,
                    status: condition[0].status,
                    archive: "0",
                }
                break;
            }
            case 5 : {
                if (description === "") {
                    toast.error("لطفا لیست مدارک ناقص را بنویسید");
                    return
                }
                obj = {
                    receiver: incomplete,
                    desc: description,
                    status: condition[1].status,
                    archive: "0",
                    reject: "0",
                }
                break;
            }
            case 6 : {
                if (evaluatorName === null) {
                    toast.error("لطفا ارزیاب مورد نظر را انتخاب کنید .");
                    return
                }
                obj = {
                    receiver: evaluatorName,
                    status: condition[2].status,
                    desc: description === '' ? "--" : description,
                    archive: "0",
                    reject: "0",
                }
                break;
            }
            case 7 : {
                if (evaluatorName === null) {
                    toast.error("لطفا کارشناس مورد نظر را انتخاب کنید .");
                    return
                }
                obj = {
                    receiver: expertName,
                    status: condition[3].status,
                    desc: description === '' ? "--" : description,
                    archive: "0",
                    reject: "0",
                }
                break;
            }
            case 8 : {
                if (expert_finalName === null) {
                    toast.error("لطفا کارشناس مورد نظر را انتخاب کنید .");
                    return
                }
                obj = {
                    receiver: expert_finalName,
                    status: condition[4].status,
                    desc: description === '' ? "--" : description,
                    archive: "0",
                    reject: "0",
                }
                break;
            }
            case 10 : {
                if (insurerName === null) {
                    toast.error("لطفا بیمه گر مورد نظر را انتخاب کنید .");
                    return
                }
                obj = {
                    receiver: insurerName,
                    status: condition[5].status,
                    desc: description === '' ? "--" : description,
                    archive: "0",
                    reject: "0",
                }
                break;
            }
        }
        new Api().post(`/backend/insurance/damage/${id}/refer`, obj)
            .then((res) => {
                if (typeof res !== "undefined") {
                    if (res.status) {
                        setAlarm(false)
                        toast.success(res.msg);
                        handleRequest()
                    } else {
                        toast.success(res.msg);
                    }
                }
            })
    }

    const handleRequest = (name, value) => {
        new Api().get("/backend/insurance/damage", {
            receiver: receiver,
            refer_id: referId,
            id: -1,
            status: status,
            insurance_type: insurance,
            city_id: -1,
            from_date: fromDate,
            to_date: toDate,
            injured: -1,
            [name]: value
        })
            .then((res) => {
                if (res.status || typeof res.status === "undefined") {
                    history.push("/")
                    setData(res)
                }
            })
    }
    const addMoreTr = () => {
        setMoreTr(
            [...moreTr,
                <tr>
                    <td>
                        <div>
                            {descriptionAccessories}
                        </div>
                    </td>
                    <td>
                        <div>
                            {separate(damagePriceList)}
                        </div>
                    </td>
                    <td>
                        <div>
                            {salary}
                        </div>
                    </td>
                </tr>
            ]
        )
        setEvaluatorList([...evaluatorList, {
            description: descriptionAccessories,
            amount: damagePriceList,
            commission: salary,
            id: moment().format("YYYY/M/D mm:ss")
        }])
        setSalaryValue([...salaryValue, salary])
        setDamagePriceListValue([...damagePriceListValue, damagePriceList])
        setDescriptionAccessoriesValue([...descriptionAccessoriesValue, descriptionAccessories])


        setTotal(total + +damagePriceList)

        setSalaryTotal(salaryTotal + +salary)

        setPriceAppliances(+damagePriceList + +priceAppliances)
        setWageAmount(+wageAmount + +salary)

        setDescriptionAccessories("")
        setSalary("")
        setDamagePriceList("")
        handleAccumulate()

    }
    const removeItem = (id) => {
        const item = evaluatorList.filter((item) => {
            if (id === item.id) {
                setPriceAppliances(parseInt(priceAppliances) - parseInt(item.amount))
                setWageAmount(parseInt(wageAmount) - parseInt(item.commission))
            }
            return id !== item.id
        })
        setEvaluatorList(item)
    }

    const log = dataLog[0] && dataLog[0].t_id
    return (
        <Container>
            {(mainLoading || detail === {}) ?
                <div style={{display: 'flex', minHeight: "500px", alignItems: "center", justifyContent: "center"}}>
                    <CircularProgress/></div> :
                <>
                    <Grid spacing={3} className={classes.paper} container>
                        {
                            <>
                                {
                                    detail && detail.refer && detail.refer[detail.refer.length - 1] && detail.refer[detail.refer.length - 1].desc !== "--" &&
                                    <Grid item xs={12}>
                                        <div className={"refer-des-alarm"}>
                                    <span>
                                        {detail && detail.refer && detail.refer[detail.refer.length - 1] && detail.refer[detail.refer.length - 1].desc}
                                    </span>
                                        </div>
                                    </Grid>
                                }

                                <Grid item container={true} xs={12}>
                                    <div className={"file-number"}>
                                       <span>
                                         {" کد پرونده:"}
                                       </span>
                                        <span>
                                              {detail.id}
                                            {" - "}
                                            {detail.insurance_type && parseInt(detail.insurance_type.id) == 2 ? "بدنه" : "ثالث"}
                                         </span>

                                        <img src={!(editCondition && editable) ? lock : unlock}/>
                                    </div>
                                </Grid>

                                <Grid container item xs={12}>
                                    <div className={classes.root}>
                                        <Modal
                                            disablePortal
                                            disableEnforceFocus
                                            disableAutoFocus
                                            open={alarmModal}
                                            onClose={() => {
                                                setAlarmModal(false)
                                            }}
                                            aria-labelledby="server-modal-title"
                                            aria-describedby="server-modal-description"
                                            className={classes.modal}
                                        >
                                            <div className={classes.paper}>
                                                <h4 id="server-modal-title">شما تغییرات ذخیره نشده دارید </h4>
                                                <ButtonGroup color="primary"
                                                             aria-label="outlined primary button group">
                                                    <Button color="primary" onClick={() => {
                                                        setAlarm(false)
                                                        setAlarmModal(false)
                                                        nextTab && setTab(nextTab)
                                                        injuredNextTab && setInjuredTab(injuredNextTab)
                                                    }}>
                                                        ادامه
                                                    </Button>
                                                    <Button color="primary" onClick={() => {
                                                        setAlarmModal(false)
                                                    }}>
                                                        ماندن
                                                    </Button>

                                                </ButtonGroup>
                                            </div>
                                        </Modal>
                                    </div>
                                    <ButtonGroup className={"btns-row"} aria-label="outlined primary button group">
                                        <Button onClick={() => {
                                            alarm && setAlarmModal(true)
                                            setNextTab(1)
                                            !alarm && setTab(1)
                                        }} variant="contained"
                                                color={tab === 1 ? "secondary" : "default"}>
                                            {"اطلاعات شرح حادثه"}
                                        </Button>
                                        {
                                            detail.insurance_type && detail.insurance_type.id === 1 &&
                                            <Button onClick={() => {
                                                alarm && setAlarmModal(true)
                                                setNextTab(2)
                                                !alarm && setTab(2)
                                            }} variant="contained"
                                                    color={tab === 2 ? "secondary" : "default"}>
                                                {"اطلاعات مقصر"}
                                            </Button>
                                        }
                                        <Button onClick={() => {
                                            setNextTab(3)
                                            alarm && setAlarmModal(true)
                                            !alarm && setTab(3)
                                        }} variant="contained"
                                                color={tab === 3 ? "secondary" : "default"}>
                                            {"اطلاعات زیان دیده"}
                                        </Button>
                                        <Button onClick={() => {
                                            setNextTab(4)
                                            alarm && setAlarmModal(true)
                                            !alarm && setTab(4)
                                        }} variant="contained"
                                                color={tab === 4 ? "secondary" : "default"}>
                                            {"ارزیابی خسارت"}
                                        </Button>
                                    </ButtonGroup>
                                </Grid>
                                {
                                    tab === 1 &&
                                    <Grid item container spacing={2}>
                                        <Grid item md={4} sm={6} xs={12}>
                                            <div className={"date_detail"}>
                                                <div className={classes.root}>
                                                    <div style={{
                                                        pointerEvents: !(editCondition && editable) && "none",
                                                    }} className='datepicker-input-container'>
                                                        <label> تاریخ وقوع حادثه :</label>
                                                        <DatePicker
                                                            isGregorian={false}
                                                            timePicker={false}
                                                            name={"date_occurrence"}
                                                            value={form.date_occurrence}
                                                            onChange={issueDateGuilty => {
                                                                form.date_occurrence = issueDateGuilty;
                                                            }}
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                        </Grid>
                                        <Grid item md={4} sm={6} xs={12}>
                                            <div className={classes.root}>
                                                <TextField
                                                           size={"small"}
                                                           name={"time_occurrence"}
                                                           onChange={(e) => {
                                                               setAlarm(true)
                                                               handleChangeElement(e)
                                                           }} value={form.time_occurrence}
                                                           className={`damage-input ${!(editCondition && editable) && "disable"}`}
                                                           label="زمان وقوع حادثه :"
                                                           variant="outlined"
                                                />
                                            </div>
                                        </Grid>
                                        <Grid item xs={12}>
                                            <div style={{display: "flex", flexDirection: "column"}}
                                                 className={classes.root}>
                                                <TextField
                                                           size={"small"}

                                                           name={"address_occurrence"}
                                                           onChange={(e) => {
                                                               setAlarm(true)
                                                               handleChangeElement(e)
                                                           }} value={form.address_occurrence}
                                                           className={`damage-input ${!(editCondition && editable) && "disable"}`}
                                                           label="آدرس محل حادثه:"
                                                           variant="outlined"

                                                />
                                            </div>
                                        </Grid>
                                        <Grid item xs={12}>
                                            <div style={{display: "flex", flexDirection: "column"}}
                                                 className={classes.root}>
                                                <TextField
                                                           size={"small"}
                                                           name={"description"}
                                                           onChange={(e) => {
                                                               setAlarm(true)
                                                               handleChangeElement(e)
                                                           }} value={form.description}
                                                           className={`damage-input ${!(editCondition && editable) && "disable"}`}
                                                           label="توضیحات : "
                                                           variant="outlined"

                                                />
                                                {
                                                    detail && detail.files && detail.files.map((item) => {
                                                        if (item.name && item.name.includes('voice')) {
                                                            return (
                                                                <Grid item md={2} sm={4} xs={12}>
                                                                    <div>
                                                                        <audio controls
                                                                               src={`${ENV.STORAGE + "/damage/" + "/" + detail.id + "/" + item.file}`}
                                                                               type="audio/mp3"/>
                                                                    </div>
                                                                </Grid>
                                                            )
                                                        }

                                                    })
                                                }
                                            </div>

                                        </Grid>
                                        <Grid item xs={12}>
                                            <div
                                                style={{
                                                    pointerEvents: !(editCondition && editable) && "none",
                                                    display: "flex",
                                                    alignItems: "center",
                                                }}
                                                className={"row-kroki"}>
                                                <span>کروکی : </span>
                                                <FormControl component="fieldset">
                                                    <RadioGroup onChange={(e) => {
                                                        setAlarm(true)
                                                        setHaveKroki(e.target.value)
                                                    }} row aria-label="position" name="position"
                                                                defaultValue={haveKroki && haveKroki.toString()}>
                                                        <FormControlLabel
                                                            value="1"
                                                            control={<Radio color="secondary"/>}
                                                            label="دارد"
                                                            labelPlacement="start"
                                                        />
                                                        <FormControlLabel
                                                            value="0"
                                                            control={<Radio color="secondary"/>}
                                                            label="ندارد"
                                                            labelPlacement="start"
                                                        />
                                                    </RadioGroup>
                                                </FormControl>
                                                {
                                                    parseInt(haveKroki) === 1 &&
                                                    <div style={{marginRight: "20px"}}>
                                                        <TextField
                                                            size={"small"}
                                                            onChange={(e) => {
                                                                setAlarm(true)
                                                                setKrokiNumber(e.target.value)

                                                            }} value={krokiNumber} className="damage-input"
                                                            label="شماره کروکی"
                                                            variant="outlined"/>
                                                    </div>
                                                }

                                            </div>
                                        </Grid>
                                        <Grid container xs={12}>
                                            {
                                                editCondition &&
                                                <ButtonGroup className={"btns-row"}
                                                             aria-label="outlined primary button group">
                                                    <Button onClick={() => {
                                                        setEditable(true)
                                                        window.scrollTo({top: 0, behavior: 'smooth'})
                                                    }} value={conditionStatus} variant="contained"
                                                            color={"default"}>
                                                        {"ویرایش"}
                                                    </Button>
                                                    <Button onClick={() => {
                                                        edit()
                                                    }} value={conditionStatus} variant="contained"
                                                            color={"default"}>
                                                        {!saveLoading ? "ثبت" : <CircularProgress/>}
                                                    </Button>
                                                </ButtonGroup>
                                            }

                                        </Grid>
                                    </Grid>
                                }
                            </>
                        }
                        <Modal
                            disablePortal
                            disableEnforceFocus
                            disableAutoFocus
                            open={imageGallery}
                            onClose={() => {
                                setImageGallery(false)
                            }}
                            aria-labelledby="server-modal-title"
                            aria-describedby="server-modal-description"
                            className={classes.modal}
                            container={() => rootRef.current}
                        >
                            <div className={"modal-section"}>
                                <img style={{maxWidth: "100%", maxHeight: "100%"}}
                                     src={`${ENV.STORAGE + "/" + "damage" + "/" + detail.id + "/" + (imageNumber.f_id ? imageNumber.f_id + "/" : "/") + imageNumber.file}`}/>
                            </div>
                        </Modal>
                        {
                            detail.insurance_type && detail.insurance_type.id === 1 && tab === 2 &&
                            <>
                                <Guilty alarm={(e) => {
                                    setAlarm(e)
                                }} editCondition={editCondition && editable} getData={(e) => {
                                    setGuiltyList(e)
                                }} detail={detail} company={companyList}/>
                                <div className={"more-image-section"}>
                                    <Grid container xs={12}>
                                        {
                                            supplementary && supplementary[0] && supplementary[0].files && supplementary[0].files.map((item, index) => {

                                                if (item && item.name && item.name.includes('guilty')) {
                                                    return (
                                                        <Grid item md={2} sm={4} xs={12}>
                                                            <div className={"image-section"}>
                                                                {
                                                                    AppState.authReducer.user.role.id !== "operator" && editCondition && editable &&
                                                                    <img onClick={() => {
                                                                        setDeleteImageModal(true)
                                                                        setImageModalId(item.id)
                                                                    }} className={"close-icon"}
                                                                         src={require("../../assets/img/close.png")}/>
                                                                }
                                                                <img onClick={() => {
                                                                    setImageGallery(true);
                                                                    setImageNumber({
                                                                        file: item.file,
                                                                        f_id: item.supplementary_id
                                                                    })
                                                                }} className={classes.img}
                                                                     src={`${ENV.STORAGE + "/" + item.directory + "/" + detail.id + "/" + (item.supplementary_id ? item.supplementary_id + "/" : "/") + item.file}`}/>
                                                            </div>
                                                            <div className={classes.root}>
                                                                <Modal
                                                                    disablePortal
                                                                    disableEnforceFocus
                                                                    disableAutoFocus
                                                                    open={deleteImageModal && (imageModalId === item.id)}
                                                                    onClose={() => {
                                                                        setDeleteImageModal(false)
                                                                    }}
                                                                    aria-labelledby="server-modal-title"
                                                                    aria-describedby="server-modal-description"
                                                                    className={classes.modal}
                                                                >
                                                                    <div className={classes.paper}>
                                                                        <h4 id="server-modal-title">آیا از حذف تصویر
                                                                            مطمئن هستید ؟</h4>
                                                                        <ButtonGroup color="primary"
                                                                                     aria-label="outlined primary button group">
                                                                            <Button color="primary" onClick={() => {
                                                                                removeFile(item, index)
                                                                            }}>
                                                                                {(loadingId) ?
                                                                                    <CircularProgress/> : "بله"}
                                                                            </Button>
                                                                            <Button color="primary" onClick={() => {
                                                                                setDeleteImageModal(false)
                                                                            }}>
                                                                                خیر
                                                                            </Button>

                                                                        </ButtonGroup>
                                                                    </div>
                                                                </Modal>
                                                            </div>
                                                        </Grid>
                                                    )
                                                }
                                            })
                                        }

                                        {
                                            addNewImageGuilty && addNewImageGuilty.map((item, index) => {
                                                if (item !== undefined)
                                                    return (
                                                        <Grid item md={2} sm={4} xs={12}>
                                                            <div className={"more-image"}>
                                                                <img src={item}/>
                                                                {
                                                                    AppState.authReducer.user.role.id !== "operator" && editCondition && editable &&
                                                                    <img onClick={() => {
                                                                        delete addNewImageGuilty[index];
                                                                        delete moreImageGuilty[index];
                                                                        setMoreImageGuilty([...moreImageGuilty]);
                                                                        setAddNewImageGuilty([...addNewImageGuilty])
                                                                    }} className={"close-icon"}
                                                                         src={require("../../assets/img/close.png")}/>
                                                                }
                                                            </div>
                                                        </Grid>
                                                    )
                                            })
                                        }

                                        {
                                            (AppState.authReducer.user.role.id === "evaluator" || AppState.authReducer.user.role.id === "super_admin") &&
                                            <Grid container={true}>
                                                <Grid item xs={12}>
                                                    <MoreImage
                                                        disable={!editable}
                                                        loading={loadingImage}
                                                        onSelect={(file) => {
                                                            setAlarm(true)
                                                            selectFile(file, "guilty", true, true);
                                                        }}
                                                    />
                                                </Grid>
                                            </Grid>
                                        }

                                    </Grid>
                                </div>
                                <Grid container xs={12}>
                                    {
                                        editCondition &&
                                        <ButtonGroup className={"btns-row"}
                                                     aria-label="outlined primary button group">
                                            <Button onClick={() => {
                                                window.scrollTo({top: 0, behavior: 'smooth'})
                                                setEditable(true)
                                            }} value={conditionStatus} variant="contained"
                                                    color={"default"}>
                                                {"ویرایش"}
                                            </Button>
                                            <Button onClick={() => {
                                                handleSetData()
                                            }} value={conditionStatus} variant="contained"
                                                    color={"default"}>
                                                {!saveLoading ? "ثبت" : <CircularProgress/>}
                                            </Button>
                                        </ButtonGroup>
                                    }

                                </Grid>
                            </>
                        }
                        <Grid spacing={3} className={classes.paper} container>
                            <Grid spacing={3} className={classes.paper} container>
                                {
                                    tab === 3 &&
                                    <>
                                        <Grid container item xs={12}>
                                            <ButtonGroup className={"btns-row"}
                                                         aria-label="outlined primary button group">
                                                {
                                                    supplementary && supplementary.length === 0 ?
                                                        <>
                                                            <Button onClick={() => {
                                                                setInjuredTab(1)
                                                            }} value={conditionStatus} variant="contained"
                                                                    color={injuredTab === 1 ? "secondary" : "default"}>
                                                                {`زیان دیده  ${supplementary[1].name_injered || 1}`}
                                                                <img className={"remove-btn-icon"} src={deleteIcon}/>
                                                            </Button>

                                                        </>
                                                        :
                                                        supplementary && supplementary.map((item, index) => {
                                                                return (
                                                                    parseInt(item.guilty) !== 1 &&
                                                                    <Button onClick={() => {
                                                                        alarm && setAlarmModal(true)
                                                                        !alarm && setInjuredTab(parseInt(supplementary[0] && supplementary[0].guilty) === 1 ? index : index + 1);
                                                                        alarm && setInjuredNextTab(parseInt(supplementary[0] && supplementary[0].guilty) === 1 ? index : index + 1)
                                                                        setAddNewImageInjured([])
                                                                    }} value={conditionStatus} variant="contained"
                                                                            color={injuredTab === (parseInt(supplementary[0] && supplementary[0].guilty) === 1 ? index : index + 1) ? "secondary" : "default"}>
                                                                        {`  ${parseInt(item && item.guilty) !== 1 && (item.name_injered || " زیان دیده " + index)}`}
                                                                        {
                                                                            editCondition &&
                                                                            <img className={"remove-btn-icon"}
                                                                                 onClick={(e) => {
                                                                                     e.target === e.currentTarget &&
                                                                                     setDeleteFileModal(true)
                                                                                     setModalId(item.id)
                                                                                 }
                                                                                 }
                                                                                 src={deleteIcon}
                                                                            />
                                                                        }

                                                                        <div className={classes.root}>
                                                                            <Modal
                                                                                disablePortal
                                                                                disableEnforceFocus
                                                                                disableAutoFocus
                                                                                open={deleteFileModal && modalId === item.id}
                                                                                onClose={() => {
                                                                                    setDeleteFileModal(false)
                                                                                }}
                                                                                aria-labelledby="server-modal-title"
                                                                                aria-describedby="server-modal-description"
                                                                                className={classes.modal}
                                                                                // container={() => rootRef.current}

                                                                            >
                                                                                <div className={classes.paper}>
                                                                                    <h4 id="server-modal-title">آیا از حذف زیان
                                                                                        دیده مطمئن هستید ؟</h4>
                                                                                    <ButtonGroup color="primary"
                                                                                                 aria-label="outlined primary button group">
                                                                                        <Button color="primary" onClick={() => {
                                                                                            setLoadingId(item.id)
                                                                                            removeInjured(item.id, index)

                                                                                        }}>
                                                                                            {(loadingId) ?
                                                                                                <CircularProgress/> : "بله"}

                                                                                        </Button>
                                                                                        <Button color="primary" onClick={() => {
                                                                                            setDeleteFileModal(false)
                                                                                        }}>
                                                                                            خیر
                                                                                        </Button>

                                                                                    </ButtonGroup>
                                                                                </div>
                                                                            </Modal>
                                                                        </div>
                                                                    </Button>
                                                                )
                                                            }
                                                        )

                                                }

                                                {
                                                    editCondition &&
                                                    <Button onClick={(e) => {
                                                        handleNewInjured(e)
                                                    }} variant="contained" color="primary">
                                                        <img className={"plus-icon"}
                                                             src={require("../../assets/img/33.png")}/>
                                                    </Button>
                                                }

                                            </ButtonGroup>
                                        </Grid>
                                        {
                                            supplementary && supplementary.length === 0 ?
                                                <Injured alarm={(e) => {
                                                    setAlarm(e)
                                                }} editCondition={editCondition && editable}
                                                         type={detail && detail.insurance_type && detail.insurance_type.id}
                                                         getData={(e) => {
                                                             setInjuredList([...injuredList, {
                                                                 ...e,
                                                                 files: moreImageInjured
                                                             }])
                                                         }} company={companyList}/>
                                                :
                                                supplementary && supplementary.map((item, index) => {
                                                    return (
                                                        injuredTab === (parseInt(supplementary[0].guilty) === 1 ? index : index + 1) &&
                                                        <>
                                                            <Injured alarm={(e) => {
                                                                setAlarm(e)
                                                            }} editCondition={editCondition && editable}
                                                                     type={detail && detail.insurance_type && detail.insurance_type.id}
                                                                     getData={(e) => {
                                                                         setInjuredList([...injuredList, {
                                                                             ...e,
                                                                             files: moreImageInjured
                                                                         }])
                                                                     }} detail={item} company={companyList}/>
                                                            <div className={"more-image-section"}>
                                                                <Grid container xs={12}>
                                                                    {
                                                                        item && item.files && item.files.map((f_item, f_index) => {
                                                                            if (f_item && f_item.name && f_item.name.includes('injured')) {
                                                                                return (
                                                                                    <Grid f_item md={2} sm={4} xs={12}>
                                                                                        <div className={"image-section"}>
                                                                                            {
                                                                                                AppState.authReducer.user.role.id !== "operator" && editCondition && editable &&
                                                                                                <img onClick={() => {
                                                                                                    setDeleteImageModal(true)
                                                                                                    setImageModalId(f_item.id)
                                                                                                }} className={"close-icon"}
                                                                                                     src={require("../../assets/img/close.png")}/>
                                                                                            }
                                                                                            <img onClick={() => {
                                                                                                setImageGallery(true);
                                                                                                setImageNumber({
                                                                                                    file: f_item.file,
                                                                                                    f_id: f_item.supplementary_id
                                                                                                })
                                                                                            }} className={classes.img}
                                                                                                 src={`${ENV.STORAGE + "/" + f_item.directory + "/" + detail.id + "/" + (f_item.supplementary_id ? f_item.supplementary_id + "/" : "/") + f_item.file}`}/>
                                                                                        </div>

                                                                                        <div className={classes.root}>
                                                                                            <Modal
                                                                                                disablePortal
                                                                                                disableEnforceFocus
                                                                                                disableAutoFocus
                                                                                                open={deleteImageModal && (imageModalId === f_item.id)}
                                                                                                onClose={() => {
                                                                                                    setDeleteImageModal(false)
                                                                                                }}
                                                                                                aria-labelledby="server-modal-title"
                                                                                                aria-describedby="server-modal-description"
                                                                                                className={classes.modal}
                                                                                            >
                                                                                                <div
                                                                                                    className={classes.paper}>
                                                                                                    <h4 id="server-modal-title">آیا
                                                                                                        از حذف تصویر
                                                                                                        مطمئن هستید ؟</h4>
                                                                                                    <ButtonGroup
                                                                                                        color="primary"
                                                                                                        aria-label="outlined primary button group">
                                                                                                        <Button
                                                                                                            color="primary"
                                                                                                            onClick={() => {
                                                                                                                removeFile(item.files[f_index], f_index)
                                                                                                            }}>
                                                                                                            {(loadingId) ?
                                                                                                                <CircularProgress/> : "بله"}
                                                                                                        </Button>
                                                                                                        <Button
                                                                                                            color="primary"
                                                                                                            onClick={() => {
                                                                                                                setDeleteImageModal(false)
                                                                                                            }}>
                                                                                                            خیر
                                                                                                        </Button>

                                                                                                    </ButtonGroup>
                                                                                                </div>
                                                                                            </Modal>
                                                                                        </div>
                                                                                    </Grid>
                                                                                )
                                                                            }
                                                                        })
                                                                    }

                                                                    {
                                                                        addNewImageInjured && addNewImageInjured.map((item, index) => {
                                                                            if (item !== undefined)
                                                                                return (
                                                                                    <Grid item md={2} sm={4} xs={12}>
                                                                                        <div className={"more-image"}>
                                                                                            <img src={item}/>

                                                                                            {
                                                                                                AppState.authReducer.user.role.id !== "operator" && editCondition && editable &&
                                                                                                <img onClick={() => {
                                                                                                    delete moreImageInjured[index];
                                                                                                    delete addNewImageInjured[index];
                                                                                                    handleDelete(index)
                                                                                                    setMoreImageInjured([...moreImageInjured]);
                                                                                                    setAddNewImageInjured([...addNewImageInjured])
                                                                                                }} className={"close-icon"}
                                                                                                     src={require("../../assets/img/close.png")}/>
                                                                                            }
                                                                                        </div>
                                                                                    </Grid>
                                                                                )
                                                                        })
                                                                    }

                                                                    {
                                                                        (AppState.authReducer.user.role.id === "evaluator" || AppState.authReducer.user.role.id === "super_admin") &&
                                                                        <Grid container={true}>
                                                                            <Grid item xs={12}>
                                                                                <MoreImage
                                                                                    disable={!editable}
                                                                                    loading={loadingImage}
                                                                                    onSelect={(file) => {
                                                                                        setAlarm(true)
                                                                                        selectFile(file, "injured", true, true);
                                                                                    }}
                                                                                />
                                                                            </Grid>
                                                                        </Grid>
                                                                    }
                                                                </Grid>
                                                            </div>
                                                        </>
                                                    )
                                                })
                                        }
                                        <Grid container xs={12}>
                                            {
                                                editCondition &&
                                                <ButtonGroup className={"btns-row"}
                                                             aria-label="outlined primary button group">
                                                    <Button onClick={() => {
                                                        setEditable(true)
                                                        window.scrollTo({top: 0, behavior: 'smooth'})
                                                    }} value={conditionStatus} variant="contained"
                                                            color={"default"}>
                                                        {"ویرایش"}
                                                    </Button>
                                                    <Button onClick={() => {
                                                        handleSetData()
                                                        setAddNewImageInjured([])
                                                        setAddNewImageGuilty([])
                                                    }} value={conditionStatus} variant="contained"
                                                            color={"default"}>
                                                        {!saveLoading ? "ثبت" : <CircularProgress/>}
                                                    </Button>
                                                </ButtonGroup>
                                            }
                                        </Grid>
                                    </>
                                }
                            </Grid>

                        </Grid>
                        {
                            tab === 4 &&
                            <Grid item xs={12}>
                                <div className={"evaluator-detail"}>
                                    <Grid container spacing={2}>
                                        <Grid item md={4} sm={6} xs={12}>
                                            <div className={classes.root}>
                                                <TextField
                                                    value={evaluatorVisitorName.name ? evaluatorVisitorName.name + " " + evaluatorVisitorName.family : ""}
                                                    className={"damage-input disable"}
                                                    label="نام ارزیاب"
                                                    variant="outlined"
                                                    size={"small"}
                                                />
                                            </div>
                                        </Grid>
                                        <Grid item md={4} sm={6} xs={12}>
                                            <div className={"date_detail"}>
                                                <div className={classes.root}>
                                                    <div style={{
                                                        pointerEvents: !(editCondition && editable) && "none",
                                                    }} className='datepicker-input-container'>
                                                        <label> تاریخ بازدید</label>
                                                        <DatePicker
                                                            isGregorian={false}
                                                            timePicker={false}
                                                            name={"date_visit"}
                                                            value={dateVisit}
                                                            onChange={issueDateGuilty => setDateVisit(issueDateGuilty)}
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                        </Grid>
                                        <Grid item md={4} sm={6} xs={12}>
                                            <div className={classes.root}>
                                                <TextField
                                                           name={"time_visit"}
                                                           onChange={(e) => {
                                                               setAlarm(true)
                                                               handleChangeElement(e)
                                                           }}
                                                           value={timeVisit}
                                                           className={`damage-input ${!(editCondition && editable) && "disable"}`}
                                                           label="زمان بازدید :"
                                                           variant="outlined"
                                                           size={"small"}

                                                />
                                            </div>
                                        </Grid>
                                    </Grid>
                                </div>
                                <div className={"damage-price"}>
                                    <div className={"description-of-accessories"}>
                                        <table cellSpacing={"0"}>
                                            <thead>
                                            <tr>
                                                <th>شرح لوازم و قطعات تعویضی و دستمزد</th>
                                                <th>مبلغ لوازم به ریال</th>
                                                <th>هزینه های دستمزد و تعمیرات به ریال</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {editCondition ?
                                                <>
                                                    <tr>
                                                        <td>
                                                            <TextField size={"small"}
                                                                       onChange={(e) => {
                                                                           setAlarm(true)
                                                                           setDescriptionAccessories(e.target.value)
                                                                       }} value={descriptionAccessories}
                                                                       className={`damage-input ${!(editCondition && editable) && "disable"}`}
                                                                       variant="outlined"/>
                                                        </td>
                                                        <td>
                                                            <TextField type={"number"} size={"small"}
                                                                       onChange={(e) => {
                                                                           setAlarm(true)
                                                                           setDamagePriceList(e.target.value);
                                                                       }} value={(damagePriceList)}
                                                                           className={`damage-input ${!(editCondition && editable) && "disable"}`}
                                                                       variant="outlined"/>
                                                        </td>
                                                        <td>
                                                            <TextField type={"number"} size={"small"}
                                                                       onChange={(e) => {
                                                                           setAlarm(true)
                                                                           setSalary(e.target.value)
                                                                       }} value={(salary)}
                                                                       className={`damage-input ${!(editCondition && editable) && "disable"}`}
                                                                       variant="outlined"/>
                                                        </td>
                                                    </tr>
                                                    {evaluatorList && evaluatorList.map((item) => {
                                                        return (
                                                            <tr>
                                                                <td>
                                                                    <div>
                                                                        {item.description}
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div>
                                                                        {separate(item.amount)}
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div>
                                                                        {separate(item.commission)}
                                                                    </div>
                                                                </td>
                                                                <td onClick={() => {
                                                                    removeItem(item.id)
                                                                }}>
                                                                    <strong style={{color: "red", cursor: "pointer"}}>
                                                                        {"حذف"}
                                                                    </strong>
                                                                </td>
                                                            </tr>
                                                        )
                                                    })}
                                                </>
                                                :
                                                evaluatorList && evaluatorList.map((item) => {
                                                    return (
                                                        <tr>
                                                            <td>
                                                                <div>
                                                                    {item.description}
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div>
                                                                    {item.amount}
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div>
                                                                    {item.commission}
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    )
                                                })
                                            }

                                            {
                                                editCondition &&
                                                <tr className={"table-btn"}>
                                                    <Button
                                                        disabled={!(damagePriceList !== "" && damagePriceList !== null && salary !== null && salary !== "" && descriptionAccessories !== "" && descriptionAccessories !== null)}
                                                        variant="contained" color="secondary" onClick={() => {
                                                        addMoreTr()
                                                    }}>
                                                        اضافه کردن به لیست لوازم
                                                    </Button>
                                                </tr>
                                            }

                                            </tbody>

                                        </table>

                                    </div>
                                    <form className={"damage-price"} noValidate autoComplete="off">
                                        <Grid container spacing={2}>
                                            <Grid item md={3} sm={6} xs={12}>
                                                <TextField
                                                    onChange={(e) => {
                                                        setPriceAppliances(e.target.value)
                                                        setAlarm(true)
                                                    }}
                                                    value={separate(priceAppliances)}
                                                    className={"damage-input disable"}
                                                    label="مبلغ لوازم"
                                                    variant="outlined"
                                                    size={"small"}

                                                />
                                            </Grid>
                                            <Grid item md={3} sm={6} xs={12}>
                                                <TextField
                                                    onChange={(e) => {
                                                        setWageAmount(e.target.value)
                                                        setAlarm(true)
                                                    }}
                                                    value={separate(wageAmount)}
                                                    className="damage-input disable"
                                                    label="مبلغ دستمزد"
                                                    size={"small"}
                                                    variant="outlined"
                                                />
                                            </Grid>
                                            <Grid item md={3} sm={6} xs={12}>
                                                <TextField
                                                           onChange={(e) => {
                                                               setTransport(e.target.value)
                                                               setAlarm(true)

                                                           }}
                                                           value={transport}
                                                           className={`damage-input ${!(editCondition && editable) && "disable"}`}
                                                           label="هزینه حمل و نقل"
                                                           variant="outlined"
                                                           size={"small"}

                                                />
                                            </Grid>
                                            <Grid item md={3} sm={6} xs={12}>
                                                <TextField
                                                           onChange={(e) => {
                                                               setAlarm(true)
                                                               setParts(e.target.value)
                                                           }} value={parts}
                                                           className={`damage-input ${!(editCondition && editable) && "disable"}`}
                                                           label="کسر قطعات داغی"
                                                           variant="outlined"
                                                           size={"small"}

                                                />
                                            </Grid>
                                            <Grid item md={3} sm={6} xs={12}>
                                                <TextField
                                                           onChange={(e) => {
                                                               setAlarm(true)
                                                               setDeductions(e.target.value)
                                                           }} value={deductions}
                                                           className={`damage-input ${!(editCondition && editable) && "disable"}`}
                                                           label="کسورات"
                                                           variant="outlined"
                                                           size={"small"}

                                                />
                                            </Grid>
                                            <Grid item md={3} sm={6} xs={12}>
                                                <TextField
                                                           value={accumulationDamages}
                                                           className="damage-input disable"
                                                           label="جمع خسارات"
                                                           variant="outlined"
                                                           size={"small"}

                                                />
                                            </Grid>

                                        </Grid>
                                    </form>
                                    <Grid item md={2} sm={6} xs={12}>
                                        <TextField
                                                   onChange={(e) => {
                                                       setAlarm(true)
                                                       setValueCar(e.target.value)
                                                   }} value={valueCar}
                                                   className={`damage-input ${!(editCondition && editable) && "disable"}`}
                                                   label="ارزش روز خودرو"
                                                   variant="outlined"
                                                   size={"small"}

                                        />
                                    </Grid>
                                    <Grid container xs={12}>
                                        {
                                            editCondition &&
                                            <ButtonGroup className={"btns-row"}
                                                         aria-label="outlined primary button group">
                                                <Button onClick={() => {
                                                    setEditable(true)
                                                    window.scrollTo({top: 0, behavior: 'smooth'})
                                                }} value={conditionStatus} variant="contained"
                                                        color={"default"}>
                                                    {"ویرایش"}
                                                </Button>
                                                <Button onClick={() => {
                                                    sendDamagePrice()
                                                }} value={conditionStatus} variant="contained"
                                                        color={"default"}>
                                                    {!saveLoading ? "ثبت" : <CircularProgress/>}

                                                </Button>
                                            </ButtonGroup>
                                        }

                                    </Grid>
                                </div>
                                <div className={"more-image-section"}>
                                    {
                                        detail && detail.files && detail.files.map((item, index) => {
                                            if (item.name && item.name.includes('evaluator') && item.name && !item.name.includes('guilty') && item.name && !item.name.includes('injured')) {
                                                return (
                                                    <Grid item md={2} sm={4} xs={12}>
                                                        <img onClick={() => {
                                                            setImageGallery(true);
                                                            setImageNumber(item.file)
                                                        }} className={classes.img}
                                                             src={`${ENV.STORAGE + "/damage" + "/" + detail.id + "/" + item.file}`}/>
                                                    </Grid>
                                                )
                                            }
                                        })
                                    }

                                </div>
                            </Grid>
                        }

                    </Grid>
                    <Grid container xs={12}>
                        <div className={classes.btns}>
                            <ButtonGroup color="primary" className={"btns-row"}
                                         aria-label="outlined primary button group">
                                {
                                    condition && condition.map((item, index) => {
                                        return (
                                            <>
                                                {
                                                    item.t_id === log &&
                                                    <Button onClick={() => {
                                                        handleSwitchCondition(item)
                                                    }} value={conditionStatus} variant="contained"
                                                            color={item.status === 12 ? "secondary" : "primary"}>
                                                        {item.title}
                                                    </Button>
                                                }
                                                {
                                                    roleCondition[AppState.authReducer.user.role.id] && roleCondition[AppState.authReducer.user.role.id].map((list, i) => {
                                                        return (
                                                            (item.t_id === list && log !== list) &&
                                                            <Button onClick={() => {
                                                                handleSwitchCondition(item)

                                                            }} value={conditionStatus} variant="contained"
                                                                    color={item.status === 12 ? "secondary" : "primary"}>
                                                                {
                                                                    item.status === 12 ? (loading.zip ?
                                                                            <CircularProgress/> : item.title)
                                                                        :
                                                                        item.title
                                                                }
                                                            </Button>
                                                        )
                                                    })
                                                }
                                            </>
                                        )
                                    })
                                }
                            </ButtonGroup>
                            {handleCondition(detail.id)}
                        </div>
                    </Grid>
                </>
            }
        </Container>

    );
};
export default InsuranceDetail;
