import React, {memo, useEffect, useState} from "react";
import {Head} from "../../components";
import Container from "@material-ui/core/Container";
import {Box} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import Api from "../../utils/api";
import {toast} from "react-toastify";
import {connect, useDispatch} from "react-redux";
import Button from "@material-ui/core/Button";
import {useHistory} from "react-router";
import {FETCH_SETTING} from "../../types";
import {Paper} from "@material-ui/core";
import {TextEditor, Loading} from './../../components'
import CurrencyFormat from "react-currency-format";
import Typography from "@material-ui/core/Typography";
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';

const Setting = memo((props) => {

    const { settingReducer, authReducer } = props;

    const dispatch = useDispatch();

    const initStat = {

    }

    const [form, setForm] = useState(initStat);
    const [loading, setLoading] = useState(false);

    useEffect(() => {

        setLoading(true);

        new Api().get('/setting').then((response) => {
            if (typeof response != "undefined") {
                dispatch({type: FETCH_SETTING, payload: response});
                let n_form = form;
                n_form['name'] = response.domain.name;
                n_form['meta_title'] = response.domain.meta_title;
                n_form['meta_description'] = response.domain.meta_description;
                n_form['introduce'] = response.domain.introduce;
                n_form['copy_right'] = response.domain.copy_right;
                n_form['blog_title'] = response.domain.blog_title;
                n_form['blog_description'] = response.domain.blog_description;


                n_form['shop_title'] = response.domain.shop_title;
                n_form['shop_description'] = response.domain.shop_description;
                n_form['free_postage'] = response.domain.free_postage;
                n_form['min_purchase'] = response.domain.min_purchase;
                n_form['default_post_cost'] = response.domain.default_post_cost;

                n_form['gateway'] = response.domain.gateway;

                setForm({...n_form});
            }

            setLoading(false);
        })
    }, []);


    const handleChangeElement = (event) => {
        let frm = {...form};
        frm[event.target.name] = event.target.value;
        setForm(frm);
    };


    const handleSubmit = (event) => {
        event.preventDefault();

        setLoading(true);

        new Api().put('/setting', form).then((response) => {
            if (typeof response !== "undefined") {
                if (response.status) {
                    toast.success(response.msg);
                } else {
                    toast.error(response.msg);
                }
            }
            setLoading(false);
        })

    }

    console.log(form)


    return(
        <Container>
            <Head data={{ 'link': '/setting', 'title': 'تنظیمات سایت'}} />
            <Box>
                <form onSubmit={handleSubmit}>
                    <Paper style={{ padding: '30px 20px'}}>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <TextField
                                    label="عنوان سایت"
                                    variant="outlined"
                                    margin='dense'
                                    value={form.name}
                                    fullWidth
                                    name='name'
                                    onChange={handleChangeElement}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    label="متا عنوان"
                                    variant="outlined"
                                    margin='dense'
                                    value={form.meta_title}
                                    fullWidth
                                    name='meta_title'
                                    onChange={handleChangeElement}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                            </Grid>
                            <Grid item xs={12} >
                                <TextField
                                    label="متا توضیحات"
                                    variant="outlined"
                                    margin='dense'
                                    value={form.meta_description}
                                    fullWidth
                                    name='meta_description'
                                    onChange={handleChangeElement}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                            </Grid>
                            <Grid item xs={12}  >
                                <TextField
                                    label="متا عنوان وبلاگ"
                                    variant="outlined"
                                    margin='dense'
                                    value={form.blog_title}
                                    fullWidth
                                    name='blog_title'
                                    onChange={handleChangeElement}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                            </Grid>
                            <Grid item xs={12}  >
                                <TextField
                                    label="متا توضیحات وبلاگ"
                                    variant="outlined"
                                    margin='dense'
                                    value={form.blog_description}
                                    fullWidth
                                    name='blog_description'
                                    onChange={handleChangeElement}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                            </Grid>
                            <Grid item xs={12} >
                                <TextEditor value={form.introduce} onChange={(value) => {
                                    let n_form = form;
                                    n_form['introduce'] = value;
                                    setForm({...n_form});
                                }} />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    label="متن کپی رایت"
                                    variant="outlined"
                                    margin='dense'
                                    value={form.copy_right}
                                    fullWidth
                                    name='copy_right'
                                    onChange={handleChangeElement}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                            </Grid>
                        </Grid>
                    </Paper>
                    <Paper style={{ padding: '30px 20px', marginTop: '15px'}}>
                        <Grid container spacing={2}>
                            <Grid item xs={12}  >
                                <TextField
                                    label="متا عنوان فروشگاه"
                                    variant="outlined"
                                    margin='dense'
                                    value={form.shop_title}
                                    fullWidth
                                    name='shop_title'
                                    onChange={handleChangeElement}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                            </Grid>
                            <Grid item xs={12}  >
                                <TextField
                                    label="متا توضیحات فروشگاه"
                                    variant="outlined"
                                    margin='dense'
                                    value={form.shop_description}
                                    fullWidth
                                    name='shop_description'
                                    onChange={handleChangeElement}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                            </Grid>
                            <Grid item xs={12} sm={4} >
                                <TextField
                                    label="حداقل خرید (تومان)"
                                    variant="outlined"
                                    margin='dense'
                                    value={form.min_purchase}
                                    fullWidth
                                    name='min_purchase'
                                    onChange={handleChangeElement}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                                <CurrencyFormat value={form.min_purchase} displayType={'text'} thousandSeparator={true}  />&nbsp;تومان
                            </Grid>
                            <Grid item xs={12} sm={4} >
                                <TextField
                                    label="خرید پستی رایگان (تومان)"
                                    variant="outlined"
                                    margin='dense'
                                    value={form.free_postage}
                                    fullWidth
                                    name='free_postage'
                                    onChange={handleChangeElement}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                                <CurrencyFormat value={form.free_postage} displayType={'text'} thousandSeparator={true}  />&nbsp;تومان
                            </Grid>
                            <Grid item xs={12} sm={4} >
                                <TextField
                                    label="هزینه پست پیش فرض (تومان)"
                                    variant="outlined"
                                    margin='dense'
                                    value={form.default_post_cost}
                                    fullWidth
                                    name='default_post_cost'
                                    onChange={handleChangeElement}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                                <CurrencyFormat value={form.default_post_cost} displayType={'text'} thousandSeparator={true}  />&nbsp;تومان
                            </Grid>
                        </Grid>
                    </Paper>
                    <Paper style={{ padding: '30px 20px', marginTop: '15px'}}>
                        <Grid container spacing={2}>
                            <Grid item xs={12} sm={4} >
                                <FormControl component="fieldset">
                                    <FormLabel component="legend">درگاه بانکی</FormLabel>
                                    <RadioGroup aria-label="gender"  onChange={handleChangeElement} name="gateway" value={form.gateway ? form.gateway : 'parsian'}>
                                        <FormControlLabel value="parsian" control={<Radio />} label="پارسیان" />
                                        <FormControlLabel value="saderat" control={<Radio />} label="صادرات" />
                                        <FormControlLabel value="zarinpal" control={<Radio />} label="زرین پال" />
                                        <FormControlLabel value="pasargad" disabled control={<Radio />} label="پاسارگاد" />
                                    </RadioGroup>
                                </FormControl>
                            </Grid>
                        </Grid>
                    </Paper>
                    {Boolean(authReducer.user.permissions.setting.setting_update.access) && <Grid item xs={12}>
                        <Button
                            disabled={loading}
                            style={{ marginTop: '15px'}}
                            type="submit"
                            variant="contained"
                            color={"primary"}
                        >
                            ذخیره تنظیمات
                        </Button>
                    </Grid>}
                </form>
                {loading && <Loading />}
            </Box>
        </Container>
    );
})

const mapStateToProps = state => ({
    settingReducer: state.settingReducer,
    authReducer: state.authReducer
})

export default connect(mapStateToProps)(Setting);