import React, {memo, useEffect, useState} from "react";
import {connect, useDispatch} from "react-redux";
import Container from "@material-ui/core/Container";
import Avatar from "@material-ui/core/Avatar";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import {Link, useHistory} from "react-router-dom";
import Box from "@material-ui/core/Box";
import {toast} from "react-toastify";
import Api from "../../../utils/api";
import PermPhoneMsgIcon from '@material-ui/icons/PermPhoneMsg';
import {Captcha, Loading} from './../../../components/index'
import ReCaptcha from "../../../components/reCaptcha";

const PasswordReset = memo((props) => {

    const dispatch = useDispatch();
    const history = useHistory();

    const [loading, setLoading] = useState(true);
    const [step, setStep] = useState(0);
    const [username, setUserName] = useState('');
    const [verifyCode, setVerifyCode] = useState('');
    const [password, setPassword] = useState('');
    const [confirm_password, setConfirmPassword] = useState('');
    const [token, setToken] = useState('');
    // captcha prefix
    const [captcha, setCaptcha] = useState(null);
    const [captchaRegenerate, setCaptchaRegenerate] = useState(true);

    useEffect(() => {
        if (captcha) {
            setCaptchaRegenerate(false);
            setLoading(false);
        }
    }, [captcha]);



    const handleSubmit = (event) => {

        event.preventDefault();

        switch (step) {
            case 0:

                setLoading(true);

                new Api().post('/auth/password/forget', { username: username, captcha: captcha }).then((response) => {
                    if (typeof response != "undefined") {
                        if (response.status) {
                            setToken(response.token);
                            setStep(parseInt(step) + 1);
                            toast.success(response.msg);
                        } else {
                            toast.error(response.msg)
                        }
                    }
                    setCaptchaRegenerate(true);
                    setLoading(false);
                })
                break;
            case 1:
                setLoading(true);
                new Api().post('/auth/verification/check', {'token': token, 'verify_code': verifyCode, captcha: captcha}).then((response) => {
                    if (typeof response != "undefined") {
                        if (response.status) {
                            setToken(response.token);
                            setStep(parseInt(step) + 1);
                            toast.success(response.msg);
                        } else {
                            toast.error(response.msg)
                        }
                    }
                    setCaptchaRegenerate(true);
                    setLoading(false);
                })
                break;
            case 2:
                if (password !== confirm_password) {
                    toast.error('کلمه عبور یکسان نیست');
                    return;
                }
                setLoading(true);
                new Api().put('/auth/password/forget', {token: token, 'password': password, captcha: captcha}).then((response) => {
                    if (typeof response != "undefined") {
                        if (response.status) {
                            toast.success(response.msg);
                            setTimeout(() => {
                                history.push('/');
                            }, 1000);
                        } else {
                            toast.error(response.msg)
                        }
                    }
                    setCaptchaRegenerate(true);
                    setLoading(false);
                })
                break;
            case 3:
                history.push('/');
                break;
        }


    }

    const stepper = () => {
        switch (step) {
            case 0:
                return(
                    <form noValidate={true} onSubmit={handleSubmit}>
                        <TextField
                            autoComplete={'off'}
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="email"
                            label="موبایل,ایمیل یا نام کاربری"
                            name="username"
                            autoFocus
                            onChange={(event) => setUserName(event.target.value)}
                        />
                        <Box mt={2} mb={2}>
                            <Button
                                disabled={loading}
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="secondary"
                            >
                                ارسال کد فعال سازی
                            </Button>
                        </Box>
                        <Box mt={2} mb={2}>
                            <Button
                                onClick={() => history.push('/')}
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                            >
                                بازگشت به صفحه ورود
                            </Button>
                        </Box>
                        <Grid container>
                        </Grid>
                    </form>
                );
            case 1:
                return (
                    <form noValidate={true} onSubmit={handleSubmit}>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id={'verify_code'}
                            label="کد فعالسازی"
                            autoFocus
                            autoComplete="off"
                            name="verify_code"
                            onChange={(event) => setVerifyCode(event.target.value)}
                        />
                        <Box mt={2} mb={2}>
                            <Button
                                disabled={loading}
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                            >
                                تایید کد فعالسازی
                            </Button>
                        </Box>
                    </form>
                );
            case 2:
                return (
                    <form noValidate={true} onSubmit={handleSubmit}>
                        <TextField
                            autoComplete={'off'}
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="email"
                            label="رمز عبور جدید"
                            name="password"
                            type="password"
                            autoFocus
                            onChange={(event) => setPassword(event.target.value)}
                        />
                        <TextField
                            autoComplete={'off'}
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="email"
                            label="تکرار رمز عبور"
                            name="confirm_password"
                            type="password"
                            onChange={(event) => setConfirmPassword(event.target.value)}
                        />
                        <Box mt={2} mb={2}>
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="secondary"
                            >
                                تغییر رمز عبور
                            </Button>
                        </Box>
                        <Grid container>
                        </Grid>
                    </form>
                );

        }
    }


    return (
        <Container maxWidth="xs">
            <Box mt={15} className='display-flex display-flex-direction-column display-flex-justify-content-center'>
                <Avatar style={{ backgroundColor: 'var(--danger-color)'}}>
                    <PermPhoneMsgIcon />
                </Avatar>
                <Typography variant="h5">
                    بازیابی رمز عبور
                </Typography>
            </Box>
            <Box mt={3}>
                { stepper() }
            </Box>
            <Box mt={2} component={"div"} style={{ position: 'relative'}}>
                {loading && <Loading />}
            </Box>
            <Box mt={10}>
                <Typography variant="body2" color="textSecondary" align="center">
                    {'Copyright © '}
                    <span style={{ fontFamily: 'tahoma'}}> {new Date().getFullYear()}</span>
                </Typography>
            </Box>
            <ReCaptcha regenerate={captchaRegenerate}  onComplete={(token) => setCaptcha(token)} />
        </Container>
    );

})


const mapStateToProps = state => ({
    authReducer: state.authReducer
});

export default connect(mapStateToProps)(PasswordReset)
