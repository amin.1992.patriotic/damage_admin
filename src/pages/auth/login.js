import React, {memo, useEffect, useState} from "react";
import {connect, useDispatch} from "react-redux";
import Container from "@material-ui/core/Container";
import Avatar from "@material-ui/core/Avatar";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import {Link, useHistory} from "react-router-dom";
import Box from "@material-ui/core/Box";
import {Captcha, Loading} from '../../components'
import {LOGIN} from "../../types";
import Api from "../../utils/api";
import {toast} from "react-toastify";
import ReCaptcha from "../../components/reCaptcha";

const Login = memo((props) => {

    const dispatch = useDispatch();
    const history = useHistory();

    const initStat = {
        username: '',
        password: '',
        captcha: '123' // set for captcha text input
    }


    const [form, setForm] = useState(initStat);
    const [loading, setLoading] = useState(true);
    // captcha prefix
    const [captcha, setCaptcha] = useState(null);
    const [captchaRegenerate, setCaptchaRegenerate] = useState(true);


    const handleChangeElement = (event) => {
        let frm = {...form};
        frm[event.target.name] = event.target.value;
        setForm(frm);
    };


    useEffect(() => {
        if (captcha) {
            setCaptchaRegenerate(false);
            setLoading(false);
        }
    }, [captcha]);



    const handleSubmit = (event) => {
        event.preventDefault()

        setLoading(true);

        form.captcha = captcha;

        new Api().post('/auth/login', form ).then((response) => {
            if (typeof response !== "undefined") {
                if (response.status) {
                    setForm(initStat);
                    // sessionStorage.setItem("construction-token" , "true")
                    dispatch({type: LOGIN, payload: {user: response.user, token: response.token}});
                    history.push('/khesarat')
                } else {
                    toast.error(response.msg);
                }
            }
            setCaptchaRegenerate(true);
            setLoading(false);
        })

    }


    return (
        <Container maxWidth="xs">
            <Box mt={15} className='display-flex display-flex-direction-column display-flex-justify-content-center'>
                <Avatar style={{ backgroundColor: 'var(--danger-color)'}}>
                    <LockOutlinedIcon />
                </Avatar>
                <Typography variant="h5">
                    پنل مدیریت
                </Typography>
            </Box>
            <Box mt={3}>
                <form noValidate={true} onSubmit={handleSubmit}>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="email"
                        label="نام کاربری"
                        name="username"
                        autoComplete="email"
                        // autoFocus
                        onChange={handleChangeElement}
                    />
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        name="password"
                        label="رمز عبور"
                        type="password"
                        id="password"
                        // autoComplete="current-password"
                        onChange={handleChangeElement}
                    />
                    <Box mt={2} mb={2}>
                        <Button
                            disabled={loading}
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                        >
                            ورود
                        </Button>
                    </Box>
                    <Grid container>
                        <Grid item xs>
                            <Typography variant={"button"} component={Link} to='/password/reset'>
                                فراموشی و تغییر رمز عبور ؟ کلیک کنید
                            </Typography>
                        </Grid>
                    </Grid>
                </form>
            </Box>
            <Box mt={2} component={"div"} style={{ position: 'relative'}}>
                {loading && <Loading />}
            </Box>
            <Box mt={10}>
                <Typography variant="body2" color="textSecondary" align="center">
                    {'Copyright © '}
                    <span style={{ fontFamily: 'tahoma'}}> {new Date().getFullYear()}</span>
                </Typography>
            </Box>
            <ReCaptcha regenerate={captchaRegenerate} onComplete={(token) => setCaptcha(token)} />
        </Container>
    );

})


const mapStateToProps = state => ({
    authReducer: state.authReducer
});

export default connect(mapStateToProps)(Login)