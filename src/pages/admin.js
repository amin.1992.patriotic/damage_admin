import React, {useEffect, useState} from 'react';
import {Loading, MapReport} from "../components";
import {Box, Container} from '@material-ui/core'
import Api from "../utils/api";
import FusionCharts from "fusioncharts";
import ReactFC from "react-fusioncharts";
// Step 4 - Include the chart type
import mscombi2d  from 'fusioncharts/fusioncharts.charts';
import moment from 'moment-jalaali'
// Step 5 - Include the theme as fusion
import FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion';
import {CircularProgress} from "@material-ui/core";
import {authRefresh} from "../actions";
import {useDispatch, useSelector} from "react-redux";
ReactFC.fcRoot(FusionCharts, mscombi2d , FusionTheme);


export const Admin = () => {


    const [mapReport, setMapReport] = useState([]);
    const [totalMapReport, setTotalMapReport] = useState([]);
    const [chart, setChart] = useState({});
    const [loading, setLoading] = useState(true);
    const dispatch = useDispatch();
    const AppState = useSelector(state => state);

    // useEffect(() => {
    //     if (moment().unix() > AppState.authReducer.expiration) {
    //         dispatch(authRefresh());
    //     }
    // }, []);



    const chartConfigs = {
        type: 'mscombi2d',// The chart type
        width: '100%', // Width of the chart
        height: '400', // Height of the chart
        dataFormat: 'json', // Data type
        dataSource: {
            "chart": {
                "caption": "گزارش میزان فروش",
                "yAxisName": "مبلغ به تومان",
                "theme": "fusion",
                "formatNumberScale" : '0',
                "decimalSeparator": ".",
                "thousandSeparator": ",",
            },
            "categories": [
                {
                    "category": chart.label
                }
            ],
            "dataset": [
                {
                    "seriesName": "تعداد کل",
                    "renderAs": "line",
                    "data": chart.total_count
                },
                {
                    "seriesName": "مبلغ کل",
                    "renderAs": "line",
                    "data": chart.total_pay
                },
                {
                    "seriesName": "تعداد ورودی درگاه",
                    "renderAs": "line",
                    "data": chart.gateway_total_count
                },
                {
                    "seriesName": "ورودی درگاه",
                    "renderAs": "line",
                    "data": chart.gateway_total_pay
                },
                {
                    "seriesName": "تعداد بن کالا",
                    "renderAs": "line",
                    "data": chart.voucher_total_count
                },
                {
                    "seriesName": "مبلغ بن کالا",
                    "renderAs": "line",
                    "data": chart.voucher_total_pay
                },
                {
                    "seriesName": "تعداد کیف پول",
                    "renderAs": "line",
                    "data": chart.wallet_total_count
                },
                {
                    "seriesName": "مبلغ کیف پول",
                    "renderAs": "line",
                    "data": chart.wallet_total_pay
                },
                {
                    "seriesName": "تعداد مرجوعی",
                    "renderAs": "line",
                    "data": chart.return_total_count
                },
                {
                    "seriesName": "مبلغ مرجوعی",
                    "renderAs": "line",
                    "data": chart.return_total_pay
                },
            ]
        }
    };

    return(
        <Container>
            {totalMapReport.length > 0 &&
                <React.Fragment>
                    <h1>گزارش کلی</h1>
                    <MapReport list={totalMapReport}/>
                </React.Fragment>
            }
        </Container>
    );
}

export default Admin;
