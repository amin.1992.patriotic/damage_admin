import React from 'react';
import plate from "../../assets/img/under_construction1.png";
import "./construction.css"
const Construction = () => {
    return (
        <div className={"construction"}>
            <span>
                همکاران عزیز، سامانه در حال بروزرسانی می باشد.
از صبر و شکیبایی شما سپاسگزاریم.
            </span>
            <img src={plate}/>
        </div>
    );
};

export default Construction;
