import Admin from './admin'

// user bundle
import User from './user'
import Role from './user/role/index'

// blog
// auth
import Login from './auth/login'
import PasswordReset from "./auth/password/reset";

//setting
import Setting from './setting'



export {
    Admin,
    Role,
    User,
    Login,
    PasswordReset,
    Setting,
    // product
}
