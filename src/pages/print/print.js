import React, {useState, useEffect} from 'react';
import "./print.css";
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import {Link, useHistory, useLocation, useParams} from "react-router-dom";
import Button from "@material-ui/core/Button";
import {ENV} from "../../env";
import ButtonGroup from '@material-ui/core/ButtonGroup';
import logo from "../../assets/img/logo.png"
import print from "../../assets/img/print.png"
import Api from "../../utils/api";
import moment from "moment-jalaali";
import plate from "../../assets/img/plate.png";
import motorPlate from "../../assets/img/motor.png";
import CONST from "../../const";

const Print = () => {
    const [evaluatorVisitorName, setEvaluatorVisitorName] = useState({});
    const [refer, setRefer] = useState({});
    const [insuranceType, setInsuranceType] = useState(null);
    const [plate, setPlate] = useState({})
    const [printHide, setPrintHide] = useState(false)
    const [data, setData] = useState([])
    const [determining, setDetermining] = useState([])
    const [companyList, setCompanyList] = useState(null);

    const param = useParams();
    useEffect(() => {
        getCompany()
        window.scrollTo({top: 0, behavior: "smooth"})
        new Api().get(`/backend/insurance/damage/${param.id}`, {})
            .then((res) => {
                if (typeof res !== "undefined") {
                    setData(res.supplementary)
                    setInsuranceType(res.insurance_type.id)
                    setRefer(res)
                }

            })
        new Api().get(`/backend/insurance/damage/${param.id}/determining`, false)
            .then((res) => {
                if (typeof res !== "undefined") {
                    setDetermining(res.result)
                }
            })
        window.scrollTo({behavior: "smooth", top: 0})
    }, [])
    const getCompany = () => {
        new Api().get("/insurance/company")
            .then((res) => {
                setCompanyList(res)
            })
    }
    const handleSelect = (key, id) => {
        let item
        if(key === "COMPANY") {
             item = companyList && companyList.filter((item) => {
                return item.id == id
            })
        } else {
             item = CONST[key] && CONST[key].filter((item) => {
                return item.id == id
            })
        }

        return item && item[0] && item[0].title
    }

    const getPlate = (list) => {
        let plate = list && list.split("-")
        let alphabet = plate && plate.filter((item) => {
            return isNaN(item) && item !== "null"
        })
        let number = plate && plate.filter((item) => {
            return !isNaN(item) && item !== "null"
        })
        const changePlate = `${number && number[2]} / ${number && number[1]} / ${alphabet && alphabet[0]} / ${number && number[0]}`
        if(number && number.length > 0) {
            return changePlate
        }
    }
    return (
        <>

            <Container>
                <div className={"print"}>
                    <div className={"print-top"}>
                        <div className={"logo-print"}>
                            <img src={logo}/>
                            <strong>شرکت ارزیابی خسارت شهر</strong>
                            <strong>شماره ثبت : 512852</strong>
                        </div>
                        <span> شماره پرونده : {param.id}</span>
                    </div>
                    <div className={"print-title"}>
                        {`گزارش نهایی ارزیابی خسارت${insuranceType === 1 ? "  ثالث ":" بدنه "}`}
                    </div>
                    <div className={"print-des"}>
                        <span>{` شرکت سهامی بیمه ${insuranceType === 1 ? handleSelect("COMPANY", data && data[0] && data[0].company_guilty) || "......" :handleSelect("COMPANY", data && data[0] && data[0].company_injered) || "......"}`}</span>
                        <span>مدیریت محترم بیمه های اتومبیل</span>
                        <span>احتراما ، با عنایت به گزارشات پیوست و اطلاعات ذیل خواهشمند است نسبت به بررسی و اقدام ، دستور مقتضی مبذول فرمایید .</span>
                    </div>
                    <div className={"print-box"}>
                        <div className={"back-logo"}>
                            <img src={logo}/>
                        </div>
                        {
                            insuranceType === 1 &&
                            <div>
                                <div className={"print-box-section"}>
                                <span>
                                    مشخصات مقصر
                                </span>
                                    <div className={"print-box-detail"}>
                                        {
                                            <div className={"print-box-detail-item"}>
                                                <span>تاریخ اعلام خسارت :</span>
                                                <span>{moment(data && data[0].created_at || new Date()).format("YYYY/M/D")}</span>
                                            </div>
                                        }
                                        {
                                            <div className={"print-box-detail-item"}>
                                                <span> نوع   خودرو :</span>
                                                <span>{handleSelect("CAR_TYPE", data && data[0].car_type_guilty)}</span>
                                            </div>
                                        }

                                        {
                                            <div className={"print-box-detail-item"}>
                                                <span>  تیپ و سیستم خودرو :</span>
                                                <span>{data && data[0].car_system_guilty}</span>
                                            </div>
                                        }
                                        {
                                            <div className={"print-box-detail-item"}>
                                                <span> اتوماتیک :</span>
                                                <span>{ data && data[0].automatic_guilty === "1" ? "بله" : "خیر"}</span>
                                            </div>
                                        }
                                        {

                                            <div className="table-body-item" style={{justifyContent: "unset"}}>
                                                شماره انتظامی :
                                                {
                                                    <span>
                                                            {getPlate(data[0].plate_guilty)}
                                                    </span>
                                                }
                                            </div>
                                        }
                                        {
                                            <div className={"print-box-detail-item"}>
                                                <span>رنگ :</span>
                                                <span>{handleSelect("COLOR", data && data[0].color_guilty)}</span>
                                            </div>
                                        }
                                        {
                                            <div className={"print-box-detail-item"}>
                                                <span> مدل :</span>
                                                <span>{data && data[0].model_guilty}</span>
                                            </div>
                                        }
                                        {
                                            <div className={"print-box-detail-item"}>
                                                <span> شماره شاسی :</span>
                                                <span>{data && data[0].chassis_number_guilty}</span>
                                            </div>
                                        }


                                        {
                                            <div className={"print-box-detail-item"}>
                                                <span> شماره موتور :</span>
                                                <span>{data && data[0].motor_number_guilty}</span>
                                            </div>
                                        }

                                        {
                                            <div className={"print-box-detail-item"}>
                                                <span>نام راننده :</span>
                                                <span>{data && data[0].name_guilty}</span>
                                            </div>
                                        }


                                        {
                                            <div className={"print-box-detail-item"}>
                                                <span>کد ملی راننده:</span>
                                                <span>{data && data[0].nationalCode_guilty}</span>
                                            </div>
                                        }

                                        {
                                            <div className={"print-box-detail-item"}>
                                                <span>نام پدر:</span>
                                                <span>{data && data[0].father_name_guilty}</span>
                                            </div>
                                        }

                                        {
                                            <div className={"print-box-detail-item"}>
                                                <span>شماره شناسنامه :</span>
                                                <span>{data && data[0].nationalCode_guilty}</span>
                                            </div>
                                        }
                                        {
                                            <div className={"print-box-detail-item"}>
                                                <span>شماره گواهینامه :</span>
                                                <span>{data && data[0].license_number_guilty}</span>
                                            </div>
                                        }

                                        {
                                            <div className={"print-box-detail-item"}>
                                                <span>شماره  پایه  گواهینامه :</span>
                                                <span>{handleSelect("BASE_NUMBER", data && data[0].base_number_guilty)}</span>
                                            </div>
                                        }


                                        {
                                            <div className={"print-box-detail-item"}>
                                                <span>تاریخ صدور :</span>
                                                <span>{moment(data && data[0].issue_date_guilty || new Date()).format("YYYY/M/D")}</span>
                                            </div>
                                        }


                                        {
                                            <div className={"print-box-detail-item"}>
                                                <span>محدودیت رانندگی :</span>
                                                <span>{data && data[0].restrictions_guilty}</span>
                                            </div>
                                        }

                                        {
                                            <div className={"print-box-detail-item"}>
                                                <span>شماره بیمه نامه:</span>
                                                <span>{data && data[0].insurance_number_guilty}</span>
                                            </div>
                                        }

                                        {
                                            <div className={"print-box-detail-item"}>
                                                <span>تاریخ شروع :</span>
                                                <span>{moment(data && data[0].start_date_guilty|| new Date()).format("YYYY/M/D")}</span>
                                            </div>
                                        }
                                        {
                                            <div className={"print-box-detail-item"}>
                                                <span>تاریخ انقضا :</span>
                                                <span>{moment(data && data[0].expirationDate_guilty|| new Date()).format("YYYY/M/D")}</span>
                                            </div>
                                        }

                                        {
                                            <div className={"print-box-detail-item"}>
                                                <span>صادره از شرکت بیمه :</span>
                                                <span>{handleSelect("COMPANY", data && data[0].company_guilty)}</span>
                                            </div>
                                        }
                                        {
                                            <div className={"print-box-detail-item"}>
                                                <span>سقف تعهدات مالی :</span>
                                                <span>{data && data[0].financial_guilty}</span>
                                            </div>
                                        }

                                        {
                                            <div className={"print-box-detail-item"}>
                                                <span> سقف تعهدات جانی :</span>
                                                <span>{data && data[0].commitment_guilty}</span>
                                            </div>
                                        }

                                        {
                                            <div className={"print-box-detail-item"}>
                                                <span> نام مالک خودرو :</span>
                                                <span>{data && data[0].owner_car_guilty}</span>
                                            </div>
                                        }

                                        {
                                            <div className={"print-box-detail-item"}>
                                                <span>کدملی مالک:</span>
                                                <span>{data && data[0].nationalCode_guilty}</span>
                                            </div>
                                        }

                                        <div className={"print-box-detail-item"}>
                                            <span>تاریخ تولد مالک:</span>
                                            <span>{moment(data && data[0].birthday_guilty|| new Date()).format("YYYY/M/D")}</span>
                                        </div>

                                        {
                                            <div className={"print-box-detail-item"}>
                                                <span>صادره از:</span>
                                                <span>{data && data[0].issued_guilty}</span>
                                            </div>
                                        }
                                        {
                                            <div className={"print-box-detail-item"}>
                                                <span>تلفن همراه :</span>
                                                <span>{data && data[0].mobile_number_guilty}</span>
                                            </div>
                                        }
                                        {
                                            <div className={"print-box-detail-item"}>
                                                <span>کد یکتا :</span>
                                                <span>{data && data[0].unique_code_guilty || data && data[0].guilty_insurance_unique_code}</span>
                                            </div>
                                        }


                                        {
                                            <div className={"print-box-detail-item"}>
                                                <span>تلفن ثابت :</span>
                                                <span>{data && data[0].phone_guilty}</span>
                                            </div>
                                        }

                                        {
                                            <div className={"print-box-detail-item full-width"}>
                                                <span>آدرس :</span>
                                                <span>{data && data[0].address_guilty}</span>
                                            </div>
                                        }
                                    </div>
                                </div>
                            </div>
                        }
                        <div>
                            {
                                data && data.map((item) => {
                                    return (
                                        parseInt(item.guilty) === 0 &&
                                        <div className={"print-box-section"}>
                                            <span>
                                                مشخصات زیان دیده
                                            </span>
                                            <div className={"print-box-detail"}>

                                                {
                                                    <div className={"print-box-detail-item"}>
                                                        <span> نوع   خودرو :</span>
                                                        <span>{handleSelect("CAR_TYPE", item.car_type_injered)}</span>
                                                    </div>
                                                }

                                                {
                                                    <div className={"print-box-detail-item"}>
                                                        <span>  تیپ و سیستم خودرو :</span>
                                                        <span>{item.car_system_injered}</span>
                                                    </div>
                                                }
                                                {
                                                    <div className={"print-box-detail-item"}>
                                                        <span> اتوماتیک :</span>
                                                        <span>{item && item.automatic_injered === "1" ? "بله" : "خیر"}</span>
                                                    </div>
                                                }
                                                {

                                                    item.plate_injered &&
                                                    <div className="table-body-item" style={{justifyContent: "unset"}}>
                                                        شماره انتظامی :

                                                        <span>
                                                      {getPlate(item.plate_injered)}
                                                    </span>
                                                    </div>
                                                }
                                                {
                                                    <div className={"print-box-detail-item"}>
                                                        <span>رنگ :</span>
                                                        <span>{handleSelect("COLOR", item.color_injered)}</span>
                                                    </div>
                                                }
                                                {
                                                    <div className={"print-box-detail-item"}>
                                                        <span> مدل :</span>
                                                        <span>{item.model_injered}</span>
                                                    </div>
                                                }
                                                {
                                                    <div className={"print-box-detail-item"}>
                                                        <span> شماره شاسی :</span>
                                                        <span>{item.chassis_number_injered}</span>
                                                    </div>
                                                }


                                                {
                                                    <div className={"print-box-detail-item"}>
                                                        <span> شماره موتور :</span>
                                                        <span>{item.motor_number_injered}</span>
                                                    </div>
                                                }

                                                {
                                                    <div className={"print-box-detail-item"}>
                                                        <span>نام راننده :</span>
                                                        <span>{item.name_injered}</span>
                                                    </div>
                                                }


                                                {
                                                    <div className={"print-box-detail-item"}>
                                                        <span>کد ملی راننده:</span>
                                                        <span>{item.national_code_injered}</span>
                                                    </div>
                                                }

                                                {
                                                    <div className={"print-box-detail-item"}>
                                                        <span>نام پدر:</span>
                                                        <span>{item.father_name_injered}</span>
                                                    </div>
                                                }

                                                {
                                                    <div className={"print-box-detail-item"}>
                                                        <span>شماره شناسنامه :</span>
                                                        <span>{item.certificate_injered}</span>
                                                    </div>
                                                }
                                                {
                                                    <div className={"print-box-detail-item"}>
                                                        <span>شماره گواهینامه :</span>
                                                        <span>{item.license_number_injered}</span>
                                                    </div>
                                                }

                                                {
                                                    <div className={"print-box-detail-item"}>
                                                        <span>شماره  پایه  گواهینامه :</span>
                                                        <span>{handleSelect("BASE_NUMBER", item.base_number_injered)}</span>
                                                    </div>
                                                }


                                                {
                                                    <div className={"print-box-detail-item"}>
                                                        <span>تاریخ صدور :</span>
                                                        <span>{moment(item.issue_date_injered|| new Date()).format("YYYY/M/D")}</span>
                                                    </div>
                                                }


                                                {
                                                    <div className={"print-box-detail-item"}>
                                                        <span>محدودیت رانندگی :</span>
                                                        <span>{item.restrictions_injered}</span>
                                                    </div>
                                                }

                                                {
                                                    <div className={"print-box-detail-item"}>
                                                        <span>شماره بیمه نامه ثالث:</span>
                                                        <span>{item.insurance_number_injered}</span>
                                                    </div>
                                                }

                                                {
                                                    <div className={"print-box-detail-item"}>
                                                        <span>تاریخ شروع ثالث:</span>
                                                        <span>{moment(item.startDate_injered|| new Date()).format("YYYY/M/D")}</span>
                                                    </div>
                                                }
                                                {
                                                    <div className={"print-box-detail-item"}>
                                                        <span>تاریخ انقضا ثالث:</span>
                                                        <span>{moment(item.expiration_date_injered|| new Date()).format("YYYY/M/D")}</span>
                                                    </div>
                                                }

                                                {
                                                    <div className={"print-box-detail-item"}>
                                                        <span>صادره از شرکت بیمه ثالث:</span>
                                                        <span>{handleSelect("COMPANY", item.company_injered)}</span>
                                                    </div>
                                                }


                                                {
                                                    <div className={"print-box-detail-item"}>
                                                        <span> نام مالک خودرو :</span>
                                                        <span>{item.owner_car_injered}</span>
                                                    </div>
                                                }

                                                {
                                                    <div className={"print-box-detail-item"}>
                                                        <span>کدملی مالک:</span>
                                                        <span>{item.owner_national_code_injered}</span>
                                                    </div>
                                                }

                                                <div className={"print-box-detail-item"}>
                                                    <span>تاریخ تولد مالک:</span>
                                                    <span>{moment(item.birthday_injered|| new Date()).format("YYYY/M/D")}</span>
                                                </div>

                                                {
                                                    <div className={"print-box-detail-item"}>
                                                        <span>صادره از:</span>
                                                        <span>{item.issued_injered}</span>
                                                    </div>
                                                }
                                                {
                                                    <div className={"print-box-detail-item"}>
                                                        <span>تلفن همراه :</span>
                                                        <span>{item.mobile_number_injered}</span>
                                                    </div>
                                                }
                                                {
                                                    <div className={"print-box-detail-item"}>
                                                        <span>کد یکتا :</span>
                                                        <span>{item.unique_code_injered || item.injered_insurance_unique_code}</span>
                                                    </div>
                                                }


                                                {
                                                    <div className={"print-box-detail-item"}>
                                                        <span>تلفن ثابت :</span>
                                                        <span>{item.phone_injered}</span>
                                                    </div>
                                                }
                                                {
                                                    <div className={"print-box-detail-item"}>
                                                        <span>شماره حساب/شبا :</span>
                                                        <span>{item.shaba}</span>
                                                    </div>
                                                }
                                                {
                                                    <div className={"print-box-detail-item"}>
                                                        <span>نام بانک :</span>
                                                        <span>{item.bank_name}</span>
                                                    </div>
                                                }
                                                {
                                                    <div className={"print-box-detail-item"}>
                                                        <span>شعبه دریافت خسارت :</span>
                                                        <span>{item.damage_branch}</span>
                                                    </div>
                                                }

                                                {
                                                    <div className={"print-box-detail-item full-width"}>
                                                        <span>آدرس :</span>
                                                        <span>{item.address_injered}</span>
                                                    </div>
                                                }
                                            </div>
                                        </div>
                                    )
                                })
                            }
                        </div>
                        <div>
                            <div className={"print-box-section"}>
                        <span>
                            اطلاعات بازدید خسارت
                        </span>
                                <div className={"print-box-detail"}>
                                    <div className={"print-box-detail-item"}>
                                        <span>نام ارزیاب :</span>
                                        <span>{(evaluatorVisitorName.name || "") + " " + (evaluatorVisitorName.family || "")}</span>
                                    </div>
                                    <div className={"print-box-detail-item"}>
                                        <span>تاریخ و زمان بازدید :</span>

                                        <span>{moment(refer.date_occurrence).format("jYYYY/jM/jD") + "  --  " + refer.time_occurrence}</span>
                                    </div>

                                    <div className={"print-box-detail-item"}>
                                        <span>مبلغ لوازم : </span>
                                        <span>{determining && determining[determining.length - 1] && determining[determining.length - 1].accessories}</span>
                                    </div>
                                    <div className={"print-box-detail-item"}>
                                        <span> مبلغ دستمزد :</span>
                                        <span>{determining && determining[determining.length - 1] && determining[determining.length - 1].commission}</span>
                                    </div>
                                    <div className={"print-box-detail-item"}>
                                        <span>هزینه حمل و نقل :</span>
                                        <span>{determining && determining[determining.length - 1] && determining[determining.length - 1].transport}</span>
                                    </div>
                                    <div className={"print-box-detail-item"}>
                                        <span> کسر قطعات داغی :</span>
                                        <span>{determining && determining[determining.length - 1] && determining[determining.length - 1].scrap_pieces}</span>
                                    </div>
                                    <div className={"print-box-detail-item"}>
                                        <span> کسورات : </span>
                                        <span>{determining && determining[determining.length - 1] && determining[determining.length - 1].deductions}</span>
                                    </div>

                                    <div className={"print-box-detail-item"}>
                                        <span>ارزش خودرو :</span>
                                        <span>{determining && determining[determining.length - 1] && determining[determining.length - 1].car_price}</span>
                                    </div>
                                    <div className={"full-width"}>
                                        <span>آدرس محل حادثه :</span>
                                        <span>{refer.address_occurrence}</span>
                                    </div>
                                    <div className={"print-box-detail-item"}>
                                        <span>نظریه کارشناس :</span>
                                        <span>تایید است</span>
                                    </div>
                                    <div className={"print-box-detail-item-border"}>
                                        <div>
                                            <span>جمع خسارت برآوردی : </span>
                                            <span>{determining && determining[determining.length - 1] && determining[determining.length - 1].total}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className={"page-break "}/>
                    </div>

                    {
                        data && data.map((item, index) => {
                            if (parseInt(item.guilty) === 1 && item.files && item.files.length > 0) {
                                return (
                                    <div className={"page-break "}>
                                        <div className={`image-title ${printHide && " hide"}`}>{`تصاویر مقصر ${item.name_guilty ? item.name_guilty : ""}`}</div>
                                        <div className={`img-print ${printHide && " hide"}`}>
                                            {
                                                item.files.map((f_item) => {
                                                    return (
                                                        <div>
                                                             <img
                                                                 src={`${ENV.STORAGE + "/" + f_item.directory + "/" + param.id + "/" + ( f_item.supplementary_id ? f_item.supplementary_id + "/" : "/"  )  + f_item.file}`}/>
                                                        </div>
                                                )
                                                })
                                            }
                                        </div>
                                    </div>
                                )
                            }
                        })
                    }
                    {
                        data && data.map((item) => {
                            if (parseInt(item.guilty)  === 0) {
                                return (
                                    <div className={"page-break "}>
                                        {
                                            item && item.files.length > 0 &&
                                            <div className={`image-title ${printHide && " hide"}`}>{`تصاویر   ${item.name_injered || "زیان دیده"}`}</div>
                                        }
                                        <div className={`img-print ${printHide && " hide"}`}>
                                            {
                                                item.files.map((f_item) => {
                                                    return (
                                                        <div>
                                                            <img
                                                                 src={`${ENV.STORAGE + "/" + f_item.directory + "/" + param.id + "/" + ( f_item.supplementary_id ? f_item.supplementary_id + "/" : "/"  )  + f_item.file}`}/>
                                                        </div>

                                                )
                                                })
                                            }
                                        </div>
                                    </div>
                                )
                            }

                        })
                    }
                    <div className={"print-btn-box"}>
                    <span>
                        <img src={print}/>
                    </span>
                        <ButtonGroup>
                            <Button className={"print-btn"} onClick={() => {
                                setTimeout(() => {
                                    setPrintHide(false);
                                    window.print()
                                }, 1000)
                            }} variant="contained" color="primary">
                                پرینت با عکس
                            </Button>
                            <Button className={"print-btn"} onClick={() => {
                                setPrintHide(true);
                                setTimeout(() => {
                                    window.print()
                                }, 1000)
                            }} variant="contained" color="secondary">
                                پرینت بدون عکس
                            </Button>
                        </ButtonGroup>
                    </div>
                </div>
            </Container>
        </>
    );
};

export default Print;
