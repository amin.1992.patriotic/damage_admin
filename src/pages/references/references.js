import React, {useEffect, useState} from 'react';
import './references.css';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import avatar from "../../assets/img/blank.png";
import Api from "../../utils/api";
import {useParams} from "react-router-dom";
import moment from "moment-jalaali";

const References = (props) => {
    const param = useParams();
    const id = param.id
    const [ data ,setData ] = useState([])
    useEffect(() => {
        new Api().get(`/backend/insurance/damage/${id}/log` , {})
            .then((res) => {
                if(typeof res.result !== "undefined") {
                    setData(res.result)
                }
            })
            .catch((err) => {
                throw err
            })
    } ,[])
    return (
        <Container>
            <Grid container>
                {
                    data && data.map((item) => {
                        return(
                            <div className={"references-list"}>
                                <div className={"references-list-avatar"}>
                                    <img src={avatar}/>
                                    <span>{item.t_full_name}</span>
                                    <span>{item.t_title}</span>
                                    <div className={"number-references"}>
                            <span>
                                 {`شماره ارجاع : ${item.id}`}
                            </span>
                                    </div>
                                </div>
                                <div className={"references-list-detail"}>
                                    <strong  className={"references-list-detail-title"}>
                                        ارجاع گیرنده
                                    </strong>
                                    <div className={"references-list-detail-item"}>
                                        <div className={"references-list-detail-item-top"}>
                                            <div>
                                                <strong> نام: </strong>
                                                <snap>{` ${item.r_full_name} / ${item.r_title} `}</snap>
                                            </div>

                                        </div>
                                        <div className={"references-list-detail-item-down"}>

                                            <div>
                                                <strong>تاریخ ارجاع : </strong>
                                                <snap>{moment(item.created_at).format('jYYYY/jMM/jDD HH:mm:ss') }</snap>
                                            </div>
                                            {
                                                item.seen === 1 &&
                                                <div>
                                                    <strong>تاریخ مشاهده :</strong>
                                                    <snap>{moment(item.last_seen).format('jYYYY/jMM/jDD HH:mm:ss') }</snap>
                                                </div>
                                            }

                                        </div>
                                        {
                                            item.refer_text !== "" && item.refer_text !== null &&
                                            <div className={"references-list-detail-item-down-ps"}>
                                                <strong>{"پی نوشت :"}</strong>
                                                <p>{item.refer_text}</p>
                                            </div>
                                        }

                                    </div>
                                </div>

                            </div>
                        )
                    })
                }

            </Grid>
        </Container>
    );
};

export default References;
