import React, {useEffect, useState} from "react";
import Container from "@material-ui/core/Container";
import {useHistory} from "react-router";
import {useSelector} from "react-redux";
import {Head, Loading} from "../../components";
import Api from "../../utils/api";
import {Paper} from "@material-ui/core";

import ListItemText from "@material-ui/core/ListItemText";
import ListItem from "@material-ui/core/ListItem";
import List from "@material-ui/core/List";
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import moment from "moment-jalaali";
import {Link} from "react-router-dom";
import './style.css'
import Divider from "@material-ui/core/Divider";
import Button from "@material-ui/core/Button";
const Insurance = (props) => {

    const { match } = props;

    const history = useHistory();

    const AppState = useSelector(state => state);

    const [transaction, setTransaction] = useState(null);
    const [loading, setLoading] = useState(true);


    useEffect(() => {

        new Api().get('/transactions/' + match.params.id).then((response) => {
            if (typeof response !== "undefined") {
                setTransaction(response);

                setLoading(false);
            }
        })
    }, []);



    if (loading) {
        return(
            <React.Fragment>
                <Loading/>
            </React.Fragment>
        ) ;
    }

    return(
        <Container>
            <Head data={{ 'link': '/transactions', 'title': 'اطلاعات تراکنش', 'description': '' }} />
            <Paper elevation={0} className={'section-to-print'}>
                <List className={'list-item'}>
                    <ListItem alignItems={'center'}>
                        <ListItemText
                            primary={'شماره تراکنش' + ':'}
                        />
                        <ListItemSecondaryAction>{transaction.id}</ListItemSecondaryAction>
                    </ListItem>
                    <Divider />
                    <ListItem alignItems={'center'}>
                        <ListItemText
                            primary={'کاربر' + ':'}
                        />
                        <ListItemSecondaryAction><Link to={'/marketer/' + transaction.user_id}>{transaction.user && (transaction.user.name && transaction.user.name + ' ' + transaction.user.family)}</Link></ListItemSecondaryAction>
                    </ListItem>
                    <Divider />
                    <ListItem alignItems={'center'}>
                        <ListItemText
                            primary={'کد پیگیری' + ':'}
                        />
                        <ListItemSecondaryAction>{transaction.ref_id}</ListItemSecondaryAction>
                    </ListItem>
                    <Divider />
                    <ListItem alignItems={'center'}>
                        <ListItemText
                            primary={'وضعیت' + ':'}
                        />
                        <ListItemSecondaryAction>{AppState.transaction.status[transaction.status]}</ListItemSecondaryAction>
                    </ListItem>
                    <Divider />
                    <ListItem alignItems={'center'}>
                        <ListItemText
                            primary={'تاریخ ثبت' + ':'}
                        />
                        <ListItemSecondaryAction>{moment(transaction.created_at).locale('fa').format('jYYYY/jMM/jDD HH:mm:ss')}</ListItemSecondaryAction>
                    </ListItem>
                    <Divider />
                    <ListItem alignItems={'center'}>
                        <ListItemText
                            primary={'تاریخ پرداخت' + ':'}
                        />
                        <ListItemSecondaryAction>{moment(transaction.payment_date).locale('fa').format('jYYYY/jMM/jDD HH:mm:ss')}</ListItemSecondaryAction>
                    </ListItem>
                    <Divider />
                    <ListItem alignItems={'center'}>
                        <ListItemText
                            primary={'نوع تراکنش' + ':'}
                        />
                        <ListItemSecondaryAction>{AppState.transaction.type && AppState.transaction.type[transaction.transactionable_type]}</ListItemSecondaryAction>
                    </ListItem>
                    <Divider />
                    <ListItem alignItems={'center'}>
                        <ListItemText
                            primary={'شناسه نوع' + ':'}
                        />
                        <ListItemSecondaryAction>{transaction.transactionable_type === 'order' &&  transaction.transactionable_id ? <Link to={'/orders/' + transaction.transactionable_id}>{'#' + transaction.transactionable_id}</Link> : 'تراکنش'}</ListItemSecondaryAction>
                    </ListItem>
                    <Divider />
                    <ListItem alignItems={'center'}>
                        <ListItemText
                            primary={'درگاه' + ':'}
                        />
                        <ListItemSecondaryAction>{AppState.transaction.gateway[transaction.gateway]}</ListItemSecondaryAction>
                    </ListItem>
                </List>
            </Paper>
            <br/>
            <br/>
            <Button onClick={() => window.print()} color={"primary"} variant={"contained"} >پرینت تراکنش</Button>
        </Container>

    );

}


export default Insurance;
