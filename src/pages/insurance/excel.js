import React, {useState, memo, useEffect, useCallback} from "react";
import ReactExport from "react-data-export";
import {Tooltip} from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import ImportExportIcon from '@material-ui/icons/ImportExport';
import moment from "moment-jalaali";
import Api from "../../utils/api";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import {Loading} from "../../components";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import {useSelector} from "react-redux";
const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

const TransactionExcelDownload = (props) => {

    const { object } = props;

    const AppState = useSelector(state => state);

    const [loading, setLoading] = useState(false);
    const [open, setOpen] = useState(false);
    const [dataSet, setDataSet] = useState([]);

    const fetchData = () => {

        setLoading(true);

        object.excel_export = 1;
        new Api().get('/transactions', object).then((response) => {
            if (typeof response !== "undefined") {
                let n_p = [];
                response.map((entity, index) => {
                    n_p[index] = [
                        {value: "'" +  entity.id + "'", style: { font: {sz: 13, bold: false}, alignment: {horizontal: "center", vertical: "center"}}},
                        {value: entity.user && (entity.user.name && entity.user.name + ' ' + entity.user.family), style: { font: {sz: 13, bold: false}, alignment: {horizontal: "center", vertical: "center"}}},
                        {value: AppState.transaction.type[entity.transactionable_type], style: { font: {sz: 13, bold: false}, alignment: {horizontal: "center", vertical: "center"}}},
                        {value: entity.transactionable_id ? entity.transactionable_id : '-', style: { font: {sz: 13, bold: false}, alignment: {horizontal: "center", vertical: "center"}}},
                        {value: entity.amount, style: { font: {sz: 13, bold: false}, alignment: {horizontal: "center", vertical: "center"}}},
                        {value: AppState.transaction.gateway ? AppState.transaction.gateway[entity.gateway] : '-', style: { font: {sz: 13, bold: false}, alignment: {horizontal: "center", vertical: "center"}}},
                        {value: entity.ref_id ? entity.ref_id : '-', style: { font: {sz: 13, bold: false}, alignment: {horizontal: "center", vertical: "center"}}},
                        {value: AppState.transaction.status[entity.status], style: { font: {sz: 13, bold: false}, alignment: {horizontal: "center", vertical: "center"}}},
                        {value: moment(entity.created_at).locale('fa').format('jYYYY/jMM/jDD HH:mm:ss'), style: { font: {sz: 13, bold: false}, alignment: {horizontal: "center", vertical: "center"}}},
                        {value: moment(entity.payment_date).locale('fa').format('jYYYY/jMM/jDD HH:mm:ss'), style: { font: {sz: 13, bold: false}, alignment: {horizontal: "center", vertical: "center"}}},
                    ]
                });

                let final = {
                    columns: [
                        {title: "شناسه", width: {wpx: 200}, style: { border: {diagonal : { style: "medium", color: {rgb: "FFFFAA00"}}}, fill: {patternType: "solid", fgColor: {rgb: "FFCCEEFF"}}, font: {sz: 15, bold: true}, alignment: {horizontal: "center", vertical: "center"}}},
                        {title: "کاربر", width: {wpx: 150}, style: { border: {diagonal : { style: "medium", color: {rgb: "FFFFAA00"}}}, fill: {patternType: "solid", fgColor: {rgb: "FFCCEEFF"}}, font: {sz: 15, bold: true}, alignment: {horizontal: "center", vertical: "center"}}},
                        {title: "نوع تراکنش", width: {wpx: 100}, style: { border: {diagonal : { style: "medium", color: {rgb: "FFFFAA00"}}}, fill: {patternType: "solid", fgColor: {rgb: "FFCCEEFF"}}, font: {sz: 15, bold: true}, alignment: {horizontal: "center", vertical: "center"}}},
                        {title: "شناسه نوع", width: {wpx: 100}, style: { border: {diagonal : { style: "medium", color: {rgb: "FFFFAA00"}}}, fill: {patternType: "solid", fgColor: {rgb: "FFCCEEFF"}}, font: {sz: 15, bold: true}, alignment: {horizontal: "center", vertical: "center"}}},
                        {title: "مبلغ", width: {wpx: 150}, style: { border: {diagonal : { style: "medium", color: {rgb: "FFFFAA00"}}}, fill: {patternType: "solid", fgColor: {rgb: "FFCCEEFF"}}, font: {sz: 15, bold: true}, alignment: {horizontal: "center", vertical: "center"}}},
                        {title: "کد پیگیری", width: {wpx: 100}, style: { border: {diagonal : { style: "medium", color: {rgb: "FFFFAA00"}}}, fill: {patternType: "solid", fgColor: {rgb: "FFCCEEFF"}}, font: {sz: 15, bold: true}, alignment: {horizontal: "center", vertical: "center"}}},
                        {title: "درگاه", width: {wpx: 150}, style: { border: {diagonal : { style: "medium", color: {rgb: "FFFFAA00"}}}, fill: {patternType: "solid", fgColor: {rgb: "FFCCEEFF"}}, font: {sz: 15, bold: true}, alignment: {horizontal: "center", vertical: "center"}}},
                        {title: "وضعیت", width: {wpx: 150}, style: { border: {diagonal : { style: "medium", color: {rgb: "FFFFAA00"}}}, fill: {patternType: "solid", fgColor: {rgb: "FFCCEEFF"}}, font: {sz: 15, bold: true}, alignment: {horizontal: "center", vertical: "center"}}},
                        {title: "تاریخ ایجاد", width: {wpx: 150}, style: { border: {diagonal : { style: "medium", color: {rgb: "FFFFAA00"}}}, fill: {patternType: "solid", fgColor: {rgb: "FFCCEEFF"}}, font: {sz: 15, bold: true}, alignment: {horizontal: "center", vertical: "center"}}},
                        {title: "تاریخ پرداخت", width: {wpx: 150}, style: { border: {diagonal : { style: "medium", color: {rgb: "FFFFAA00"}}}, fill: {patternType: "solid", fgColor: {rgb: "FFCCEEFF"}}, font: {sz: 15, bold: true}, alignment: {horizontal: "center", vertical: "center"}}},
                    ],
                    data: n_p
                };

                setDataSet([{...final}]);

                setOpen(true);
                setLoading(false);
            }
        })
    };


    return (
        <React.Fragment>
            <IconButton onClick={() => fetchData()}>
                <ImportExportIcon />
            </IconButton>
            {dataSet[0] && dataSet[0].data && dataSet[0].data.length > 0 &&  <ExcelFile filename={'transactons_' + moment().unix()} hideElement={false} element={
                <Dialog
                    fullWidth={true}
                    open={open}
                    keepMounted
                    onClose={() => {
                        setOpen(false);
                        setDataSet([]);
                    }}
                    aria-labelledby="alert-dialog-slide-title"
                    aria-describedby="alert-dialog-slide-description"
                >
                    <DialogTitle id="alert-dialog-slide-title">جلوگیری از ارسال درخواست  توسط ربات</DialogTitle>
                    <DialogContent>
                        {loading && <Loading />}
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => {
                            setOpen(false);
                            setDataSet([]);
                        }} color="primary">
                            دانلود
                        </Button>
                    </DialogActions>
                </Dialog>
            }>
                <ExcelSheet dataSet={dataSet} name={'transactons_' + moment().unix()} />
            </ExcelFile>}
            {loading && <Loading />}
        </React.Fragment>
    );

}

export default memo(TransactionExcelDownload);

