import React, {useEffect, useState} from 'react';
import Container from "@material-ui/core/Container";
import IconButton from "@material-ui/core/IconButton";
import SyncIcon from '@material-ui/icons/Sync';
import {Box, Tooltip} from "@material-ui/core";
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import SortIcon from '@material-ui/icons/Sort';
import Pagination from "react-js-pagination";
import moment from 'moment-jalaali'
import MenuItem from '@material-ui/core/MenuItem';
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionActions from '@material-ui/core/AccordionActions';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import CurrencyFormat from "react-currency-format";
import {useDispatch, useSelector} from "react-redux";
import VisibilityIcon from '@material-ui/icons/Visibility';
import {Link, useHistory, useLocation} from "react-router-dom";
import { DatePicker } from "jalali-react-datepicker";
import Api from "../../../utils/api";
import {BrandAutoComplete, Head, Loading, UserAutocomplete} from "../../../components";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import {toast} from "react-toastify";
import Autocomplete from "@material-ui/lab/Autocomplete";

const UserLog = ( props ) => {


    const location = useLocation();
    const history = useHistory();
    const  AppState  = useSelector(state => state);

    const dispatch = useDispatch();

    const [searchTime, setSearchTime] = useState("");
    const [loading, setLoading] = useState(false);
    const [entities, setEntities] = useState([]);
    const [parameter, setParameter] = useState({});
    const [open, setOpen] = useState(false);
    const [permissions, setPermissions] = useState([]);

    useEffect(() => {
        new Api().get('/users/permissions').then((response) => {
            setPermissions(response)
        });

    }, []);

    useEffect(() => {
        if (!open) {
            setParameter({})
        }
    }, [open])


    const handleChangeSearchInput = (event, type = false) => {

        if (!type) {
            _makeQueryStringParams(event.target.name, event.target.value)

        } else {
            const word = event.target.value;
            const name = event.target.name;

            if (searchTime) {
                clearTimeout(searchTime)
            }
            let loop = setTimeout(()=>{

                _makeQueryStringParams(name, word)

            },500);

            setSearchTime(loop);
        }


    }

    const handleChangeSearchInputWithAutoComplete = (name, value) => {

        _makeQueryStringParams(name, value)

    }



    const queryString = require('query-string');

    const query = queryString.parse(location.search);

    // state container
    const [params, setParams] = useState({

        page: query.page && !isNaN( query.page) ? parseInt(query.page) : 1,
        limit: query.limit && !isNaN( query.limit) ? parseInt(query.limit) : 100,
        sort_field: query.sort_field ? query.sort_field : 'id',
        sort_type: query.sort_type ? query.sort_type : 'desc',

        title: query.permission_id ? query.permission_id : '',

        from_date: query.from_date ? query.from_date : '',
        to_date: query.to_date ? query.to_date : '',

        user_id: query.user_id && !isNaN( query.user_id) ? parseInt(query.user_id) : '',
        name: query.name ? query.name : '',
        username:query.username ? query.username : '',
        national_code: query.national_code ? query.national_code : '',
        mobile: query.mobile ? query.mobile : '',
        email: query.email ? query.email : '',

    });


    const handleRequest = () => {

        setLoading(true);

        new Api().get('/users/log', params).then((response) => {
            if (typeof response !== "undefined") {
                setEntities(response);
            }
            setLoading(false);
        })
    }

    useEffect(() => {


        handleRequest();
        setTimeout(() => {
            const element = document.querySelector('body');
            if (element) {
                element.scrollIntoView({ behavior: 'smooth', block: 'start' });
            }
        }, 500);

    }, [

        params.sort_field,
        params.sort_type,
        params.page,
        params.limit,


        params.permission_id,

        params.from_date,
        params.to_date,

        params.user_id,
        params.name,
        params.username,
        params.national_code,
        params.mobile,
        params.email,
    ]);


    useEffect(() => {

        let n_params = params;

        const keys = Object.keys(query);

        for (let i = 0; i < keys.length; i++) {
            switch (keys[i]) {
                case 'page':
                    if ((n_params.page) !== (query.page)) {
                        n_params.page = parseInt(query.page);
                    }
                    break;
                case 'sort_field':
                    if (n_params.sort_field !== query.sort_field) {
                        n_params.sort_field = query.sort_field;
                        n_params.sort_type = n_params.sort_type === 'desc' ? 'asc' : 'desc';
                    }
                    break;
                case 'limit':
                    if ((n_params.limit) !== (query.limit)) {
                        n_params.limit = parseInt(query.limit);
                    }
                    break;

                case 'title':
                    if (n_params.permission_id !== query.permission_id) {
                        n_params.permission_id = query.permission_id;
                    }
                    break;

                case 'from_date':
                    if (n_params.from_date !== query.from_date) {
                        n_params.from_date = query.from_date;
                    }
                    break;
                case 'to_date':
                    if (n_params.to_date !== query.to_date) {
                        n_params.to_date = query.to_date;
                    }
                    break;


                case 'user_id':
                    if ((n_params.user_id) !== (query.user_id)) {
                        n_params.user_id = parseInt(query.user_id);
                    }
                    break;
                case 'name':
                    if (n_params.name !== query.name) {
                        n_params.name = query.name;
                    }
                    break;
                case 'username':
                    if (n_params.username !== query.username) {
                        n_params.username = query.username;
                    }
                    break;
                case 'national_code':
                    if (n_params.national_code !== query.national_code) {
                        n_params.national_code = query.national_code;
                    }
                    break;
                case 'mobile':
                    if (n_params.mobile !== query.mobile) {
                        n_params.mobile = query.mobile;
                    }
                    break;
                case 'email':
                    if (n_params.email !== query.email) {
                        n_params.email = query.email;
                    }
                    break;
                default:
                    break;
            }
        }

        setParams({...n_params});

    }, [
        query.sort_field,
        query.sort_type,
        query.page,
        query.limit,

        query.permission_id,

        query.from_date,
        query.to_date,

        query.user_id,
        query.name,
        query.username,
        query.national_code,
        query.mobile,
        query.email,
    ]);



    const _makeQueryStringParams = (type, value) => {

        let n_params = params;
        n_params.page = 1;

        switch (type) {
            case 'page':
                n_params.page = parseInt(value);
                break;
            case 'sort_field':
                n_params.sort_field = value;
                n_params.sort_type = n_params.sort_type === 'desc' ? 'asc' : 'desc';
                break;
            case 'limit':
                n_params.limit = parseInt(value);
                break;

            case 'permission_id':
                n_params.permission_id = value;
                break;

            case 'from_date':
                n_params.from_date = value;
                break;
            case 'to_date':
                n_params.to_date = value;
                break;


            case 'user_id':
                n_params.user_id = parseInt(value);
                break;
            case 'parent_id':
                n_params.parent_id = parseInt(value);
                break;
            case 'name':
                n_params.name = value;
                break;
            case 'username':
                n_params.username = value;
                break;
            case 'national_code':
                n_params.national_code = value;
                break;
            case 'mobile':
                n_params.mobile = value;
                break;
            case 'email':
                n_params.email = value;
                break;
        }

        // create url
        let url = '?';
        url += `page=${n_params.page}`;
        url += `&sort_field=${n_params.sort_field}`;
        url += `&sort_type=${n_params.sort_type}`;
        url += `&limit=${n_params.limit}`;

        url += `&permission_id=${n_params.permission_id}`;

        url += `&from_date=${n_params.from_date}`;
        url += `&to_date=${n_params.to_date}`;

        url += `&user_id=${n_params.user_id}`;

        url += `&name=${n_params.name}`;
        url += `&username=${n_params.username}`;
        url += `&national_code=${n_params.national_code}`;
        url += `&mobile=${n_params.mobile}`;
        url += `&email=${n_params.email}`;


        history.push(`/users/log/${url}`);

    };



    console.log(params)
    return(
        <Container>
            <Head data={{ 'link': '/', 'title': 'گزارش گیری از عملکرد اپراتورها', 'description': 'سرچ براساس فیلدهای مختلف' }} />
            <Box style={{ margin: '20px 0'}} boxShadow={0}>
                <Accordion defaultExpanded={false}>
                    <AccordionSummary
                        expandIcon={<ExpandMoreIcon />}
                        aria-controls="panel1c-content"
                        id="panel1c-header"
                    >
                        <div>
                            <Typography><b>جستجو</b></Typography>
                        </div>
                    </AccordionSummary>
                    <AccordionDetails>
                        <Grid container spacing={2}>
                            <Grid item xs={12} sm={6}>
                                <Autocomplete
                                    onChange={((event, value) => handleChangeSearchInputWithAutoComplete('permission_id', (value ? value.id : '') ))}
                                    options={permissions}
                                    getOptionLabel={(option) => option.title}
                                    renderInput={(params) =>
                                        <TextField
                                            {...params}
                                            fullWidth
                                            name='package_type_id'
                                            margin={"dense"}
                                            label="پرمیشن ها"
                                            variant="outlined"
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                        />
                                    }
                                />
                            </Grid>
                            <Grid item xs={12} sm={4} md={3}>
                                <UserAutocomplete onChange={(value) => handleChangeSearchInputWithAutoComplete('user_id', value)}/>
                            </Grid>
                            <Grid item xs={12} sm={4} md={3} >
                                <TextField
                                    id="outlined-name"
                                    label="موبایل"
                                    variant="outlined"
                                    margin='dense'
                                    fullWidth
                                    name='mobile'
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    onChange={(event) => handleChangeSearchInput(event, true)}
                                />
                            </Grid>
                            <Grid item xs={12} sm={4} md={3} >
                                <TextField
                                    id="outlined-name"
                                    label="نام و نام خانوادگی"
                                    variant="outlined"
                                    margin='dense'
                                    fullWidth
                                    name='name'
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    onChange={(event) => handleChangeSearchInput(event, true)}
                                />
                            </Grid>
                            <Grid item xs={12} sm={4} md={3} >
                                <TextField
                                    id="outlined-name"
                                    label="نام کاربری"
                                    variant="outlined"
                                    margin='dense'
                                    fullWidth
                                    name='username'
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    onChange={(event) => handleChangeSearchInput(event, true)}
                                />
                            </Grid>
                            <Grid item xs={12} sm={4} md={3} >
                                <TextField
                                    id="outlined-name"
                                    label="کد ملی"
                                    variant="outlined"
                                    margin='dense'
                                    fullWidth
                                    name='national_code'
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    onChange={(event) => handleChangeSearchInput(event, true)}
                                />
                            </Grid>
                            <Grid item xs={12} sm={4} md={3} >
                                <TextField
                                    id="outlined-name"
                                    label="ایمیل"
                                    variant="outlined"
                                    margin='dense'
                                    fullWidth
                                    name='email'
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    onChange={(event) => handleChangeSearchInput(event, true)}
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <div className='datepicker-input-container'>
                                    <label>از تاریخ</label>
                                    <DatePicker
                                        onClickSubmitButton={(value) => {
                                            _makeQueryStringParams('from_date', moment(value.value).locale('en').format('YYYY/MM/DD HH:mm:ss'))
                                        }}
                                    />

                                </div>
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <div className='datepicker-input-container'>
                                    <label>تا تاریخ</label>
                                    <DatePicker
                                        onClickSubmitButton={(value) => {
                                            _makeQueryStringParams('to_date', moment(value.value).locale('en').format('YYYY/MM/DD HH:mm:ss'))
                                        }}
                                    />

                                </div>
                            </Grid>
                        </Grid>
                    </AccordionDetails>
                    <Divider />
                    <AccordionActions>
                        <Button size="small" color="primary">
                            جستجو
                        </Button>
                    </AccordionActions>
                </Accordion>
            </Box>
            <Box mb={2}>
                <Grid container alignItems="center" >
                    <Grid item xs={4} sm={6}>
                        <TextField
                            select
                            value={params.limit}
                            margin='dense'
                            InputLabelProps={{
                                shrink: true,
                            }}
                            onChange={(event) => _makeQueryStringParams('limit', event.target.value)}
                        >
                            <MenuItem  value="10">10</MenuItem>
                            <MenuItem  value="20">20</MenuItem>
                            <MenuItem  value="30">30</MenuItem>
                            <MenuItem  value="50">50</MenuItem>
                            <MenuItem  value="100">100</MenuItem>
                            <MenuItem  value="200">200</MenuItem>
                            <MenuItem  value="500">500</MenuItem>
                        </TextField>
                    </Grid>
                    <Grid item xs={8} sm={6}>
                        <Pagination
                            activePage={parseInt(params.page)}
                            itemsCountPerPage={parseInt(entities.per_page)}
                            totalItemsCount={parseInt(entities.total)}
                            pageRangeDisplayed={5}
                            onChange={(page) => _makeQueryStringParams('page', page)}
                        />
                    </Grid>
                </Grid>
            </Box>
            <div style={{ display: 'flex', direction: 'row', justifyContent: 'flex-end'}}>
                <Tooltip title="سینک" onClick={() => handleRequest()}>
                    <IconButton>
                        <SyncIcon />
                    </IconButton>
                </Tooltip>
            </div>
            <div style={{ overflowX: 'auto'}}>
                <table className='table'>
                    <thead>
                    <tr>
                        <th onClick={() => _makeQueryStringParams('sort_field', 'id') } >ردیف&nbsp;{ params.sort_field === 'id' ? (params.sort_type === 'desc'  ? <ArrowDownwardIcon /> : <ArrowUpwardIcon />) : <SortIcon />}</th>
                        <th onClick={() => _makeQueryStringParams('sort_field', 'user_id') } >کاربر&nbsp;{ params.sort_field === 'user_id' ? (params.sort_type === 'desc'  ? <ArrowDownwardIcon /> : <ArrowUpwardIcon />) : <SortIcon />}</th>
                        <th onClick={() => _makeQueryStringParams('sort_field', 'permission_id') } >پرمیشن&nbsp;{ params.sort_field === 'permission_id' ? (params.sort_type === 'desc'  ? <ArrowDownwardIcon /> : <ArrowUpwardIcon />) : <SortIcon />}</th>
                        <th onClick={() => _makeQueryStringParams('sort_field', 'ip') } >IP&nbsp;{ params.sort_field === 'ip' ? (params.sort_type === 'desc'  ? <ArrowDownwardIcon /> : <ArrowUpwardIcon />) : <SortIcon />}</th>
                        <th onClick={() => _makeQueryStringParams('sort_field', 'url') } >آدرس&nbsp;{ params.sort_field === 'url' ? (params.sort_type === 'desc'  ? <ArrowDownwardIcon /> : <ArrowUpwardIcon />) : <SortIcon />}</th>
                        <th onClick={() => _makeQueryStringParams('sort_field', 'created_at') } >تاریخ&nbsp;{ params.sort_field === 'created_at' ? (params.sort_type === 'desc'  ? <ArrowDownwardIcon /> : <ArrowUpwardIcon />) : <SortIcon />}</th>
                        <th>عملیات</th>
                    </tr>
                    </thead>
                    <tbody>
                    {entities.data && entities.data.map((entity, index) => {
                        return(
                            <tr key={index}>
                                <td>{entity.id}</td>
                                <td>{entity.user && entity.user.name + ' ' + entity.user.family}</td>
                                <td>{entity.permission && entity.permission.title}</td>
                                <td>{entity.ip}</td>
                                <td>{entity.url}</td>
                                <td>{moment(entity.created_at).locale('fa').format('jYYYY/jMM/jDD HH:mm:ss')}</td>
                                <td>
                                    <Button onClick={() => {
                                        if (Object.keys(JSON.parse(entity.parameter)).length > 0) {
                                            setParameter(entity.parameter);
                                            setOpen(true);
                                        } else {
                                            toast.info('بدون پارامتر');
                                        }

                                    }} variant={"text"} color={"primary"}>پارامترهای ارسال</Button>
                                </td>
                            </tr>
                        );
                    })}
                    </tbody>
                </table>
            </div>
            <Pagination
                activePage={parseInt(params.page)}
                itemsCountPerPage={parseInt(entities.per_page)}
                totalItemsCount={parseInt(entities.total)}
                pageRangeDisplayed={5}
                onChange={(page) => _makeQueryStringParams('page', page)}
            />
            {loading && <Loading />}
            <Dialog
                open={open}
                fullWidth={true}
                onClose={() => setOpen(false)}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        <table style={{ width: '100%'}}>
                            {open && Object.keys(JSON.parse(parameter)).map((item, index) => {
                                return(
                                    <tr>
                                        <td>{item}</td>
                                        <td>{JSON.parse(parameter)[item]}</td>
                                    </tr>
                                );
                            })}
                        </table>
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => setOpen(false)} color="primary">
                        بستن
                    </Button>
                </DialogActions>
            </Dialog>
        </Container>

    );
}


export default UserLog;
