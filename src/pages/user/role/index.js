import React, {useEffect, useState} from 'react';
import {connect, useDispatch} from 'react-redux'
import {Head, RoleCreate} from "../../../components";
import {Box, Paper} from "@material-ui/core";
import {Tooltip} from "@material-ui/core";
import Container from "@material-ui/core/Container";
import IconButton from "@material-ui/core/IconButton";
import SyncIcon from '@material-ui/icons/Sync';
import AspectRatioIcon from '@material-ui/icons/AspectRatio';
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Grid from "@material-ui/core/Grid";
import Radio from "@material-ui/core/Radio";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Typography from "@material-ui/core/Typography";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";

import {roleAction} from './../../../actions'
import {SET_ROLE} from "../../../types";
import {Permissions} from './../../../components'
import role from "../../../reducers/role";


const Role = ({ fetchRoles, roles, authReducer }) => {

    const dispatch = useDispatch();

    useEffect(() => {
        handleRequest();
    }, []);

    const handleRequest = () => {
        fetchRoles();
        dispatch({type: SET_ROLE, payload: null})
    };


    return(
        <Container>
            <Head data={{ 'link': '/', 'title': 'مدیریت نقش ها', 'description': 'تعیین نقش ها و سطوح دسترسی' }} />
            <ExpansionPanel>
                <ExpansionPanelSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel1c-content"
                    id="panel1c-header"
                >
                    <div>
                        <Typography><b>راهنما</b></Typography>
                    </div>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails >
                    <ul>
                        <li>امکان حذف نقش وجود ندارد.</li>
                        <li>جهت ویرایش عنوان از طریق دکمه پلاس کلید اصلی را تکرار کنید.</li>
                    </ul>
                </ExpansionPanelDetails>
            </ExpansionPanel>
            <Box mt={5}>
                <div style={{ display: 'flex', direction: 'row', justifyContent: 'flex-end'}}>
                    {authReducer.user.permissions.user && Boolean(authReducer.user.permissions.user.role_store) && <RoleCreate reload={() => handleRequest()}/>}
                    {authReducer.user.permissions.user && Boolean(authReducer.user.permissions.user.role_has_permissions) && roles.role &&  <Permissions role_id={roles.role}/>}
                    <Tooltip title="سینک" onClick={() => handleRequest()}>
                        <IconButton>
                            <SyncIcon />
                        </IconButton>
                    </Tooltip>
                </div>
                <Paper elevation={1} style={{ padding: '15px'}}>
                    <Grid container>
                        {roles.data && roles.data.map((item, index) => {
                            return(
                                <Grid key={index} item xs={12} sm={3}>
                                    <FormControlLabel key={index} control={
                                        <Radio
                                            checked={(roles.role ? roles.role : false ) === item.id}
                                            value={item.id}
                                            onChange={() => dispatch({type: SET_ROLE, payload: item.id})}
                                        />
                                    } label={item.title ? item.title : item.id} />
                                </Grid>
                            );
                        })}
                    </Grid>
                </Paper>
            </Box>
        </Container>

    );
}


const mapStateToProps = state => ({
    roles: state.roleReducer,
    authReducer: state.authReducer
});

const mapDispatchToProps = (dispatch) => ({
    fetchRoles: () => {
        dispatch(roleAction.fetchRoles())
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(Role)
