import React, {useEffect, useState} from 'react';
import {connect, useDispatch} from 'react-redux'
import {Head, UserCreate, UserEdit, UserPassword} from "../../components";
import {Paper} from "@material-ui/core";
import Container from "@material-ui/core/Container";
import IconButton from "@material-ui/core/IconButton";
import SyncIcon from '@material-ui/icons/Sync';
import {roleAction, userAction} from "../../actions";
import {Box, Tooltip} from "@material-ui/core";
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import SortIcon from '@material-ui/icons/Sort';
import Pagination from "react-js-pagination";
import Chip from "@material-ui/core/Chip";
import LockIcon from '@material-ui/icons/Lock';
import EditIcon from '@material-ui/icons/Edit';
import moment from 'moment-jalaali'
import './style.css'
import {ENV} from "../../env";
import MenuItem from '@material-ui/core/MenuItem';
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import Api from "../../utils/api";
import {toast} from "react-toastify";

const Users = ({ fetchUsers, users, roles, fetchRoles, authReducer }) => {

    const dispatch = useDispatch();

    const [page, setPage] = useState(1);
    const [sort_field, setSortField] = useState('id');
    const [sort_type, setSortType] = useState('desc');
    const [limit, setLimit] = useState(50);
    const [filter, setFilter] = useState({});

    useEffect(() => {
        fetchRoles();
    }, [])

    useEffect(() => {
        handleRequest();
    }, [sort_field, sort_type, limit, filter, page]);

    const handleChangeSearchInput = (event) => {

        let search = filter;

        search[event.target.name] = event.target.value;

        setFilter({...search})

    }


    const handleRequest = () => {
        fetchUsers({
            filter,
            sort_field: sort_field,
            sort_type: sort_type,
            page: page,
            limit: limit
        })
    };

    const handleChangeStatue = (id) => {
        new Api().put(`/backend/users/${id}/status`).then((response) => {
            if (typeof response !== "undefined") {
                if (response.status) {
                    handleRequest()
                } else {
                    toast.error(response.msg);
                }
            }
        })
    }


    return(
        <Container>
            <Head data={{ 'link': '/', 'title': 'مدیریت کاربران', 'description': 'کاربران را مدیریت کنید' }} />
            <Box mb={5}>
                <ExpansionPanel>
                    <ExpansionPanelSummary
                        expandIcon={<ExpandMoreIcon />}
                        aria-controls="panel1c-content"
                        id="panel1c-header"
                    >
                        <div>
                            <Typography>جستجو در لیست</Typography>
                        </div>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <Grid container spacing={2}>
                            <Grid item xs={12} sm={4} md={3} >
                                <TextField
                                    id="outlined-name"
                                    label="نام کاربر"
                                    variant="outlined"
                                    margin='dense'
                                    fullWidth
                                    name='name'
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    onChange={handleChangeSearchInput}
                                />
                            </Grid>
                            <Grid item xs={12} sm={4} md={3} >
                                <TextField
                                    id="outlined-name"
                                    label="نام خانوادگی کاربر"
                                    variant="outlined"
                                    margin='dense'
                                    fullWidth
                                    name='family'
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    onChange={handleChangeSearchInput}
                                />
                            </Grid>
                            <Grid item xs={12} sm={4} md={3} >
                                <TextField
                                    label="موبایل"
                                    variant="outlined"
                                    margin='dense'
                                    fullWidth
                                    name='mobile'
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    onChange={handleChangeSearchInput}
                                />
                            </Grid>
                            <Grid item xs={12} sm={4} md={3} >
                                <TextField
                                    label="ایمیل"
                                    variant="outlined"
                                    margin='dense'
                                    fullWidth
                                    name='email'
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    onChange={handleChangeSearchInput}
                                />
                            </Grid>
                            <Grid item xs={12} sm={4} md={3} >
                                <TextField
                                    label="username"
                                    variant="outlined"
                                    margin='dense'
                                    fullWidth
                                    name='username'
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    onChange={handleChangeSearchInput}
                                />
                            </Grid>
                            <Grid item xs={12} sm={4} md={3} >
                                <TextField
                                    select
                                    label="نقش"
                                    variant="outlined"
                                    value={filter.role_id}
                                    margin='dense'
                                    fullWidth
                                    name='role_id'
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    onChange={handleChangeSearchInput}
                                >
                                    <MenuItem key={0} value={0}>انتخاب</MenuItem>
                                    {roles && roles.data && roles.data.map((role, index) => {
                                        return <MenuItem key={index} value={role.id}>{role.title ? role.title : role.id}</MenuItem>
                                    })}
                                </TextField>
                            </Grid>
                            <Grid item xs={12} sm={4} md={3} >
                                <TextField
                                    select
                                    label="وضعیت"
                                    variant="outlined"
                                    value={filter.status}
                                    margin='dense'
                                    fullWidth
                                    name='status'
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    onChange={handleChangeSearchInput}
                                >
                                    <MenuItem key={0} value={-1}>انتخاب</MenuItem>
                                    <MenuItem key={1} value={1}>فعال</MenuItem>
                                    <MenuItem key={2} value={0}>غیرفعال</MenuItem>
                                </TextField>
                            </Grid>
                        </Grid>
                    </ExpansionPanelDetails>
                    <Divider />
                    <ExpansionPanelActions>
                        <Button color="primary">
                            جستجو
                        </Button>
                    </ExpansionPanelActions>
                </ExpansionPanel>
            </Box>
            <Box mb={2}>
                <Grid container alignItems="center" >
                    <Grid item xs={4} sm={6}>
                        <TextField
                            select
                            value={limit}
                            margin='dense'
                            InputLabelProps={{
                                shrink: true,
                            }}
                            onChange={(event) => {
                                setLimit(event.target.value) ; setPage(1)
                            }}
                        >
                            <MenuItem  value="50">50</MenuItem>
                            <MenuItem  value="100">100</MenuItem>
                            <MenuItem  value="200">200</MenuItem>
                        </TextField>
                    </Grid>
                    <Grid item xs={8} sm={6}>
                        <Pagination
                            activePage={page}
                            itemsCountPerPage={parseInt(users.users.per_page)}
                            totalItemsCount={parseInt(users.users.total)}
                            pageRangeDisplayed={5}
                            onChange={(prev) => setPage(prev)}
                        />
                    </Grid>
                </Grid>
            </Box>
            <div style={{ display: 'flex', direction: 'row', justifyContent: 'flex-end'}}>
                {Boolean(authReducer.user.permissions.user.user_store) && <UserCreate reload={() => handleRequest()}/>}
                <Tooltip title="سینک" onClick={() => handleRequest()}>
                    <IconButton>
                        <SyncIcon />
                    </IconButton>
                </Tooltip>
            </div>
            <>
                <div style={{ overflowX: 'auto'}}>
                    <table className='table'>
                        <thead>
                        <tr>
                            <th onClick={() => { setSortField('id'); setSortType(sort_type === 'desc' ? 'asc' : 'desc') }}>#&nbsp;{ sort_field === 'id' ? (sort_type === 'desc'  ? <ArrowDownwardIcon /> : <ArrowUpwardIcon />) : <SortIcon />}</th>
                            <th onClick={() => { setSortField('name'); setSortType(sort_type === 'desc' ? 'asc' : 'desc') }}>نام &nbsp;{sort_field === 'name' ? (sort_type === 'desc'  ? <ArrowDownwardIcon /> : <ArrowUpwardIcon />) : <SortIcon />}</th>
                            <th onClick={() => { setSortField('role_id'); setSortType(sort_type === 'desc' ? 'asc' : 'desc') }}>نقش&nbsp;{sort_field === 'role_id' ? (sort_type === 'desc'  ? <ArrowDownwardIcon /> : <ArrowUpwardIcon />) : <SortIcon />}</th>
                            <th onClick={() => { setSortField('company_id'); setSortType(sort_type === 'desc' ? 'asc' : 'desc') }}>شرکت&nbsp;{sort_field === 'company_id' ? (sort_type === 'desc'  ? <ArrowDownwardIcon /> : <ArrowUpwardIcon />) : <SortIcon />}</th>
                            <th onClick={() => { setSortField('region_id'); setSortType(sort_type === 'desc' ? 'asc' : 'desc') }}>شهر&nbsp;{sort_field === 'region_id' ? (sort_type === 'desc'  ? <ArrowDownwardIcon /> : <ArrowUpwardIcon />) : <SortIcon />}</th>
                            <th onClick={() => { setSortField('mobile'); setSortType(sort_type === 'desc' ? 'asc' : 'desc') }}>موبایل&nbsp;{sort_field === 'mobile' ? (sort_type === 'desc'  ? <ArrowDownwardIcon /> : <ArrowUpwardIcon />) : <SortIcon />}</th>
                            <th onClick={() => { setSortField('username'); setSortType(sort_type === 'desc' ? 'asc' : 'desc') }}>نام کاربری&nbsp;{sort_field === 'email' ? (sort_type === 'desc'  ? <ArrowDownwardIcon /> : <ArrowUpwardIcon />) : <SortIcon />}</th>
                            <th onClick={() => { setSortField('created_at'); setSortType(sort_type === 'desc' ? 'asc' : 'desc') }}>تاریخ ثبت نام&nbsp;{sort_field === 'created_at' ? (sort_type === 'desc'  ? <ArrowDownwardIcon /> : <ArrowUpwardIcon />) : <SortIcon />}</th>
                            <th onClick={() => { setSortField('status'); setSortType(sort_type === 'desc' ? 'asc' : 'desc') }}>وضعیت&nbsp;{sort_field === 'status' ? (sort_type === 'desc' ? <ArrowDownwardIcon/> : <ArrowUpwardIcon/>) : <SortIcon/>}</th>
                            <th>عملیات</th>
                        </tr>
                        </thead>
                        <tbody>
                        {users.users.data && users.users.data.map((user, index) => {
                            return(
                                <tr key={index}>
                                    <td>{user.id}</td>
                                    <td>{user.name + ' ' + user.family}</td>
                                    <td>{user.role ? user.role.title ? user.role.title: user.role.key : '-'}</td>
                                    <td>{user.company && user.company.title}</td>
                                    <td>{user.region && user.region.title}</td>
                                    <td>{user.mobile}</td>
                                    <td>{user.username}</td>
                                    <td style={{ direction: 'ltr'}}>{moment(user.created_at).locale('fa').format('jYYYY/jMM/jDD HH:mm:ss')}</td>
                                    <td>
                                        <Tooltip title="وضعیت">
                                            <Chip size={"small"} variant={"outlined"} color={user.status ? "primary": "secondary"}  label={user.status ? 'فعال' : 'بلاک'} clickable={true} onClick={() => handleChangeStatue(user.id)} />
                                        </Tooltip>
                                    </td>
                                    <td style={{ display:'flex', 'direction': 'row', justifyContent: 'center'}}>
                                        {Boolean(authReducer.user.permissions.user.user_update) && <UserEdit user={user.id} reload={() => handleRequest()} />}
                                        {Boolean(authReducer.user.permissions.user.user_change_password) && <UserPassword user={user.id}/>}
                                    </td>
                                </tr>
                            );
                        })}
                        </tbody>
                    </table>
                </div>
                <Pagination
                    activePage={page}
                    itemsCountPerPage={parseInt(users.users.per_page)}
                    totalItemsCount={parseInt(users.users.total)}
                    pageRangeDisplayed={5}
                    onChange={(prev) => setPage(prev)}
                />
            </>
        </Container>

    );
}


const mapStateToProps = state => ({
    users: state.userReducer,
    roles: state.roleReducer,
    authReducer: state.authReducer

});

const mapDispatchToProps = (dispatch) => ({
    fetchRoles: () => {
        dispatch(roleAction.fetchRoles())
    },
    fetchUsers: (object) => {
        dispatch(userAction.fetchUsers(object))
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(Users)
