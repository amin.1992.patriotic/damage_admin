import React, {useEffect, useState} from 'react';
import "./filing.css"
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import {Link, useHistory, useLocation , useParams} from "react-router-dom";
import Button from "@material-ui/core/Button";
import {toast} from "react-toastify";
import Api from "../../utils/api";
import ButtonGroup from '@material-ui/core/ButtonGroup';

import Injured from "../../components/injured/injured";
import Guilty from "../../components/guilty/guilty";

const MyComponent = () => {
    const  param = useParams();
    const history = useHistory();
    const detail = history.location.state.detail
    const [injuredList , setInjuredList] = useState([])
    const [guiltyList , setGuiltyList] = useState([])





    return (
        <Container>
            <Grid item xs={12}>
                <>
                    <Guilty getData={ (e) => {setGuiltyList(e) } } detail={detail}/>
                    <Injured getData={ (e) => {setInjuredList([...injuredList , e]) } } detail={detail}/>

                </>
                <ButtonGroup variant="contained" aria-label="outlined primary button group">
                    <Button onClick={() => {

                    }} variant="contained" color="primary">
                        {"ثبت"}
                    </Button>

                </ButtonGroup>
            </Grid>

        </Container>
    );
};

export default MyComponent;
