import React, {useEffect, useState} from 'react';
import {BrandAutoComplete, Head, Loading, UserAutocomplete} from "../../components";
import Pagination from "react-js-pagination";
import Button from '@material-ui/core/Button';
import {useDispatch, useSelector} from "react-redux";
import {Link, useHistory, useLocation, useParams} from "react-router-dom";
import ButtonGroup from '@material-ui/core/ButtonGroup';
import moment from "moment-jalaali";
import MenuItem from '@material-ui/core/MenuItem';
import Api from "../../utils/api";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Typography from "@material-ui/core/Typography";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Divider from "@material-ui/core/Divider";
import AccordionActions from "@material-ui/core/AccordionActions";
import {Box, CircularProgress} from "@material-ui/core";
import DatePicker from 'react-datepicker2';
import "./archive.css"
import Modal from "@material-ui/core/Modal";
import {makeStyles} from '@material-ui/core/styles';
import {toast} from "react-toastify";

const Archive = (props) => {


    const history = useHistory()
    const location = useLocation();
    const role = useSelector((state) => state.authReducer.user.role.id)
    const queryString = require('query-string');

    const query = queryString.parse(location.search);


    useEffect(() => {
        new Api().get(`/backend/roles/evaluator/users`)
            .then((res) => {
                if (typeof res !== "undefined") {
                    setEvaluator(res)
                }
            })
    }, [])

    const condition = [
        {status: 2, title: "رد درخواست"},
        {status: 3, title: "در حال بررسی"},
        {status: 4, title: "تکمیل شده"},
        {status: 5, title: "نقص مدارک"},
        {status: 6, title: "ارجاع به ارزیاب"},
        {status: 7, title: "ارجاع به کارشناس"},
        {status: 10, title: "ارجاع به بیمه گر"},
    ]
    const AppState = useSelector(state => state);

    const [loading, setLoading] = useState(false);
    const [evaluator, setEvaluator] = useState("");
    const [data, setData] = useState();
    const [city, setCity] = useState("");
    const [insurance, setInsurance] = useState(query.insurance_type ||-1);
    const [mobile, setMobile] = useState(query.mobile || null);
    const [status, setStatus] = useState(query.status ||-1);
    const [referId, setReferId] = useState(query.refer_id ||-1);
    const [fromDate, setFromDate] = useState(query.from_date || false);
    const [toDate, setToDate] = useState(false);
    const [receiver, setReceiver] = useState(query.receiver ||-1);
    const [page, setPage] = useState(parseInt(query.page) || 1);
    const [loop, setloop] = useState(1);
    const [id, setId] = useState(query.id || -1);
    const [deleteFileModal, setDeleteFileModal] = useState(false);
    const [limit, setLimit] = useState(parseInt(query.limit) || 50);
    const [loadingId, setLoadingId] = useState(null);
    const [mainLoading, setMainLoading] = useState(false);


    useEffect(() => {
        if(parseInt(query.limit) !== limit) {
            setPage(1)
            handleRequest("page" , 1)
        }
    }, [ limit ]);

    useEffect(() => {
        handleRequest()
    }, []);


    useEffect(() => {
        new Api().get("/region")
            .then(((res) => {
                setCity(res)
            }))
    }, [])
    const handleArchive = (id) => {
        setLoading(true)
        new Api().post(`/backend/insurance/damage/archive` , {id:id , archive:"unArchive"})
            .then((res) => {
                if(typeof res !=="undefined") {
                    if (res.status) {
                        toast.success(res.msg)
                        handleRequest()
                    }
                }
            })
            .catch((err) => {
                throw err
            })
    }
    const useStyles = makeStyles((theme) => ({
        root: {
            display:"flex",
            alignItems:'center',
            justifyContent:"center"
        },
        modal: {
            display: 'flex',
            padding: theme.spacing(1),
            alignItems: 'center',
            justifyContent: 'center',
        },
        paper: {
            width: 400,
            backgroundColor: theme.palette.background.paper,
            border: '2px solid #000',
            boxShadow: theme.shadows[5],
            padding: theme.spacing(2, 4, 3),
        },
    }));
    const classes = useStyles();

    const deleteFile = (id) => {
        setLoading(true)
        new Api().delete(`/backend/insurance/damage/${id}`)
            .then((res) => {
                if (res.status) {
                    handleRequest()
                }
            })
            .catch((err) => {
                throw err
            })
    }

    const handleRequest = (name, value) => {
        setMainLoading(true)

        const modifiedQuery = {...query,[name]: value}
        for (const key of Object.keys(modifiedQuery)) {
            if (modifiedQuery[key] === "") {
                delete modifiedQuery[key];
            }
        }
        history.replace({
            pathname: location.pathname,
            search: queryString.stringify(modifiedQuery)
        })
        if (name === "mobile" || name === "id") {
            clearTimeout(loop)
            setloop(
                setTimeout(() => {
                    new Api().get(`/backend/insurance/damage`, {
                        op:"archive",
                        receiver:query.receiver || receiver,
                        refer_id:query.refer_id || referId ,
                        status:query.status ||  status ,
                        insurance_type:query.insurance_type ||  insurance,
                        city_id: -1,
                        from_date:query.from_date || fromDate,
                        to_date:query.to_date || toDate,
                        injured: -1,
                        mobile:query.mobile || mobile,
                        page:query.page || page,
                        id:  query.id || id,
                        limit:query.limit || limit,
                        [name]: value
                    })
                        .then((res) => {
                            setMainLoading(false)
                            setLoading(false)
                            if (typeof res !== "undefined") {
                                if (res.status || typeof res.status === "undefined") {
                                    setData(res)
                                }
                            }
                        })
                        .catch((err) => {
                            setMainLoading(false)
                            setLoading(false)
                            throw err
                        })
                }, 1000)
            )
        } else {
            new Api().get(`/backend/insurance/damage`, {
                op:"archive",
                receiver:query.receiver || receiver,
                refer_id:query.refer_id || referId ,
                status:query.status ||  status ,
                insurance_type:query.insurance_type ||  insurance,
                city_id: -1,
                from_date:query.from_date || fromDate,
                to_date:query.to_date || toDate,
                injured: -1,
                mobile:query.mobile || mobile,
                page:query.page || page,
                id:  query.id || id,
                limit:query.limit || limit,
                [name]: value
            })
                .then((res) => {
                    if (typeof res !== "undefined") {
                        setMainLoading(false)
                        setLoading(false)
                        if (res.status || typeof res.status === "undefined") {
                            setData(res)
                        }
                    }
                })
                .catch((err) => {
                    setMainLoading(false)
                    setLoading(false)
                    throw err
                })
        }
    }
    const _makeQueryStringParams = async (name, value) => {
        switch (name) {
            case "insurance_type":
                await setInsurance(value)
                break
            case "mobile":
                await setMobile(value)
                break
            case "status":
                setStatus(value)
                break
            case "from_date":
                setFromDate(value)
                break
            case "to_date":
                setToDate(value)
                break
            case "receiver":
                setReceiver(value)
                break
            case "refer_id":
                setReferId(value)
                break
            case "page":
                setPage(value)
                break
            case "limit":
                setLimit(value)
                break
            case "id":
                setId(value)
                break
        }
        await handleRequest(name, value)
    };


    const handleStatus = (status) => {
        switch (parseInt(status)) {
            case -1 :
                return "انتخاب"
            case 0 :
                return "ارجاع  به اپراتور"
            case 2 :
                return "رد درخواست"
            case 3 :
                return "در حال بررسی"
            case 4 :
                return "تکمیل شده"
            case 5 :
                return "نقص مدارک"
            case 6 :
                return "ارجاع به ارزیاب"
            case 7 :
                return "ارجاع به کارشناس"
            case 8 :
                return "ارجاع به  کارشناس نهایی"
            case 10 :
                return "ارجاع به بیمه گر"
            case 13 :
                return "ارجاع به بیمه گر"
        }
    }

    return (
        <>
            {
                (mainLoading && !loading)  ? <div style={{display:'flex' , minHeight:"500px" , alignItems:"center" ,justifyContent:"center"}}><CircularProgress/></div> :
                    <div style={{padding: '0 20px'}}>
                        <Box style={{margin: '20px 0'}} boxShadow={0}>
                            <Accordion defaultExpanded={false}>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon/>}
                                    aria-controls="panel1c-content"
                                    id="panel1c-header"
                                >
                                    <div>
                                        <Typography>جستجو</Typography>
                                    </div>
                                </AccordionSummary>
                                <AccordionDetails>

                                    <Grid container spacing={2}>
                                        <Grid item xs={12} sm={4} md={3}>
                                            <UserAutocomplete name={"کاربر قبلی"}
                                                              onChange={(value) => _makeQueryStringParams("receiver", value)}/>
                                        </Grid>
                                        <Grid item xs={12} sm={4} md={3}>
                                            <UserAutocomplete name={"کاربر فعلی"}
                                                              onChange={(value) => _makeQueryStringParams("refer_id", value)}/>
                                        </Grid>
                                        <Grid item xs={12} sm={4} md={3}>
                                            <TextField
                                                select
                                                label="رشته"
                                                value={insurance}
                                                variant="outlined"
                                                margin='dense'
                                                fullWidth
                                                name='insurance'
                                                InputLabelProps={{
                                                    shrink: true,
                                                }}
                                                onChange={(event) => {
                                                    _makeQueryStringParams("insurance_type", event.target.value);
                                                }}
                                            >
                                                <MenuItem key={-1} value={-1}>انتخاب</MenuItem>
                                                <MenuItem key={2} value={2}>بدنه</MenuItem>
                                                <MenuItem key={1} value={1}>ثالث</MenuItem>
                                            </TextField>
                                        </Grid>

                                        <Grid item xs={12} sm={4} md={3}>
                                            <TextField
                                                select
                                                label="وضعیت"
                                                value={status}
                                                variant="outlined"
                                                margin='dense'
                                                fullWidth
                                                name='insurance'
                                                InputLabelProps={{
                                                    shrink: true,
                                                }}
                                                onChange={(event) => {
                                                    _makeQueryStringParams("status", event.target.value);
                                                }}
                                            >
                                                <MenuItem key={-1} value={-1}>انتخاب</MenuItem>
                                                {
                                                    condition && condition.map((item) => {
                                                        return (
                                                            <MenuItem key={item.status} value={item.status}>{item.title}</MenuItem>
                                                        )
                                                    })
                                                }
                                            </TextField>
                                        </Grid>
                                        <Grid item xs={12} sm={4} md={3}>
                                            <TextField
                                                id="outlined-basic"
                                                variant="outlined"
                                                margin="dense"
                                                label="کد پرونده"
                                                name="id"
                                                fullWidth
                                                value={id === -1 ? "" : id}
                                                onChange={(event) => {
                                                    _makeQueryStringParams("id", event.target.value === "" ? -1 : event.target.value);
                                                }}
                                            />
                                        </Grid>
                                        <Grid item xs={12} sm={4} md={3}>
                                            <TextField
                                                id="outlined-basic"
                                                variant="outlined"
                                                margin="dense"
                                                label="موبایل"
                                                name="mobile"
                                                fullWidth
                                                value={mobile}
                                                onChange={(event) => {
                                                    _makeQueryStringParams("mobile", event.target.value);
                                                }}
                                            />
                                        </Grid>
                                        <Grid item xs={12} sm={4} md={3}>
                                            <div className={"date_detail"}>
                                                <div className={classes.root}>
                                                    <div style={{width:"100%"}} className='datepicker-input-container'>
                                                        <label style={{marginTop:"-1px"}} >از تاریخ</label>
                                                        <DatePicker
                                                            isGregorian={false}
                                                            timePicker={false}
                                                            name={"date_occurrence"}
                                                            onChange={(value) => {
                                                                _makeQueryStringParams('from_date', moment(value).locale('en').format('YYYY/MM/DD'))
                                                            }}
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                        </Grid>
                                        <Grid item xs={12} sm={4} md={3}>
                                            <div className={"date_detail"}>
                                                <div className={classes.root}>
                                                    <div style={{width:"100%"}} className='datepicker-input-container'>
                                                        <label style={{marginTop:"-1px"}} >تا تاریخ</label>
                                                        <DatePicker
                                                            isGregorian={false}
                                                            timePicker={false}
                                                            name={"date_occurrence"}
                                                            onChange={(value) => {
                                                                _makeQueryStringParams('to_date', moment(value).locale('en').format('YYYY/MM/DD'))
                                                            }}
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                        </Grid>

                                    </Grid>
                                </AccordionDetails>
                                <Divider/>
                                <AccordionActions>
                                    <Button size="small" color="primary">
                                        جستجو
                                    </Button>
                                </AccordionActions>
                            </Accordion>
                        </Box>

                        <Head data={{'link': '/', 'title': 'پرونده های بایگانی شده'}}/>
                        <Grid item xs={4} sm={6}>
                            <TextField
                                select
                                value={limit}
                                margin='dense'
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                onChange={(event) => {
                                    setLimit(event.target.value);
                                    _makeQueryStringParams('limit', event.target.value);

                                }}
                            >
                                <MenuItem  value="50">50</MenuItem>
                                <MenuItem  value="100">100</MenuItem>
                                <MenuItem  value="200">200</MenuItem>
                            </TextField>
                        </Grid>
                        <div style={{overflowX: 'auto'}}>
                            <table className='table'>
                                <thead>
                                <tr>
                                    <th>ردیف</th>
                                    <th>شماره پرونده</th>
                                    <th>رشته</th>
                                    <th>بیمه گذار</th>
                                    <th>موبایل بیمه گذار</th>
                                    <th>زمان وقوع حادثه</th>
                                    <th>شهر وقوع حادثه</th>
                                    <th>آدرس</th>
                                    <th>کاربر فعلی</th>
                                    <th>وضعیت</th>
                                    <th>تعیین وضعیت</th>
                                </tr>
                                </thead>
                                <tbody>
                                {typeof data !== "undefined" && data && data.data && data.data.map((item, index) => {
                                    return (
                                        <tr key={index}>
                                            <td>{index + 1}</td>
                                            <td>{item.id}</td>
                                            <td>{item.insurance_type.title}</td>
                                            <td>{item.injured.name + " " + item.injured.family}</td>
                                            <td>{item.injured.mobile}</td>
                                            {
                                                item.date_occurrence.includes("1400")
                                                    ?
                                                    <td>{item.time_occurrence + " -- " + moment(item.date_occurrence).format("YYYY/M/D")}</td>
                                                    :
                                                    <td>{item.time_occurrence + " -- " + moment(item.date_occurrence).format("jYYYY/jM/jD")}</td>

                                            }
                                            <td>{item.city.title}</td>
                                            <td>{item.address_occurrence}</td>
                                            <td>{`${item.operator.name} ${item.operator && item.operator.family}`}</td>

                                            <td>{handleStatus(item.status)}</td>
                                            <td>
                                                <ButtonGroup color="primary" aria-label="outlined primary button group">
                                                    <Button onClick={() => {
                                                        history.push(`/khesarat/${item.id}`, {item})
                                                    }}>
                                                        مشاهده
                                                    </Button>
                                                    <Button onClick={() => {
                                                        history.push(`/references/${item.id}`, {item})
                                                    }}>
                                                        ارجاعات
                                                    </Button>
                                                    {/*{*/}
                                                    {/*    role === "super_admin" &&*/}
                                                    {/*    <Button onClick={() => {*/}
                                                    {/*        setDeleteFileModal(item.id)*/}
                                                    {/*    }}>*/}
                                                    {/*        حذف*/}
                                                    {/*    </Button>*/}
                                                    {/*}*/}
                                                </ButtonGroup>
                                                <div className={classes.root} >
                                                    <Modal
                                                        disablePortal
                                                        disableEnforceFocus
                                                        disableAutoFocus
                                                        open={deleteFileModal === item.id}
                                                        onClose={() => {
                                                            setDeleteFileModal(false)
                                                        }}
                                                        aria-labelledby="server-modal-title"
                                                        aria-describedby="server-modal-description"
                                                        className={classes.modal}
                                                        // container={() => rootRef.current}

                                                    >
                                                        <div className={classes.paper}>
                                                            <h2 id="server-modal-title">آیا از ادامه عملیات مطمئن هستید ؟</h2>
                                                            <ButtonGroup color="primary" aria-label="outlined primary button group">
                                                                <Button color="primary" onClick={() => {
                                                                    setLoadingId(item.id)
                                                                    deleteFile(item.id)
                                                                }}>
                                                                    {(loading && deleteFileModal === item.id) ? <CircularProgress/> :  "بله"}

                                                                </Button>
                                                                <Button color="primary" onClick={() => {
                                                                    setDeleteFileModal(false)
                                                                }}>
                                                                    خیر
                                                                </Button>

                                                            </ButtonGroup>
                                                        </div>
                                                    </Modal>
                                                </div>
                                            </td>
                                            {/*<td>*/}
                                            {/*    <ButtonGroup color="primary" aria-label="outlined primary button group">*/}
                                            {/*        <Button onClick={() => {setLoadingId(item.id) ; handleArchive(item.id)}}>*/}
                                            {/*            {(loading && item.id === loadingId) ? <CircularProgress/> :  "بازگردانی"}*/}
                                            {/*        </Button>*/}
                                            {/*    </ButtonGroup>*/}
                                            {/*</td>*/}
                                        </tr>
                                    );
                                })}
                                </tbody>
                            </table>
                        </div>
                        {
                            data && data.data && data.data.length > 0 &&
                            <Pagination
                                activePage={page}
                                itemsCountPerPage={data && data.per_page}
                                totalItemsCount={data && data.total}
                                pageRangeDisplayed={5}
                                onChange={(page) => _makeQueryStringParams('page', page)}
                            />
                        }
                    </div>
            }
        </>


    );
}


export default Archive;
