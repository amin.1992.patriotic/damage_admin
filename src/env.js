export const ENV = {
    "API": {
        // main
        "admin.digiattar.com" : "https://api.digiattar.com/api/backend",
        "dev-admin.beenetwork.ir" : "https://api.digiattar.com/api/backend",
        "la.bimic.ir" : "https://laapi.bimic.ir/api",
        "zen-booth-84d677.netlify.app" : "https://la1.bimic.ir/api",
        "elastic-colden-65ee1f.netlify.app" : "https://la1.bimic.ir/api",
        // test
        "localhost:3000": "https://test.taroneh.ir/index.php/api",
        // "localhost:3000": "https://laapi.bimic.ir/api",
        "la1.bimic.ir": "https://la1.bimic.ir/api",
        "localhost:3001": "https://laapi.bimic.ir/api",
        "localhost:3002": "http://127.0.0.1:8000/api",
        "localhost:3004": "http://127.0.0.1:8000/api",
        "localhost:3005": "http://127.0.0.1:8000/api",
        "localhost:3003": "https://laapi.bimic.ir/api",
    },

    'STORAGE': 'https://laapi.bimic.ir/storage',

    'k': 'c0ff70cc197a07dff1fb709688170426',
    'i': 'f8b4e45085a1045902c3c69c80e67a7c',

    TinyMCEApiKey: '1iu7rhp54lw7udhv0zn9l9rzyu98jy5w97f742h8zgpt6i5f',

    captcha: '6LdgBBMaAAAAAGqvXz31pzkg6Xu5s-9G-pTVnofv'
}
