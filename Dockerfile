# Stage 0 - build react app
FROM node:alpine

WORKDIR /admin

COPY package.json /admin

RUN yarn install

COPY . /admin

CMD ["yarn", "run", "start"]